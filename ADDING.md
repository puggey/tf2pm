
# Adding new models and hats to TF2 Player Model Manager

Some notes about adding models:
* Add as much as possible hats and models in a single file since Lua file limit is a thing.
* TF2PM does not (and never will) support manual positioning of hats. Only bonemerge.
* Technically, there is no limit to hats added, and no limit to worn hats at the same time. Don't try to do anything stupid.

## Lua files

You add hats using Lua files. Your files need to go onto `lua/tf2pm/autorun/`.
**Files there are included both on client and server**, and you **don't** need to add
these files into `AddCSLuaFile()` manually! This is done automatically.

Adding hats is done via `tf2_pm.RegisterHat(classname, model, definition)` function.

Adding player models is done via `tf2_pm.RegisterModel(classname, model, definition)` function.

## Structure

`definition` of hat differ from `definition` of player model.

`model` argument of `RegisterHat` can be not specified if `definition` of hat contain `model` key.

**Definition is not copied by either functions. If you modify table passed to these functions, it will affect registered hat/playermodel aswell.**

Example registration of hat:

```lua
tf2_pm.RegisterHat("spy_fancy_fedora", { -- MAKE SURE that first value (classname string) you specified is UNIQUE amoung ALL hats ever registered
	-- Collisions will cause overwrites of other hats
	-- Also this classname is stored in save files

	applicable = {"spy"}, -- It is applicable to playermodels which have `spy` category
	-- By default, when model is registered, it's category is specified to it's classname
	-- If not present/omit it is set to `true`, meaning ANY model can wear it!
	-- example: "scout" or {"pyro", "demoman", "soldier"}

	targets = {"hat"}, -- Targets. Can be an empty table (if it is supposed to be empty, you can just omit specifying it)
	-- Please use generic names, since this is mostly used to determine incompatible hats
	-- In TF2, there are hats with `target` (aka wear/equip region) `whole_head`, etc. Please replace them with
	-- with table like {"hat", "glasses", ...} since they are just aliases

	paintable = true, -- whenever material of hat is paintable

	name = "Fancy Fedora", -- fallback name
	-- It is displayed when no localized name is present (defined below), or it is invalid

	localized_name = "info.tf2pm.hat.spy_hat_1", -- DLib.i18n localized name
	-- To understand how they work, you should check DLib's wiki
	-- But most of time you would want to use `DLib.i18n.registerPhrase`
	-- to avoid excessive Lua files
	-- such as:
	-- DLib.i18n.registerPhrase("en", "info.tf2pm.hat.spy_hat_1", "Fancy Fedora")
	-- DLib.i18n.registerPhrase("ru", "info.tf2pm.hat.spy_hat_1", "Фетровая шляпа")
	-- MAKE SURE you use unique names for this, otherwise you will overwrite other's translation strings.

	description = "", -- Same as name fallback. Don't specify if there is no description available for Hat
	localized_description = "info.tf2pm.hat.spy_hat_1_desc", -- same as above

	icon = "backpack/player/items/spy/spy_hat", -- Icon to be displayed in GUI
	-- No icon will display default icon

	model = "models/player/items/spy/spy_hat.mdl", -- the model to be bonemerged to player model
	-- this is required (or to be specified as second argument of tf2_pm.RegisterHat)

	bodygroup_overrides = { -- Bodygroup values override
	-- When hat is worn, these bodygroups get applied
		["hat"] = 1,
	},

	responsive_skin = {0, 1}, -- Skin to use. Omit to ignore skin of playermodel.
	-- Skin indexes of playermodel are mapped directly to hat's responsive_skin table.
	-- The example shows responsive skin for RED and BLU teams.

	-- Styles table. Omit of there are no styles present for hat.
	-- keep in mind you CAN define a style with zero index!
	-- styles with zero index will always be applied if nothing else is selected
	styles = {
		{
			name = 'Fallback name', -- name to be utilized as fallback
			localized_name = 'title.tf2pm.item.myitem.name', -- optional, but recommended
			localized_description = 'title.tf2pm.item.myitem.desc', -- optional
			responsive_skin = {2, 3, ...}, -- optional, overrides hat's responsive_skin property
			model = 'newmodel.mdl', -- overrides model
			bodygroup_overrides = {["hat" = 2]}, -- overrides bodygroups of parent model
			bodygroups_self = {["antenna" = 1]}, -- overrides bodygroups of this model
			skin = 1, -- when this is present, responsive_skin is ignored

			-- keep in mind that those overrides are applied *after* root overrides
		},
	},

	-- Overrides per model category
	-- Omit if not utilized.
	-- Most of time you don't need this in hat
	overrides = {
		["scout"] = { -- we want to push model's "target" overrides when this hat is being worn by "scout" model
			model_skins = {5, 6},
		},
	},

	-- Same as above, but always applied
	global_overrides = {
		model_skins = {5, 6},
	},
})
```
