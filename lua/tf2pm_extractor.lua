
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

--[==[
	{
		capabilities           = { --[[ table: 0x02933a6a5a60 ]] },
		craft_class            = "hat",
		craft_material_type    = "hat",
		drop_sound             = "ui/item_helmet_drop.wav",
		drop_type              = "drop",
		equip_region           = "hat",
		first_sale_date        = "2010/09/29",
		image_inventory        = "backpack/player/items/demo/demo_scott",
		image_inventory_size_h =  82,
		image_inventory_size_w = 128,
		item_class             = "tf_wearable",
		item_description       = "#TF_Demo_Scott_Hat_Desc",
		item_name              = "#TF_Demo_Scott_Hat",
		item_quality           = "unique",
		item_slot              = "head",
		item_type_name         = "#TF_Wearable_Hat",
		loadondemand           =   1,
		model_player           = "models/player/items/demo/demo_scott.mdl",
		mouse_pressed_sound    = "ui/item_helmet_pickup.wav",
		name                   = "Glengarry Bonnet",
		prefab                 = "valve base_hat",
		show_in_armory         =   1,
		used_by_classes        = { --[[ table: 0x02933a6a5c10 ]] }
	}
]==]

--[[
	-- All possible equip regions in items_game.txt
	{
		["arms"] = true,
		["back"] = true,
		["beard"] = true,
		["disconnected_floating_item"] = true,
		["engineer_belt"] = true,
		["engineer_hair"] = true,
		["engineer_left_arm"] = true,
		["engineer_pocket"] = true,
		["face"] = true,
		["feet"] = true,
		["flair"] = true,
		["glasses"] = true,
		["grenades"] = true,
		["hat"] = true,
		["head_skin"] = true,
		["heavy_hair"] = true,
		["heavy_hip"] = true,
		["heavy_pocket"] = true,
		["lenses"] = true,
		["medal"] = true,
		["necklace"] = true,
		["pyro_head_replacement"] = true,
		["scout_backpack"] = true,
		["shirt"] = true,
		["sleeves"] = true,
		["sniper_headband"] = true,
		["sniper_pocket"] = true,
		["soldier_cigar"] = true,
		["whole_head"] = true,
		["zombie_body"] = true,
	}
]]

if not file.Exists('tf2pm_proc/items_game.txt', 'DATA') then
	print('data/tf2pm_proc/items_game.txt not found')
	return
end

local TF2ITEMS = util.KeyValuesToTable(file.Read('tf2pm_proc/items_game.txt', 'DATA'))
local TF2ITEMS_PREFABS = {}

local function merge_append(destination, source)
	local len = #destination

	if source[0] then
		len = len + 1
	end

	for k, v in pairs(source) do
		local k2 = isnumber(k) and (k + len) or k

		if not istable(v) or not istable(destination[k2]) then
			destination[k2] = v
		else
			merge_append(destination[k2], v)
		end
	end
end

local function get_prefab(name)
	if TF2ITEMS_PREFABS[name] then return TF2ITEMS_PREFABS[name] end
	TF2ITEMS_PREFABS[name] = {}

	local prefab = TF2ITEMS.prefabs[name]
	if not prefab then return TF2ITEMS_PREFABS[name] end

	if prefab.prefab then
		local classes = prefab.prefab:split(' ')

		for _, classname in ipairs(classes) do
			local data = get_prefab(classname)

			if data then
				table.Merge(TF2ITEMS_PREFABS[name], data)
				--merge_append(TF2ITEMS_PREFABS[name], prefab)
				table.Merge(TF2ITEMS_PREFABS[name], prefab)
			end
		end
	else
		table.Merge(TF2ITEMS_PREFABS[name], prefab)
	end

	TF2ITEMS_PREFABS[name].prefab = nil
	return TF2ITEMS_PREFABS[name]
end

local function inherit_prefabs(target)
	if not istable(target) then return end
	if not isstring(target.prefab) then return end

	local classes = target.prefab:split(' ')

	for _, classname in ipairs(classes) do
		local data = get_prefab(classname)

		if data then
			table.Merge(target, data)
		end
	end
end

file.mkdir('tf2pm_proc/output')
file.mkdir('tf2pm_proc/output/i18n')

local translations = {
	['en-pt'] = 'pirate',
	['es-es'] = 'spanish',
	['sv-se'] = 'swedish',
	['zh-cn'] = 'schinese',
	bg = 'bulgarian',
	br = 'brazilian',
	cs = 'czech',
	de = 'german',
	el = 'greek',
	en = 'english',
	fi = 'finnish',
	fr = 'french',
	hu = 'hungarian',
	it = 'italian',
	jp = 'japanese',
	nl = 'dutch',
	no = 'norwegian',
	pl = 'polish',
	ru = 'russian',
	th = 'thai',
	tr = 'turkish',
	ua = 'ukrainian',
}

file.Delete('tf2pm_proc/output/medals.txt')
file.Delete('tf2pm_proc/output/allclass.txt')
file.Delete('tf2pm_proc/output/multiclass.txt')
file.Delete('tf2pm_proc/output/scout.txt')
file.Delete('tf2pm_proc/output/soldier.txt')
file.Delete('tf2pm_proc/output/pyro.txt')
file.Delete('tf2pm_proc/output/demoman.txt')
file.Delete('tf2pm_proc/output/heavy.txt')
file.Delete('tf2pm_proc/output/engineer.txt')
file.Delete('tf2pm_proc/output/medic.txt')
file.Delete('tf2pm_proc/output/sniper.txt')
file.Delete('tf2pm_proc/output/spy.txt')

for finalcode, translation in pairs(translations) do
	file.Delete('tf2pm_proc/output/i18n/' .. finalcode .. '.txt')
end

local nextmedal = 1
local medalswritten = 0
local medallimit = 600
local medals = file.Open('tf2pm_proc/output/medals1.txt', 'wb', 'DATA')

local allclass = file.Open('tf2pm_proc/output/allclass.txt', 'wb', 'DATA')
local multiclass = file.Open('tf2pm_proc/output/multiclass.txt', 'wb', 'DATA')
local scout = file.Open('tf2pm_proc/output/scout.txt', 'wb', 'DATA')
local soldier = file.Open('tf2pm_proc/output/soldier.txt', 'wb', 'DATA')
local pyro = file.Open('tf2pm_proc/output/pyro.txt', 'wb', 'DATA')
local demoman = file.Open('tf2pm_proc/output/demoman.txt', 'wb', 'DATA')
local heavy = file.Open('tf2pm_proc/output/heavy.txt', 'wb', 'DATA')
local engineer = file.Open('tf2pm_proc/output/engineer.txt', 'wb', 'DATA')
local medic = file.Open('tf2pm_proc/output/medic.txt', 'wb', 'DATA')
local sniper = file.Open('tf2pm_proc/output/sniper.txt', 'wb', 'DATA')
local spy = file.Open('tf2pm_proc/output/spy.txt', 'wb', 'DATA')

local translation_lines = {}
local translation_tables = {}

for finalcode, translation in pairs(translations) do
	translation_tables[finalcode] = util.KeyValuesToTable(file.Read('tf2pm_proc/tf_' .. translation .. '.txt', 'DATA'), true)
end

local translated = {}

local function get_translation(strin)
	if translated[strin] ~= nil then return translated[strin] end

	local strin2 = strin

	if strin2:startsWith('#') then
		strin2 = strin2:sub(2)
		strin = strin2
	end

	if strin2:startsWith('TF_') then
		strin2 = strin2:sub(4)
	end

	translated[strin] = 'info.tf2pm.hat.' .. strin2:lower()
	local hit = false

	local lower = strin:lower()

	for finalcode, data in pairs(translation_tables) do
		if data.tokens[lower] and data.tokens[lower] ~= '' then
			local clean = tostring(data.tokens[lower]):replace('%', '%%'):replace('&nbsp', ''):trim()

			if clean ~= '' then
				hit = true
				translation_lines[finalcode] = translation_lines[finalcode] or {}
				translation_lines[finalcode][strin2:lower()] = clean
			end
		end
	end

	if not hit then
		translated[strin] = false
	end

	return translated[strin]
end

local streams = {
	allclass, multiclass, scout, soldier, pyro,
	demoman, heavy, engineer, medic, sniper, spy,
	medals
}

for i, stream in ipairs(streams) do
	stream:Write('\n-- Auto generated at ' .. DLib.string.qdate() .. '\nreturn {\n')
end

for i, item in pairs(TF2ITEMS.items) do
	inherit_prefabs(item)

	if item.item_class ~= 'tf_wearable' then goto CONTINUE end -- hat
	if item.item_slot == 'action' then goto CONTINUE end -- hat

	local used_by_classes = istable(item.used_by_classes) and item.used_by_classes or isstring(item.used_by_classes) and {[item.used_by_classes] = 1} or false

	if not used_by_classes then goto CONTINUE end

	local stream
	local _count = table.Count(used_by_classes)
	local equip_regions = istable(item.equip_regions) and item.equip_regions or isstring(item.equip_region) and {[item.equip_region] = 1} or {}

	if _count == 0 then
		goto CONTINUE
	elseif _count == 1 then
		local classname = next(used_by_classes)

		if classname == 'scout' then
			stream = scout
		elseif classname == 'soldier' then
			stream = soldier
		elseif classname == 'pyro' then
			stream = pyro
		elseif classname == 'demoman' then
			stream = demoman
		elseif classname == 'heavy' then
			stream = heavy
		elseif classname == 'engineer' then
			stream = engineer
		elseif classname == 'medic' then
			stream = medic
		elseif classname == 'sniper' then
			stream = sniper
		elseif classname == 'spy' then
			stream = spy
		end
	elseif _count < 9 then
		stream = multiclass
	elseif isstring(item.item_name) and item.item_name:lower():find('tf_tournamentmedal') or equip_regions['medal'] then
		if medalswritten > medallimit then
			medals:Write('\n}\n')
			medals:Close()

			nextmedal = nextmedal + 1
			medalswritten = 0
			medals = file.Open('tf2pm_proc/output/medals' .. nextmedal .. '.txt', 'wb', 'DATA')
			streams[#streams] = medals
			medals:Write('\n-- Auto generated at ' .. DLib.string.qdate() .. '\nreturn {\n')
		end

		stream = medals
		medalswritten = medalswritten + 1
	else
		stream = allclass
	end

	if not stream then goto CONTINUE end

	local paintable = item.capabilities and item.capabilities.paintable ~= nil

	local classes = {}
	local equip_regions2 = {}

	for class in pairs(used_by_classes) do
		if class == 'demoman' then class = 'demo' end
		table.insert(classes, string.format('%q', class))
	end

	for region in pairs(equip_regions) do
		table.insert(equip_regions2, string.format('%q', region))
	end

	stream:Write('\t{\n')
	stream:Write('\t\tclasses = {' .. table.concat(classes, ', ') .. '},\n')
	stream:Write('\t\ttargets = {' .. table.concat(equip_regions2, ', ') .. '},\n')
	stream:Write('\t\tpaintable = ' .. (paintable and 'true' or 'false') .. ',\n')

	if isstring(item.name) then
		stream:Write('\t\tname = ' .. string.format('%q', item.name) .. ',\n')
	end

	if isstring(item.armory_desc) then
		stream:Write('\t\tdescription = ' .. string.format('%q', item.armory_desc) .. ',\n')
	end

	if isstring(item.item_name) then
		local getstring = get_translation(item.item_name)

		if getstring then
			stream:Write('\t\tlocalized_name = ' .. string.format('%q', getstring) .. ',\n')
		end
	end

	if isstring(item.item_description) then
		local getstring = get_translation(item.item_description)

		if getstring then
			stream:Write('\t\tlocalized_description = ' .. string.format('%q', getstring) .. ',\n')
		end
	end

	if isstring(item.image_inventory) then
		stream:Write('\t\ticon = ' .. string.format('%q', item.image_inventory) .. ',\n')
	end

	local model_player_per_class = {}

	if istable(item.model_player_per_class) then
		for pclass, model in pairs(item.model_player_per_class) do
			model_player_per_class[pclass] = model
		end

		--[[if model_player_per_class.basename then
			-- wtf did valve do
			for pclass in pairs(used_by_classes) do
				local ptarget = pclass == 'demoman' and 'demo' or pclass
				model_player_per_class[ptarget] = model_player_per_class[pclass] or string.format(model_player_per_class.basename, ptarget, ptarget, ptarget, ptarget)
			end

			model_player_per_class.basename = nil
		end]]
	elseif isstring(item.model_player) then
		--[[for pclass in pairs(used_by_classes) do
			model_player_per_class[pclass] = item.model_player
		end]]

		model_player_per_class['basename'] = item.model_player
	end

	stream:Write('\t\tmodels = {\n')

	for k, v in pairs(model_player_per_class) do
		stream:Write(string.format('\t\t\t[%q] = %q,\n', k, v))
	end

	stream:Write('\t\t},\n')

	if istable(item.visuals) then
		if istable(item.visuals.styles) then
			stream:Write('\t\tstyles = {\n')

			for num, styledef in pairs(item.visuals.styles) do
				stream:Write(string.format('\t\t\t[%d] = {\n', num))

				if isnumber(styledef.skin_red) and isnumber(styledef.skin_blu) then
					stream:Write(string.format('\t\t\t\tresponsive_skins = {%d, %d},\n', styledef.skin_red, styledef.skin_blu))
				end

				if isnumber(styledef.skin) then
					stream:Write(string.format('\t\t\t\tskin = %d,\n', styledef.skin))
				end

				if isstring(styledef.name) then
					local getstring = get_translation(styledef.name)

					if getstring then
						stream:Write(string.format('\t\t\t\tlocalized_name = %q,\n', getstring))
					end
				end

				if istable(styledef.additional_hidden_bodygroups) then
					stream:Write(string.format('\t\t\t\tbodygroup_overrides = {\n', styledef.name))

					for k, v in pairs(styledef.additional_hidden_bodygroups) do
						stream:Write(string.format('\t\t\t\t\t[%q] = %d,\n', k, v))
					end

					stream:Write(string.format('\t\t\t\t},\n', styledef.name))
				end

				local model_player_per_class = {}

				if istable(styledef.model_player_per_class) then
					for pclass, model in pairs(styledef.model_player_per_class) do
						model_player_per_class[pclass] = model
					end

					--[[if model_player_per_class.basename then
						-- wtf did valve do
						for pclass in pairs(used_by_classes) do
							local ptarget = pclass == 'demoman' and 'demo' or pclass
							model_player_per_class[ptarget] = model_player_per_class[pclass] or string.format(model_player_per_class.basename, ptarget, ptarget, ptarget, ptarget)
						end

						model_player_per_class.basename = nil
					end]]
				elseif isstring(styledef.model_player) then
					--[[for pclass in pairs(used_by_classes) do
						model_player_per_class[pclass] = styledef.model_player
					end]]

					model_player_per_class['basename'] = styledef.model_player
				end

				if table.Count(model_player_per_class) ~= 0 then
					stream:Write('\t\t\t\tmodels = {\n')

					for k, v in pairs(model_player_per_class) do
						stream:Write(string.format('\t\t\t\t\t[%q] = %q,\n', k, v))
					end

					stream:Write('\t\t\t\t}\n')
				end

				stream:Write('\t\t\t},\n')
			end

			stream:Write('\t\t},\n')
		end

		if istable(item.visuals.player_bodygroups) then
			stream:Write('\t\tbodygroup_overrides = {\n')

			for key, value in pairs(item.visuals.player_bodygroups) do
				stream:Write(string.format('\t\t\t[%q] = %d,\n', key, value))
			end

			stream:Write('\t\t},\n')
		end
	end

	stream:Write('\t},\n')
	stream:Flush()

	::CONTINUE::
end

for i, stream in ipairs(streams) do
	stream:Write('\n}\n')
	stream:Close()
end

local base64 = util.Base64Encode(util.Compress(util.TableToJSON(translation_lines)))
local newlines = base64:split('\n')
-- local rebuild = '\nreturn {\n\t' .. table.concat(base64:split('\n'), '\n\t') .. '\n}\n'

do
	local bytes = 0
	local fileid = 1
	local stream = file.Open('tf2pm_proc/output/i18n/1.txt', 'wb', 'DATA')
	local header = '\n-- Auto generated at ' .. DLib.string.qdate() .. '\n-- those files contain translation strings for hats\n-- because of gmod limitations over Lua filesize and it\'s contents, this is base64 encoded\n-- LZMA compressed JSON with translation strings for DLib.i18n\n\nreturn {\n'
	stream:Write(header)
	local limit = 1024 * 56 -- 56 KB. Hard limit is actually 64 KB
	-- Also some unicode symbols in Lua files can corrupt entire transfer and render
	-- connection to server failed

	print('mv lua/tf2pm/lang/1.txt lua/tf2pm/lang/1.lua')

	for i, line in ipairs(newlines) do
		if bytes + #line > limit then
			stream:Write('}\n')
			stream:Close()
			bytes = 0
			fileid = fileid + 1
			stream = file.Open('tf2pm_proc/output/i18n/' .. fileid .. '.txt', 'wb', 'DATA')
			print('mv lua/tf2pm/lang/' .. fileid .. '.txt lua/tf2pm/lang/' .. fileid .. '.lua')

			stream:Write(header)
		end

		stream:Write('\t"')
		stream:Write(line)
		stream:Write('",\n')
		bytes = bytes + #line
	end

	stream:Write('}\n')
	stream:Close()
end
