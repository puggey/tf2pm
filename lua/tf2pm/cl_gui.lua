
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local tf2_pm = tf2_pm

file.mkdir('tf2pm')

function tf2_pm.RequestApply(fromdef)
	local localdef = LocalTF2PM()

	for hat, hatdef in pairs(localdef.hat_defs) do
		if not fromdef.hat_defs[hat] then
			tf2_pm.RequestRemoveHat(hat)
		end
	end

	for hat, hatdef in pairs(fromdef.hat_defs) do
		if not localdef.hat_defs[hat] then
			tf2_pm.RequestWearHat(hat)
		end
	end

	for hat, style in pairs(fromdef.hat_styles) do
		tf2_pm.RequestSetHatStyle(hat, style)
	end

	for hat, color in pairs(fromdef.hat_colors) do
		tf2_pm.RequestSetHatColor(hat, color)
	end

	for hat, style in pairs(localdef.hat_styles) do
		if fromdef.hat_styles[hat] == nil then
			tf2_pm.RequestUnsetHatStyle(hat)
		end
	end

	for hat, color in pairs(localdef.hat_colors) do
		if fromdef.hat_colors[hat] == nil then
			tf2_pm.RequestUnsetHatColor(hat)
		end
	end

	if fromdef.team ~= localdef.team or fromdef.team_index ~= localdef.team_index then
		if fromdef.team then
			tf2_pm.RequestSetTeam(fromdef.team_index)
		else
			tf2_pm.RequestUnsetTeam()
		end
	end

	for name, value in pairs(localdef.custom_bodygroup_overrides) do
		if not fromdef.custom_bodygroup_overrides[name] then
			tf2_pm.RequestUnsetBodygroup(name)
		end
	end

	for name, value in pairs(fromdef.custom_bodygroup_overrides) do
		if localdef.custom_bodygroup_overrides[name] ~= value then
			tf2_pm.RequestSetBodygroup(name, value)
		end
	end
end

if IsValid(tf2_pm.EditorWindow) then
	tf2_pm.EditorWindow:Remove()
end

function tf2_pm.OpenEditor()
	if IsValid(tf2_pm.EditorWindow) then
		tf2_pm.EditorWindow:SetVisible(true)
		tf2_pm.EditorWindow:Center()
		tf2_pm.EditorWindow:MakePopup()

		tf2_pm.EditorWindow.model:HideModels(false)

		return
	end

	local self = vgui.Create('DLib_Window')
	tf2_pm.EditorWindow = self
	self:SetTitle('gui.tf2pm.editor.title')

	self:SetDeleteOnClose(false)

	local model = LocalPlayer():GetModel()
	local modeldata = tf2_pm.LookupModel(model)
	local applycurrent = true

	if not modeldata then
		modeldata = table.Random(tf2_pm.model_registry)
		model = modeldata.model
		applycurrent = false
	end

	local function patchcolor()
		local ply = LocalPlayer()

		function self.model.Entity.GetPlayerColor()
			return ply:GetPlayerColor()
		end
	end

	local function backup(prefix, backupstr)
		prefix = prefix or 'autosave'
		backupstr = backupstr or self.model:GetTF2Data():SerializeString()

		if not file.Exists('tf2pm/' .. modeldata.classname, 'DATA') then
			file.mkdir('tf2pm/' .. modeldata.classname)
		end

		if not file.Exists('tf2pm/' .. modeldata.classname .. '/backups', 'DATA') then
			file.mkdir('tf2pm/' .. modeldata.classname .. '/backups')
		end

		local y, m, d = os.date('%Y'), os.date('%m'), os.date('%d')

		if not file.Exists('tf2pm/' .. modeldata.classname .. '/backups', 'DATA') then
			file.mkdir('tf2pm/' .. modeldata.classname .. '/backups')
		end

		if not file.Exists('tf2pm/' .. modeldata.classname .. '/backups/' .. y, 'DATA') then
			file.mkdir('tf2pm/' .. modeldata.classname .. '/backups/' .. y)
		end

		if not file.Exists('tf2pm/' .. modeldata.classname .. '/backups/' .. y .. '/' .. m, 'DATA') then
			file.mkdir('tf2pm/' .. modeldata.classname .. '/backups/' .. y.. '/' .. m)
		end

		if not file.Exists('tf2pm/' .. modeldata.classname .. '/backups/' .. y .. '/' .. m .. '/' .. d, 'DATA') then
			file.mkdir('tf2pm/' .. modeldata.classname .. '/backups/' .. y.. '/' .. m .. '/' .. d)
		end

		file.Write(string.format('tf2pm/%s/backups/%s/%s/%s/%s-%s.dat', modeldata.classname, y, m, d, prefix, os.date('%H-%M-%S')), backupstr)
	end

	function self.MarkDirty()
		self:SetTitle('gui.tf2pm.editor.title_modified')
		timer.Create('TF2PM Autosave', 3, 1, backup)
		self.dirty = true
	end

	function self.MarkClean()
		self:SetTitle('gui.tf2pm.editor.title')
		timer.Remove('TF2PM Autosave')
		self.dirty = false
	end

	function self.BackupIfDirty()
		if not self.dirty then return end
		backup('autobackup')
	end

	self.model = vgui.Create('TF2PM_ModelPanel', self)
	self.model:Dock(FILL)
	self.model:SetModel(model)
	self.model:SetSaveTarget(self)
	patchcolor()

	self.left = vgui.Create('DScrollPanel', self)
	self.left:Dock(LEFT)

	self.right = vgui.Create('DScrollPanel', self)
	self.right:Dock(RIGHT)

	function self:PerformLayout2(w, h)
		self.left:SetWide(w * 0.333)
		self.right:SetWide(w * 0.333)
		self.model:SetWide(w * 0.333)

		self.apply:SetPos(self:GetWide() - self.apply:GetWide() - 100, 4)
		self.load:SetPos(self:GetWide() - self.apply:GetWide() - self.load:GetWide() - 100, 4)
		self.save:SetPos(self:GetWide() - self.apply:GetWide() - self.load:GetWide() - self.save:GetWide() - 100, 4)
		self.clear:SetPos(self:GetWide() - self.apply:GetWide() - self.load:GetWide() - self.save:GetWide() - self.clear:GetWide() - 170, 4)

		self.modelselect:SetPos(200, 0)
		self.skinselect:SetPos(300, 0)
		self.bodygroup_controls:SetPos(400, 0)
	end

	self.hatpanels = {}
	self.hatpanels_left = {}
	self.hatpanels_right = {}
	local updateRight, updateLeft

	local function doupdate(listing, parentpanel, unpdatefn)
		local free = {}

		for i, panel in ipairs(listing) do
			if not panel:GetHatDef() then
				table.insert(free, panel)
			end
		end

		if #free == 0 then
			local panel = vgui.Create('TF2PM_HatCell', parentpanel)
			panel:Dock(TOP)
			panel:SetModelPanelTarget(self.model)
			panel:SetUpdateFunction(unpdatefn)
			table.insert(self.hatpanels, panel)
			table.insert(listing, panel)
		elseif #free > 2 and #listing > 6 then
			for i = #free, 3, -1 do
				local panel = free[i]
				table.RemoveByValue(listing, panel)
				table.RemoveByValue(self.hatpanels, panel)
				panel:Remove()
			end
		end
	end

	local function updateRight()
		doupdate(self.hatpanels_right, self.right, updateRight)
	end

	local function updateLeft()
		doupdate(self.hatpanels_left, self.left, updateLeft)
	end

	local function updateall()
		updateRight()
		updateLeft()
	end

	for i = 1, 4 do
		local panel = vgui.Create('TF2PM_HatCell', self.left)
		panel:Dock(TOP)
		panel:SetModelPanelTarget(self.model)
		panel:SetUpdateFunction(updateLeft)
		table.insert(self.hatpanels, panel)
		table.insert(self.hatpanels_left, panel)

		panel = vgui.Create('TF2PM_HatCell', self.right)
		panel:Dock(TOP)
		panel:SetModelPanelTarget(self.model)
		panel:SetUpdateFunction(updateRight)
		table.insert(self.hatpanels, panel)
		table.insert(self.hatpanels_right, panel)
	end

	self.apply = vgui.Create('DButton', self)
	self.apply:SetText('gui.misc.apply')
	self.apply:SizeToContents()
	self.apply:SetWide(self.apply:GetWide() + 40)

	self.save = vgui.Create('DButton', self)
	self.save:SetText('gui.tf2pm.editor.save_button')
	self.save:SizeToContents()
	self.save:SetWide(self.save:GetWide() + 40)

	self.load = vgui.Create('DButton', self)
	self.load:SetText('gui.tf2pm.editor.load_button')
	self.load:SizeToContents()
	self.load:SetWide(self.load:GetWide() + 40)

	self.clear = vgui.Create('DButton', self)
	self.clear:SetText('gui.tf2pm.editor.clear_button')
	self.clear:SizeToContents()
	self.clear:SetWide(self.clear:GetWide() + 40)

	self.modelselect = vgui.Create('DComboBox', self)
	self.modelselect:SetValue(modeldata.classname)
	self.modelselect:SetWide(100)

	self.skinselect = vgui.Create('DComboBox', self)
	self.skinselect:SetValue(DLib.i18n.localize('gui.tf2pm.editor.no_skin'))
	self.skinselect:SetWide(100)

	self.bodygroup_controls = vgui.Create('DButton', self)
	self.bodygroup_controls:SetText('gui.tf2pm.editor.bodygroup_controls')
	self.bodygroup_controls:SetWide(100)

	local modellist = {}

	for k in pairs(tf2_pm.model_registry) do
		table.insert(modellist, k)
	end

	table.sort(modellist)

	for i, modelname in ipairs(modellist) do
		self.modelselect:AddChoice(modelname, modelname)
	end

	function self.modelselect.OnSelect(_, index, classname, data)
		self.BackupIfDirty()
		self.MarkClean()
		modeldata = tf2_pm.LookupModel(data)
		model = modeldata.model
		applycurrent = model == LocalPlayer():GetModel()
		self.model:RemoveAllHats()
		self.model:SetModel(model)
		self.model:RebuildModelDef()
		patchcolor()

		if applycurrent then
			LocalTF2PM():CopyTo(self.model:GetTF2Data())
		else
			self.ReloadControls()
		end

		self.ReloadHatPanels()
		self.model:ProcessSkins()
	end

	function self.skinselect.OnSelect(_, index, classname, data)
		self.MarkDirty()

		if data > 0 then
			self.model:SetTeam(data)
		else
			self.model:UnsetTeam(data)
		end
	end

	function self.clear.DoClick()
		local function confirm()
			self.model:RemoveAllHats()
			self.ReloadHatPanels()
			self.MarkClean()
		end

		Derma_Query('gui.tf2pm.editor.clear_text', 'gui.tf2pm.editor.clear_button', 'gui.misc.ok', confirm, 'gui.misc.cancel')
	end

	function self.apply.DoClick()
		if self.model:GetModel() == LocalPlayer():GetModel() then
			tf2_pm.RequestApply(self.model:GetTF2Data())
		end
	end

	function self.apply.Think()
		if self.model:GetModel() == LocalPlayer():GetModel() then
			self.apply:SetTooltip('')
			self.apply:SetEnabled(true)
		else
			self.apply:SetTooltip('gui.tf2pm.editor.model_mismatch')
			self.apply:SetEnabled(false)
		end
	end

	function self.save.DoClick()
		local function confirm(userinput)
			self.BackupIfDirty()

			userinput = userinput:lower()
			file.mkdir('tf2pm/' .. modeldata.classname)
			local name = 'tf2pm/' .. modeldata.classname .. '/' .. userinput .. '.dat'

			if file.Exists(name, 'DATA') then
				backup(userinput, file.Read(name, 'DATA'))
			end

			file.Write(name, self.model:GetTF2Data():SerializeString())
			self.MarkClean()
		end

		Derma_StringRequest('gui.tf2pm.editor.save_button', 'gui.tf2pm.editor.save_text', '', confirm, nil, 'gui.misc.ok', 'gui.misc.cancel')
	end

	function self.ReloadHatPanels()
		for i, panel in ipairs(self.hatpanels) do
			panel:SetHatDef(nil)
		end

		local next = 1

		for hat, hatdef in pairs(self.model:GetTF2Data().hat_defs) do
			local panel = self.hatpanels[next]
			next = next + 1

			if not panel then
				updateall()
				panel = self.hatpanels[next]
			end

			if not panel then break end

			panel:SetHatDef(hatdef)
		end

		updateall()

		local index = self.model:GetTeam()

		self.skinselect:Clear()

		if index then
			self.skinselect:SetValue(DLib.i18n.localize('gui.tf2pm.editor.skin_num', index))
		else
			self.skinselect:SetValue(DLib.i18n.localize('gui.tf2pm.editor.no_skin'))
		end

		local modeldef = self.model:GetTF2Data().modeldef

		self.skinselect:AddChoice('gui.tf2pm.editor.no_skin', -1)

		if modeldef and modeldef.model_skins then
			for i = 1, #modeldef.model_skins do
				self.skinselect:AddChoice(DLib.i18n.localize('gui.tf2pm.editor.skin_num', i), i)
			end
		end
	end

	local loadwindow

	function self.load.DoClick()
		local function callback(userinput)
			self.model:DeserializeString(file.Read('tf2pm/' .. modeldata.classname .. '/' .. userinput, 'DATA'))
			self.ReloadHatPanels()
		end

		local function callbackremove(userinput)
			file.Delete('tf2pm/' .. modeldata.classname .. '/' .. userinput)
		end

		loadwindow = tf2_pm.ShowLoadFileGUI(file.Find('tf2pm/' .. modeldata.classname .. '/*.dat', 'DATA'), callback, callbackremove)

		hook.Add('Think', loadwindow, function()
			if self:HasHierarchicalFocus() then
				loadwindow:MakePopup()
			end
		end)
	end

	function self.ReloadControls()
		if file.Exists('tf2pm/' .. modeldata.classname .. '/autoload.dat', 'DATA') then
			self.model:DeserializeString(file.Read('tf2pm/' .. modeldata.classname .. '/autoload.dat', 'DATA'))
			self.ReloadHatPanels()
		else
			local files = file.Find('tf2pm/' .. modeldata.classname .. '/*.dat', 'DATA')
			local choose = table.Random(files)

			if choose then
				self.model:DeserializeString(file.Read('tf2pm/' .. modeldata.classname .. '/' .. choose, 'DATA'))
				self.ReloadHatPanels()
			end
		end

		self.model:ProcessSkins()
	end

	function self.bodygroup_controls.DoClick()
		if IsValid(self.bodygroup_controls_window) then
			self.bodygroup_controls_window:Remove()
		end

		self.bodygroup_controls_window = vgui.Create('DLib_Window')
		self.bodygroup_controls_window:SetTitle('gui.tf2pm.editor.bodygroup_controls')
		local lx, ly = self.model:LocalToScreen(self.model:GetWide(), 0)
		self.bodygroup_controls_window:SetPos(lx, ly)
		self.bodygroup_controls_window:SetSize(ScrW() - lx - 30, math.clamp(ScrH() - ly - 30, 200, 600))
		self.bodygroup_controls_window:MakePopup()

		local scroll = vgui.Create('DScrollPanel', self.bodygroup_controls_window)
		scroll:Dock(FILL)

		local i = 0

		local tf2_pm = self.model:GetTF2Data()

		for k, v in pairs(tf2_pm._bodygroups) do
			if table.Count(v.submodels) > 1 then
				i = i + 1

				local parent = vgui.Create('EditablePanel', scroll)
				parent:Dock(TOP)
				parent:SetTall(20)
				parent:SetZPos(i)

				local label = vgui.Create('DLabel', parent)
				local checkbox = vgui.Create('DCheckBox', parent)
				local combobox = vgui.Create('DComboBox', parent)

				label:SetZPos(-1)
				checkbox:SetZPos(0)
				combobox:SetZPos(1)

				label:Dock(LEFT)
				label:DockMargin(2, 0, 2, 0)
				checkbox:Dock(LEFT)
				checkbox:DockMargin(2, 0, 2, 0)
				combobox:Dock(FILL)
				combobox:DockMargin(2, 0, 2, 0)

				label:SetText(v.name)

				for _i, modelname in pairs(v.submodels) do
					if modelname == '' then
						modelname = 'gui.tf2pm.editor.nothing'
					end

					combobox:AddChoice(modelname, _i)
				end

				local picked_value = 0

				if tf2_pm.custom_bodygroup_overrides[v.name] then
					picked_value = tf2_pm.custom_bodygroup_overrides[v.name]
					checkbox:SetValue(true)
				end

				if v.submodels[picked_value] == '' then
					combobox:SetValue('gui.tf2pm.editor.nothing')
				else
					combobox:SetValue(v.submodels[picked_value] or 'gui.tf2pm.editor.default')
				end

				function combobox.OnSelect(_, index, value, data)
					picked_value = data

					if checkbox:GetChecked() then
						self.model:SetCustomBodygroup(v.name, picked_value)
					else
						self.model:UnsetCustomBodygroup(v.name)
					end
				end

				function checkbox.OnChange(_, state)
					if state then
						self.model:SetCustomBodygroup(v.name, picked_value)
					else
						self.model:UnsetCustomBodygroup(v.name)
					end
				end
			end
		end
	end

	if applycurrent then
		LocalTF2PM():CopyTo(self.model:GetTF2Data())
		self.ReloadHatPanels()
	else
		self.ReloadControls()
	end

	self.model:RebuildModelDef()
	self.model:ProcessSkins()

	function self.OnClose()
		-- i don't want to manually redraw them so
		-- we'll just hide them on GUI close
		self.model:HideModels(true)

		if IsValid(loadwindow) then
			loadwindow:Close()
		end

		for i, panel in ipairs(self.hatpanels) do
			if IsValid(panel) then
				panel:OnClose()
			end
		end
	end
end

surface.DLibCreateFont('TF2PM_Tooltip', {
	font = 'Roboto',
	extended = true,
	size = 16,
})

surface.DLibCreateFont('TF2PM_Tooltip2', {
	font = 'Roboto',
	extended = true,
	size = 12,
})

local i18n = DLib.i18n
local ScreenSize = ScreenSize

local function PostRenderVGUI()
	local panel = vgui.GetHoveredPanel()
	if not IsValid(panel) or not panel._tf2pm_hat_model then return end
	if not panel.hatdef then return end
	local hatdef = panel.hatdef
	local mx, my = input.GetCursorPos()

	local innerPadding, innerPadding2 = ScreenSize(4):round(), ScreenSize(7):round()
	local x, y = mx + innerPadding2, my + innerPadding2

	local name, description = i18n.localize(hatdef.localized_name),
		i18n.exists(hatdef.localized_description) and i18n.localize(hatdef.localized_description) or
		(hatdef.description ~= hatdef.localized_description and hatdef.description or nil) or ''

	description = tf2_pm.GetWrappedDescription(description)

	surface.SetFont('TF2PM_Tooltip')
	local nw, nh = surface.GetTextSize(name)
	surface.SetFont('TF2PM_Tooltip2')
	local dw, dh = 0, 0

	local paddingX, paddingY = ScreenSize(4):round(), ScreenSize(3):round()

	if description ~= '' then
		dw, dh = surface.GetTextSize(description)
		dw = dw + paddingX
		dh = dh + paddingY
	end

	local tw, th = math.max(nw, dw), nh + dh

	local nowearreason, cw, ch

	if not panel:IsEnabled() then
		nowearreason = panel:GetCantWearReason() and i18n.localize(panel:GetCantWearReason()) or i18n.localize('gui.tf2pm.editor.cant_wear')
		cw, ch = surface.GetTextSize(nowearreason)

		tw, th = math.max(tw, cw + paddingX), th + ch
	end

	if x + tw + innerPadding2 > ScrW() then
		x = x - tw - innerPadding2 * 1.2
	end

	if y + th + innerPadding2 > ScrH() then
		y = y - th - innerPadding2 * 1.2
	end

	DLib.blur.RefreshNow(true)
	DLib.blur.Draw(x - innerPadding, y - innerPadding, tw + innerPadding * 2, th + innerPadding * 2)

	surface.SetDrawColor(0, 0, 0, 200)
	surface.DrawRect(x - innerPadding, y - innerPadding, tw + innerPadding * 2, th + innerPadding * 2)

	surface.SetFont('TF2PM_Tooltip')
	surface.SetTextColor(255, 255, 255)
	surface.SetTextPos(x, y)
	surface.DrawText(name)

	y = y + nh

	if description ~= '' then
		surface.SetDrawColor(255, 255, 255)
		surface.DrawLine(x, y + paddingY * 0.4, x + tw, y + paddingY * 0.4)

		y = y + paddingY

		draw.DrawText(description, 'TF2PM_Tooltip2', x + paddingX, y, color_white)
		y = y + dh - paddingY
	end

	if nowearreason then
		draw.DrawText(nowearreason, 'TF2PM_Tooltip2', x + 10, y, color_red)
		y = y + ch
	end
end

hook.Add('PostRenderVGUI', 'tf2pm', PostRenderVGUI)

function tf2_pm.ShowLoadFileGUI(listing, callback, callbackremove)
	local self = vgui.Create('DLib_Window')

	self:SetSize(400, math.clamp(#listing * 30, 300, ScrH() - 100))
	self:Center()
	self:MakePopup()

	self:SetTitle('gui.tf2pm.editor.load_button')

	self.quicksearch = vgui.Create('DTextEntry', self)
	self.quicksearch:SetPlaceholderText(DLib.i18n.localize('gui.tf2pm.editor.quicksearch_file'))
	self.quicksearch:SetUpdateOnType(true)
	self.quicksearch:Dock(TOP)

	self.bottom = vgui.Create('EditablePanel', self)
	self.bottom:Dock(BOTTOM)

	self.load = vgui.Create('DButton', self.bottom)
	self.load:Dock(LEFT)
	self.load:SetText('gui.tf2pm.editor.load_button')

	self.cancel = vgui.Create('DButton', self.bottom)
	self.cancel:Dock(RIGHT)
	self.cancel:SetText('gui.misc.cancel')

	function self.cancel.DoClick()
		self:Close()
	end

	function self.load.DoClick()
		if not self.list:GetSelectedLine() then return end
		callback(select(2, self.list:GetSelectedLine()):GetColumnText(1))
		self:Close()
	end

	function self.bottom.PerformLayout(_, w, h)
		local w2 = math.round(w / 2)
		self.load:SetSize(w2)
		self.cancel:SetSize(w - w2)
	end

	self.list = vgui.Create('DListView', self)
	self.list:Dock(FILL)
	self.list:AddColumn('gui.tf2pm.editor.filename')

	function self.list.DoDoubleClick(_, index, line)
		callback(line:GetColumnText(1))
		self:Close()
	end

	function self.list.OnRowRightClick(_, index, line)
		local menu = DermaMenu()

		menu:AddOption('gui.tf2pm.editor.load', function()
			callback(line:GetColumnText(1))
			self:Close()
		end):SetIcon('icon16/database_go.png')

		menu:AddOption('gui.tf2pm.editor.remove', function()
			callbackremove(line:GetColumnText(1))
		end):SetIcon('icon16/database_delete.png')

		menu:Open()
	end

	local rebuildList

	function self.quicksearch.OnValueChange()
		rebuildList()
	end

	function rebuildList()
		self.list:Clear()
		local text = self.quicksearch:GetValue() or ''

		if text == '' then
			for _, filename in ipairs(listing) do
				self.list:AddLine(filename)
			end
		else
			text = text:lower()

			for _, filename in ipairs(listing) do
				if filename:lower():find(text) then
					self.list:AddLine(filename)
				end
			end
		end
	end

	rebuildList()

	return self
end

concommand.Add('tf2pm_editor', tf2_pm.OpenEditor)

IconData = {
	title = 'TF2 PM',
	icon = 'gui/tf2pm_icon2.png',
	width = 960,
	height = 700,
	onewindow = true,
	init = function(icon, window)
		window:Remove()
		tf2_pm.OpenEditor()
	end
}

list.Set('DesktopWindows', 'TF2PM', IconData)

if IsValid(g_ContextMenu) then
	CreateContextMenu()
end
