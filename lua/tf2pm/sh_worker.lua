
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local tf2_pm = tf2_pm
local net = DLib.net

local Worker = {}
local WorkerClass = {}

function Worker:ctor(ent)
	self.entity = ent
	self.hat_models = {}
	self.hat_defs = {}
	self.hat_styles = {}
	self.hat_colors = {}
	self.custom_bodygroup_overrides = {}
	self.team = false
	self.team_index = -1
	self.hide_status = false
	self:RebuildModelDef()
end

function Worker:RemoveAllHats()
	for hat in pairs(self.hat_defs) do
		self:RemoveHat(hat)
	end
end

function Worker:GetStyleTable(hat, hatdef)
	hatdef = hatdef or self.hat_defs[hat]
	if not hatdef then return false end

	local style = false

	if hatdef.styles and self.hat_styles[hat] then
		style = hatdef.styles[self.hat_styles[hat]] or hatdef.styles[0] or hatdef.styles[1]
	elseif hatdef.styles and hatdef.styles[0] and not self.hat_styles[hat] then
		style = hatdef.styles[0]
	end

	return style
end

function Worker:RebuildModelDef()
	local olddef = self.modeldef
	self.modeldef = tf2_pm._model_registry[self.entity:GetModel()]

	if self.modeldef then
		self.modeldef_overrides = table.Copy(self.modeldef)
	else
		self.modeldef_overrides = {bodygroup_overrides = {}}
	end

	if olddef ~= self.modeldef then
		if self.modeldef then
			for hat, hatdef in pairs(self.hat_defs) do
				if not tf2_pm.IsApplicable(hatdef, self.modeldef) then
					self:RemoveHat(hat)
				end
			end
		else
			self.modeldef = olddef
			self:RemoveAllHats()
			self:UnsetTeam()
			self:ProcessSkins()
			self:ProcessBodygroups()
			self.modeldef = nil
		end
	end

	if self.modeldef then
		local targets = {}

		for hat, hatdef in pairs(self.hat_defs) do
			for _, targetname in ipairs(hatdef.targets) do
				if not table.qhasValue(targets, targetname) then
					table.insert(targets, targetname)
				end
			end
		end

		for i, target in ipairs(targets) do
			if self.modeldef.targets[target] then
				table.Merge(self.modeldef_overrides, self.modeldef.targets[target])
			end
		end
	end

	self.bodygroup_overrides = self.modeldef_overrides.bodygroup_overrides
	self._bodygroups = self.entity:GetBodyGroups()
	self._bodygroups_mapping = {}
	self._bodygroups_mapping_hands = {}
	self._do_hands = false
	self._hands_entity = NULL

	if not self.modeldef then return end

	for _, bodygroup in ipairs(self._bodygroups) do
		bodygroup.name = bodygroup.name:lower()
		self._bodygroups_mapping[bodygroup.name] = bodygroup.id
	end

	if self.entity.GetHands and self.entity:GetHands():IsValid() and self.entity:GetHands():GetModel() == self.modeldef.hands then
		self._do_hands = true
		self._hands_entity = self.entity:GetHands()
		self._hands_bodygroups = self._hands_entity:GetBodyGroups()

		if self.modeldef.hands_bodygroups then
			for _, bodygroup in ipairs(self._hands_bodygroups) do
				bodygroup.name = bodygroup.name:lower()
				self._bodygroups_mapping_hands[bodygroup.name] = bodygroup.id
			end
		end
	end

	for hat, hatdef in pairs(self.hat_defs) do
		if hatdef.global_overrides then
			table.Merge(self.modeldef_overrides, hatdef.global_overrides)
		end

		if hatdef.overrides[self.modeldef.classname] then
			table.Merge(self.modeldef_overrides, hatdef.overrides[self.modeldef.classname])
		end

		if hatdef.bodygroup_overrides then
			for bodygroup, value in pairs(hatdef.bodygroup_overrides) do
				self.bodygroup_overrides[bodygroup:lower()] = value
			end
		end

		local style = self:GetStyleTable(hat, hatdef)

		if style then
			if istable(style.bodygroup_overrides) then
				for bodygroup, value in pairs(style.bodygroup_overrides) do
					self.bodygroup_overrides[bodygroup:lower()] = value
				end
			end
		end
	end
end

function Worker:GetModelDef()
	return self.modeldef
end

function Worker:GetModelDefOverrides()
	return self.modeldef_overrides
end

function Worker:GetHatList()
	return self.hat_defs
end

function Worker:HasHatColor(hat)
	return self.hat_colors[hat] ~= nil
end

function Worker:HasHatStyle(hat)
	return self.hat_styles[hat] ~= nil
end

function Worker:GetHatColor(hat)
	return self.hat_colors[hat] or false
end

function Worker:GetHatStyle(hat)
	return self.hat_styles[hat] or false
end

function Worker:GetHatArray()
	local output = {}

	for _, hatdef in pairs(self.hat_defs) do
		table.insert(output, hatdef)
	end

	return output
end

function Worker:GetTeam()
	return self.team and self.team_index or false
end

function Worker:Remove()
	self:RemoveAllHats()
	self.entity.tf2_pm = nil
end

function Worker:SerializeTable()
	local colors = {}
	local hats = {}

	for k, v in pairs(self.hat_colors) do
		colors[k] = Color(v)
	end

	for k in pairs(self.hat_defs) do
		table.insert(hats, k)
	end

	return {
		hats = hats,
		styles = table.Copy(self.hat_styles),
		colors = colors,
		team_index = self.team_index,
		team = self.team,
		bodygroup_overrides = self.custom_bodygroup_overrides,
	}
end

function Worker:Serialize()
	return DLib.GON.Serialize(self:SerializeTable())
end

function Worker:SerializeString()
	return self:Serialize():ToString()
end

function Worker:DeserializeTable(input)
	self:RemoveAllHats()
	self:UnsetCustomBodygroupAll()

	for _, hat in ipairs(input.hats) do
		self:WearHat(hat)
	end

	for hat, style in pairs(input.styles) do
		self:SetHatStyle(hat, style)
	end

	for hat, color in pairs(input.colors) do
		self:SetHatColor(hat, color)
	end

	if input.team and input.team_index then
		self:SetTeam(input.team_index)
	else
		self:UnsetTeam()
	end

	if input.bodygroup_overrides then
		for k, v in pairs(input.bodygroup_overrides) do
			self:SetCustomBodygroup(k, v)
		end
	end
end

function Worker:Deserialize(input)
	self:DeserializeTable(DLib.GON.Deserialize(input, true))
end

function Worker:DeserializeString(input)
	self:Deserialize(DLib.BytesBuffer(input))
end

function Worker:CopyTo(otherdata)
	for hat, hatdef in pairs(otherdata.hat_defs) do
		if not self.hat_defs[hat] then
			otherdata:RemoveHat(hat)
		end
	end

	for hat, hatdef in pairs(self.hat_defs) do
		if not otherdata.hat_defs[hat] then
			otherdata:WearHat(hat)
		end
	end

	for hat, style in pairs(otherdata.hat_styles) do
		if not self.hat_styles[hat] then
			otherdata:UnetHatStyle(hat)
		end
	end

	for hat, color in pairs(self.hat_colors) do
		if not self.hat_colors[hat] then
			otherdata:UnsetHatColor(hat)
		end
	end

	for hat, style in pairs(self.hat_styles) do
		otherdata:SetHatStyle(hat, style)
	end

	for hat, color in pairs(self.hat_colors) do
		otherdata:SetHatColor(hat, color)
	end

	otherdata:SetTeam(self.team_index)
end

function Worker:SetHatColor(hat, color)
	assert(isstring(hat), 'Invalid hat')
	assert(IsColor(color), 'Invalid color')

	if color.r == 0 and color.g == 0 and color.b == 0 then
		return self:UnsetHatColor(hat)
	end

	if not self.hat_defs[hat] then return false end
	if not self.hat_defs[hat].paintable then return false end

	self.hat_colors[hat] = color

	if CLIENT then
		if IsValid(self.hat_models[hat]) then
			self.hat_models[hat].tf2_pm_paint = color:ToVector()
		end
	else
		net.Start('tf2_pm_set_hat_color')
		net.WriteEntity(self.entity)
		net.WriteString(hat)
		net.WriteColor(color)
		net.Broadcast()
	end

	return true
end

function Worker:UnsetHatColor(hat)
	assert(isstring(hat), 'Invalid hat')

	if not self.hat_defs[hat] then return false end

	self.hat_colors[hat] = nil

	if CLIENT then
		if IsValid(self.hat_models[hat]) then
			self.hat_models[hat].tf2_pm_paint = nil
		end
	else
		net.Start('tf2_pm_unset_hat_color')
		net.WriteEntity(self.entity)
		net.WriteString(hat)
		net.Broadcast()
	end

	return true
end

function Worker:SetHatStyle(hat, style)
	assert(isstring(hat), 'Invalid hat')
	assert(isnumber(style), 'Invalid style')

	if style <= 0 then
		return self:UnsetHatStyle(hat, style)
	end

	if not self.hat_defs[hat] then return false end
	if not self.hat_defs[hat].styles then return false end
	if not self.hat_defs[hat].styles[style] then return false end

	self.hat_styles[hat] = style

	if SERVER then
		net.Start('tf2_pm_set_hat_style')
		net.WriteEntity(self.entity)
		net.WriteString(hat)
		net.WriteUInt16(style)
		net.Broadcast()
	end

	self:UpdateEverything()

	return true
end

function Worker:UnsetHatStyle(hat)
	assert(isstring(hat), 'Invalid hat')

	if not self.hat_defs[hat] then return false end
	if not self.hat_styles[hat] then return false end

	self.hat_styles[hat] = nil

	if SERVER then
		net.Start('tf2_pm_unset_hat_style')
		net.WriteEntity(self.entity)
		net.WriteString(hat)
		net.Broadcast()
	end

	self:UpdateEverything()

	return true
end

function Worker:WearHat(hat)
	local hatdef = tf2_pm.hat_registry[hat]

	if not hatdef then return false end

	if not self.modeldef or self.modeldef.model ~= self.entity:GetModel() then
		self:RebuildModelDef()
	end

	if not self.modeldef then return end

	if not tf2_pm.IsApplicable(hatdef, self.modeldef) then return false end

	if CLIENT and IsValid(self.hat_models[hat]) then
		self.hat_models[hat]:SetParent(self.entity)
		self.hat_models[hat]:AddEffects(EF_BONEMERGE)
		self.hat_models[hat]:SetNoDraw(self.hide_status)
		self:UpdateEverything()
		return true
	end

	if not self.hat_defs[hat] and tf2_pm.IsConflictingList(hatdef, self.hat_defs) then return false end

	self.hat_defs[hat] = hatdef
	self.hat_styles[hat] = nil
	self.hat_colors[hat] = nil

	self:UpdateEverything()

	if SERVER then
		net.Start('tf2_pm_apply_hat')
		net.WriteEntity(self.entity)
		net.WriteString(hat)
		net.Broadcast() -- not PVS since players are networked outside of PVS to other players
	end

	return true
end

function Worker:Send(ply)
	for hat in pairs(self.hat_defs) do
		net.Start('tf2_pm_apply_hat')
		net.WriteEntity(self.entity)
		net.WriteString(hat)
		net.Send(ply)
	end

	for hat, style in pairs(self.hat_styles) do
		net.Start('tf2_pm_set_hat_style')
		net.WriteEntity(self.entity)
		net.WriteString(hat)
		net.WriteUInt16(style)
		net.Send(ply)
	end

	for hat, color in pairs(self.hat_colors) do
		net.Start('tf2_pm_set_hat_color')
		net.WriteEntity(self.entity)
		net.WriteString(hat)
		net.WriteColor(color)
		net.Send(ply)
	end
end

function Worker:RemoveHat(hat)
	local hatdef = tf2_pm.hat_registry[hat]
	if not hatdef then return false end

	if not self.hat_defs[hat] then return false end

	if CLIENT and IsValid(self.hat_models[hat]) then
		local model = self.hat_models[hat]

		for i, model2 in ipairs(tf2_pm.known_models) do
			if model2 == model then
				table.remove(tf2_pm.known_models, i)
				break
			end
		end

		model.garbage = true
		model:Remove()
	end

	self.hat_defs[hat] = nil
	self.hat_styles[hat] = nil
	self.hat_colors[hat] = nil

	if CLIENT then
		self.hat_models[hat] = nil
	end

	self:UpdateEverything()

	if SERVER then
		net.Start('tf2_pm_remove_hat')
		net.WriteEntity(self.entity)
		net.WriteString(hat)
		net.Broadcast() -- not PVS since players are networked outside of PVS to other players
	end

	return true
end

function Worker:UpdateEverything()
	self:RebuildModelDef()

	if CLIENT then
		self:UpdateModels()
	end

	self:ProcessBodygroups()
	self:ProcessSkins()
end

function Worker:UpdateModels()
	for hat, hatdef in pairs(self.hat_defs) do
		local findmodel = hatdef.model
		local style = self:GetStyleTable(hat, hatdef)

		if style and style.model then
			findmodel = style.model
		end

		if not IsValid(self.hat_models[hat]) then
			self.hat_models[hat] = ClientsideModel(findmodel, RENDERGROUP_BOTH)

			if IsValid(self.hat_models[hat]) then
				self.hat_models[hat]:SetNoDraw(self.hide_status)
				self.hat_models[hat].tf2_pm = true
				self.hat_models[hat].tf2_owner = self.entity
				self.hat_models[hat].tf2_def = hatdef
			else
				self.hat_models[hat] = nil
			end

			table.insert(tf2_pm.known_models, self.hat_models[hat])
		end

		if self.hat_models[hat] then
			self.hat_models[hat]:SetParent(self.entity)
			self.hat_models[hat]:AddEffects(EF_BONEMERGE)

			if self.hat_models[hat]:GetModel() ~= findmodel then
				self.hat_models[hat]:SetModel(findmodel)
			end
		end
	end
end

function Worker:MergeModelsTo(ent)
	self:UpdateModels()

	for hat, model in pairs(self.hat_models) do
		model:SetParent(ent)
		model:AddEffects(EF_BONEMERGE)
	end
end

function Worker:HideModels(hideStatus)
	self.hide_status = hideStatus

	-- self:UpdateModels()

	for hat, model in pairs(self.hat_models) do
		if IsValid(model) then
			model:SetNoDraw(hideStatus)
		end
	end
end

function Worker:UnsetCustomBodygroupAll()
	for k in pairs(self.custom_bodygroup_overrides) do
		self:UnsetCustomBodygroup(k)
	end
end

function Worker:SetCustomBodygroup(name, value)
	self.custom_bodygroup_overrides[assert(name, 'missing bodygroup name'):lower()] = assert(value, 'missing bodyrgoup value')
	self:ProcessBodygroups()

	if SERVER and IsValid(self.entity) then
		net.Start('tf2_pm_set_bodygroup')
		net.WriteEntity(self.entity)
		net.WriteBool(true)
		net.WriteString(name)
		net.WriteUInt8(value)
		net.Broadcast()
	end
end

function Worker:UnsetCustomBodygroup(name)
	self.custom_bodygroup_overrides[assert(name, 'missing bodygroup name'):lower()] = nil
	self:ProcessBodygroups()

	if SERVER and IsValid(self.entity) then
		net.Start('tf2_pm_set_bodygroup')
		net.WriteBool(false)
		net.WriteString(name)
		net.Broadcast()
	end
end

function Worker:ProcessBodygroups()
	if not self.modeldef then return end

	local affected = tf2_pm.FindAffectedTargets(self.hat_defs)

	for _, bodygroup in ipairs(self._bodygroups) do
		self.entity:SetBodygroup(bodygroup.id, self.bodygroup_overrides[bodygroup.name] or 0)
	end

	if self._do_hands and self.modeldef.hands_bodygroups then
		self._hands_entity = self.entity:GetHands()

		if IsValid(self._hands_entity) then
			self._hands_bodygroups = self._hands_entity:GetBodyGroups()

			if self.modeldef.hands_bodygroups then
				for _, bodygroup in ipairs(self._hands_bodygroups) do
					bodygroup.name = bodygroup.name:lower()
					self._bodygroups_mapping_hands[bodygroup.name] = bodygroup.id
				end

				for _, bodygroup in ipairs(self._hands_bodygroups) do
					self._hands_entity:SetBodygroup(bodygroup.id, self.bodygroup_overrides[bodygroup.name] or 0)
				end
			end
		end
	end

	if self.modeldef_overrides.targets then
		for _, target in ipairs(affected) do
			if self.modeldef_overrides.targets[target] then
				for _, data in ipairs(self.modeldef_overrides.targets[target]) do
					if data.type == 'bodygroup' and not self.bodygroup_overrides[data.name:lower()] then
						self.entity:SetBodygroup(self._bodygroups_mapping[data.name:lower()], data.value)
					end
				end
			end
		end
	end

	for k, v in pairs(self.custom_bodygroup_overrides) do
		if self._bodygroups_mapping[k] then
			self.entity:SetBodygroup(self._bodygroups_mapping[k], v)
		end
	end
end

function Worker:_ResponsiveSkin(tableInput, fallback)
	if fallback == nil then fallback = false end
	if not istable(tableInput) then return fallback end
	if not self.modeldef_overrides then return fallback or tableInput[1] end

	if not self.modeldef_overrides.remap_responsive_skins then
		return tableInput[self.team_index] or fallback or tableInput[1]
	end

	return self.modeldef_overrides.remap_responsive_skins[self.team_index] and
		tableInput[self.modeldef_overrides.remap_responsive_skins[self.team_index]] or
		tableInput[self.team_index] or fallback or tableInput[1]
end

function Worker:ProcessSkins()
	if not self.modeldef then return end

	if self.team then
		if self.modeldef_overrides.model_skins then
			self.entity:SetSkin(
				self.modeldef_overrides.model_skins[self.team_index] or
				self.modeldef_overrides.model_skins[1] or
				0)
		end

		if self._do_hands and self.modeldef.hands_skin then
			self._hands_entity = self.entity:GetHands()

			if IsValid(self._hands_entity) then
				if self.modeldef_overrides.hands_skin_map then
					self._hands_entity:SetSkin(
						self.modeldef_overrides.hands_skin_map[self.team_index] or
						self.modeldef_overrides.hands_skin_map[1] or
						0)
				else
					self._hands_entity:SetSkin(
						self.modeldef_overrides.model_skins[self.team_index] or
						self.modeldef_overrides.model_skins[1] or
						0)
				end
			end
		end
	else
		self.entity:SetSkin(self.modeldef.player_color_skin or self.modeldef_overrides.model_skins and self.modeldef_overrides.model_skins[1] or 0)

		if self._do_hands and self.modeldef.hands_skin then
			self._hands_entity = self.entity:GetHands()

			if IsValid(self._hands_entity) then
				self._hands_entity:SetSkin(
					self.modeldef_overrides.hands_skin_map and
					self.modeldef_overrides.hands_skin_map[1] or
					self.modeldef.player_color_skin or
					self.modeldef_overrides.model_skins and
					self.modeldef_overrides.model_skins[1] or
					0)
			end
		end
	end

	if CLIENT then
		for hat, hatdef in pairs(self.hat_defs) do
			if IsValid(self.hat_models[hat]) then
				local responsiveSkin = self:_ResponsiveSkin(hatdef.responsive_skins, hatdef.fallback_skin)

				if responsiveSkin then
					self.hat_models[hat]:SetSkin(responsiveSkin)
				end

				local style = self:GetStyleTable(hat, hatdef)

				if style then
					if isnumber(style.skin) then
						self.hat_models[hat]:SetSkin(style.skin)
					elseif istable(style.responsive_skins) then
						local responsiveSkin = self:_ResponsiveSkin(style.responsive_skins,
							self:_ResponsiveSkin(style.responsive_skins, hatdef.fallback_skin) or style.fallback_skin)

						if responsiveSkin then
							self.hat_models[hat]:SetSkin(responsiveSkin)
						end
					end
				end
			end
		end
	end
end

function Worker:UnsetTeam()
	self.team = false
	self.team_index = -1

	if SERVER then
		net.Start('tf2_pm_unset_team')
		net.WriteEntity(self.entity)
		net.WriteBool(newTeam)
		net.Broadcast()
	end

	self:ProcessSkins()
end

function Worker:SetTeam(newTeam)
	if newTeam == nil then newTeam = 0 end
	if newTeam < 1 then return self:UnsetTeam() end

	self.team = true
	self.team_index = newTeam

	if SERVER then
		net.Start('tf2_pm_set_team')
		net.WriteEntity(self.entity)
		net.WriteUInt16(newTeam)
		net.Broadcast()
	end

	self:ProcessSkins()
end

tf2_pm.Worker = DLib.CreateMoonClassBare('Worker', Worker, WorkerClass, nil, tf2_pm.Worker)

function tf2_pm.FindAffectedTargets(hatlist)
	local affected = {}

	for _, hatdef in pairs(hatlist) do
		if istable(hatdef.targets) then
			for i, target in ipairs(hatdef.targets) do
				if not table.qhasValue(affected) then
					table.insert(affected, target)
				end
			end
		end
	end

	return affected
end

local plyMeta = FindMetaTable('Player')
local CSEnt = FindMetaTable('CSEnt')

function plyMeta:GetTF2PM()
	if not self.tf2_pm then
		self.tf2_pm = tf2_pm.Worker(self)
	end

	return self.tf2_pm
end

if CSEnt then
	CSEnt.GetTF2PM = plyMeta.GetTF2PM
end

for _, ply in ipairs(player.GetAll()) do
	if ply.tf2_pm then
		--[[local hat_defs = table.GetKeys(ply.tf2_pm.hat_defs)
		local hat_styles = ply.tf2_pm.hat_styles
		local hat_colors = ply.tf2_pm.hat_colors
		local team = ply.tf2_pm.team_index]]

		ply.tf2_pm:Remove()
		ply.tf2_pm = nil

		--[[if SERVER then
			timer.Simple(1, function()
				local pm = ply:GetTF2PM()

				for _, hat in ipairs(hat_defs) do
					pm:WearHat(hat)

					if hat_styles[hat] then
						pm:SetHatStyle(hat, hat_styles[hat])
					end

					if hat_colors[hat] then
						pm:SetHatColor(hat, hat_colors[hat])
					end

					pm:SetTeam(team)
				end
			end)
		end]]

		if SERVER then
			timer.Simple(1, function()
				net.Start('tf2_pm_request_reset')
				net.Send(ply)
			end)
		end
	end
end
