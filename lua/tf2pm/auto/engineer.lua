
-- Auto generated at 2020-12-04 12:56:46 UTC+07:00
return {
	{
		classes = {"engineer"},
		targets = {},
		paintable = false,
		name = "Mining Light",
		localized_name = "info.tf2pm.hat.engineer_hat_1",
		localized_description = "info.tf2pm.hat.engineer_hat_1_desc",
		icon = "backpack/player/items/engineer/mining_hat",
		models = {
			["basename"] = "models/player/items/engineer/mining_hat.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Texas Ten Gallon",
		localized_name = "info.tf2pm.hat.engineer_cowboy_hat",
		localized_description = "info.tf2pm.hat.engineer_cowboy_hat_desc",
		icon = "backpack/player/items/engineer/engineer_cowboy_hat",
		models = {
			["basename"] = "models/player/items/engineer/engineer_cowboy_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Engineer's Cap",
		localized_name = "info.tf2pm.hat.engineer_train_hat",
		icon = "backpack/player/items/engineer/engineer_train_hat",
		models = {
			["basename"] = "models/player/items/engineer/engineer_train_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "Texas Slim's Dome Shine",
		localized_name = "info.tf2pm.hat.hatless_engineer",
		localized_description = "info.tf2pm.hat.hatless_engineer_desc",
		icon = "backpack/player/items/engineer/engineer_nohat",
		models = {
			["basename"] = "models/player/items/engineer/engineer_nohat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "Engineer Welding Mask",
		localized_name = "info.tf2pm.hat.engineerweldingmask",
		localized_description = "info.tf2pm.hat.engineerweldingmask_desc",
		icon = "backpack/player/items/engineer/weldingmask",
		models = {
			["basename"] = "models/player/items/engineer/weldingmask.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Engineer Earmuffs",
		localized_name = "info.tf2pm.hat.engineerearmuffs",
		localized_description = "info.tf2pm.hat.engineerearmuffs_desc",
		icon = "backpack/player/items/engineer/engy_earphones",
		models = {
			["basename"] = "models/player/items/engineer/engy_earphones.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Buckaroos Hat",
		localized_name = "info.tf2pm.hat.buckarooshat",
		icon = "backpack/player/items/engineer/engineer_buckaroos_hat",
		models = {
			["basename"] = "models/player/items/engineer/engineer_buckaroos_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "Industrial Festivizer",
		localized_name = "info.tf2pm.hat.coloredlights",
		icon = "backpack/player/items/engineer/engineer_colored_lights",
		models = {
			["basename"] = "models/player/items/engineer/engineer_colored_lights.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "Western Wear",
		localized_name = "info.tf2pm.hat.engineertophat",
		localized_description = "info.tf2pm.hat.engineertophat_desc",
		icon = "backpack/player/items/engineer/engineer_top_hat",
		models = {
			["basename"] = "models/player/items/engineer/engineer_top_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Big Country",
		localized_name = "info.tf2pm.hat.bigcountry",
		localized_description = "info.tf2pm.hat.bigcountry_desc",
		icon = "backpack/player/items/crafting/mullet_hat_icon",
		models = {
			["basename"] = "models/player/items/engineer/mullet_hat.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.bigcountry_style0",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
			[1] = {
				localized_name = "info.tf2pm.hat.bigcountry_style1",
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"glasses", "hat"},
		paintable = true,
		name = "Professor's Peculiarity",
		localized_name = "info.tf2pm.hat.professorhair",
		localized_description = "info.tf2pm.hat.professorhair_desc",
		icon = "backpack/player/items/crafting/professor_hair_icon",
		models = {
			["basename"] = "models/player/items/engineer/professor_hair.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.professorhair_style0",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
			[1] = {
				localized_name = "info.tf2pm.hat.professorhair_style1",
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = false,
		name = "Teddy Roosebelt",
		localized_name = "info.tf2pm.hat.teddyroosebelt",
		localized_description = "info.tf2pm.hat.teddyroosebelt_desc",
		icon = "backpack/player/items/engineer/teddy_roosebelt",
		models = {
			["basename"] = "models/player/items/engineer/teddy_roosebelt.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"lenses"},
		paintable = true,
		name = "Googly Gazer",
		localized_name = "info.tf2pm.hat.googlygazer",
		localized_description = "info.tf2pm.hat.googlygazer_desc",
		icon = "backpack/player/items/engineer/mad_eye",
		models = {
			["basename"] = "models/player/items/engineer/mad_eye.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.gazer_style0",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.gazer_style1",
			},
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Ol' Geezer",
		localized_name = "info.tf2pm.hat.ol_geezer",
		localized_description = "info.tf2pm.hat.ol_geezer_desc",
		icon = "backpack/player/items/engineer/prospector_hat",
		models = {
			["basename"] = "models/player/items/engineer/prospector_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Polish War Babushka",
		localized_name = "info.tf2pm.hat.polishwarbabushka",
		localized_description = "info.tf2pm.hat.polishwarbabushka_desc",
		icon = "backpack/player/items/engineer/mbsf_engineer",
		models = {
			["basename"] = "models/player/items/engineer/mbsf_engineer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"feet"},
		paintable = true,
		name = "Prairie Heel Biters",
		localized_name = "info.tf2pm.hat.heelbiters",
		icon = "backpack/player/items/crafting/spurs_backpack_icon",
		models = {
			["basename"] = "models/player/items/engineer/spurs.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_left_arm"},
		paintable = false,
		name = "Pip-Boy",
		localized_name = "info.tf2pm.hat.pipboy",
		localized_description = "info.tf2pm.hat.pipboy_desc",
		icon = "backpack/workshop_partner/player/items/engineer/bet_pb/bet_pb",
		models = {
			["basename"] = "models/workshop_partner/player/items/engineer/bet_pb/bet_pb.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_belt"},
		paintable = false,
		name = "Wingstick",
		localized_name = "info.tf2pm.hat.wingstick",
		icon = "backpack/workshop_partner/player/items/engineer/bet_wingstick/bet_wingstick",
		models = {
			["basename"] = "models/workshop_partner/player/items/engineer/bet_wingstick/bet_wingstick.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "Clockwerk's Helm",
		localized_name = "info.tf2pm.hat.clockwerk_hat",
		localized_description = "info.tf2pm.hat.dotagamescom2011_hat_desc",
		icon = "backpack/player/items/engineer/clockwerk_hat",
		models = {
			["basename"] = "models/player/items/engineer/clockwerk_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"whole_head"},
		paintable = false,
		name = "Buzz Killer",
		localized_name = "info.tf2pm.hat.hwn_engineerhat",
		icon = "backpack/player/items/engineer/hwn_engineer_hat",
		models = {
			["basename"] = "models/player/items/engineer/hwn_engineer_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_wings"},
		paintable = false,
		name = "Frontier Flyboy",
		localized_name = "info.tf2pm.hat.hwn_engineermisc1",
		icon = "backpack/player/items/engineer/hwn_engineer_misc1",
		models = {
			["basename"] = "models/player/items/engineer/hwn_engineer_misc1.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"feet"},
		paintable = false,
		name = "Legend of Bugfoot",
		localized_name = "info.tf2pm.hat.hwn_engineermisc2",
		icon = "backpack/player/items/engineer/hwn_engineer_misc2",
		models = {
			["basename"] = "models/player/items/engineer/hwn_engineer_misc2.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_hair"},
		paintable = false,
		name = "The Brainiac Hairpiece",
		localized_name = "info.tf2pm.hat.brainiac",
		localized_description = "info.tf2pm.hat.brainiac_desc",
		icon = "backpack/player/items/engineer/drg_brainiac_hair",
		models = {
			["basename"] = "models/player/items/engineer/drg_brainiac_hair.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.brainiac_style_helmet",
			},
			[1] = {
				localized_name = "info.tf2pm.hat.brainiac_style_nohelmet",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
		},
	},
	{
		classes = {"engineer"},
		targets = {"glasses"},
		paintable = false,
		name = "The Brainiac Goggles",
		localized_name = "info.tf2pm.hat.brainiac_goggles",
		localized_description = "info.tf2pm.hat.brainiac_goggles_desc",
		icon = "backpack/player/items/engineer/drg_brainiac_goggles",
		models = {
			["basename"] = "models/player/items/engineer/drg_brainiac_goggles.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {},
		paintable = false,
		name = "The Pencil Pusher",
		localized_name = "info.tf2pm.hat.engineerhat1",
		localized_description = "info.tf2pm.hat.engineerhat1_desc",
		icon = "backpack/player/items/all_class/heavy_thinker",
		models = {
			["basename"] = "models/player/items/engineer/heavy_thinker.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = true,
		name = "The Builder's Blueprints",
		localized_name = "info.tf2pm.hat.engineerblueprints",
		localized_description = "info.tf2pm.hat.engineerblueprints_desc",
		icon = "backpack/player/items/engineer/fwk_engineer_blueprints",
		models = {
			["basename"] = "models/player/items/engineer/fwk_engineer_blueprints.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "The Virtual Reality Headset",
		localized_name = "info.tf2pm.hat.engineerhat2",
		localized_description = "info.tf2pm.hat.engineerhat2_desc",
		icon = "backpack/player/items/engineer/fwk_engineer_cranial",
		models = {
			["basename"] = "models/player/items/engineer/fwk_engineer_cranial.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = false,
		name = "The Stocking Stuffer",
		localized_name = "info.tf2pm.hat.winter2011_engineerstocking",
		localized_description = "info.tf2pm.hat.winter2011_engineerstocking_desc",
		icon = "backpack/player/items/engineer/xms_engineer_stocking",
		models = {
			["basename"] = "models/player/items/engineer/xms_engineer_stocking.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"pants"},
		paintable = true,
		name = "The Texas Half-Pants",
		localized_name = "info.tf2pm.hat.engineerchaps",
		localized_description = "info.tf2pm.hat.engineerchaps_desc",
		icon = "backpack/player/items/engineer/engineer_chaps",
		models = {
			["basename"] = "models/player/items/engineer/engineer_chaps.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.engineerchaps_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.engineerchaps_style1",
			},
		},
	},
	{
		classes = {"engineer"},
		targets = {"back"},
		paintable = true,
		name = "The Idea Tube",
		localized_name = "info.tf2pm.hat.engineerblueprintsback",
		localized_description = "info.tf2pm.hat.engineerblueprintsback_desc",
		icon = "backpack/player/items/engineer/engineer_blueprints_back",
		models = {
			["basename"] = "models/player/items/engineer/engineer_blueprints_back.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = true,
		name = "The Pocket Purrer",
		localized_name = "info.tf2pm.hat.engineerpocketcat",
		localized_description = "info.tf2pm.hat.engineerpocketcat_desc",
		icon = "backpack/player/items/engineer/engineer_pocketcat",
		models = {
			["basename"] = "models/player/items/engineer/engineer_pocketcat.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "The Tin-1000",
		localized_name = "info.tf2pm.hat.engineer_robot_hat",
		localized_description = "info.tf2pm.hat.engineer_robot_hat_desc",
		icon = "backpack/player/items/mvm_loot/engineer/robo_engy_hat",
		models = {
			["basename"] = "models/player/items/mvm_loot/engineer/robo_engy_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "The Master Mind",
		localized_name = "info.tf2pm.hat.mastermind",
		localized_description = "info.tf2pm.hat.mastermind_desc",
		icon = "backpack/player/items/engineer/engineer_brain",
		models = {
			["basename"] = "models/player/items/engineer/engineer_brain.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "The Barnstormer",
		localized_name = "info.tf2pm.hat.thebarnstormer",
		localized_description = "info.tf2pm.hat.thebarnstormer_desc",
		icon = "backpack/player/items/engineer/engineer_barnstormer_s01",
		models = {
			["basename"] = "models/player/items/engineer/engineer_barnstormer_s01.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.thebarnstormer_style0",
				models = {
					["basename"] = "models/player/items/engineer/engineer_barnstormer_s01.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.thebarnstormer_style1",
				models = {
					["basename"] = "models/player/items/engineer/engineer_barnstormer_s02.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = true,
		name = "The Prize Plushy",
		localized_name = "info.tf2pm.hat.prizeplushy",
		localized_description = "info.tf2pm.hat.prizeplushy_desc",
		icon = "backpack/workshop_partner/player/items/engineer/cave_bear/cave_bear",
		models = {
			["basename"] = "models/workshop_partner/player/items/engineer/cave_bear/cave_bear.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"beard"},
		paintable = true,
		name = "The Grizzled Growth",
		localized_name = "info.tf2pm.hat.grizzledgrowth",
		localized_description = "info.tf2pm.hat.grizzledgrowth_desc",
		icon = "backpack/workshop_partner/player/items/engineer/cave_beard/cave_beard",
		models = {
			["basename"] = "models/workshop_partner/player/items/engineer/cave_beard/cave_beard.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "The Last Straw",
		localized_name = "info.tf2pm.hat.laststraw",
		localized_description = "info.tf2pm.hat.laststraw_desc",
		icon = "backpack/workshop_partner/player/items/engineer/cave_hat/cave_hat",
		models = {
			["basename"] = "models/workshop_partner/player/items/engineer/cave_hat/cave_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Vox Diabolus",
		localized_name = "info.tf2pm.hat.voxdiabolus",
		localized_description = "info.tf2pm.hat.voxdiabolus_desc",
		icon = "backpack/player/items/engineer/bio_voxhood",
		models = {
			["basename"] = "models/player/items/engineer/bio_voxhood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "MvM GateBot Light Engineer",
		icon = "backpack",
		models = {
			["basename"] = "models/bots/gameplay_cosmetic/light_engineer_on.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_engineer_on.mdl",
				}
			},
			[1] = {
				responsive_skins = {1, 1},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_engineer_off.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = false,
		name = "Mister Bubbles",
		localized_name = "info.tf2pm.hat.misterbubbles",
		localized_description = "info.tf2pm.hat.misterbubbles_desc",
		icon = "backpack/workshop/player/items/engineer/bi_big_daddy_doll/bi_big_daddy_doll",
		models = {
			["basename"] = "models/workshop/player/items/engineer/bi_big_daddy_doll/bi_big_daddy_doll.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 engineer_detectiveglasses",
		localized_name = "info.tf2pm.hat.dec2014_engineer_detectiveglasses",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/engineer/dec2014_engineer_detectiveglasses/dec2014_engineer_detectiveglasses",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec2014_engineer_detectiveglasses/dec2014_engineer_detectiveglasses.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = true,
		name = "dec2014 engineer_detectiveradio",
		localized_name = "info.tf2pm.hat.dec2014_engineer_detectiveradio",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/engineer/dec2014_engineer_detectiveradio/dec2014_engineer_detectiveradio",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec2014_engineer_detectiveradio/dec2014_engineer_detectiveradio.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = true,
		name = "dec2014 engineer_detectiveholster",
		localized_name = "info.tf2pm.hat.dec2014_engineer_detectiveholster",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/engineer/dec2014_engineer_detectiveholster/dec2014_engineer_detectiveholster",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec2014_engineer_detectiveholster/dec2014_engineer_detectiveholster.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = false,
		name = "Rocket Operator",
		localized_name = "info.tf2pm.hat.invasion_rocket_operator",
		icon = "backpack/workshop/player/items/engineer/invasion_rocket_operator/invasion_rocket_operator",
		models = {
			["basename"] = "models/workshop/player/items/engineer/invasion_rocket_operator/invasion_rocket_operator.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "The Data Mining Light",
		localized_name = "info.tf2pm.hat.robo_engineer_mining_light",
		localized_description = "info.tf2pm.hat.robo_engineer_mining_light_desc",
		icon = "backpack/workshop/player/items/engineer/robo_engineer_mining_light/robo_engineer_mining_light",
		models = {
			["basename"] = "models/workshop/player/items/engineer/robo_engineer_mining_light/robo_engineer_mining_light.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_belt"},
		paintable = true,
		name = "Conaghers' Utility Idol",
		localized_name = "info.tf2pm.hat.fall17_conaghers_utility_idol",
		localized_description = "info.tf2pm.hat.fall17_conaghers_utility_idol_desc",
		icon = "backpack/workshop/player/items/engineer/fall17_conaghers_utility_idol/fall17_conaghers_utility_idol",
		models = {
			["basename"] = "models/workshop/player/items/engineer/fall17_conaghers_utility_idol/fall17_conaghers_utility_idol.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"pants"},
		paintable = true,
		name = "The Flared Frontiersman",
		localized_name = "info.tf2pm.hat.jul13_king_pants",
		localized_description = "info.tf2pm.hat.jul13_king_pants_desc",
		icon = "backpack/workshop/player/items/engineer/jul13_king_pants/jul13_king_pants",
		models = {
			["basename"] = "models/workshop/player/items/engineer/jul13_king_pants/jul13_king_pants.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Texas Toast",
		localized_name = "info.tf2pm.hat.sum19_texas_toast",
		icon = "backpack/workshop/player/items/engineer/sum19_texas_toast/sum19_texas_toast",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sum19_texas_toast/sum19_texas_toast.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_left_arm"},
		paintable = true,
		name = "Iron Fist",
		localized_name = "info.tf2pm.hat.sf14_iron_fist",
		icon = "backpack/workshop/player/items/engineer/sf14_iron_fist/sf14_iron_fist",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sf14_iron_fist/sf14_iron_fist.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Plumber's Cap",
		localized_name = "info.tf2pm.hat.spr17_plumbers_cap",
		localized_description = "info.tf2pm.hat.spr17_plumbers_cap_desc",
		icon = "backpack/workshop/player/items/engineer/spr17_plumbers_cap/spr17_plumbers_cap",
		models = {
			["basename"] = "models/workshop/player/items/engineer/spr17_plumbers_cap/spr17_plumbers_cap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Texas Tin-Gallon",
		localized_name = "info.tf2pm.hat.robo_engineer_texastingallon",
		localized_description = "info.tf2pm.hat.robo_engineer_texastingallon_desc",
		icon = "backpack/workshop/player/items/engineer/robo_engineer_texastingallon/robo_engineer_texastingallon",
		models = {
			["basename"] = "models/workshop/player/items/engineer/robo_engineer_texastingallon/robo_engineer_texastingallon.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Blitzen Bowl",
		localized_name = "info.tf2pm.hat.dec20_blitzen_bowl",
		icon = "backpack/workshop/player/items/engineer/dec20_blitzen_bowl/dec20_blitzen_bowl",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec20_blitzen_bowl/dec20_blitzen_bowl.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = true,
		name = "Tropical Toad",
		localized_name = "info.tf2pm.hat.fall17_tropical_toad",
		localized_description = "info.tf2pm.hat.fall17_tropical_toad_desc",
		icon = "backpack/workshop/player/items/engineer/fall17_tropical_toad/fall17_tropical_toad",
		models = {
			["basename"] = "models/workshop/player/items/engineer/fall17_tropical_toad/fall17_tropical_toad.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Telefragger Toque",
		localized_name = "info.tf2pm.hat.dec19_telefragger_toque",
		icon = "backpack/workshop/player/items/engineer/dec19_telefragger_toque/dec19_telefragger_toque",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec19_telefragger_toque/dec19_telefragger_toque.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Tiny Texan",
		localized_name = "info.tf2pm.hat.sf14_tiny_texan",
		icon = "backpack/workshop/player/items/engineer/sf14_tiny_texan/sf14_tiny_texan",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sf14_tiny_texan/sf14_tiny_texan.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Sheriff's Stetson",
		localized_name = "info.tf2pm.hat.cc_summer2015_sheriffs_stetson",
		icon = "backpack/workshop/player/items/engineer/cc_summer2015_sheriffs_stetson/cc_summer2015_sheriffs_stetson",
		models = {
			["basename"] = "models/workshop/player/items/engineer/cc_summer2015_sheriffs_stetson/cc_summer2015_sheriffs_stetson.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.cc_summer2015_sheriffs_stetson_style1",
				models = {
					["basename"] = "models/workshop/player/items/engineer/cc_summer2015_sheriffs_stetson/cc_summer2015_sheriffs_stetson.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.cc_summer2015_sheriffs_stetson_style2",
				models = {
					["basename"] = "models/workshop/player/items/engineer/cc_summer2015_sheriffs_stetson_style1/cc_summer2015_sheriffs_stetson_style1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {},
		paintable = true,
		name = "Beep Man",
		localized_name = "info.tf2pm.hat.sf14_beep_man",
		icon = "backpack/workshop/player/items/engineer/sf14_beep_man/sf14_beep_man",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sf14_beep_man/sf14_beep_man.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = false,
		name = "Iron Lung",
		localized_name = "info.tf2pm.hat.hwn2015_iron_lung",
		icon = "backpack/workshop/player/items/engineer/hwn2015_iron_lung/hwn2015_iron_lung",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2015_iron_lung/hwn2015_iron_lung.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "The Timeless Topper",
		localized_name = "info.tf2pm.hat.robo_engineer_rustin",
		localized_description = "info.tf2pm.hat.robo_engineer_rustin_desc",
		icon = "backpack/workshop/player/items/engineer/robo_engineer_rustin/robo_engineer_rustin",
		models = {
			["basename"] = "models/workshop/player/items/engineer/robo_engineer_rustin/robo_engineer_rustin.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"glasses", "hat"},
		paintable = true,
		name = "The Pardner's Pompadour",
		localized_name = "info.tf2pm.hat.jul13_king_hair",
		localized_description = "info.tf2pm.hat.jul13_king_hair_desc",
		icon = "backpack/workshop/player/items/engineer/jul13_king_hair/jul13_king_hair",
		models = {
			["basename"] = "models/workshop/player/items/engineer/jul13_king_hair/jul13_king_hair.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"back"},
		paintable = true,
		name = "The Puggyback",
		localized_name = "info.tf2pm.hat.dec18_puggyback",
		icon = "backpack/workshop/player/items/engineer/dec18_puggyback/dec18_puggyback",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec18_puggyback/dec18_puggyback.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Wavefinder",
		localized_name = "info.tf2pm.hat.hwn2020_wavefinder",
		icon = "backpack/workshop/player/items/engineer/hwn2020_wavefinder/hwn2020_wavefinder",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2020_wavefinder/hwn2020_wavefinder.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"glasses", "hat"},
		paintable = true,
		name = "The Danger",
		localized_name = "info.tf2pm.hat.short2014_chemists_pride",
		icon = "backpack/workshop/player/items/engineer/short2014_chemists_pride/short2014_chemists_pride",
		models = {
			["basename"] = "models/workshop/player/items/engineer/short2014_chemists_pride/short2014_chemists_pride.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "The Plug-In Prospector",
		localized_name = "info.tf2pm.hat.robo_engineer_greaser",
		localized_description = "info.tf2pm.hat.robo_engineer_greaser_desc",
		icon = "backpack/workshop/player/items/engineer/robo_engineer_greaser/robo_engineer_greaser",
		models = {
			["basename"] = "models/workshop/player/items/engineer/robo_engineer_greaser/robo_engineer_greaser.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Smokey Sombrero",
		localized_name = "info.tf2pm.hat.hwn2015_western_hat",
		icon = "backpack/workshop/player/items/engineer/hwn2015_western_hat/hwn2015_western_hat",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2015_western_hat/hwn2015_western_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"right_shoulder"},
		paintable = true,
		name = "Aim Assistant",
		localized_name = "info.tf2pm.hat.hwn2018_aimbot_assistant",
		icon = "backpack/workshop/player/items/engineer/hwn2018_aimbot_assistant/hwn2018_aimbot_assistant",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2018_aimbot_assistant/hwn2018_aimbot_assistant.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2018_aimbot_assistant_style1",
				models = {
					["basename"] = "models/workshop/player/items/engineer/hwn2018_aimbot_assistant/hwn2018_aimbot_assistant.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2018_aimbot_assistant_style2",
				models = {
					["basename"] = "models/workshop/player/items/engineer/hwn2018_aimbot_assistant_s2/hwn2018_aimbot_assistant_s2.mdl",
				}
			},
		},
	},
	{
		classes = {"engineer"},
		targets = {"beard"},
		paintable = true,
		name = "El Patron",
		localized_name = "info.tf2pm.hat.hwn2015_western_beard",
		icon = "backpack/workshop/player/items/engineer/hwn2015_western_beard/hwn2015_western_beard",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2015_western_beard/hwn2015_western_beard.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Defragmenting Hard Hat 17%",
		localized_name = "info.tf2pm.hat.hwn2018_defragmenting_hat",
		icon = "backpack/workshop/player/items/engineer/hwn2018_defragmenting_hat/hwn2018_defragmenting_hat",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2018_defragmenting_hat/hwn2018_defragmenting_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"Back"},
		paintable = false,
		name = "Packable Provisions",
		localized_name = "info.tf2pm.hat.dec16_packable_provisions",
		localized_description = "info.tf2pm.hat.dec16_packable_provisions_desc",
		icon = "backpack/workshop/player/items/engineer/dec16_packable_provisions/dec16_packable_provisions",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec16_packable_provisions/dec16_packable_provisions.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"back"},
		paintable = true,
		name = "The Ghoul Box",
		localized_name = "info.tf2pm.hat.hwn2020_ghoul_box",
		icon = "backpack/workshop/player/items/engineer/hwn2020_ghoul_box/hwn2020_ghoul_box",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2020_ghoul_box/hwn2020_ghoul_box.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"head_skin"},
		paintable = false,
		name = "Goblineer",
		localized_name = "info.tf2pm.hat.hwn2020_goblineer",
		icon = "backpack/workshop/player/items/engineer/hwn2020_goblineer/hwn2020_goblineer",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2020_goblineer/hwn2020_goblineer.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2020_goblineer_style1",
				models = {
					["basename"] = "models/workshop/player/items/engineer/hwn2020_goblineer/hwn2020_goblineer.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2020_goblineer_style2",
				models = {
					["basename"] = "models/workshop/player/items/engineer/hwn2020_goblineer_style2/hwn2020_goblineer_style2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"zombie_body"},
		paintable = false,
		name = "Zombie Engineer",
		localized_name = "info.tf2pm.hat.item_zombieengineer",
		icon = "backpack/player/items/engineer/engineer_zombie",
		models = {
			["basename"] = "models/player/items/engineer/engineer_zombie.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "Eingineer",
		localized_name = "info.tf2pm.hat.hwn2019_eingineer",
		icon = "backpack/workshop/player/items/engineer/hwn2019_eingineer/hwn2019_eingineer",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2019_eingineer/hwn2019_eingineer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"beard"},
		paintable = true,
		name = "The Scotch Saver",
		localized_name = "info.tf2pm.hat.sbox2014_scotch_saver",
		icon = "backpack/workshop/player/items/engineer/sbox2014_scotch_saver/sbox2014_scotch_saver",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sbox2014_scotch_saver/sbox2014_scotch_saver.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "El Mostacho",
		localized_name = "info.tf2pm.hat.hwn2019_el_mostacho",
		icon = "backpack/workshop/player/items/engineer/hwn2019_el_mostacho/hwn2019_el_mostacho",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2019_el_mostacho/hwn2019_el_mostacho.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"face"},
		paintable = true,
		name = "The Grease Monkey",
		localized_name = "info.tf2pm.hat.hw2013_grease_monkey",
		icon = "backpack/workshop/player/items/engineer/hw2013_grease_monkey/hw2013_grease_monkey",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hw2013_grease_monkey/hw2013_grease_monkey.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "The Pug Mug",
		localized_name = "info.tf2pm.hat.hwn2020_pug_mug",
		icon = "backpack/workshop/player/items/engineer/hwn2020_pug_mug/hwn2020_pug_mug",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2020_pug_mug/hwn2020_pug_mug.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2020_pug_mug_style1",
				models = {
					["basename"] = "models/workshop/player/items/engineer/hwn2020_pug_mug/hwn2020_pug_mug.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2020_pug_mug_style2",
				models = {
					["basename"] = "models/workshop/player/items/engineer/hwn2020_pug_mug_style2/hwn2020_pug_mug_style2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = true,
		name = "The Endothermic Exowear",
		localized_name = "info.tf2pm.hat.short2014_endothermic_exowear",
		icon = "backpack/workshop/player/items/engineer/short2014_endothermic_exowear/short2014_endothermic_exowear",
		models = {
			["basename"] = "models/workshop/player/items/engineer/short2014_endothermic_exowear/short2014_endothermic_exowear.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = true,
		name = "Teddy Robobelt",
		localized_name = "info.tf2pm.hat.robo_engineer_teddy",
		localized_description = "info.tf2pm.hat.robo_engineer_teddy_desc",
		icon = "backpack/workshop/player/items/engineer/robo_engineer_teddy/robo_engineer_teddy",
		models = {
			["basename"] = "models/workshop/player/items/engineer/robo_engineer_teddy/robo_engineer_teddy.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = false,
		name = "tw_engineerbot_helmet",
		localized_name = "info.tf2pm.hat.tw_engineerbot_helmet",
		icon = "backpack/workshop/player/items/engineer/tw_engineerbot_helmet/tw_engineerbot_helmet",
		models = {
			["basename"] = "models/workshop/player/items/engineer/tw_engineerbot_helmet/tw_engineerbot_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"back"},
		paintable = true,
		name = "Winter Backup",
		localized_name = "info.tf2pm.hat.dec15_winter_backup",
		icon = "backpack/workshop/player/items/engineer/dec15_winter_backup/dec15_winter_backup",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec15_winter_backup/dec15_winter_backup.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_belt"},
		paintable = true,
		name = "Mini-Engy",
		localized_name = "info.tf2pm.hat.hwn2018_mini_engy",
		icon = "backpack/workshop/player/items/engineer/hwn2018_mini_engy/hwn2018_mini_engy",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2018_mini_engy/hwn2018_mini_engy.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = false,
		name = "tw_engineerbot_armor",
		localized_name = "info.tf2pm.hat.tw_engineerbot_armor",
		icon = "backpack/workshop/player/items/engineer/tw_engineerbot_armor/tw_engineerbot_armor",
		models = {
			["basename"] = "models/workshop/player/items/engineer/tw_engineerbot_armor/tw_engineerbot_armor.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = true,
		name = "The Dry Gulch Gulp",
		localized_name = "info.tf2pm.hat.jul13_thirst_quencher",
		localized_description = "info.tf2pm.hat.jul13_thirst_quencher_desc",
		icon = "backpack/workshop/player/items/engineer/jul13_thirst_quencher/jul13_thirst_quencher",
		models = {
			["basename"] = "models/workshop/player/items/engineer/jul13_thirst_quencher/jul13_thirst_quencher.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"feet"},
		paintable = true,
		name = "The Lonesome Loafers",
		localized_name = "info.tf2pm.hat.short2014_engineer_nerd_feet",
		icon = "backpack/workshop/player/items/engineer/short2014_engineer_nerd_feet/short2014_engineer_nerd_feet",
		models = {
			["basename"] = "models/workshop/player/items/engineer/short2014_engineer_nerd_feet/short2014_engineer_nerd_feet.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"belt_misc"},
		paintable = true,
		name = "The Trash Toter",
		localized_name = "info.tf2pm.hat.jul13_scrap_reserve",
		localized_description = "info.tf2pm.hat.jul13_scrap_reserve_desc",
		icon = "backpack/workshop/player/items/engineer/jul13_scrap_reserve/jul13_scrap_reserve",
		models = {
			["basename"] = "models/workshop/player/items/engineer/jul13_scrap_reserve/jul13_scrap_reserve.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = true,
		name = "El Caballero",
		localized_name = "info.tf2pm.hat.hwn2015_western_poncho",
		icon = "backpack/workshop/player/items/engineer/hwn2015_western_poncho/hwn2015_western_poncho",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2015_western_poncho/hwn2015_western_poncho.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = true,
		name = "The El Paso Poncho",
		localized_name = "info.tf2pm.hat.hwn2016_el_paso_poncho",
		icon = "backpack/workshop/player/items/engineer/hwn2016_el_paso_poncho/hwn2016_el_paso_poncho",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2016_el_paso_poncho/hwn2016_el_paso_poncho.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = true,
		name = "EOTL_Insulated_innovator",
		localized_name = "info.tf2pm.hat.eotl_insulated_innovator",
		icon = "backpack/workshop/player/items/engineer/insulated_innovator/insulated_innovator",
		models = {
			["basename"] = "models/workshop/player/items/engineer/insulated_innovator/insulated_innovator.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = false,
		name = "Life Support System",
		localized_name = "info.tf2pm.hat.invasion_life_support_system",
		icon = "backpack/workshop/player/items/engineer/invasion_life_support_system/invasion_life_support_system",
		models = {
			["basename"] = "models/workshop/player/items/engineer/invasion_life_support_system/invasion_life_support_system.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = false,
		name = "The Trencher's Tunic",
		localized_name = "info.tf2pm.hat.sbox2014_trenchers_tunic",
		icon = "backpack/workshop/player/items/engineer/sbox2014_trenchers_tunic/sbox2014_trenchers_tunic",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sbox2014_trenchers_tunic/sbox2014_trenchers_tunic.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"back"},
		paintable = true,
		name = "The Joe-on-the-Go",
		localized_name = "info.tf2pm.hat.short2014_poopyj_backpack",
		icon = "backpack/workshop/player/items/engineer/short2014_poopyj_backpack/short2014_poopyj_backpack",
		models = {
			["basename"] = "models/workshop/player/items/engineer/short2014_poopyj_backpack/short2014_poopyj_backpack.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"beard"},
		paintable = true,
		name = "The Gold Digger",
		localized_name = "info.tf2pm.hat.fall2013_the_gold_digger",
		localized_description = "info.tf2pm.hat.fall2013_the_gold_digger_desc",
		icon = "backpack/workshop/player/items/engineer/fall2013_the_gold_digger/fall2013_the_gold_digger",
		models = {
			["basename"] = "models/workshop/player/items/engineer/fall2013_the_gold_digger/fall2013_the_gold_digger.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"feet"},
		paintable = true,
		name = "EOTL_winter_pants",
		localized_name = "info.tf2pm.hat.eotl_winter_pants",
		icon = "backpack/workshop/player/items/engineer/eotl_winter_pants/eotl_winter_pants",
		models = {
			["basename"] = "models/workshop/player/items/engineer/eotl_winter_pants/eotl_winter_pants.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {},
		paintable = false,
		name = "The Tools of the Trade",
		localized_name = "info.tf2pm.hat.short2014_engie_toolbelt",
		icon = "backpack/workshop/player/items/engineer/short2014_engie_toolbelt/short2014_engie_toolbelt",
		models = {
			["basename"] = "models/workshop/player/items/engineer/short2014_engie_toolbelt/short2014_engie_toolbelt.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"head_skin"},
		paintable = true,
		name = "The Corpus Christi Cranium",
		localized_name = "info.tf2pm.hat.hwn2016_corpus_christi_cranium",
		icon = "backpack/workshop/player/items/engineer/hwn2016_corpus_christi_cranium/hwn2016_corpus_christi_cranium",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2016_corpus_christi_cranium/hwn2016_corpus_christi_cranium.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"glasses"},
		paintable = true,
		name = "Head Mounted Double Observatory",
		localized_name = "info.tf2pm.hat.spr17_double_observatory",
		localized_description = "info.tf2pm.hat.spr17_double_observatory_desc",
		icon = "backpack/workshop/player/items/engineer/spr17_double_observatory/spr17_double_observatory",
		models = {
			["basename"] = "models/workshop/player/items/engineer/spr17_double_observatory/spr17_double_observatory.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = false,
		name = "The Egghead's Overalls",
		localized_name = "info.tf2pm.hat.short2014_engineer_nerd_shirt",
		icon = "backpack/workshop/player/items/engineer/short2014_engineer_nerd_shirt/short2014_engineer_nerd_shirt",
		models = {
			["basename"] = "models/workshop/player/items/engineer/short2014_engineer_nerd_shirt/short2014_engineer_nerd_shirt.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"left_shoulder"},
		paintable = false,
		name = "Ein",
		localized_name = "info.tf2pm.hat.sbox2014_einstein",
		icon = "backpack/workshop/player/items/engineer/sbox2014_einstein/sbox2014_einstein",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sbox2014_einstein/sbox2014_einstein.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"beard"},
		paintable = false,
		name = "The Level Three Chin",
		localized_name = "info.tf2pm.hat.short2014_engineer_nerd_chin",
		icon = "backpack/workshop/player/items/engineer/short2014_engineer_nerd_chin/short2014_engineer_nerd_chin",
		models = {
			["basename"] = "models/workshop/player/items/engineer/short2014_engineer_nerd_chin/short2014_engineer_nerd_chin.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = false,
		name = "dec2014 Thermal Insulation Layer",
		localized_name = "info.tf2pm.hat.dec2014_thermal_insulation_layer",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/engineer/dec2014_thermal_insulation_layer/dec2014_thermal_insulation_layer",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec2014_thermal_insulation_layer/dec2014_thermal_insulation_layer.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"belt_misc"},
		paintable = true,
		name = "Final Frontier Freighter",
		localized_name = "info.tf2pm.hat.hwn2016_final_frontiersman",
		icon = "backpack/workshop/player/items/engineer/hwn2016_final_frontiersman/hwn2016_final_frontiersman",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2016_final_frontiersman/hwn2016_final_frontiersman.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = true,
		name = "The Dogfighter",
		localized_name = "info.tf2pm.hat.xms2013_dogfighter_jacket",
		icon = "backpack/workshop/player/items/engineer/xms2013_dogfighter_jacket/xms2013_dogfighter_jacket",
		models = {
			["basename"] = "models/workshop/player/items/engineer/xms2013_dogfighter_jacket/xms2013_dogfighter_jacket.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Provisions Cap",
		localized_name = "info.tf2pm.hat.dec19_provisions_cap",
		icon = "backpack/workshop/player/items/engineer/dec19_provisions_cap/dec19_provisions_cap",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec19_provisions_cap/dec19_provisions_cap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = false,
		name = "dec2014 engineer_seal",
		localized_name = "info.tf2pm.hat.dec2014_engineer_seal",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/engineer/dec2014_engineer_seal/dec2014_engineer_seal",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec2014_engineer_seal/dec2014_engineer_seal.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "The Wide-Brimmed Bandito",
		localized_name = "info.tf2pm.hat.hwn2016_wide_brimmed_bandito",
		icon = "backpack/workshop/player/items/engineer/hwn2016_wide_brimmed_bandito/hwn2016_wide_brimmed_bandito",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2016_wide_brimmed_bandito/hwn2016_wide_brimmed_bandito.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Flash of Inspiration",
		localized_name = "info.tf2pm.hat.spr17_flash_of_inspiration",
		localized_description = "info.tf2pm.hat.spr17_flash_of_inspiration_desc",
		icon = "backpack/workshop/player/items/engineer/spr17_flash_of_inspiration/spr17_flash_of_inspiration",
		models = {
			["basename"] = "models/workshop/player/items/engineer/spr17_flash_of_inspiration/spr17_flash_of_inspiration.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"back"},
		paintable = false,
		name = "A Shell of a Mann",
		localized_name = "info.tf2pm.hat.hwn2018_shell_of_a_mann",
		icon = "backpack/workshop/player/items/engineer/hwn2018_shell_of_a_mann/hwn2018_shell_of_a_mann",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2018_shell_of_a_mann/hwn2018_shell_of_a_mann.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Brain Interface",
		localized_name = "info.tf2pm.hat.sum19_brain_interface",
		icon = "backpack/workshop/player/items/engineer/sum19_brain_interface/sum19_brain_interface",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sum19_brain_interface/sum19_brain_interface.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"head_skin"},
		paintable = true,
		name = "Dell in the Shell",
		localized_name = "info.tf2pm.hat.hwn2018_dell_in_the_shell",
		icon = "backpack/workshop/player/items/engineer/hwn2018_dell_in_the_shell/hwn2018_dell_in_the_shell",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2018_dell_in_the_shell/hwn2018_dell_in_the_shell.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = true,
		name = "Dad Duds",
		localized_name = "info.tf2pm.hat.may16_dad_duds",
		icon = "backpack/workshop/player/items/engineer/all_work_and_no_plaid/all_work_and_no_plaid",
		models = {
			["basename"] = "models/workshop/player/items/engineer/all_work_and_no_plaid/all_work_and_no_plaid.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"engineer_pocket"},
		paintable = true,
		name = "The Pocket Pyro",
		localized_name = "info.tf2pm.hat.pocket_protector",
		localized_description = "info.tf2pm.hat.pocket_protector_desc",
		icon = "backpack/workshop/player/items/engineer/pocket_protector/pocket_protector",
		models = {
			["basename"] = "models/workshop/player/items/engineer/pocket_protector/pocket_protector.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = false,
		name = "The Antarctic Researcher",
		localized_name = "info.tf2pm.hat.sbox2014_antarctic_researcher",
		icon = "backpack/workshop/player/items/engineer/sbox2014_antarctic_researcher/sbox2014_antarctic_researcher",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sbox2014_antarctic_researcher/sbox2014_antarctic_researcher.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"shirt"},
		paintable = true,
		name = "Wild West Waistcoat",
		localized_name = "info.tf2pm.hat.cc_summer2015_wild_west_waistcoat",
		icon = "backpack/workshop/player/items/engineer/cc_summer2015_wild_west_waistcoat/cc_summer2015_wild_west_waistcoat",
		models = {
			["basename"] = "models/workshop/player/items/engineer/cc_summer2015_wild_west_waistcoat/cc_summer2015_wild_west_waistcoat.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"beard"},
		paintable = true,
		name = "Garden Bristles",
		localized_name = "info.tf2pm.hat.sf14_hw2014_engi_gnome_beard",
		icon = "backpack/workshop/player/items/engineer/sf14_hw2014_engi_gnome_beard/sf14_hw2014_engi_gnome_beard",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sf14_hw2014_engi_gnome_beard/sf14_hw2014_engi_gnome_beard.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Soul of 'Spenser's Past",
		localized_name = "info.tf2pm.hat.sf14__soul_of_spensers_past",
		icon = "backpack/workshop/player/items/engineer/sf14__soul_of_spensers_past/sf14__soul_of_spensers_past",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sf14__soul_of_spensers_past/sf14__soul_of_spensers_past.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"feet"},
		paintable = true,
		name = "Roboot",
		localized_name = "info.tf2pm.hat.hwn2015_roboot",
		icon = "backpack/workshop/player/items/engineer/hwn2015_roboot/hwn2015_roboot",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2015_roboot/hwn2015_roboot.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "The Trencher's Topper",
		localized_name = "info.tf2pm.hat.sbox2014_trenchers_topper",
		icon = "backpack/workshop/player/items/engineer/sbox2014_trenchers_topper/sbox2014_trenchers_topper",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sbox2014_trenchers_topper/sbox2014_trenchers_topper.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sbox2014_trenchers_topper_style1",
				models = {
					["basename"] = "models/workshop/player/items/engineer/sbox2014_trenchers_topper/sbox2014_trenchers_topper.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sbox2014_trenchers_topper_style2",
				models = {
					["basename"] = "models/workshop/player/items/engineer/sbox2014_trenchers_topper_s2/sbox2014_trenchers_topper_s2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"beard"},
		paintable = true,
		name = "Wise Whiskers",
		localized_name = "info.tf2pm.hat.dec18_wise_whiskers",
		icon = "backpack/workshop/player/items/engineer/dec18_wise_whiskers/dec18_wise_whiskers",
		models = {
			["basename"] = "models/workshop/player/items/engineer/dec18_wise_whiskers/dec18_wise_whiskers.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.dec18_wise_whiskers_hat",
			},
			[1] = {
				localized_name = "info.tf2pm.hat.dec18_wise_whiskers_nohat",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
		},
	},
	{
		classes = {"engineer"},
		targets = {"back"},
		paintable = true,
		name = "The Cold Case",
		localized_name = "info.tf2pm.hat.spr18_cold_case",
		icon = "backpack/workshop/player/items/engineer/spr18_cold_case/spr18_cold_case",
		models = {
			["basename"] = "models/workshop/player/items/engineer/spr18_cold_case/spr18_cold_case.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"beard"},
		paintable = true,
		name = "Face Full of Festive",
		localized_name = "info.tf2pm.hat.xms2013_festive_beard",
		icon = "backpack/workshop/player/items/engineer/xms2013_festive_beard/xms2013_festive_beard",
		models = {
			["basename"] = "models/workshop/player/items/engineer/xms2013_festive_beard/xms2013_festive_beard.mdl",
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "The Peacenik's Ponytail",
		localized_name = "info.tf2pm.hat.short2014_engineer_nerd_hair",
		icon = "backpack/workshop/player/items/engineer/short2014_engineer_nerd_hair/short2014_engineer_nerd_hair",
		models = {
			["basename"] = "models/workshop/player/items/engineer/short2014_engineer_nerd_hair/short2014_engineer_nerd_hair.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.short2014_engineer_nerd_hair_style0",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.short2014_engineer_nerd_hair_style1",
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Head Of Defense",
		localized_name = "info.tf2pm.hat.sum20_head_of_defense",
		icon = "backpack/workshop/player/items/engineer/sum20_head_of_defense_style1/sum20_head_of_defense_style1",
		models = {
			["basename"] = "models/workshop/player/items/engineer/sum20_head_of_defense_style1/sum20_head_of_defense_style1.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum20_head_of_defense_style1",
				models = {
					["basename"] = "models/workshop/player/items/engineer/sum20_head_of_defense_style1/sum20_head_of_defense_style1.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum20_head_of_defense_style2",
				models = {
					["basename"] = "models/workshop/player/items/engineer/sum20_head_of_defense_style2/sum20_head_of_defense_style2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer"},
		targets = {"head_skin"},
		paintable = false,
		name = "Dead'er Alive",
		localized_name = "info.tf2pm.hat.hwn2015_mechanical_engineer",
		icon = "backpack/workshop/player/items/engineer/hwn2015_mechanical_engineer/hwn2015_mechanical_engineer",
		models = {
			["basename"] = "models/workshop/player/items/engineer/hwn2015_mechanical_engineer/hwn2015_mechanical_engineer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},

}
