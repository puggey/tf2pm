
-- Auto generated at 2020-12-04 12:56:46 UTC+07:00
return {
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Demoman's Fro",
		localized_name = "info.tf2pm.hat.demo_hat_1",
		icon = "backpack/player/items/demo/demo_afro",
		models = {
			["basename"] = "models/player/items/demo/demo_afro.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Glengarry Bonnet",
		localized_name = "info.tf2pm.hat.demo_scott_hat",
		icon = "backpack/player/items/demo/demo_scott",
		models = {
			["basename"] = "models/player/items/demo/demo_scott.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Scotsman's Stove Pipe",
		localized_name = "info.tf2pm.hat.demo_top_hat",
		icon = "backpack/player/items/demo/top_hat",
		models = {
			["basename"] = "models/player/items/demo/top_hat.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.stovepipe_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.stovepipe_style1",
			},
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Demoman Hallmark",
		localized_name = "info.tf2pm.hat.demomanhallmark",
		icon = "backpack/player/items/demo/hallmark",
		models = {
			["basename"] = "models/player/items/demo/hallmark.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Demoman Tricorne",
		localized_name = "info.tf2pm.hat.demomantricorne",
		localized_description = "info.tf2pm.hat.demomantricorne_desc",
		icon = "backpack/player/items/demo/demo_tricorne",
		models = {
			["basename"] = "models/player/items/demo/demo_tricorne.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Rimmed Raincatcher",
		localized_name = "info.tf2pm.hat.demomanpirate",
		localized_description = "info.tf2pm.hat.demomanpirate_desc",
		icon = "backpack/player/items/demo/drinking_hat",
		models = {
			["basename"] = "models/player/items/demo/drinking_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Sober Stuntman",
		localized_name = "info.tf2pm.hat.demostunthelmet",
		localized_description = "info.tf2pm.hat.demostunthelmet_desc",
		icon = "backpack/player/items/demo/stunt_helmet",
		models = {
			["basename"] = "models/player/items/demo/stunt_helmet.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Carouser's Capotain",
		localized_name = "info.tf2pm.hat.demoinquisitor",
		localized_description = "info.tf2pm.hat.demoinquisitor_desc",
		icon = "backpack/workshop/player/items/demo/inquisitor/inquisitor",
		models = {
			["basename"] = "models/workshop/player/items/demo/inquisitor/inquisitor.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"glasses"},
		paintable = true,
		name = "TTG Glasses",
		localized_name = "info.tf2pm.hat.ttg_glasses",
		localized_description = "info.tf2pm.hat.ttg_glasses_desc",
		icon = "backpack/player/items/demo/ttg_glasses",
		models = {
			["basename"] = "models/player/items/demo/ttg_glasses.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "Scotch Bonnet",
		localized_name = "info.tf2pm.hat.scotchbonnet",
		localized_description = "info.tf2pm.hat.scotchbonnet_desc",
		icon = "backpack/player/items/demo/demo_scotchbonnet",
		models = {
			["basename"] = "models/player/items/demo/demo_scotchbonnet.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Prince Tavish's Crown",
		localized_name = "info.tf2pm.hat.tavishcrown",
		icon = "backpack/player/items/demo/crown",
		models = {
			["basename"] = "models/player/items/demo/crown.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "Demo Kabuto",
		localized_name = "info.tf2pm.hat.demokabuto",
		localized_description = "info.tf2pm.hat.demokabuto_desc",
		icon = "backpack/workshop_partner/player/items/demo/demo_shogun_kabuto/demo_shogun_kabuto",
		models = {
			["basename"] = "models/workshop_partner/player/items/demo/demo_shogun_kabuto/demo_shogun_kabuto.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Reggaelator",
		localized_name = "info.tf2pm.hat.reggaelator",
		localized_description = "info.tf2pm.hat.reggaelator_desc",
		icon = "backpack/player/items/demo/demo_dreads",
		models = {
			["basename"] = "models/player/items/demo/demo_dreads.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.reggaelator_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.reggaelator_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.reggaelator_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.reggaelator_style3",
			},
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "Sultan's Ceremonial",
		localized_name = "info.tf2pm.hat.sultansceremonial",
		icon = "backpack/player/items/demo/demo_sultan_hat",
		models = {
			["basename"] = "models/player/items/demo/demo_sultan_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"feet"},
		paintable = false,
		name = "Ali Baba's Wee Booties",
		localized_name = "info.tf2pm.hat.ali_babas_wee_booties",
		icon = "backpack/workshop/player/items/demo/demo_booties/demo_booties",
		models = {
			["basename"] = "models/workshop/player/items/demo/demo_booties/demo_booties.mdl",
		},
		bodygroup_overrides = {
			["shoes"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Conjurer's Cowl",
		localized_name = "info.tf2pm.hat.conjurerscowl",
		localized_description = "info.tf2pm.hat.conjurerscowl_desc",
		icon = "backpack/player/items/demo/demo_hood",
		models = {
			["basename"] = "models/player/items/demo/demo_hood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Tam O'Shanter",
		localized_name = "info.tf2pm.hat.tamoshanter",
		icon = "backpack/player/items/demo/tamoshanter",
		models = {
			["basename"] = "models/player/items/demo/tamoshanter.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"whole_head"},
		paintable = true,
		name = "Mask of the Shaman",
		localized_name = "info.tf2pm.hat.maskoftheshaman",
		localized_description = "info.tf2pm.hat.maskoftheshaman_desc",
		icon = "backpack/player/items/demo/dust_mask",
		models = {
			["basename"] = "models/player/items/demo/dust_mask.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "Hair of the Dog",
		localized_name = "info.tf2pm.hat.hwn_demohat",
		icon = "backpack/player/items/demo/hwn_demo_hat",
		models = {
			["basename"] = "models/player/items/demo/hwn_demo_hat.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"face"},
		paintable = false,
		name = "Scottish Snarl",
		localized_name = "info.tf2pm.hat.hwn_demomisc1",
		icon = "backpack/player/items/demo/hwn_demo_misc1",
		models = {
			["basename"] = "models/player/items/demo/hwn_demo_misc1.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"feet"},
		paintable = false,
		name = "Pickled Paws",
		localized_name = "info.tf2pm.hat.hwn_demomisc2",
		icon = "backpack/player/items/demo/hwn_demo_misc2",
		models = {
			["basename"] = "models/player/items/demo/hwn_demo_misc2.mdl",
		},
		bodygroup_overrides = {
			["shoes"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Tavish DeGroot Experience",
		localized_name = "info.tf2pm.hat.demohat1",
		localized_description = "info.tf2pm.hat.demohat1_desc",
		icon = "backpack/player/items/demo/fwk_demo_sashhat",
		models = {
			["basename"] = "models/player/items/demo/fwk_demo_sashhat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Buccaneer's Bicorne",
		localized_name = "info.tf2pm.hat.buccaneersbicorne",
		localized_description = "info.tf2pm.hat.buccaneersbicorne_desc",
		icon = "backpack/player/items/demo/mightypirate",
		models = {
			["basename"] = "models/player/items/demo/mighty_pirate.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"feet"},
		paintable = false,
		name = "The Bootlegger",
		localized_name = "info.tf2pm.hat.bootlegger",
		localized_description = "info.tf2pm.hat.bootlegger_desc",
		icon = "backpack/workshop/player/items/demo/pegleg/pegleg",
		models = {
			["basename"] = "models/workshop/player/items/demo/pegleg/pegleg.mdl",
		},
		bodygroup_overrides = {
			["shoes"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"grenades"},
		paintable = false,
		name = "A Whiff of the Old Brimstone",
		localized_name = "info.tf2pm.hat.oldbrimstone",
		localized_description = "info.tf2pm.hat.oldbrimstone_desc",
		icon = "backpack/player/items/demo/bombs",
		models = {
			["basename"] = "models/player/items/demo/demo_bombs.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"face", "hat"},
		paintable = true,
		name = "The Bolgan",
		localized_name = "info.tf2pm.hat.bolganhelmet",
		localized_description = "info.tf2pm.hat.bolganhelmet_desc",
		icon = "backpack/player/items/all_class/all_reckoning_bolgan_demo",
		models = {
			["basename"] = "models/player/items/all_class/all_reckoning_bolgan_demo.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"demo_belt"},
		paintable = false,
		name = "Aladdin's Private Reserve",
		localized_name = "info.tf2pm.hat.demolamp",
		localized_description = "info.tf2pm.hat.demolamp_desc",
		icon = "backpack/player/items/demo/djinn_lamp",
		models = {
			["basename"] = "models/player/items/demo/djinn_lamp.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"glasses"},
		paintable = false,
		name = "The Snapped Pupil",
		localized_name = "info.tf2pm.hat.demosnappedpupil",
		localized_description = "info.tf2pm.hat.demosnappedpupil_desc",
		icon = "backpack/player/items/demo/eyePhoto",
		models = {
			["basename"] = "models/player/items/demo/eyePhoto.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"back"},
		paintable = true,
		name = "The Liquor Locker",
		localized_name = "info.tf2pm.hat.demotreasurechest",
		localized_description = "info.tf2pm.hat.demotreasurechest_desc",
		icon = "backpack/player/items/demo/demo_chest_back",
		models = {
			["basename"] = "models/player/items/demo/demo_chest_back.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.demotreasurechest_style0",
				models = {
					["basename"] = "models/player/items/demo/demo_chest_back.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.demotreasurechest_style1",
				models = {
					["basename"] = "models/player/items/demo/demo_chest_front.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.demotreasurechest_style2",
				models = {
					["basename"] = "models/player/items/demo/demo_chest_r.mdl",
				}
			},
			[3] = {
				localized_name = "info.tf2pm.hat.demotreasurechest_style3",
				models = {
					["basename"] = "models/player/items/demo/demo_chest_l.mdl",
				}
			},
		},
	},
	{
		classes = {"demo"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "The Bird-Man of Aberdeen",
		localized_name = "info.tf2pm.hat.demoparrot",
		localized_description = "info.tf2pm.hat.demoparrot_desc",
		icon = "backpack/player/items/demo/demo_parrot",
		models = {
			["basename"] = "models/player/items/demo/demo_parrot.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.demoparrot_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.demoparrot_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.demoparrot_style2",
			},
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "The Grenadier Helm",
		localized_name = "info.tf2pm.hat.ha_demo",
		localized_description = "info.tf2pm.hat.ha_demo_desc",
		icon = "backpack/workshop_partner/player/items/demo/hero_academy_demo/hero_academy_demo",
		models = {
			["basename"] = "models/workshop_partner/player/items/demo/hero_academy_demo/hero_academy_demo.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"beard"},
		paintable = true,
		name = "The Bearded Bombardier",
		localized_name = "info.tf2pm.hat.demobeardpipe",
		localized_description = "info.tf2pm.hat.demobeardpipe_desc",
		icon = "backpack/workshop/player/items/demo/demo_beardpipe/demo_beardpipe",
		models = {
			["basename"] = "models/workshop/player/items/demo/demo_beardpipe/demo_beardpipe.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.demobeardpipe_style0",
				models = {
					["basename"] = "models/workshop/player/items/demo/demo_beardpipe/demo_beardpipe.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.demobeardpipe_style1",
				models = {
					["basename"] = "models/workshop/player/items/demo/demo_beardpipe_s2/demo_beardpipe_s2.mdl",
				}
			},
		},
	},
	{
		classes = {"demo"},
		targets = {"grenades"},
		paintable = false,
		name = "The Battery Bandolier",
		localized_name = "info.tf2pm.hat.demo_robot_grenades",
		localized_description = "info.tf2pm.hat.demo_robot_grenades_desc",
		icon = "backpack/player/items/mvm_loot/demo/battery_grenade",
		models = {
			["basename"] = "models/player/items/mvm_loot/demo/battery_grenade.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"back"},
		paintable = false,
		name = "The King of Scotland Cape",
		localized_name = "info.tf2pm.hat.tw_cape",
		localized_description = "info.tf2pm.hat.tw_cape_desc",
		icon = "backpack/workshop_partner/player/items/demo/tw_kingcape/tw_kingcape",
		models = {
			["basename"] = "models/workshop_partner/player/items/demo/tw_kingcape/tw_kingcape.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Voodoo Juju (Slight Return)",
		localized_name = "info.tf2pm.hat.voodoojuju",
		localized_description = "info.tf2pm.hat.voodoojuju_desc",
		icon = "backpack/player/items/demo/demo_bonehat",
		models = {
			["basename"] = "models/player/items/demo/demo_bonehat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"pants"},
		paintable = false,
		name = "The Cool Breeze",
		localized_name = "info.tf2pm.hat.thecoolbreeze",
		localized_description = "info.tf2pm.hat.thecoolbreeze_desc",
		icon = "backpack/workshop/player/items/demo/demo_kilt/demo_kilt",
		models = {
			["basename"] = "models/workshop/player/items/demo/demo_kilt/demo_kilt.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"demo_eyepatch"},
		paintable = true,
		name = "Blind Justice",
		localized_name = "info.tf2pm.hat.blindjustice",
		localized_description = "info.tf2pm.hat.blindjustice_desc",
		icon = "backpack/player/items/demo/bio_demo_patch",
		models = {
			["basename"] = "models/player/items/demo/bio_demo_patch.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "MvM GateBot Light Demoman",
		icon = "backpack",
		models = {
			["basename"] = "models/bots/gameplay_cosmetic/light_demo_on.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_demo_on.mdl",
				}
			},
			[1] = {
				responsive_skins = {1, 1},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_demo_off.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "Forgotten King's Restless Head",
		localized_name = "info.tf2pm.hat.sf14_deadking_head",
		icon = "backpack/workshop/player/items/demo/sf14_deadking_head/sf14_deadking_head",
		models = {
			["basename"] = "models/workshop/player/items/demo/sf14_deadking_head/sf14_deadking_head.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Dayjogger",
		localized_name = "info.tf2pm.hat.may16_dayjogger",
		icon = "backpack/workshop/player/items/demo/vampire_shades/vampire_shades",
		models = {
			["basename"] = "models/workshop/player/items/demo/vampire_shades/vampire_shades.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"demoman_collar"},
		paintable = false,
		name = "The Lordly Lapels",
		localized_name = "info.tf2pm.hat.hw2013_the_ghoulic_extension",
		icon = "backpack/workshop/player/items/demo/hw2013_the_ghoulic_extension/hw2013_the_ghoulic_extension",
		models = {
			["basename"] = "models/workshop/player/items/demo/hw2013_the_ghoulic_extension/hw2013_the_ghoulic_extension.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Helm Helm",
		localized_name = "info.tf2pm.hat.dec20_helm_helm",
		icon = "backpack/workshop/player/items/demo/dec20_helm_helm/dec20_helm_helm",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec20_helm_helm/dec20_helm_helm.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "Explosive Mind",
		localized_name = "info.tf2pm.hat.sf14_explosive_mind",
		icon = "backpack/workshop/player/items/demo/sf14_explosive_mind/sf14_explosive_mind",
		models = {
			["basename"] = "models/workshop/player/items/demo/sf14_explosive_mind/sf14_explosive_mind.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Pirate Bandana",
		localized_name = "info.tf2pm.hat.fall2013_pirate_bandana",
		icon = "backpack/workshop/player/items/demo/fall2013_pirate_bandana/fall2013_pirate_bandana",
		models = {
			["basename"] = "models/workshop/player/items/demo/fall2013_pirate_bandana/fall2013_pirate_bandana.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"shirt"},
		paintable = true,
		name = "The Hurt Locher",
		localized_name = "info.tf2pm.hat.fall2013_eod_suit",
		localized_description = "info.tf2pm.hat.fall2013_eod_suit_desc",
		icon = "backpack/workshop/player/items/demo/fall2013_eod_suit/fall2013_eod_suit",
		models = {
			["basename"] = "models/workshop/player/items/demo/fall2013_eod_suit/fall2013_eod_suit.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"back"},
		paintable = true,
		name = "The Scrumpy Strongbox",
		localized_name = "info.tf2pm.hat.robo_demo_chest",
		localized_description = "info.tf2pm.hat.robo_demo_chest_desc",
		icon = "backpack/workshop/player/items/demo/robo_demo_chest/robo_demo_chest",
		models = {
			["basename"] = "models/workshop/player/items/demo/robo_demo_chest/robo_demo_chest.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"demoman_collar"},
		paintable = false,
		name = "Forgotten King's Pauldrons",
		localized_name = "info.tf2pm.hat.sf14_deadking_pauldrons",
		icon = "backpack/workshop/player/items/demo/sf14_deadking_pauldrons/sf14_deadking_pauldrons",
		models = {
			["basename"] = "models/workshop/player/items/demo/sf14_deadking_pauldrons/sf14_deadking_pauldrons.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"back"},
		paintable = false,
		name = "The Horsemann's Hand-Me-Down",
		localized_name = "info.tf2pm.hat.hw2013_demo_cape",
		icon = "backpack/workshop/player/items/demo/hw2013_demo_cape/hw2013_demo_cape",
		models = {
			["basename"] = "models/workshop/player/items/demo/hw2013_demo_cape/hw2013_demo_cape.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Elf Esteem",
		localized_name = "info.tf2pm.hat.dec16_elf_esteem",
		localized_description = "info.tf2pm.hat.dec16_elf_esteem_desc",
		icon = "backpack/workshop/player/items/demo/dec16_elf_esteem/dec16_elf_esteem",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec16_elf_esteem/dec16_elf_esteem.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "The Transylvania Top",
		localized_name = "info.tf2pm.hat.hw2013_demon_fro",
		icon = "backpack/workshop/player/items/demo/hw2013_demon_fro/hw2013_demon_fro",
		models = {
			["basename"] = "models/workshop/player/items/demo/hw2013_demon_fro/hw2013_demon_fro.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Spiky Viking",
		localized_name = "info.tf2pm.hat.dec19_spiky_viking",
		icon = "backpack/workshop/player/items/demo/dec19_spiky_viking/dec19_spiky_viking",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec19_spiky_viking/dec19_spiky_viking.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec19_spiky_viking_style0",
				models = {
					["basename"] = "models/workshop/player/items/demo/dec19_spiky_viking/dec19_spiky_viking.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec19_spiky_viking_style1",
				models = {
					["basename"] = "models/workshop/player/items/demo/dec19_spiky_viking_s2/dec19_spiky_viking_s2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"back"},
		paintable = true,
		name = "The Gaelic Golf Bag",
		localized_name = "info.tf2pm.hat.jul13_scotsmans_golfbag",
		localized_description = "info.tf2pm.hat.jul13_scotsmans_golfbag_desc",
		icon = "backpack/workshop/player/items/demo/jul13_scotsmans_golfbag/jul13_scotsmans_golfbag",
		models = {
			["basename"] = "models/workshop/player/items/demo/jul13_scotsmans_golfbag/jul13_scotsmans_golfbag.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"back"},
		paintable = true,
		name = "Melody Of Misery",
		localized_name = "info.tf2pm.hat.dec17_melody_of_misery",
		icon = "backpack/workshop/player/items/demo/dec17_melody_of_misery/dec17_melody_of_misery",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec17_melody_of_misery/dec17_melody_of_misery.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"grenades"},
		paintable = true,
		name = "Backbreaker's Guards",
		localized_name = "info.tf2pm.hat.sum19_backbreakers_guards",
		icon = "backpack/workshop/player/items/demo/sum19_backbreakers_guards/sum19_backbreakers_guards",
		models = {
			["basename"] = "models/workshop/player/items/demo/sum19_backbreakers_guards/sum19_backbreakers_guards.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Black Watch",
		localized_name = "info.tf2pm.hat.jul13_blam_o_shanter",
		localized_description = "info.tf2pm.hat.jul13_blam_o_shanter_desc",
		icon = "backpack/workshop/player/items/demo/jul13_blam_o_shanter/jul13_blam_o_shanter",
		models = {
			["basename"] = "models/workshop/player/items/demo/jul13_blam_o_shanter/jul13_blam_o_shanter.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Backbreaker's Skullcracker",
		localized_name = "info.tf2pm.hat.sum19_backbreakers_skullcracker",
		icon = "backpack/workshop/player/items/demo/sum19_backbreakers_skullcracker/sum19_backbreakers_skullcracker",
		models = {
			["basename"] = "models/workshop/player/items/demo/sum19_backbreakers_skullcracker/sum19_backbreakers_skullcracker.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Stormin' Norman",
		localized_name = "info.tf2pm.hat.jul13_stormn_normn",
		localized_description = "info.tf2pm.hat.jul13_stormn_normn_desc",
		icon = "backpack/workshop/player/items/demo/jul13_stormn_normn/jul13_stormn_normn",
		models = {
			["basename"] = "models/workshop/player/items/demo/jul13_stormn_normn/jul13_stormn_normn.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"shirt"},
		paintable = false,
		name = "Dynamite Abs",
		localized_name = "info.tf2pm.hat.sum19_dynamite_abs",
		icon = "backpack/workshop/player/items/demo/sum19_dynamite_abs/sum19_dynamite_abs",
		models = {
			["basename"] = "models/workshop/player/items/demo/sum19_dynamite_abs/sum19_dynamite_abs.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum19_dynamite_abs_style1",
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum19_dynamite_abs_style2",
				bodygroup_overrides = {
					["grenades"] = 1,
				},
			},
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Bolted Bicorne",
		localized_name = "info.tf2pm.hat.robo_demo_buccaneer_bicorne",
		localized_description = "info.tf2pm.hat.robo_demo_buccaneer_bicorne_desc",
		icon = "backpack/workshop/player/items/demo/robo_demo_buccaneer_bicorne/robo_demo_buccaneer_bicorne",
		models = {
			["basename"] = "models/workshop/player/items/demo/robo_demo_buccaneer_bicorne/robo_demo_buccaneer_bicorne.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Headtaker's Hood",
		localized_name = "info.tf2pm.hat.hw2013_demo_executioner_hood",
		icon = "backpack/workshop/player/items/demo/hw2013_demo_executioner_hood/hw2013_demo_executioner_hood",
		models = {
			["basename"] = "models/workshop/player/items/demo/hw2013_demo_executioner_hood/hw2013_demo_executioner_hood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "The Juggernaut Jacket",
		localized_name = "info.tf2pm.hat.sbox2014_juggernaut_jacket",
		icon = "backpack/workshop/player/items/demo/sbox2014_juggernaut_jacket/sbox2014_juggernaut_jacket",
		models = {
			["basename"] = "models/workshop/player/items/demo/sbox2014_juggernaut_jacket/sbox2014_juggernaut_jacket.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Bruce's Bonnet",
		localized_name = "info.tf2pm.hat.cc_summer2015_bruces_bonnet",
		icon = "backpack/workshop/player/items/demo/cc_summer2015_bruces_bonnet/cc_summer2015_bruces_bonnet",
		models = {
			["basename"] = "models/workshop/player/items/demo/cc_summer2015_bruces_bonnet/cc_summer2015_bruces_bonnet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"shirt"},
		paintable = false,
		name = "tw_sentrybuster",
		localized_name = "info.tf2pm.hat.tw_sentrybuster",
		icon = "backpack/workshop/player/items/demo/tw_sentrybuster/tw_sentrybuster",
		models = {
			["basename"] = "models/workshop/player/items/demo/tw_sentrybuster/tw_sentrybuster.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Hungover Hero",
		localized_name = "info.tf2pm.hat.dec17_hungover_hero",
		icon = "backpack/workshop/player/items/demo/dec17_hungover_hero/dec17_hungover_hero",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec17_hungover_hero/dec17_hungover_hero.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Bomber Knight",
		localized_name = "info.tf2pm.hat.dec16_bomber_knight",
		localized_description = "info.tf2pm.hat.dec16_bomber_knight_desc",
		icon = "backpack/workshop/player/items/demo/dec16_bomber_knight/dec16_bomber_knight",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec16_bomber_knight/dec16_bomber_knight.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Polly Putrid",
		localized_name = "info.tf2pm.hat.hw2013_zombie_parrot",
		icon = "backpack/workshop/player/items/demo/hw2013_zombie_parrot/hw2013_zombie_parrot",
		models = {
			["basename"] = "models/workshop/player/items/demo/hw2013_zombie_parrot/hw2013_zombie_parrot.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"face"},
		paintable = true,
		name = "The Parasight",
		localized_name = "info.tf2pm.hat.hw2013_the_parasight",
		icon = "backpack/workshop/player/items/demo/hw2013_the_parasight/hw2013_the_parasight",
		models = {
			["basename"] = "models/workshop/player/items/demo/hw2013_the_parasight/hw2013_the_parasight.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"pants"},
		paintable = true,
		name = "EOTL_demopants",
		localized_name = "info.tf2pm.hat.eotl_demopants",
		icon = "backpack/workshop/player/items/demo/eotl_demopants/eotl_demopants",
		models = {
			["basename"] = "models/workshop/player/items/demo/eotl_demopants/eotl_demopants.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"grenades"},
		paintable = false,
		name = "Six Pack Abs",
		localized_name = "info.tf2pm.hat.demomanbeergrenades",
		icon = "backpack/player/items/demo/demo_fiesta_bottles",
		models = {
			["basename"] = "models/player/items/demo/demo_fiesta_bottles.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"beard"},
		paintable = false,
		name = "Gaelic Glutton",
		localized_name = "info.tf2pm.hat.hwn2018_gaelic_glutton",
		icon = "backpack/workshop/player/items/demo/hwn2018_gaelic_glutton/hwn2018_gaelic_glutton",
		models = {
			["basename"] = "models/workshop/player/items/demo/hwn2018_gaelic_glutton/hwn2018_gaelic_glutton.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "The Allbrero",
		localized_name = "info.tf2pm.hat.demosombrero",
		icon = "backpack/player/items/demo/demo_fiesta_sombrero",
		models = {
			["basename"] = "models/player/items/demo/demo_fiesta_sombrero.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Bomb Beanie",
		localized_name = "info.tf2pm.hat.dec16_bomb_beanie",
		localized_description = "info.tf2pm.hat.dec16_bomb_beanie_desc",
		icon = "backpack/workshop/player/items/demo/dec16_bomb_beanie/dec16_bomb_beanie",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec16_bomb_beanie/dec16_bomb_beanie.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Squid's Lid",
		localized_name = "info.tf2pm.hat.hw2013_blackguards_bicorn",
		icon = "backpack/workshop/player/items/demo/hw2013_blackguards_bicorn/hw2013_blackguards_bicorn",
		models = {
			["basename"] = "models/workshop/player/items/demo/hw2013_blackguards_bicorn/hw2013_blackguards_bicorn.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Broadband Bonnet",
		localized_name = "info.tf2pm.hat.robo_demo_glengarry_botnet",
		localized_description = "info.tf2pm.hat.robo_demo_glengarry_botnet_desc",
		icon = "backpack/workshop/player/items/demo/robo_demo_glengarry_botnet/robo_demo_glengarry_botnet",
		models = {
			["basename"] = "models/workshop/player/items/demo/robo_demo_glengarry_botnet/robo_demo_glengarry_botnet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "EOTL_summerhat",
		localized_name = "info.tf2pm.hat.eotl_summerhat",
		icon = "backpack/workshop/player/items/demo/eotl_summerhat/eotl_summerhat",
		models = {
			["basename"] = "models/workshop/player/items/demo/eotl_summerhat/eotl_summerhat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Mann-Bird of Aberdeen",
		localized_name = "info.tf2pm.hat.hw2013_manbird_of_aberdeen",
		icon = "backpack/workshop/player/items/demo/hw2013_manbird_of_aberdeen/hw2013_manbird_of_aberdeen",
		models = {
			["basename"] = "models/workshop/player/items/demo/hw2013_manbird_of_aberdeen/hw2013_manbird_of_aberdeen.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"shirt"},
		paintable = false,
		name = "Bushi-Dou",
		localized_name = "info.tf2pm.hat.sbox2014_demo_samurai_armour",
		icon = "backpack/workshop/player/items/demo/sbox2014_demo_samurai_armour/sbox2014_demo_samurai_armour",
		models = {
			["basename"] = "models/workshop/player/items/demo/sbox2014_demo_samurai_armour/sbox2014_demo_samurai_armour.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Frontier Djustice",
		localized_name = "info.tf2pm.hat.short2014_badlands_wanderer",
		icon = "backpack/workshop/player/items/demo/short2014_badlands_wanderer/short2014_badlands_wanderer",
		models = {
			["basename"] = "models/workshop/player/items/demo/short2014_badlands_wanderer/short2014_badlands_wanderer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "The Glasgow Great Helm",
		localized_name = "info.tf2pm.hat.jul13_pillagers_barrel",
		localized_description = "info.tf2pm.hat.jul13_pillagers_barrel_desc",
		icon = "backpack/workshop/player/items/demo/jul13_pillagers_barrel/jul13_pillagers_barrel",
		models = {
			["basename"] = "models/workshop/player/items/demo/jul13_pillagers_barrel/jul13_pillagers_barrel.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"beard"},
		paintable = true,
		name = "The Bolted Bombardier",
		localized_name = "info.tf2pm.hat.robo_demo_beard_bombardier",
		localized_description = "info.tf2pm.hat.robo_demo_beard_bombardier_desc",
		icon = "backpack/workshop/player/items/demo/robo_demo_beard_bombardier/robo_demo_beard_bombardier",
		models = {
			["basename"] = "models/workshop/player/items/demo/robo_demo_beard_bombardier/robo_demo_beard_bombardier.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Blast Defense",
		localized_name = "info.tf2pm.hat.spr17_blast_defense",
		localized_description = "info.tf2pm.hat.spr17_blast_defense_desc",
		icon = "backpack/workshop/player/items/demo/spr17_blast_defense/spr17_blast_defense",
		models = {
			["basename"] = "models/workshop/player/items/demo/spr17_blast_defense/spr17_blast_defense.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"glasses"},
		paintable = false,
		name = "The HDMI Patch",
		localized_name = "info.tf2pm.hat.robo_demo_pupil",
		localized_description = "info.tf2pm.hat.robo_demo_pupil_desc",
		icon = "backpack/workshop/player/items/demo/robo_demo_pupil/robo_demo_pupil",
		models = {
			["basename"] = "models/workshop/player/items/demo/robo_demo_pupil/robo_demo_pupil.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Stylish DeGroot",
		localized_name = "info.tf2pm.hat.sbox2014_stylish_degroot",
		icon = "backpack/workshop/player/items/demo/sbox2014_stylish_degroot/sbox2014_stylish_degroot",
		models = {
			["basename"] = "models/workshop/player/items/demo/sbox2014_stylish_degroot/sbox2014_stylish_degroot.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"zombie_body"},
		paintable = false,
		name = "Zombie Demo",
		localized_name = "info.tf2pm.hat.item_zombiedemoman",
		icon = "backpack/player/items/demo/demo_zombie",
		models = {
			["basename"] = "models/player/items/demo/demo_zombie.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"glasses"},
		paintable = false,
		name = "Seeing Double",
		localized_name = "info.tf2pm.hat.demomanmargaritashades",
		icon = "backpack/player/items/demo/demo_fiesta_shades",
		models = {
			["basename"] = "models/player/items/demo/demo_fiesta_shades.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"left_shoulder"},
		paintable = false,
		name = "The Dark Age Defender",
		localized_name = "info.tf2pm.hat.mail_bomber",
		localized_description = "info.tf2pm.hat.mail_bomber_desc",
		icon = "backpack/workshop/player/items/demo/mail_bomber/mail_bomber",
		models = {
			["basename"] = "models/workshop/player/items/demo/mail_bomber/mail_bomber.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"pants"},
		paintable = false,
		name = "Shin Shredders",
		localized_name = "info.tf2pm.hat.dec15_shin_shredders",
		icon = "backpack/workshop/player/items/demo/dec15_shin_shredders/dec15_shin_shredders",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec15_shin_shredders/dec15_shin_shredders.mdl",
		},
		bodygroup_overrides = {
			["shoes"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Tartan Spartan",
		localized_name = "info.tf2pm.hat.jul13_trojan_helmet",
		localized_description = "info.tf2pm.hat.jul13_trojan_helmet_desc",
		icon = "backpack/workshop/player/items/demo/jul13_trojan_helmet/jul13_trojan_helmet",
		models = {
			["basename"] = "models/workshop/player/items/demo/jul13_trojan_helmet/jul13_trojan_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"grenades"},
		paintable = true,
		name = "EOTL_demo_dynamite",
		localized_name = "info.tf2pm.hat.eotl_demo_dynamite",
		icon = "backpack/workshop/player/items/demo/demo_dynamite/demo_dynamite",
		models = {
			["basename"] = "models/workshop/player/items/demo/demo_dynamite/demo_dynamite.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"shirt"},
		paintable = false,
		name = "tw_demobot_armor",
		localized_name = "info.tf2pm.hat.tw_demobot_armor",
		icon = "backpack/workshop/player/items/demo/tw_demobot_armor/tw_demobot_armor",
		models = {
			["basename"] = "models/workshop/player/items/demo/tw_demobot_armor/tw_demobot_armor.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"demo_belt"},
		paintable = true,
		name = "Aerobatics Demonstrator",
		localized_name = "info.tf2pm.hat.hwn2016_aerobatics_demonstrator",
		icon = "backpack/workshop/player/items/demo/hwn2016_aerobatics_demonstrator/hwn2016_aerobatics_demonstrator",
		models = {
			["basename"] = "models/workshop/player/items/demo/hwn2016_aerobatics_demonstrator/hwn2016_aerobatics_demonstrator.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"grenades"},
		paintable = true,
		name = "Blast Blocker",
		localized_name = "info.tf2pm.hat.dec17_blast_blocker",
		icon = "backpack/workshop/player/items/demo/dec17_blast_blocker/dec17_blast_blocker",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec17_blast_blocker/dec17_blast_blocker.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"shirt", "sleeves"},
		paintable = true,
		name = "The Whiskey Bib",
		localized_name = "info.tf2pm.hat.jul13_gallant_gael",
		localized_description = "info.tf2pm.hat.jul13_gallant_gael_desc",
		icon = "backpack/workshop/player/items/demo/jul13_gallant_gael/jul13_gallant_gael",
		models = {
			["basename"] = "models/workshop/player/items/demo/jul13_gallant_gael/jul13_gallant_gael.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "The Tartan Shade",
		localized_name = "info.tf2pm.hat.tw2_demo_hood",
		localized_description = "info.tf2pm.hat.tw2_demo_hood_desc",
		icon = "backpack/workshop_partner/player/items/demo/tw2_demo_hood/tw2_demo_hood",
		models = {
			["basename"] = "models/workshop_partner/player/items/demo/tw2_demo_hood/tw2_demo_hood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"face"},
		paintable = true,
		name = "Unforgiven Glory",
		localized_name = "info.tf2pm.hat.sum19_unforgiven_glory",
		icon = "backpack/workshop/player/items/demo/sum19_unforgiven_glory/sum19_unforgiven_glory",
		models = {
			["basename"] = "models/workshop/player/items/demo/sum19_unforgiven_glory/sum19_unforgiven_glory.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Pure Tin Capotain",
		localized_name = "info.tf2pm.hat.robo_demo_capotain",
		localized_description = "info.tf2pm.hat.robo_demo_capotain_desc",
		icon = "backpack/workshop/player/items/demo/robo_demo_capotain/robo_demo_capotain",
		models = {
			["basename"] = "models/workshop/player/items/demo/robo_demo_capotain/robo_demo_capotain.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "tw_demobot_helmet",
		localized_name = "info.tf2pm.hat.tw_demobot_helmet",
		icon = "backpack/workshop/player/items/demo/tw_demobot_helmet/tw_demobot_helmet",
		models = {
			["basename"] = "models/workshop/player/items/demo/tw_demobot_helmet/tw_demobot_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Razor Cut",
		localized_name = "info.tf2pm.hat.short2014_demo_mohawk",
		icon = "backpack/workshop/player/items/demo/short2014_demo_mohawk/short2014_demo_mohawk",
		models = {
			["basename"] = "models/workshop/player/items/demo/short2014_demo_mohawk/short2014_demo_mohawk.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Hazard Headgear",
		localized_name = "info.tf2pm.hat.sum20_hazard_headgear",
		icon = "backpack/workshop/player/items/demo/sum20_hazard_headgear/sum20_hazard_headgear",
		models = {
			["basename"] = "models/workshop/player/items/demo/sum20_hazard_headgear/sum20_hazard_headgear.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"sleeves"},
		paintable = true,
		name = "Mann of the Seven Sees",
		localized_name = "info.tf2pm.hat.sept2014_mann_of_the_seven_sees",
		localized_description = "info.tf2pm.hat.sept2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/demo/sept2014_mann_of_the_seven_sees/sept2014_mann_of_the_seven_sees",
		models = {
			["basename"] = "models/workshop/player/items/demo/sept2014_mann_of_the_seven_sees/sept2014_mann_of_the_seven_sees.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Frag Proof Fragger",
		localized_name = "info.tf2pm.hat.spr18_frag_proof_fragger",
		icon = "backpack/workshop/player/items/demo/spr18_frag_proof_fragger/spr18_frag_proof_fragger",
		models = {
			["basename"] = "models/workshop/player/items/demo/spr18_frag_proof_fragger/spr18_frag_proof_fragger.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Hood of Sorrows",
		localized_name = "info.tf2pm.hat.bak_hood_of_sorrows",
		icon = "backpack/workshop/player/items/demo/bak_hood_of_sorrows/bak_hood_of_sorrows",
		models = {
			["basename"] = "models/workshop/player/items/demo/bak_hood_of_sorrows/bak_hood_of_sorrows.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Outta' Sight",
		localized_name = "info.tf2pm.hat.cc_summer2015_outta_sight",
		icon = "backpack/workshop/player/items/demo/cc_summer2015_outta_sight/cc_summer2015_outta_sight",
		models = {
			["basename"] = "models/workshop/player/items/demo/cc_summer2015_outta_sight/cc_summer2015_outta_sight.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Cyborg Stunt Helmet",
		localized_name = "info.tf2pm.hat.robo_demo_stuntman",
		localized_description = "info.tf2pm.hat.robo_demo_stuntman_desc",
		icon = "backpack/workshop/player/items/demo/robo_demo_stuntman/robo_demo_stuntman",
		models = {
			["basename"] = "models/workshop/player/items/demo/robo_demo_stuntman/robo_demo_stuntman.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Spirit of the Bombing Past",
		localized_name = "info.tf2pm.hat.hwn2016_spirit_of_the_bombing_past",
		icon = "backpack/workshop/player/items/demo/hwn2016_spirit_of_the_bombing_past/hwn2016_spirit_of_the_bombing_past",
		models = {
			["basename"] = "models/workshop/player/items/demo/hwn2016_spirit_of_the_bombing_past/hwn2016_spirit_of_the_bombing_past.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "Tartan Tyrolean",
		localized_name = "info.tf2pm.hat.xms2013_demo_plaid_hat",
		icon = "backpack/workshop/player/items/demo/xms2013_demo_plaid_hat/xms2013_demo_plaid_hat",
		models = {
			["basename"] = "models/workshop/player/items/demo/xms2013_demo_plaid_hat/xms2013_demo_plaid_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"demoman_collar"},
		paintable = true,
		name = "The Demo's Dustcatcher",
		localized_name = "info.tf2pm.hat.may16_demos_dustcatcher",
		icon = "backpack/workshop/player/items/demo/demolitionists_dustcatcher/demolitionists_dustcatcher",
		models = {
			["basename"] = "models/workshop/player/items/demo/demolitionists_dustcatcher/demolitionists_dustcatcher.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"face", "beard"},
		paintable = true,
		name = "Cap'n Calamari",
		localized_name = "info.tf2pm.hat.hw2013_octo_face",
		icon = "backpack/workshop/player/items/demo/hw2013_octo_face/hw2013_octo_face",
		models = {
			["basename"] = "models/workshop/player/items/demo/hw2013_octo_face/hw2013_octo_face.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "The FR-0",
		localized_name = "info.tf2pm.hat.robo_demo_fro",
		localized_description = "info.tf2pm.hat.robo_demo_fro_desc",
		icon = "backpack/workshop/player/items/demo/robo_demo_fro/robo_demo_fro",
		models = {
			["basename"] = "models/workshop/player/items/demo/robo_demo_fro/robo_demo_fro.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"feet"},
		paintable = true,
		name = "Highland High Heels",
		localized_name = "info.tf2pm.hat.xms2013_demo_plaid_boots",
		icon = "backpack/workshop/player/items/demo/xms2013_demo_plaid_boots/xms2013_demo_plaid_boots",
		models = {
			["basename"] = "models/workshop/player/items/demo/xms2013_demo_plaid_boots/xms2013_demo_plaid_boots.mdl",
		},
		bodygroup_overrides = {
			["shoes"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"sleeves"},
		paintable = true,
		name = "The Sangu Sleeves",
		localized_name = "info.tf2pm.hat.sbox2014_demo_samurai_sleeves",
		icon = "backpack/workshop/player/items/demo/sbox2014_demo_samurai_sleeves/sbox2014_demo_samurai_sleeves",
		models = {
			["basename"] = "models/workshop/player/items/demo/sbox2014_demo_samurai_sleeves/sbox2014_demo_samurai_sleeves.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"pants"},
		paintable = false,
		name = "The Tartantaloons",
		localized_name = "info.tf2pm.hat.tw2_demo_pants",
		localized_description = "info.tf2pm.hat.tw2_demo_pants_desc",
		icon = "backpack/workshop_partner/player/items/demo/tw2_demo_pants/tw2_demo_pants",
		models = {
			["basename"] = "models/workshop_partner/player/items/demo/tw2_demo_pants/tw2_demo_pants.mdl",
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = false,
		name = "The Strontium Stove Pipe",
		localized_name = "info.tf2pm.hat.robo_demo_scotsmans_stovepipe",
		localized_description = "info.tf2pm.hat.robo_demo_scotsmans_stovepipe_desc",
		icon = "backpack/workshop/player/items/demo/robo_demo_scotsmans_stovepipe/robo_demo_scotsmans_stovepipe",
		models = {
			["basename"] = "models/workshop/player/items/demo/robo_demo_scotsmans_stovepipe/robo_demo_scotsmans_stovepipe.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"feet"},
		paintable = true,
		name = "dec2014 Viking Boots",
		localized_name = "info.tf2pm.hat.dec2014_viking_boots",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/demo/dec2014_viking_boots/dec2014_viking_boots",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec2014_viking_boots/dec2014_viking_boots.mdl",
		},
		bodygroup_overrides = {
			["shoes"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"shirt", "sleeves"},
		paintable = true,
		name = "The Gaelic Garb",
		localized_name = "info.tf2pm.hat.jul13_gaelic_garb",
		localized_description = "info.tf2pm.hat.jul13_gaelic_garb_desc",
		icon = "backpack/workshop/player/items/demo/jul13_gaelic_garb/jul13_gaelic_garb",
		models = {
			["basename"] = "models/workshop/player/items/demo/jul13_gaelic_garb/jul13_gaelic_garb.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 Viking Helmet",
		localized_name = "info.tf2pm.hat.dec2014_viking_helmet",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/demo/dec2014_viking_helmet/dec2014_viking_helmet",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec2014_viking_helmet/dec2014_viking_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 Comforter",
		localized_name = "info.tf2pm.hat.dec2014_comforter",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/demo/dec2014_comforter/dec2014_comforter",
		models = {
			["basename"] = "models/workshop/player/items/demo/dec2014_comforter/dec2014_comforter.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo"},
		targets = {"glasses"},
		paintable = true,
		name = "Eyeborg",
		localized_name = "info.tf2pm.hat.sf14_demo_cyborg",
		icon = "backpack/workshop/player/items/demo/sf14_demo_cyborg/sf14_demo_cyborg",
		models = {
			["basename"] = "models/workshop/player/items/demo/sf14_demo_cyborg/sf14_demo_cyborg.mdl",
		},
	},

}
