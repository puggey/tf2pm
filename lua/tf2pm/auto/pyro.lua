
-- Auto generated at 2020-12-04 12:56:46 UTC+07:00
return {
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Pyro's Beanie",
		localized_name = "info.tf2pm.hat.pyro_hat_1",
		icon = "backpack/player/items/pyro/pyro_hat",
		models = {
			["basename"] = "models/player/items/pyro/pyro_hat.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Respectless Rubber Glove",
		localized_name = "info.tf2pm.hat.pyro_chicken_hat",
		localized_description = "info.tf2pm.hat.pyro_chicken_hat_desc",
		icon = "backpack/player/items/pyro/pyro_chicken",
		models = {
			["basename"] = "models/player/items/pyro/pyro_chicken.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Brigade Helm",
		localized_name = "info.tf2pm.hat.pyro_fireman_helmet",
		icon = "backpack/player/items/pyro/fireman_helmet",
		models = {
			["basename"] = "models/player/items/pyro/fireman_helmet.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Pyro Brain Sucker",
		localized_name = "info.tf2pm.hat.pyrobrainsucker",
		localized_description = "info.tf2pm.hat.pyrobrainsucker_desc",
		icon = "backpack/player/items/pyro/pyro_brainsucker",
		models = {
			["basename"] = "models/player/items/pyro/pyro_brainsucker.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"beard"},
		paintable = true,
		name = "Pyro Monocle",
		localized_name = "info.tf2pm.hat.pyromonocle",
		localized_description = "info.tf2pm.hat.pyromonocle_desc",
		icon = "backpack/player/items/pyro/pyro_monocle",
		models = {
			["basename"] = "models/player/items/pyro/pyro_monocle.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Pyro Helm",
		localized_name = "info.tf2pm.hat.pyrohelm",
		localized_description = "info.tf2pm.hat.pyrohelm_desc",
		icon = "backpack/player/items/pyro/pyro_pyrolean",
		models = {
			["basename"] = "models/player/items/pyro/pyro_pyrolean.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "The Attendant",
		localized_name = "info.tf2pm.hat.theattendant",
		icon = "backpack/player/items/pyro/attendant",
		models = {
			["basename"] = "models/player/items/pyro/attendant.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Old Guadalajara",
		localized_name = "info.tf2pm.hat.pyrofiestasombrero",
		localized_description = "info.tf2pm.hat.pyrofiestasombrero_desc",
		icon = "backpack/player/items/pyro/fiesta_sombrero",
		models = {
			["basename"] = "models/player/items/pyro/fiesta_sombrero.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Napper's Respite",
		localized_name = "info.tf2pm.hat.pyrobeanie",
		localized_description = "info.tf2pm.hat.pyrobeanie_desc",
		icon = "backpack/player/items/pyro/pyro_beanie",
		models = {
			["basename"] = "models/player/items/pyro/pyro_beanie.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Handyman's Handle",
		localized_name = "info.tf2pm.hat.pyroplunger",
		localized_description = "info.tf2pm.hat.pyroplunger_desc",
		icon = "backpack/player/items/pyro/pyro_plunger",
		models = {
			["basename"] = "models/player/items/pyro/pyro_plunger.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"beard"},
		paintable = true,
		name = "Pyromancer's Mask",
		localized_name = "info.tf2pm.hat.pyromancersmask",
		icon = "backpack/player/items/pyro/pyro_pyromancers_mask",
		models = {
			["basename"] = "models/player/items/pyro/pyro_pyromancers_mask.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.pyromancer_style1",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.pyromancer_style2",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.pyromancer_style3",
			},
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "Prancer's Pride",
		localized_name = "info.tf2pm.hat.prancerspride",
		icon = "backpack/player/items/pyro/prancers_pride",
		models = {
			["basename"] = "models/player/items/pyro/prancers_pride.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Madame Dixie",
		localized_name = "info.tf2pm.hat.madamedixie",
		icon = "backpack/player/items/pyro/pyro_madame_dixie",
		models = {
			["basename"] = "models/player/items/pyro/pyro_madame_dixie.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "KF Pyro Mask",
		localized_name = "info.tf2pm.hat.kf_pyro_mask",
		icon = "backpack/player/items/pyro/pyro_tripwire_mask",
		models = {
			["basename"] = "models/player/items/pyro/pyro_tripwire_mask.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"necklace"},
		paintable = false,
		name = "KF Pyro Tie",
		localized_name = "info.tf2pm.hat.kf_pyro_tie",
		icon = "backpack/player/items/pyro/pyro_tripwire_tie",
		models = {
			["basename"] = "models/player/items/pyro/pyro_tripwire_tie.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Hottie's Hoodie",
		localized_name = "info.tf2pm.hat.hottieshoodie",
		localized_description = "info.tf2pm.hat.hottieshoodie_desc",
		icon = "backpack/player/items/pyro/hotties_hoodie",
		models = {
			["basename"] = "models/player/items/pyro/hotties_hoodie.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"lenses"},
		paintable = false,
		name = "Sight for Sore Eyes",
		localized_name = "info.tf2pm.hat.soreeyes",
		localized_description = "info.tf2pm.hat.soreeyes_desc",
		icon = "backpack/player/items/pyro/sore_eyes",
		models = {
			["basename"] = "models/player/items/pyro/sore_eyes.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Connoisseur's Cap",
		localized_name = "info.tf2pm.hat.connoisseurs_cap",
		localized_description = "info.tf2pm.hat.connoisseurs_cap_desc",
		icon = "backpack/player/items/pyro/pyro_chef_hat",
		models = {
			["basename"] = "models/player/items/pyro/pyro_chef_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "Traffic Cone",
		localized_name = "info.tf2pm.hat.trafficcone",
		localized_description = "info.tf2pm.hat.trafficcone_desc",
		icon = "backpack/player/items/pyro/traffic_cone",
		models = {
			["basename"] = "models/player/items/pyro/traffic_cone.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "Stately Steel Toe",
		localized_name = "info.tf2pm.hat.statelysteeltoe",
		icon = "backpack/player/items/pyro/boot_hat",
		models = {
			["basename"] = "models/player/items/pyro/boot_hat.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"lenses"},
		paintable = false,
		name = "Blazing Bull",
		localized_name = "info.tf2pm.hat.hwn_pyrohat",
		icon = "backpack/player/items/pyro/hwn_pyro_hat",
		models = {
			["basename"] = "models/player/items/pyro/hwn_pyro_hat.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_wings"},
		paintable = false,
		name = "Fallen Angel",
		localized_name = "info.tf2pm.hat.hwn_pyromisc1",
		icon = "backpack/player/items/pyro/hwn_pyro_misc1",
		models = {
			["basename"] = "models/player/items/pyro/hwn_pyro_misc1.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_tail"},
		paintable = false,
		name = "Tail From the Crypt",
		localized_name = "info.tf2pm.hat.hwn_pyromisc2",
		icon = "backpack/player/items/pyro/hwn_pyro_misc2",
		models = {
			["basename"] = "models/player/items/pyro/hwn_pyro_misc2.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = false,
		name = "The Last Breath",
		localized_name = "info.tf2pm.hat.hwn_pyrogasmask",
		icon = "backpack/workshop/player/items/pyro/pyro_halloween_gasmask/pyro_halloween_gasmask",
		models = {
			["basename"] = "models/workshop/player/items/pyro/pyro_halloween_gasmask/pyro_halloween_gasmask.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Apparition's Aspect",
		localized_name = "info.tf2pm.hat.ghost_aspect",
		icon = "backpack/workshop/player/items/pyro/ghost_aspect/ghost_aspect",
		models = {
			["basename"] = "models/workshop/player/items/pyro/ghost_aspect/ghost_aspect.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = false,
		name = "The Moonman Backpack",
		localized_name = "info.tf2pm.hat.grordbortpyro_tank",
		localized_description = "info.tf2pm.hat.grordbortpyro_tank_desc",
		icon = "backpack/player/items/pyro/drg_pyro_fueltank",
		models = {
			["basename"] = "models/player/items/pyro/drg_pyro_fuelTank.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
			["medic_backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "The Bubble Pipe",
		localized_name = "info.tf2pm.hat.bubble_helmet",
		localized_description = "info.tf2pm.hat.bubble_helmet_desc",
		icon = "backpack/player/items/pyro/drg_pyro_bubblehelmet",
		models = {
			["basename"] = "models/player/items/pyro/drg_pyro_bubbleHelmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Little Buddy",
		localized_name = "info.tf2pm.hat.pyrohat1",
		localized_description = "info.tf2pm.hat.pyrohat1_desc",
		icon = "backpack/player/items/pyro/fwk_pyro_sailor",
		models = {
			["basename"] = "models/player/items/pyro/fwk_pyro_sailor.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Birdcage",
		localized_name = "info.tf2pm.hat.pyrohat2",
		localized_description = "info.tf2pm.hat.pyrohat2_desc",
		icon = "backpack/player/items/pyro/fwk_pyro_birdcage",
		models = {
			["basename"] = "models/player/items/pyro/fwk_pyro_birdcage.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Flamboyant Flamenco",
		localized_name = "info.tf2pm.hat.pyrohat3",
		localized_description = "info.tf2pm.hat.pyrohat3_desc",
		icon = "backpack/player/items/pyro/fwk_pyro_flamenco",
		models = {
			["basename"] = "models/player/items/pyro/fwk_pyro_flamenco.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"grenades"},
		paintable = true,
		name = "The Cremator's Conscience",
		localized_name = "info.tf2pm.hat.pyroconscience",
		localized_description = "info.tf2pm.hat.pyroconscience_desc",
		icon = "backpack/player/items/pyro/fwk_pyro_conscience",
		models = {
			["basename"] = "models/player/items/pyro/fwk_pyro_conscience.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"glasses", "hat"},
		paintable = true,
		name = "The Head Warmer",
		localized_name = "info.tf2pm.hat.incineratorsinsulator",
		localized_description = "info.tf2pm.hat.incineratorsinsulator_desc",
		icon = "backpack/player/items/pyro/xms_pyro_parka",
		models = {
			["basename"] = "models/player/items/pyro/xms_pyro_parka.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"grenades"},
		paintable = true,
		name = "The Jingle Belt",
		localized_name = "info.tf2pm.hat.jinglehell",
		localized_description = "info.tf2pm.hat.jinglehell_desc",
		icon = "backpack/player/items/pyro/xms_pyro_bells",
		models = {
			["basename"] = "models/player/items/pyro/xms_pyro_bells.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = false,
		name = "The Infernal Orchestrina",
		localized_name = "info.tf2pm.hat.pyromusicdevice",
		localized_description = "info.tf2pm.hat.pyromusicdevice_desc",
		icon = "backpack/player/items/pyro/mtp_backpack",
		models = {
			["basename"] = "models/player/items/%s/mtp_backpack.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"grenades"},
		paintable = false,
		name = "The Burning Bongos",
		localized_name = "info.tf2pm.hat.pyrobongos",
		localized_description = "info.tf2pm.hat.pyrobongos_desc",
		icon = "backpack/player/items/pyro/mtp_bongos",
		models = {
			["basename"] = "models/player/items/%s/mtp_bongos.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Waxy Wayfinder",
		localized_name = "info.tf2pm.hat.pyrocandle",
		localized_description = "info.tf2pm.hat.pyrocandle_desc",
		icon = "backpack/player/items/pyro/pyro_candle",
		models = {
			["basename"] = "models/player/items/pyro/pyro_candle.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Triclops",
		localized_name = "info.tf2pm.hat.supermnc_pyro",
		localized_description = "info.tf2pm.hat.supermnc_pyro_desc",
		icon = "backpack/player/items/pyro/smnc_pyro",
		models = {
			["basename"] = "models/player/items/pyro/smnc_pyro.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.supermnc_pyro_style0",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.supermnc_pyro_style1",
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"necklace"},
		paintable = true,
		name = "The Tribal Bones",
		localized_name = "info.tf2pm.hat.ha_pyro",
		localized_description = "info.tf2pm.hat.ha_pyro_desc",
		icon = "backpack/workshop_partner/player/items/pyro/hero_academy_pyro/hero_academy_pyro",
		models = {
			["basename"] = "models/workshop_partner/player/items/pyro/hero_academy_pyro/hero_academy_pyro.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = false,
		name = "The Russian Rocketeer",
		localized_name = "info.tf2pm.hat.awes_pyro",
		localized_description = "info.tf2pm.hat.awes_pyro_desc",
		icon = "backpack/player/items/pyro/awes_jetpack",
		models = {
			["basename"] = "models/player/items/pyro/awes_jetpack.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
			["medic_backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = false,
		name = "The Pyrobotics Pack",
		localized_name = "info.tf2pm.hat.pyro_robot_backpack",
		localized_description = "info.tf2pm.hat.pyro_robot_backpack_desc",
		icon = "backpack/player/items/mvm_loot/pyro/pyrobo_backpack",
		models = {
			["basename"] = "models/player/items/mvm_loot/pyro/pyrobo_backpack.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
			["medic_backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "Area 451",
		localized_name = "info.tf2pm.hat.xcom_pyro",
		localized_description = "info.tf2pm.hat.xcom_pyro_desc",
		icon = "backpack/player/items/pyro/xcom_sectoid_mask",
		models = {
			["basename"] = "models/player/items/pyro/xcom_sectoid_mask.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "The Pyrotechnic Tote",
		localized_name = "info.tf2pm.hat.pyro_fireworksbag",
		localized_description = "info.tf2pm.hat.pyro_fireworksbag_desc",
		icon = "backpack/player/items/pyro/pyro_fireworksbag",
		models = {
			["basename"] = "models/player/items/pyro/pyro_fireworksbag.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
			["medic_backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Plutonidome",
		localized_name = "info.tf2pm.hat.plutonidome",
		localized_description = "info.tf2pm.hat.plutonidome_desc",
		icon = "backpack/player/items/pyro/pyro_brainhead",
		models = {
			["basename"] = "models/player/items/pyro/pyro_brainhead.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.plutonidome_style0",
				models = {
					["basename"] = "models/player/items/pyro/pyro_brainhead.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.plutonidome_style1",
				models = {
					["basename"] = "models/player/items/pyro/pyro_brainhead_s1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["head"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "The Wraith Wrap",
		localized_name = "info.tf2pm.hat.wraithwrap",
		localized_description = "info.tf2pm.hat.wraithwrap_desc",
		icon = "backpack/player/items/pyro/hwn_pyro_spookyhood",
		models = {
			["basename"] = "models/player/items/pyro/hwn_pyro_spookyhood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = false,
		name = "The Coffin Kit",
		localized_name = "info.tf2pm.hat.coffinkit",
		localized_description = "info.tf2pm.hat.coffinkit_desc",
		icon = "backpack/player/items/pyro/hwn_pyro_coffinpack",
		models = {
			["basename"] = "models/player/items/pyro/hwn_pyro_coffinpack.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 0,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The DethKapp",
		localized_name = "info.tf2pm.hat.dethkapp",
		localized_description = "info.tf2pm.hat.dethkapp_desc",
		icon = "backpack/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_hat",
		models = {
			["basename"] = "models/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_hat_hair.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dethkapp_style0",
				models = {
					["basename"] = "models/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_hat_hair.mdl",
				}
			},
			[1] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.dethkapp_style1",
				models = {
					["basename"] = "models/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_hat_hair.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dethkapp_style2",
				models = {
					["basename"] = "models/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_hat.mdl",
				}
			},
			[3] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.dethkapp_style3",
				models = {
					["basename"] = "models/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_hat.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Nose Candy",
		localized_name = "info.tf2pm.hat.nosecandy",
		localized_description = "info.tf2pm.hat.nosecandy_desc",
		icon = "backpack/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_mask",
		models = {
			["basename"] = "models/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_mask.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.nosecandy_style0",
				bodygroup_overrides = {
					["head"] = 1,
					["hat"] = 1,
				},
				models = {
					["basename"] = "models/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_mask.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.nosecandy_style1",
				models = {
					["basename"] = "models/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_mask_nose.mdl",
				}
			},
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_spikes"},
		paintable = true,
		name = "Rail Spikes",
		localized_name = "info.tf2pm.hat.railspikes",
		localized_description = "info.tf2pm.hat.railspikes_desc",
		icon = "backpack/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_spikes",
		models = {
			["basename"] = "models/workshop_partner/player/items/pyro/pyro_rocks/pyro_rocks_spikes.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Winter Wonderland Wrap",
		localized_name = "info.tf2pm.hat.thewinterwonderlandwrap",
		localized_description = "info.tf2pm.hat.thewinterwonderlandwrap_desc",
		icon = "backpack/player/items/pyro/winter_pyro_mask",
		models = {
			["basename"] = "models/player/items/pyro/winter_pyro_mask.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.thewinterwonderlandwrap_style0",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.thewinterwonderlandwrap_style1",
			},
		},
		bodygroup_overrides = {
			["head"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Person in the Iron Mask",
		localized_name = "info.tf2pm.hat.ironmask",
		localized_description = "info.tf2pm.hat.ironmask_desc",
		icon = "backpack/player/items/pyro/bio_fireman",
		models = {
			["basename"] = "models/player/items/pyro/bio_fireman.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "The Necronomicrown",
		localized_name = "info.tf2pm.hat.necronomicrown",
		localized_description = "info.tf2pm.hat.necronomicrown_desc",
		icon = "backpack/player/items/pyro/pn2_evilash",
		models = {
			["basename"] = "models/player/items/pyro/pn2_evilash.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Breather Bag",
		localized_name = "info.tf2pm.hat.breatherbag",
		localized_description = "info.tf2pm.hat.breatherbag_desc",
		icon = "backpack/workshop_partner/player/items/pyro/as_pyro_cleansuit/as_pyro_cleansuit",
		models = {
			["basename"] = "models/workshop_partner/player/items/pyro/as_pyro_cleansuit/as_pyro_cleansuit.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "MvM GateBot Light Pyro",
		icon = "backpack",
		models = {
			["basename"] = "models/bots/gameplay_cosmetic/light_pyro_on.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_pyro_on.mdl",
				}
			},
			[1] = {
				responsive_skins = {1, 1},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_pyro_off.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = false,
		name = "The Portable Smissmas Spirit Dispenser",
		localized_name = "info.tf2pm.hat.winter2013_spiritdispenser",
		localized_description = "info.tf2pm.hat.winter2013_spiritdispenser_desc",
		icon = "backpack/player/items/pyro/xms_backpack_snowglobe",
		models = {
			["basename"] = "models/player/items/pyro/xms_backpack_snowglobe.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = false,
		name = "The Nabler",
		localized_name = "info.tf2pm.hat.nabler",
		icon = "backpack/workshop/player/items/pyro/threea_nabler/threea_nabler",
		models = {
			["basename"] = "models/workshop/player/items/pyro/threea_nabler/threea_nabler.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = true,
		name = "Crusader's Getup",
		localized_name = "info.tf2pm.hat.hwn2015_firebug_suit",
		icon = "backpack/workshop/player/items/pyro/hwn2015_firebug_suit/hwn2015_firebug_suit",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2015_firebug_suit/hwn2015_firebug_suit.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"sleeves"},
		paintable = true,
		name = "Arsonist Apparatus",
		localized_name = "info.tf2pm.hat.sf14_hw2014_robot_arm",
		icon = "backpack/workshop/player/items/pyro/sf14_hw2014_robot_arm/sf14_hw2014_robot_arm",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sf14_hw2014_robot_arm/sf14_hw2014_robot_arm.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Centurion",
		localized_name = "info.tf2pm.hat.tw2_greek_helm",
		localized_description = "info.tf2pm.hat.tw2_greek_helm_desc",
		icon = "backpack/workshop_partner/player/items/pyro/tw2_greek_helm/tw2_greek_helm",
		models = {
			["basename"] = "models/workshop_partner/player/items/pyro/tw2_greek_helm/tw2_greek_helm.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "The Toy Tailor",
		localized_name = "info.tf2pm.hat.xms2013_pyro_tailor_hat",
		icon = "backpack/workshop/player/items/pyro/xms2013_pyro_tailor_hat/xms2013_pyro_tailor_hat",
		models = {
			["basename"] = "models/workshop/player/items/pyro/xms2013_pyro_tailor_hat/xms2013_pyro_tailor_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Creature's Grin",
		localized_name = "info.tf2pm.hat.sf14_the_creatures_grin",
		icon = "backpack/workshop/player/items/pyro/sf14_the_creatures_grin/sf14_the_creatures_grin",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sf14_the_creatures_grin/sf14_the_creatures_grin.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = false,
		name = "The Steel Sixpack",
		localized_name = "info.tf2pm.hat.tw2_greek_armor",
		localized_description = "info.tf2pm.hat.tw2_greek_armor_desc",
		icon = "backpack/workshop_partner/player/items/pyro/tw2_greek_armor/tw2_greek_armor",
		models = {
			["basename"] = "models/workshop_partner/player/items/pyro/tw2_greek_armor/tw2_greek_armor.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt", "grenades"},
		paintable = true,
		name = "The Sengoku Scorcher",
		localized_name = "info.tf2pm.hat.short2014_sengoku_scorcher",
		icon = "backpack/workshop/player/items/pyro/short2014_sengoku_scorcher/short2014_sengoku_scorcher",
		models = {
			["basename"] = "models/workshop/player/items/pyro/short2014_sengoku_scorcher/short2014_sengoku_scorcher.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Hard-Headed Hardware",
		localized_name = "info.tf2pm.hat.hw2013_hardheaded_hardware",
		icon = "backpack/workshop/player/items/pyro/hw2013_hardheaded_hardware/hw2013_hardheaded_hardware",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_hardheaded_hardware/hw2013_hardheaded_hardware.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 2014_pyromancer_hood",
		localized_name = "info.tf2pm.hat.dec2014_2014_pyromancer_hood",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/pyro/dec2014_2014_pyromancer_hood/dec2014_2014_pyromancer_hood",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec2014_2014_pyromancer_hood/dec2014_2014_pyromancer_hood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Beast From Below",
		localized_name = "info.tf2pm.hat.hw2013_beast_from_below",
		icon = "backpack/workshop/player/items/pyro/hw2013_beast_from_below/hw2013_beast_from_below",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_beast_from_below/hw2013_beast_from_below.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Fire Fighter",
		localized_name = "info.tf2pm.hat.sum20_fire_fighter",
		icon = "backpack/workshop/player/items/pyro/sum20_fire_fighter_style1/sum20_fire_fighter_style1",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sum20_fire_fighter_style1/sum20_fire_fighter_style1.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum20_fire_fighter_style1",
				models = {
					["basename"] = "models/workshop/player/items/pyro/sum20_fire_fighter_style1/sum20_fire_fighter_style1.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum20_fire_fighter_style2",
				models = {
					["basename"] = "models/workshop/player/items/pyro/sum20_fire_fighter_style2/sum20_fire_fighter_style2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Melted Mop",
		localized_name = "info.tf2pm.hat.sum19_melted_mop",
		icon = "backpack/workshop/player/items/pyro/sum19_melted_mop/sum19_melted_mop",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sum19_melted_mop/sum19_melted_mop.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Flamehawk",
		localized_name = "info.tf2pm.hat.sum20_flamehawk",
		icon = "backpack/workshop/player/items/pyro/sum20_flamehawk/sum20_flamehawk",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sum20_flamehawk/sum20_flamehawk.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Arthropod's Aspect",
		localized_name = "info.tf2pm.hat.hwn2015_firebug_mask",
		icon = "backpack/workshop/player/items/pyro/hwn2015_firebug_mask/hwn2015_firebug_mask",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2015_firebug_mask/hwn2015_firebug_mask.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"left_shoulder"},
		paintable = false,
		name = "The Fiery Phoenix",
		localized_name = "info.tf2pm.hat.sum19_fiery_phoenix",
		icon = "backpack/workshop/player/items/pyro/sum19_fiery_phoenix/sum19_fiery_phoenix",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sum19_fiery_phoenix/sum19_fiery_phoenix.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"sleeves"},
		paintable = false,
		name = "dec2014 Armoured Appendages",
		localized_name = "info.tf2pm.hat.dec2014_armoured_appendages",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/pyro/dec2014_armoured_appendages/dec2014_armoured_appendages",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec2014_armoured_appendages/dec2014_armoured_appendages.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = false,
		name = "dec2014 Torchers Tabard",
		localized_name = "info.tf2pm.hat.dec2014_torchers_tabard",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/pyro/dec2014_torchers_tabard/dec2014_torchers_tabard",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec2014_torchers_tabard/dec2014_torchers_tabard.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"feet"},
		paintable = true,
		name = "The Hot Huaraches",
		localized_name = "info.tf2pm.hat.fall17_hot_huaraches",
		localized_description = "info.tf2pm.hat.fall17_hot_huaraches_desc",
		icon = "backpack/workshop/player/items/pyro/fall17_hot_huaraches/fall17_hot_huaraches",
		models = {
			["basename"] = "models/workshop/player/items/pyro/fall17_hot_huaraches/fall17_hot_huaraches.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = true,
		name = "dec2014 Pyromancer's Raiments",
		localized_name = "info.tf2pm.hat.dec2014_pyromancers_raiments",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/pyro/dec2014_pyromancers_raiments/dec2014_pyromancers_raiments",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec2014_pyromancers_raiments/dec2014_pyromancers_raiments.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = false,
		name = "The Trail-Blazer",
		localized_name = "info.tf2pm.hat.xms2013_pyro_sled",
		icon = "backpack/workshop/player/items/pyro/xms2013_pyro_sled/xms2013_pyro_sled",
		models = {
			["basename"] = "models/workshop/player/items/pyro/xms2013_pyro_sled/xms2013_pyro_sled.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "The Round-A-Bout",
		localized_name = "info.tf2pm.hat.dec20_round_a_bout",
		icon = "backpack/workshop/player/items/pyro/dec20_round_a_bout/dec20_round_a_bout",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec20_round_a_bout/dec20_round_a_bout.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Pyro the Flamedeer",
		localized_name = "info.tf2pm.hat.dec16_pyro_the_flamedeer",
		localized_description = "info.tf2pm.hat.dec16_pyro_the_flamedeer_desc",
		icon = "backpack/workshop/player/items/pyro/dec16_pyro_the_flamedeer/dec16_pyro_the_flamedeer",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec16_pyro_the_flamedeer/dec16_pyro_the_flamedeer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "Fireman's Essentials",
		localized_name = "info.tf2pm.hat.fall17_firemans_essentials",
		localized_description = "info.tf2pm.hat.fall17_firemans_essentials_desc",
		icon = "backpack/workshop/player/items/pyro/fall17_firemanns_essentials/fall17_firemanns_essentials",
		models = {
			["basename"] = "models/workshop/player/items/pyro/fall17_firemanns_essentials/fall17_firemanns_essentials.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Bozo's Bouffant",
		localized_name = "info.tf2pm.hat.hw2013_the_haha_hairdo",
		icon = "backpack/workshop/player/items/pyro/hw2013_the_haha_hairdo/hw2013_the_haha_hairdo",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_the_haha_hairdo/hw2013_the_haha_hairdo.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = true,
		name = "Trickster's Turnout Gear",
		localized_name = "info.tf2pm.hat.fall2013_the_insidious_incinerator",
		localized_description = "info.tf2pm.hat.fall2013_the_insidious_incinerator_desc",
		icon = "backpack/workshop/player/items/pyro/fall2013_the_insidious_incinerator/fall2013_the_insidious_incinerator",
		models = {
			["basename"] = "models/workshop/player/items/pyro/fall2013_the_insidious_incinerator/fall2013_the_insidious_incinerator.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "The Mucous Membrain",
		localized_name = "info.tf2pm.hat.hw2013_mucus_membrane",
		icon = "backpack/workshop/player/items/pyro/hw2013_mucus_membrane/hw2013_mucus_membrane",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_mucus_membrane/hw2013_mucus_membrane.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "Sacrificial Stone",
		localized_name = "info.tf2pm.hat.fall17_sacrificial_stone",
		localized_description = "info.tf2pm.hat.fall17_sacrificial_stone_desc",
		icon = "backpack/workshop/player/items/pyro/fall17_sacrificial_stone/fall17_sacrificial_stone",
		models = {
			["basename"] = "models/workshop/player/items/pyro/fall17_sacrificial_stone/fall17_sacrificial_stone.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Filamental",
		localized_name = "info.tf2pm.hat.robo_pyro_figment_filament",
		localized_description = "info.tf2pm.hat.robo_pyro_figment_filament_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_figment_filament/robo_pyro_figment_filament",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_figment_filament/robo_pyro_figment_filament.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"belt_misc"},
		paintable = true,
		name = "Pocket Pardner",
		localized_name = "info.tf2pm.hat.dec18_pocket_pardner",
		icon = "backpack/workshop/player/items/pyro/dec18_pocket_pardner/dec18_pocket_pardner",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec18_pocket_pardner/dec18_pocket_pardner.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec18_pocket_pardner_style0",
				models = {
					["basename"] = "models/workshop/player/items/pyro/dec18_pocket_pardner/dec18_pocket_pardner.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec18_pocket_pardner_style1",
				models = {
					["basename"] = "models/workshop/player/items/pyro/dec18_pocket_pardner_style/dec18_pocket_pardner_style.mdl",
				}
			},
		},
	},
	{
		classes = {"pyro"},
		targets = {"belt_misc"},
		paintable = true,
		name = "Deity's Dress",
		localized_name = "info.tf2pm.hat.fall17_deitys_dress",
		localized_description = "info.tf2pm.hat.fall17_deitys_dress_desc",
		icon = "backpack/workshop/player/items/pyro/fall17_deitys_dress/fall17_deitys_dress",
		models = {
			["basename"] = "models/workshop/player/items/pyro/fall17_deitys_dress/fall17_deitys_dress.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "The Raven's Visage",
		localized_name = "info.tf2pm.hat.hw2013_visage_of_the_crow",
		icon = "backpack/workshop/player/items/pyro/hw2013_visage_of_the_crow/hw2013_visage_of_the_crow",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_visage_of_the_crow/hw2013_visage_of_the_crow.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"lenses"},
		paintable = true,
		name = "D-eye-monds",
		localized_name = "info.tf2pm.hat.fall17_deyemonds",
		localized_description = "info.tf2pm.hat.fall17_deyemonds_desc",
		icon = "backpack/workshop/player/items/pyro/fall17_deyemonds/fall17_deyemonds",
		models = {
			["basename"] = "models/workshop/player/items/pyro/fall17_deyemonds/fall17_deyemonds.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = false,
		name = "Sweet Smissmas Sweater",
		localized_name = "info.tf2pm.hat.dec16_smissmas_sweater",
		localized_description = "info.tf2pm.hat.dec16_smissmas_sweater_desc",
		icon = "backpack/workshop/player/items/pyro/dec16_smissmas_sweater/dec16_smissmas_sweater",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec16_smissmas_sweater/dec16_smissmas_sweater.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Blizzard Breather",
		localized_name = "info.tf2pm.hat.xms2013_pyro_arctic_mask",
		icon = "backpack/workshop/player/items/pyro/xms2013_pyro_arctic_mask/xms2013_pyro_arctic_mask",
		models = {
			["basename"] = "models/workshop/player/items/pyro/xms2013_pyro_arctic_mask/xms2013_pyro_arctic_mask.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Air Raider",
		localized_name = "info.tf2pm.hat.fall2013_fire_bird",
		localized_description = "info.tf2pm.hat.fall2013_fire_bird_desc",
		icon = "backpack/workshop/player/items/pyro/fall2013_fire_bird/fall2013_fire_bird",
		models = {
			["basename"] = "models/workshop/player/items/pyro/fall2013_fire_bird/fall2013_fire_bird.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Respectless Robo-Glove",
		localized_name = "info.tf2pm.hat.robo_pyro_respectless_glove",
		localized_description = "info.tf2pm.hat.robo_pyro_respectless_glove_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_respectless_glove/robo_pyro_respectless_glove",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_respectless_glove/robo_pyro_respectless_glove.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"sleeves"},
		paintable = true,
		name = "The Abhorrent Appendages",
		localized_name = "info.tf2pm.hat.hw2013_the_abhorrent_appendages",
		icon = "backpack/workshop/player/items/pyro/hw2013_the_abhorrent_appendages/hw2013_the_abhorrent_appendages",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_the_abhorrent_appendages/hw2013_the_abhorrent_appendages.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"lenses"},
		paintable = true,
		name = "Discovision",
		localized_name = "info.tf2pm.hat.dec19_discovision",
		icon = "backpack/workshop/player/items/pyro/dec19_discovision/dec19_discovision",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec19_discovision/dec19_discovision.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Bone Dome",
		localized_name = "info.tf2pm.hat.fall2013_air_raider",
		localized_description = "info.tf2pm.hat.fall2013_air_raider_desc",
		icon = "backpack/workshop/player/items/pyro/fall2013_air_raider/fall2013_air_raider",
		models = {
			["basename"] = "models/workshop/player/items/pyro/fall2013_air_raider/fall2013_air_raider.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.fall2013_air_raider_style1",
				models = {
					["basename"] = "models/workshop/player/items/pyro/fall2013_air_raider/fall2013_air_raider.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.fall2013_air_raider_style2",
				models = {
					["basename"] = "models/workshop/player/items/pyro/fall2013_air_raider_s2/fall2013_air_raider_s2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Firewall Helmet",
		localized_name = "info.tf2pm.hat.robo_pyro_firewall_helmet",
		localized_description = "info.tf2pm.hat.robo_pyro_firewall_helmet_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_firewall_helmet/robo_pyro_firewall_helmet",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_firewall_helmet/robo_pyro_firewall_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_tail"},
		paintable = true,
		name = "The Cauterizer's Caudal Appendage",
		localized_name = "info.tf2pm.hat.hw2013_dragonbutt",
		icon = "backpack/workshop/player/items/pyro/hw2013_dragonbutt/hw2013_dragonbutt",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_dragonbutt/hw2013_dragonbutt.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = true,
		name = "The Soot Suit",
		localized_name = "info.tf2pm.hat.jul13_soot_suit",
		localized_description = "info.tf2pm.hat.jul13_soot_suit_desc",
		icon = "backpack/workshop/player/items/pyro/jul13_soot_suit/jul13_soot_suit",
		models = {
			["basename"] = "models/workshop/player/items/pyro/jul13_soot_suit/jul13_soot_suit.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"Back"},
		paintable = true,
		name = "Flammable Favor",
		localized_name = "info.tf2pm.hat.dec16_flammable_favor",
		localized_description = "info.tf2pm.hat.dec16_flammable_favor_desc",
		icon = "backpack/workshop/player/items/pyro/dec16_flammable_favor/dec16_flammable_favor",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec16_flammable_favor/dec16_flammable_favor.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = false,
		name = "PY-40 Incinibot",
		localized_name = "info.tf2pm.hat.hw2013_py40_automaton",
		icon = "backpack/workshop/player/items/pyro/hw2013_py40_automaton/hw2013_py40_automaton",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_py40_automaton/hw2013_py40_automaton.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Electric Escorter",
		localized_name = "info.tf2pm.hat.robo_pyro_electric_escorter",
		localized_description = "info.tf2pm.hat.robo_pyro_electric_escorter_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_electric_escorter/robo_pyro_electric_escorter",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_electric_escorter/robo_pyro_electric_escorter.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Burning Bandana",
		localized_name = "info.tf2pm.hat.jul13_hot_rag",
		localized_description = "info.tf2pm.hat.jul13_hot_rag_desc",
		icon = "backpack/workshop/player/items/pyro/jul13_hot_rag/jul13_hot_rag",
		models = {
			["basename"] = "models/workshop/player/items/pyro/jul13_hot_rag/jul13_hot_rag.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Sole Mate",
		localized_name = "info.tf2pm.hat.sbox2014_sole_mate",
		icon = "backpack/workshop/player/items/pyro/sbox2014_sole_mate/sbox2014_sole_mate",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sbox2014_sole_mate/sbox2014_sole_mate.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "The Backpack Broiler",
		localized_name = "info.tf2pm.hat.jul13_furious_fryup",
		localized_description = "info.tf2pm.hat.jul13_furious_fryup_desc",
		icon = "backpack/workshop/player/items/pyro/jul13_furious_fryup/jul13_furious_fryup",
		models = {
			["basename"] = "models/workshop/player/items/pyro/jul13_furious_fryup/jul13_furious_fryup.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Rusty Reaper",
		localized_name = "info.tf2pm.hat.robo_pyro_last_watt",
		localized_description = "info.tf2pm.hat.robo_pyro_last_watt_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_last_watt/robo_pyro_last_watt",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_last_watt/robo_pyro_last_watt.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "The Creature From The Heap",
		localized_name = "info.tf2pm.hat.hw2013_the_creature_from_the_heap",
		icon = "backpack/workshop/player/items/pyro/hw2013_the_creature_from_the_heap/hw2013_the_creature_from_the_heap",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_the_creature_from_the_heap/hw2013_the_creature_from_the_heap.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "Neptune's Nightmare",
		localized_name = "info.tf2pm.hat.hwn2015_neptunes_nightmare",
		icon = "backpack/workshop/player/items/pyro/hwn2015_neptunes_nightmare/hwn2015_neptunes_nightmare",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2015_neptunes_nightmare/hwn2015_neptunes_nightmare.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt", "grenades"},
		paintable = true,
		name = "El Muchacho",
		localized_name = "info.tf2pm.hat.jul13_el_muchacho",
		localized_description = "info.tf2pm.hat.jul13_el_muchacho_desc",
		icon = "backpack/workshop/player/items/pyro/jul13_el_muchacho/jul13_el_muchacho",
		models = {
			["basename"] = "models/workshop/player/items/pyro/jul13_el_muchacho/jul13_el_muchacho.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = false,
		name = "The Hollowhead",
		localized_name = "info.tf2pm.hat.hw2013_hollowhead",
		icon = "backpack/workshop/player/items/pyro/hw2013_hollowhead/hw2013_hollowhead",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_hollowhead/hw2013_hollowhead.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Combustible Kabuto",
		localized_name = "info.tf2pm.hat.short2014_honnoji_helm",
		icon = "backpack/workshop/player/items/pyro/short2014_honnoji_helm/short2014_honnoji_helm",
		models = {
			["basename"] = "models/workshop/player/items/pyro/short2014_honnoji_helm/short2014_honnoji_helm.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pants"},
		paintable = true,
		name = "Flavorful Baggies",
		localized_name = "info.tf2pm.hat.hwn2020_flavorful_baggies",
		icon = "backpack/workshop/player/items/pyro/hwn2020_flavorful_baggies/hwn2020_flavorful_baggies",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2020_flavorful_baggies/hwn2020_flavorful_baggies.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = false,
		name = "The Frymaster",
		localized_name = "info.tf2pm.hat.short2014_fowl_fryer",
		icon = "backpack/workshop/player/items/pyro/short2014_fowl_fryer/short2014_fowl_fryer",
		models = {
			["basename"] = "models/workshop/player/items/pyro/short2014_fowl_fryer/short2014_fowl_fryer.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "The Scrap Sack",
		localized_name = "info.tf2pm.hat.robo_pyro_pyrobotic_tote",
		localized_description = "info.tf2pm.hat.robo_pyro_pyrobotic_tote_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_pyrobotic_tote/robo_pyro_pyrobotic_tote",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_pyrobotic_tote/robo_pyro_pyrobotic_tote.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Rugged Respirator",
		localized_name = "info.tf2pm.hat.hw2013_rugged_respirator",
		icon = "backpack/workshop/player/items/pyro/hw2013_rugged_respirator/hw2013_rugged_respirator",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_rugged_respirator/hw2013_rugged_respirator.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Hive Minder",
		localized_name = "info.tf2pm.hat.jul13_bee_keeper",
		localized_description = "info.tf2pm.hat.jul13_bee_keeper_desc",
		icon = "backpack/workshop/player/items/pyro/jul13_bee_keeper/jul13_bee_keeper",
		models = {
			["basename"] = "models/workshop/player/items/pyro/jul13_bee_keeper/jul13_bee_keeper.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Pyr'o Lantern",
		localized_name = "info.tf2pm.hat.hwn2019_pyro_lantern",
		icon = "backpack/workshop/player/items/pyro/hwn2019_pyro_lantern/hwn2019_pyro_lantern",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2019_pyro_lantern/hwn2019_pyro_lantern.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"whole_head", "pyro_head_replacement"},
		paintable = true,
		name = "The Gothic Guise",
		localized_name = "info.tf2pm.hat.hw2013_gothic_guise",
		icon = "backpack/workshop/player/items/pyro/hw2013_gothic_guise/hw2013_gothic_guise",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_gothic_guise/hw2013_gothic_guise.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"face"},
		paintable = false,
		name = "The Face of Mercy",
		localized_name = "info.tf2pm.hat.hwn2015_face_of_mercy",
		icon = "backpack/workshop/player/items/pyro/hwn2015_face_of_mercy/hwn2015_face_of_mercy",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2015_face_of_mercy/hwn2015_face_of_mercy.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Plumber's Pipe",
		localized_name = "info.tf2pm.hat.robo_pyro_prancers_pride",
		localized_description = "info.tf2pm.hat.robo_pyro_prancers_pride_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_prancers_pride/robo_pyro_prancers_pride",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_prancers_pride/robo_pyro_prancers_pride.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"glasses"},
		paintable = true,
		name = "Pyro in Chinatown",
		localized_name = "info.tf2pm.hat.hwn2018_pyro_in_chinatown",
		icon = "backpack/workshop/player/items/pyro/hwn2018_pyro_in_chinatown/hwn2018_pyro_in_chinatown",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2018_pyro_in_chinatown/hwn2018_pyro_in_chinatown.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2018_pyro_in_chinatown_style1",
				models = {
					["basename"] = "models/workshop/player/items/pyro/hwn2018_pyro_in_chinatown/hwn2018_pyro_in_chinatown.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2018_pyro_in_chinatown_style2",
				models = {
					["basename"] = "models/workshop/player/items/pyro/hwn2018_pyro_in_little_chinatown/hwn2018_pyro_in_little_chinatown.mdl",
				}
			},
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Mishap Mercenary",
		localized_name = "info.tf2pm.hat.sept2014_pyro_radioactive_mask",
		localized_description = "info.tf2pm.hat.sept2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/pyro/sept2014_pyro_radioactive_mask/sept2014_pyro_radioactive_mask",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sept2014_pyro_radioactive_mask/sept2014_pyro_radioactive_mask.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement", "hat"},
		paintable = true,
		name = "The Corpsemopolitan",
		localized_name = "info.tf2pm.hat.hw2013_corpsemopolitan",
		icon = "backpack/workshop/player/items/pyro/hw2013_corpsemopolitan/hw2013_corpsemopolitan",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_corpsemopolitan/hw2013_corpsemopolitan.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Fire Tooth",
		localized_name = "info.tf2pm.hat.hwn2020_fire_tooth",
		icon = "backpack/workshop/player/items/pyro/hwn2020_fire_tooth/hwn2020_fire_tooth",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2020_fire_tooth/hwn2020_fire_tooth.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Cat's Pajamas",
		localized_name = "info.tf2pm.hat.dec17_cats_pajamas",
		icon = "backpack/workshop/player/items/pyro/dec17_cats_pajamas/dec17_cats_pajamas",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec17_cats_pajamas/dec17_cats_pajamas.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "tw_pyrobot_helmet",
		localized_name = "info.tf2pm.hat.tw_pyrobot_helmet",
		icon = "backpack/workshop/player/items/pyro/tw_pyrobot_helmet/tw_pyrobot_helmet",
		models = {
			["basename"] = "models/workshop/player/items/pyro/tw_pyrobot_helmet/tw_pyrobot_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Employee of the Mmmph",
		localized_name = "info.tf2pm.hat.short2014_pyro_chickenhat",
		icon = "backpack/workshop/player/items/pyro/short2014_pyro_chickenhat/short2014_pyro_chickenhat",
		models = {
			["basename"] = "models/workshop/player/items/pyro/short2014_pyro_chickenhat/short2014_pyro_chickenhat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Seared Sorcerer",
		localized_name = "info.tf2pm.hat.hwn2020_seared_sorcerer",
		icon = "backpack/workshop/player/items/pyro/hwn2020_seared_sorcerer/hwn2020_seared_sorcerer",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2020_seared_sorcerer/hwn2020_seared_sorcerer.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2020_seared_sorcerer_style1",
				bodygroup_overrides = {
					["head"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/pyro/hwn2020_seared_sorcerer/hwn2020_seared_sorcerer.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2020_seared_sorcerer_style2",
				models = {
					["basename"] = "models/workshop/player/items/pyro/hwn2020_seared_sorcerer_style2/hwn2020_seared_sorcerer_style2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = false,
		name = "tw_pyrobot_armor",
		localized_name = "info.tf2pm.hat.tw_pyrobot_armor",
		icon = "backpack/workshop/player/items/pyro/tw_pyrobot_armor/tw_pyrobot_armor",
		models = {
			["basename"] = "models/workshop/player/items/pyro/tw_pyrobot_armor/tw_pyrobot_armor.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Pyro Shark",
		localized_name = "info.tf2pm.hat.hwn2019_pyro_shark",
		icon = "backpack/workshop/player/items/pyro/hwn2019_pyro_shark/hwn2019_pyro_shark",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2019_pyro_shark/hwn2019_pyro_shark.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Treehugger",
		localized_name = "info.tf2pm.hat.hwn2020_treehugger",
		icon = "backpack/workshop/player/items/pyro/hwn2020_treehugger/hwn2020_treehugger",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2020_treehugger/hwn2020_treehugger.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Mair Mask",
		localized_name = "info.tf2pm.hat.hazeguard",
		localized_description = "info.tf2pm.hat.hazeguard_desc",
		icon = "backpack/workshop/player/items/pyro/hazeguard/hazeguard",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hazeguard/hazeguard.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Vicious Visage",
		localized_name = "info.tf2pm.hat.hw2013_head_of_the_lake_monster",
		icon = "backpack/workshop/player/items/pyro/hw2013_head_of_the_lake_monster/hw2013_head_of_the_lake_monster",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_head_of_the_lake_monster/hw2013_head_of_the_lake_monster.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement", "zombie_body"},
		paintable = false,
		name = "Zombie Pyro",
		localized_name = "info.tf2pm.hat.item_zombiepyro",
		icon = "backpack/player/items/pyro/pyro_zombie",
		models = {
			["basename"] = "models/player/items/pyro/pyro_zombie.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Skullbrero",
		localized_name = "info.tf2pm.hat.hwn2019_skullbrero",
		icon = "backpack/workshop/player/items/pyro/hwn2019_skullbrero/hwn2019_skullbrero",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2019_skullbrero/hwn2019_skullbrero.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = true,
		name = "EOTL_pyro_sweater",
		localized_name = "info.tf2pm.hat.eotl_pyro_sweater",
		icon = "backpack/workshop/player/items/pyro/pyro_sweater/pyro_sweater",
		models = {
			["basename"] = "models/workshop/player/items/pyro/pyro_sweater/pyro_sweater.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Arachno-Arsonist",
		localized_name = "info.tf2pm.hat.hwn2018_arachno_arsonist",
		icon = "backpack/workshop/player/items/pyro/hwn2018_arachno_arsonist/hwn2018_arachno_arsonist",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2018_arachno_arsonist/hwn2018_arachno_arsonist.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Vampyro",
		localized_name = "info.tf2pm.hat.sf14_vampyro",
		icon = "backpack/workshop/player/items/pyro/sf14_vampyro/sf14_vampyro",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sf14_vampyro/sf14_vampyro.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Feathered Fiend",
		localized_name = "info.tf2pm.hat.fall17_feathered_fiend",
		localized_description = "info.tf2pm.hat.fall17_feathered_fiend_desc",
		icon = "backpack/workshop/player/items/pyro/fall17_feathered_fiend/fall17_feathered_fiend",
		models = {
			["basename"] = "models/workshop/player/items/pyro/fall17_feathered_fiend/fall17_feathered_fiend.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Pampered Pyro",
		localized_name = "info.tf2pm.hat.jul13_pyro_towel",
		localized_description = "info.tf2pm.hat.jul13_pyro_towel_desc",
		icon = "backpack/workshop/player/items/pyro/jul13_pyro_towel/jul13_pyro_towel",
		models = {
			["basename"] = "models/workshop/player/items/pyro/jul13_pyro_towel/jul13_pyro_towel.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"glasses"},
		paintable = true,
		name = "Up Pyroscopes",
		localized_name = "info.tf2pm.hat.hw2013_per_eye_scopes",
		icon = "backpack/workshop/player/items/pyro/hw2013_per_eye_scopes/hw2013_per_eye_scopes",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_per_eye_scopes/hw2013_per_eye_scopes.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Mr. Quackers",
		localized_name = "info.tf2pm.hat.hwn2018_mr_quackers",
		icon = "backpack/workshop/player/items/pyro/hwn2018_mr_quackers/hwn2018_mr_quackers",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2018_mr_quackers/hwn2018_mr_quackers.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "The Crispy Golden Locks",
		localized_name = "info.tf2pm.hat.hw2013_golden_crisp_locks",
		icon = "backpack/workshop/player/items/pyro/hw2013_golden_crisp_locks/hw2013_golden_crisp_locks",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_golden_crisp_locks/hw2013_golden_crisp_locks.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "The Grisly Gumbo",
		localized_name = "info.tf2pm.hat.hw2013_gristly_gumbo_pot",
		icon = "backpack/workshop/player/items/pyro/hw2013_gristly_gumbo_pot/hw2013_gristly_gumbo_pot",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_gristly_gumbo_pot/hw2013_gristly_gumbo_pot.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "The Handhunter",
		localized_name = "info.tf2pm.hat.hw2013_handhunter",
		icon = "backpack/workshop/player/items/pyro/hw2013_handhunter/hw2013_handhunter",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_handhunter/hw2013_handhunter.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "Burning Beanie",
		localized_name = "info.tf2pm.hat.dec17_burning_beanie",
		icon = "backpack/workshop/player/items/pyro/dec17_burning_beanie/dec17_burning_beanie",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec17_burning_beanie/dec17_burning_beanie.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"Back"},
		paintable = true,
		name = "Hovering Hotshot",
		localized_name = "info.tf2pm.hat.hwn2016_hovering_hotshot",
		icon = "backpack/workshop/player/items/pyro/hwn2016_hovering_hotshot/hwn2016_hovering_hotshot",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2016_hovering_hotshot/hwn2016_hovering_hotshot.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Mr. Juice",
		localized_name = "info.tf2pm.hat.sf14_mr_juice",
		icon = "backpack/workshop/player/items/pyro/sf14_mr_juice/sf14_mr_juice",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sf14_mr_juice/sf14_mr_juice.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Airtight Arsonist",
		localized_name = "info.tf2pm.hat.spr17_airtight_arsonist",
		localized_description = "info.tf2pm.hat.spr17_airtight_arsonist_desc",
		icon = "backpack/workshop/player/items/pyro/spr17_airtight_arsonist/spr17_airtight_arsonist",
		models = {
			["basename"] = "models/workshop/player/items/pyro/spr17_airtight_arsonist/spr17_airtight_arsonist.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Burny's Boney Bonnet",
		localized_name = "info.tf2pm.hat.hw2013_dragon_hood",
		icon = "backpack/workshop/player/items/pyro/hw2013_dragon_hood/hw2013_dragon_hood",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_dragon_hood/hw2013_dragon_hood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"lenses"},
		paintable = false,
		name = "The Googol Glass Eyes",
		localized_name = "info.tf2pm.hat.robo_pyro_site_for_sore_eyes",
		localized_description = "info.tf2pm.hat.robo_pyro_site_for_sore_eyes_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_site_for_sore_eyes/robo_pyro_site_for_sore_eyes",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_site_for_sore_eyes/robo_pyro_site_for_sore_eyes.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Cranial Carcharodon",
		localized_name = "info.tf2pm.hat.hwn2016_pyro_shark",
		icon = "backpack/workshop/player/items/pyro/hwn2016_pyro_shark/hwn2016_pyro_shark",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2016_pyro_shark/hwn2016_pyro_shark.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = false,
		name = "The Scorched Skirt",
		localized_name = "info.tf2pm.hat.hw2013_scorched_skirt",
		icon = "backpack/workshop/player/items/pyro/hw2013_scorched_skirt/hw2013_scorched_skirt",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_scorched_skirt/hw2013_scorched_skirt.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Carrion Companion",
		localized_name = "info.tf2pm.hat.hw2013_carrion_cohort",
		icon = "backpack/workshop/player/items/pyro/hw2013_carrion_cohort/hw2013_carrion_cohort",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_carrion_cohort/hw2013_carrion_cohort.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "Pyro's Boron Beanie",
		localized_name = "info.tf2pm.hat.robo_pyro_whirly_bird",
		localized_description = "info.tf2pm.hat.robo_pyro_whirly_bird_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_whirly_bird/robo_pyro_whirly_bird",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_whirly_bird/robo_pyro_whirly_bird.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_spikes"},
		paintable = false,
		name = "The Maniac's Manacles",
		localized_name = "info.tf2pm.hat.hw2013_maniacs_manacles",
		icon = "backpack/workshop/player/items/pyro/hw2013_maniacs_manacles/hw2013_maniacs_manacles",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_maniacs_manacles/hw2013_maniacs_manacles.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Combustible Cutie",
		localized_name = "info.tf2pm.hat.hwn2016_combustible_cutie",
		icon = "backpack/workshop/player/items/pyro/hwn2016_combustible_cutie/hwn2016_combustible_cutie",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2016_combustible_cutie/hwn2016_combustible_cutie.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Head of the Dead",
		localized_name = "info.tf2pm.hat.hwn2019_head_of_the_dead",
		icon = "backpack/workshop/player/items/pyro/hwn2019_head_of_the_dead/hwn2019_head_of_the_dead",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2019_head_of_the_dead/hwn2019_head_of_the_dead.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2019_head_of_the_dead_style0",
				models = {
					["basename"] = "models/workshop/player/items/pyro/hwn2019_head_of_the_dead/hwn2019_head_of_the_dead.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2019_head_of_the_dead_style1",
				models = {
					["basename"] = "models/workshop/player/items/pyro/hwn2019_head_of_the_dead_style2/hwn2019_head_of_the_dead_style2.mdl",
				}
			},
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "Tiny Timber",
		localized_name = "info.tf2pm.hat.xms2013_pyro_wood",
		icon = "backpack/workshop/player/items/pyro/xms2013_pyro_wood/xms2013_pyro_wood",
		models = {
			["basename"] = "models/workshop/player/items/pyro/xms2013_pyro_wood/xms2013_pyro_wood.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Fear Monger",
		localized_name = "info.tf2pm.hat.bak_fear_monger",
		icon = "backpack/workshop/player/items/pyro/bak_fear_monger/bak_fear_monger",
		models = {
			["basename"] = "models/workshop/player/items/pyro/bak_fear_monger/bak_fear_monger.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.bak_fear_monger_style1",
				bodygroup_overrides = {
					["head"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/pyro/bak_fear_monger/bak_fear_monger.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.bak_fear_monger_style2",
				models = {
					["basename"] = "models/workshop/player/items/pyro/bak_fear_monger_s1/bak_fear_monger_s1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["head"] = 0,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Metal Slug",
		localized_name = "info.tf2pm.hat.robo_pyro_tribtrojan",
		localized_description = "info.tf2pm.hat.robo_pyro_tribtrojan_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_tribtrojan/robo_pyro_tribtrojan",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_tribtrojan/robo_pyro_tribtrojan.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"whole_head", "pyro_head_replacement"},
		paintable = true,
		name = "The Macabre Mask",
		localized_name = "info.tf2pm.hat.hw2013_the_creeps_cowl",
		icon = "backpack/workshop/player/items/pyro/hw2013_the_creeps_cowl/hw2013_the_creeps_cowl",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_the_creeps_cowl/hw2013_the_creeps_cowl.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Phobos Filter",
		localized_name = "info.tf2pm.hat.invasion_phobos_filter",
		icon = "backpack/workshop/player/items/pyro/invasion_phobos_filter/invasion_phobos_filter",
		models = {
			["basename"] = "models/workshop/player/items/pyro/invasion_phobos_filter/invasion_phobos_filter.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Burning Question",
		localized_name = "info.tf2pm.hat.spr18_burning_question",
		icon = "backpack/workshop/player/items/pyro/spr18_burning_question/spr18_burning_question",
		models = {
			["basename"] = "models/workshop/player/items/pyro/spr18_burning_question/spr18_burning_question.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = false,
		name = "The Spectralnaut",
		localized_name = "info.tf2pm.hat.hw2013_space_oddity",
		icon = "backpack/workshop/player/items/pyro/hw2013_space_oddity/hw2013_space_oddity",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_space_oddity/hw2013_space_oddity.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt", "grenades"},
		paintable = true,
		name = "The Lunatic's Leathers",
		localized_name = "info.tf2pm.hat.short2014_wildfire_wrappers",
		icon = "backpack/workshop/player/items/pyro/short2014_wildfire_wrappers/short2014_wildfire_wrappers",
		models = {
			["basename"] = "models/workshop/player/items/pyro/short2014_wildfire_wrappers/short2014_wildfire_wrappers.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = true,
		name = "The Space Diver",
		localized_name = "info.tf2pm.hat.invasion_the_space_diver",
		icon = "backpack/workshop/player/items/pyro/invasion_the_space_diver/invasion_the_space_diver",
		models = {
			["basename"] = "models/workshop/player/items/pyro/invasion_the_space_diver/invasion_the_space_diver.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"feet"},
		paintable = true,
		name = "The Monster's Stompers",
		localized_name = "info.tf2pm.hat.hw2013_dragon_shoes",
		icon = "backpack/workshop/player/items/pyro/hw2013_dragon_shoes/hw2013_dragon_shoes",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_dragon_shoes/hw2013_dragon_shoes.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = true,
		name = "The Hot Case",
		localized_name = "info.tf2pm.hat.spr18_hot_case",
		icon = "backpack/workshop/player/items/pyro/spr18_hot_case/spr18_hot_case",
		models = {
			["basename"] = "models/workshop/player/items/pyro/spr18_hot_case/spr18_hot_case.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "The Gas Guzzler",
		localized_name = "info.tf2pm.hat.short2014_the_gas_guzzler",
		icon = "backpack/workshop/player/items/pyro/short2014_the_gas_guzzler/short2014_the_gas_guzzler",
		models = {
			["basename"] = "models/workshop/player/items/pyro/short2014_the_gas_guzzler/short2014_the_gas_guzzler.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Smoking Skid Lid",
		localized_name = "info.tf2pm.hat.short2014_spiked_armourgeddon",
		icon = "backpack/workshop/player/items/pyro/short2014_spiked_armourgeddon/short2014_spiked_armourgeddon",
		models = {
			["basename"] = "models/workshop/player/items/pyro/short2014_spiked_armourgeddon/short2014_spiked_armourgeddon.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"shirt"},
		paintable = false,
		name = "Wanderer's Wear",
		localized_name = "info.tf2pm.hat.sum19_spawn_camper_jacket",
		icon = "backpack/workshop/player/items/pyro/sum19_spawn_camper_jacket/sum19_spawn_camper_jacket",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sum19_spawn_camper_jacket/sum19_spawn_camper_jacket.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "Jupiter Jetpack",
		localized_name = "info.tf2pm.hat.invasion_jupiter_jetpack",
		icon = "backpack/workshop/player/items/pyro/invasion_jupiter_jetpack/invasion_jupiter_jetpack",
		models = {
			["basename"] = "models/workshop/player/items/pyro/invasion_jupiter_jetpack/invasion_jupiter_jetpack.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Firefly",
		localized_name = "info.tf2pm.hat.bak_firefly",
		icon = "backpack/workshop/player/items/pyro/bak_firefly/bak_firefly",
		models = {
			["basename"] = "models/workshop/player/items/pyro/bak_firefly/bak_firefly.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "The External Organ",
		localized_name = "info.tf2pm.hat.hw2013_dark_orchestra",
		icon = "backpack/workshop/player/items/pyro/hw2013_dark_orchestra/hw2013_dark_orchestra",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_dark_orchestra/hw2013_dark_orchestra.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "A Head Full of Hot Air",
		localized_name = "info.tf2pm.hat.invasion_a_head_full_of_hot_air",
		icon = "backpack/workshop/player/items/pyro/invasion_a_head_full_of_hot_air/invasion_a_head_full_of_hot_air",
		models = {
			["basename"] = "models/workshop/player/items/pyro/invasion_a_head_full_of_hot_air/invasion_a_head_full_of_hot_air.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"grenades"},
		paintable = false,
		name = "The Death Support Pack",
		localized_name = "info.tf2pm.hat.hw2013_tin_can",
		icon = "backpack/workshop/player/items/pyro/hw2013_tin_can/hw2013_tin_can",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_tin_can/hw2013_tin_can.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"back"},
		paintable = true,
		name = "Spawn Camper",
		localized_name = "info.tf2pm.hat.sum19_spawn_camper_backpack",
		icon = "backpack/workshop/player/items/pyro/sum19_spawn_camper_backpack/sum19_spawn_camper_backpack",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sum19_spawn_camper_backpack/sum19_spawn_camper_backpack.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Glob",
		localized_name = "info.tf2pm.hat.hw2013_the_glob",
		icon = "backpack/workshop/player/items/pyro/hw2013_the_glob/hw2013_the_glob",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hw2013_the_glob/hw2013_the_glob.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"lenses"},
		paintable = true,
		name = "Pop-eyes",
		localized_name = "info.tf2pm.hat.fall2013_popeyes",
		icon = "backpack/workshop/player/items/pyro/fall2013_popeyes/fall2013_popeyes",
		models = {
			["basename"] = "models/workshop/player/items/pyro/fall2013_popeyes/fall2013_popeyes.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Lollichop Licker",
		localized_name = "info.tf2pm.hat.sf14_lollichop_licker",
		icon = "backpack/workshop/player/items/pyro/sf14_lollichop_licker/sf14_lollichop_licker",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sf14_lollichop_licker/sf14_lollichop_licker.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"hat"},
		paintable = true,
		name = "The Bolted Birdcage",
		localized_name = "info.tf2pm.hat.robo_pyro_birdcage",
		localized_description = "info.tf2pm.hat.robo_pyro_birdcage_desc",
		icon = "backpack/workshop/player/items/pyro/robo_pyro_birdcage/robo_pyro_birdcage",
		models = {
			["basename"] = "models/workshop/player/items/pyro/robo_pyro_birdcage/robo_pyro_birdcage.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Promo The Firefly",
		localized_name = "info.tf2pm.hat.bak_firefly",
		icon = "backpack/workshop/player/items/pyro/bak_firefly/bak_firefly",
		models = {
			["basename"] = "models/workshop/player/items/pyro/bak_firefly/bak_firefly.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Promo Fear Monger",
		localized_name = "info.tf2pm.hat.bak_fear_monger",
		icon = "backpack/workshop/player/items/pyro/bak_fear_monger/bak_fear_monger",
		models = {
			["basename"] = "models/workshop/player/items/pyro/bak_fear_monger/bak_fear_monger.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.bak_fear_monger_style1",
				bodygroup_overrides = {
					["head"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/pyro/bak_fear_monger/bak_fear_monger.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.bak_fear_monger_style2",
				models = {
					["basename"] = "models/workshop/player/items/pyro/bak_fear_monger_s1/bak_fear_monger_s1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["head"] = 0,
		},
	},
	{
		classes = {"pyro"},
		targets = {"feet"},
		paintable = true,
		name = "Moccasin Machinery",
		localized_name = "info.tf2pm.hat.sf14_hw2014_robot_legg",
		icon = "backpack/workshop/player/items/pyro/sf14_hw2014_robot_legg/sf14_hw2014_robot_legg",
		models = {
			["basename"] = "models/workshop/player/items/pyro/sf14_hw2014_robot_legg/sf14_hw2014_robot_legg.mdl",
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "EOTL_Skier",
		localized_name = "info.tf2pm.hat.eotl_skier",
		icon = "backpack/workshop/player/items/pyro/skier/skier",
		models = {
			["basename"] = "models/workshop/player/items/pyro/skier/skier.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "Candy Cranium",
		localized_name = "info.tf2pm.hat.hwn2019_candycranium",
		icon = "backpack/workshop/player/items/pyro/hwn2019_candycranium/hwn2019_candycranium",
		models = {
			["basename"] = "models/workshop/player/items/pyro/hwn2019_candycranium/hwn2019_candycranium.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"pyro"},
		targets = {"pyro_head_replacement"},
		paintable = false,
		name = "dec2014 Black Knights Bascinet",
		localized_name = "info.tf2pm.hat.dec2014_black_knights_bascinet",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/pyro/dec2014_black_knights_bascinet/dec2014_black_knights_bascinet",
		models = {
			["basename"] = "models/workshop/player/items/pyro/dec2014_black_knights_bascinet/dec2014_black_knights_bascinet.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},

}
