
-- Auto generated at 2020-12-04 12:56:46 UTC+07:00
return {
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Football Helmet",
		localized_name = "info.tf2pm.hat.heavy_hat_1",
		icon = "backpack/player/items/heavy/football_helmet",
		models = {
			["basename"] = "models/player/items/heavy/football_helmet.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Officer's Ushanka",
		localized_name = "info.tf2pm.hat.heavy_ushanka_hat",
		localized_description = "info.tf2pm.hat.heavy_ushanka_hat_desc",
		icon = "backpack/player/items/heavy/heavy_ushanka",
		models = {
			["basename"] = "models/player/items/heavy/heavy_ushanka.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Tough Guy's Toque",
		localized_name = "info.tf2pm.hat.heavy_stocking_cap",
		localized_description = "info.tf2pm.hat.heavy_stocking_cap_desc",
		icon = "backpack/player/items/heavy/heavy_stocking_cap",
		models = {
			["basename"] = "models/player/items/heavy/heavy_stocking_cap.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"glasses", "hat"},
		paintable = true,
		name = "Heavy Hair",
		localized_name = "info.tf2pm.hat.heavyhair",
		localized_description = "info.tf2pm.hat.heavyhair_desc",
		icon = "backpack/player/items/heavy/hounddog",
		models = {
			["basename"] = "models/player/items/heavy/hounddog.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Heavy Do-rag",
		localized_name = "info.tf2pm.hat.heavydorag",
		localized_description = "info.tf2pm.hat.heavydorag_desc",
		icon = "backpack/player/items/heavy/heavy_bandana",
		models = {
			["basename"] = "models/player/items/heavy/heavy_bandana.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Pugilist's Protector",
		localized_name = "info.tf2pm.hat.heavypugilistprotector",
		icon = "backpack/player/items/heavy/pugilist_protector",
		models = {
			["basename"] = "models/player/items/heavy/pugilist_protector.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Hard Counter",
		localized_name = "info.tf2pm.hat.heavyumbrella",
		localized_description = "info.tf2pm.hat.heavyumbrella_desc",
		icon = "backpack/player/items/heavy/heavy_umbrella",
		models = {
			["basename"] = "models/player/items/heavy/heavy_umbrella.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "Cadaver's Cranium",
		localized_name = "info.tf2pm.hat.cadavers_cranium",
		icon = "backpack/player/items/heavy/cadavers_cranium",
		models = {
			["basename"] = "models/player/items/heavy/cadavers_cranium.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Poker Visor",
		localized_name = "info.tf2pm.hat.ttg_pokervisor",
		localized_description = "info.tf2pm.hat.ttg_pokervisor_desc",
		icon = "backpack/player/items/heavy/ttg_visor",
		models = {
			["basename"] = "models/player/items/heavy/ttg_visor.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "Big Chief",
		localized_name = "info.tf2pm.hat.bigchief",
		icon = "backpack/player/items/heavy/heavy_big_chief",
		models = {
			["basename"] = "models/player/items/heavy/heavy_big_chief.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Magnificent Mongolian",
		localized_name = "info.tf2pm.hat.magnificentmongolian",
		icon = "backpack/player/items/heavy/heavy_magnificent_mongolian",
		models = {
			["basename"] = "models/player/items/heavy/heavy_magnificent_mongolian.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Coupe D'isaster",
		localized_name = "info.tf2pm.hat.coupedisaster",
		icon = "backpack/player/items/heavy/coupe_disaster",
		models = {
			["basename"] = "models/player/items/heavy/coupe_disaster.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "Heavy Topknot",
		localized_name = "info.tf2pm.hat.heavytopknot",
		localized_description = "info.tf2pm.hat.heavytopknot_desc",
		icon = "backpack/workshop_partner/player/items/heavy/heavy_shogun_topknot/heavy_shogun_topknot",
		models = {
			["basename"] = "models/workshop_partner/player/items/heavy/heavy_shogun_topknot/heavy_shogun_topknot.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"head_skin"},
		paintable = true,
		name = "Large Luchadore",
		localized_name = "info.tf2pm.hat.largeluchadore",
		localized_description = "info.tf2pm.hat.largeluchadore_desc",
		icon = "backpack/player/items/heavy/wrestling_mask",
		models = {
			["basename"] = "models/player/items/heavy/wrestling_mask.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.luchadore_style0",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.luchadore_style1",
			},
			[2] = {
				responsive_skins = {4, 5},
				localized_name = "info.tf2pm.hat.luchadore_style2",
			},
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Capone's Capper",
		localized_name = "info.tf2pm.hat.caponescapper",
		localized_description = "info.tf2pm.hat.caponescapper_desc",
		icon = "backpack/player/items/heavy/capones_capper",
		models = {
			["basename"] = "models/player/items/heavy/capones_capper.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "Copper's Hard Top",
		localized_name = "info.tf2pm.hat.cophelmet",
		icon = "backpack/player/items/heavy/cop_helmet",
		models = {
			["basename"] = "models/player/items/heavy/cop_helmet.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.cophelmet_style0",
			},
			[1] = {
				responsive_skins = {2, 2},
				localized_name = "info.tf2pm.hat.cophelmet_style1",
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"glasses"},
		paintable = false,
		name = "Security Shades",
		localized_name = "info.tf2pm.hat.copglasses",
		icon = "backpack/player/items/heavy/cop_glasses",
		models = {
			["basename"] = "models/player/items/heavy/cop_glasses.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"beard"},
		paintable = false,
		name = "Big Steel Jaw of Summer Fun",
		localized_name = "info.tf2pm.hat.steeljaw",
		localized_description = "info.tf2pm.hat.steeljaw_desc",
		icon = "backpack/player/items/heavy/big_jaw",
		models = {
			["basename"] = "models/player/items/heavy/big_jaw.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Pilotka",
		localized_name = "info.tf2pm.hat.pilotka",
		icon = "backpack/player/items/heavy/ro_pilotka",
		models = {
			["basename"] = "models/player/items/heavy/ro_pilotka.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"face", "hat"},
		paintable = false,
		name = "Dragonborn Helmet",
		localized_name = "info.tf2pm.hat.dragonbornhelmet",
		localized_description = "info.tf2pm.hat.dragonbornhelmet_desc",
		icon = "backpack/workshop_partner/player/items/heavy/skyrim_helmet/skyrim_helmet",
		models = {
			["basename"] = "models/workshop_partner/player/items/heavy/skyrim_helmet/skyrim_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"arms"},
		paintable = false,
		name = "The Purity Fist",
		localized_name = "info.tf2pm.hat.dex_arm",
		icon = "backpack/workshop_partner/player/items/heavy/dex_sarifarm/dex_sarifarm",
		models = {
			["basename"] = "models/workshop_partner/player/items/heavy/dex_sarifarm/dex_sarifarm.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "Storm Spirit's Jolly Hat",
		localized_name = "info.tf2pm.hat.stormspirit_hat",
		localized_description = "info.tf2pm.hat.dotagamescom2011_hat_desc",
		icon = "backpack/player/items/heavy/stormspirit_hat",
		models = {
			["basename"] = "models/player/items/heavy/stormspirit_hat.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "Can Opener",
		localized_name = "info.tf2pm.hat.hwn_heavyhat",
		icon = "backpack/player/items/heavy/hwn_heavy_hat",
		models = {
			["basename"] = "models/player/items/heavy/hwn_heavy_hat.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"sleeves"},
		paintable = false,
		name = "Soviet Stitch-Up",
		localized_name = "info.tf2pm.hat.hwn_heavymisc1",
		icon = "backpack/player/items/heavy/hwn_heavy_misc1",
		models = {
			["basename"] = "models/player/items/heavy/hwn_heavy_misc1.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"feet"},
		paintable = false,
		name = "Steel-Toed Stompers",
		localized_name = "info.tf2pm.hat.hwn_heavymisc2",
		icon = "backpack/player/items/heavy/hwn_heavy_misc2",
		models = {
			["basename"] = "models/player/items/heavy/hwn_heavy_misc2.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"head_skin"},
		paintable = true,
		name = "Cold War Luchador",
		localized_name = "info.tf2pm.hat.luchador",
		localized_description = "info.tf2pm.hat.luchador_desc",
		icon = "backpack/player/items/heavy/sr3_heavy_mask",
		models = {
			["basename"] = "models/player/items/heavy/sr3_heavy_mask.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The One-Man Army",
		localized_name = "info.tf2pm.hat.heavyhat1",
		localized_description = "info.tf2pm.hat.heavyhat1_desc",
		icon = "backpack/workshop/player/items/heavy/fwk_heavy_bandanahair/fwk_heavy_bandanahair",
		models = {
			["basename"] = "models/workshop/player/items/heavy/fwk_heavy_bandanahair/fwk_heavy_bandanahair.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.heavyhat1_style0",
				models = {
					["basename"] = "models/workshop/player/items/heavy/fwk_heavy_bandanahair/fwk_heavy_bandanahair.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.heavyhat1_style1",
				models = {
					["basename"] = "models/workshop/player/items/heavy/fwk_heavy_bandanahair_s2/fwk_heavy_bandanahair_s2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Outdoorsman",
		localized_name = "info.tf2pm.hat.heavyhat2",
		localized_description = "info.tf2pm.hat.heavyhat2_desc",
		icon = "backpack/player/items/heavy/fwk_heavy_lumber",
		models = {
			["basename"] = "models/player/items/heavy/fwk_heavy_lumber.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Gym Rat",
		localized_name = "info.tf2pm.hat.heavyhat3",
		localized_description = "info.tf2pm.hat.heavyhat3_desc",
		icon = "backpack/player/items/heavy/fwk_heavy_gym",
		models = {
			["basename"] = "models/player/items/heavy/fwk_heavy_gym.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "War Head",
		localized_name = "info.tf2pm.hat.trnhelmet",
		localized_description = "info.tf2pm.hat.trnhelmet_desc",
		icon = "backpack/player/items/heavy/trn_heavy_knight",
		models = {
			["basename"] = "models/player/items/heavy/trn_heavy_knight.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_hip"},
		paintable = false,
		name = "The Sandvich Safe",
		localized_name = "info.tf2pm.hat.sandvichsafe",
		localized_description = "info.tf2pm.hat.sandvichsafe_desc",
		icon = "backpack/player/items/heavy/xms_heavy_sandvichsafe",
		models = {
			["basename"] = "models/player/items/heavy/xms_heavy_sandvichsafe.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_towel"},
		paintable = true,
		name = "The Toss-Proof Towel",
		localized_name = "info.tf2pm.hat.heavyboxingtowel",
		localized_description = "info.tf2pm.hat.heavyboxingtowel_desc",
		icon = "backpack/player/items/heavy/heavy_boxingtowel",
		models = {
			["basename"] = "models/player/items/heavy/heavy_boxingtowel.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt", "arm_tatoos"},
		paintable = false,
		name = "The Apparatchik's Apparel",
		localized_name = "info.tf2pm.hat.heavyshirt",
		localized_description = "info.tf2pm.hat.heavyshirt_desc",
		icon = "backpack/player/items/heavy/heavy_shirt",
		models = {
			["basename"] = "models/player/items/heavy/heavy_shirt.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.heavyshirt_style0",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.heavyshirt_style1",
			},
			[2] = {
				responsive_skins = {4, 5},
				localized_name = "info.tf2pm.hat.heavyshirt_style2",
			},
			[3] = {
				responsive_skins = {6, 7},
				localized_name = "info.tf2pm.hat.heavyshirt_style3",
			},
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Soviet Gentleman",
		localized_name = "info.tf2pm.hat.heavymustachehat",
		localized_description = "info.tf2pm.hat.heavymustachehat_desc",
		icon = "backpack/workshop/player/items/heavy/mustachehat/mustachehat",
		models = {
			["basename"] = "models/workshop/player/items/heavy/mustachehat/mustachehat.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.heavymustachehat_style1",
				models = {
					["basename"] = "models/workshop/player/items/heavy/mustachehat/mustachehat.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.heavymustachehat_style2",
				models = {
					["basename"] = "models/workshop/player/items/heavy/mustachehat_s2/mustachehat_s2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The U-clank-a",
		localized_name = "info.tf2pm.hat.heavy_robot_ushanka_hat",
		localized_description = "info.tf2pm.hat.heavy_robot_ushanka_hat_desc",
		icon = "backpack/player/items/mvm_loot/heavy/robo_ushanka",
		models = {
			["basename"] = "models/player/items/mvm_loot/heavy/robo_ushanka.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "The Heavy Artillery Officer's Cap",
		localized_name = "info.tf2pm.hat.coh2_heavy",
		localized_description = "info.tf2pm.hat.coh2_heavy_desc",
		icon = "backpack/player/items/heavy/coh_heavyhat",
		models = {
			["basename"] = "models/player/items/heavy/coh_heavyhat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_hip"},
		paintable = true,
		name = "The Grand Duchess Tutu",
		localized_name = "info.tf2pm.hat.grandduchesstutu",
		localized_description = "info.tf2pm.hat.grandduchesstutu_desc",
		icon = "backpack/player/items/heavy/heavy_fairy_tutu",
		models = {
			["basename"] = "models/player/items/heavy/heavy_fairy_tutu.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"back"},
		paintable = true,
		name = "The Grand Duchess Fairy Wings",
		localized_name = "info.tf2pm.hat.grandduchessfairywings",
		localized_description = "info.tf2pm.hat.grandduchessfairywings_desc",
		icon = "backpack/player/items/heavy/heavy_fairy_wings",
		models = {
			["basename"] = "models/player/items/heavy/heavy_fairy_wings.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Grand Duchess Tiara",
		localized_name = "info.tf2pm.hat.grandduchesstiara",
		localized_description = "info.tf2pm.hat.grandduchesstiara_desc",
		icon = "backpack/player/items/heavy/heavy_fairy_tiara",
		models = {
			["basename"] = "models/player/items/heavy/heavy_fairy_tiara.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = true,
		name = "The Siberian Sophisticate",
		localized_name = "info.tf2pm.hat.hm_shirt",
		localized_description = "info.tf2pm.hat.hm_shirt_desc",
		icon = "backpack/workshop_partner/player/items/heavy/hm_shirt/hm_shirt",
		models = {
			["basename"] = "models/workshop_partner/player/items/heavy/hm_shirt/hm_shirt.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Brock's Locks",
		localized_name = "info.tf2pm.hat.brockslocks",
		localized_description = "info.tf2pm.hat.brockslocks_desc",
		icon = "backpack/workshop_partner/player/items/heavy/heavy_hockeyhair/heavy_hockeyhair",
		models = {
			["basename"] = "models/workshop_partner/player/items/heavy/heavy_hockeyhair/heavy_hockeyhair.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_hair"},
		paintable = true,
		name = "Heavy's Hockey Hair",
		localized_name = "info.tf2pm.hat.heavyshockeyhair",
		localized_description = "info.tf2pm.hat.heavyshockeyhair_desc",
		icon = "backpack/workshop/player/items/heavy/skullet/skullet",
		models = {
			["basename"] = "models/workshop/player/items/heavy/skullet/skullet.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Carl",
		localized_name = "info.tf2pm.hat.heavy_carl",
		localized_description = "info.tf2pm.hat.heavy_carl_desc",
		icon = "backpack/workshop_partner/player/items/heavy/heavy_carl_hair/heavy_carl_hair",
		models = {
			["basename"] = "models/workshop_partner/player/items/heavy/heavy_carl_hair/heavy_carl_hair.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"feet"},
		paintable = true,
		name = "Aqua Flops",
		localized_name = "info.tf2pm.hat.heavy_aquaflops",
		localized_description = "info.tf2pm.hat.heavy_aquaflops_desc",
		icon = "backpack/workshop_partner/player/items/heavy/heavy_carl_flops/heavy_carl_flops",
		models = {
			["basename"] = "models/workshop_partner/player/items/heavy/heavy_carl_flops/heavy_carl_flops.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt", "necklace"},
		paintable = false,
		name = "The Hunger Force",
		localized_name = "info.tf2pm.hat.heavy_hungerforce",
		localized_description = "info.tf2pm.hat.heavy_hungerforce_desc",
		icon = "backpack/workshop_partner/player/items/heavy/heavy_carl_medallion_shirt/heavy_carl_medallion_shirt",
		models = {
			["basename"] = "models/workshop_partner/player/items/heavy/heavy_carl_medallion_shirt/heavy_carl_medallion_shirt.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Pounding Father",
		localized_name = "info.tf2pm.hat.poundingfather",
		localized_description = "info.tf2pm.hat.poundingfather_desc",
		icon = "backpack/player/items/heavy/bio_heavy_wig",
		models = {
			["basename"] = "models/player/items/heavy/bio_heavy_wig.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_belt_back"},
		paintable = false,
		name = "The Samson Skewer",
		localized_name = "info.tf2pm.hat.samsonskewer",
		localized_description = "info.tf2pm.hat.samsonskewer_desc",
		icon = "backpack/player/items/heavy/pn2_knife_lunchbox",
		models = {
			["basename"] = "models/player/items/heavy/pn2_knife_lunchbox.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.samsonskewer_style0",
				models = {
					["basename"] = "models/player/items/heavy/pn2_knife_lunchbox.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.samsonskewer_style1",
				models = {
					["basename"] = "models/player/items/heavy/pn2_knife_canteen.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy"},
		targets = {},
		paintable = false,
		name = "MvM GateBot Light Heavy",
		icon = "backpack",
		models = {
			["basename"] = "models/bots/gameplay_cosmetic/light_heavy_on.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_heavy_on.mdl",
				}
			},
			[1] = {
				responsive_skins = {1, 1},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_heavy_off.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "Der Maschinensoldaten-Helm",
		localized_name = "info.tf2pm.hat.maschinensoldatenhelm",
		localized_description = "info.tf2pm.hat.maschinensoldatenhelm_desc",
		icon = "backpack/player/items/heavy/heavy_wolf_helm",
		models = {
			["basename"] = "models/player/items/heavy/heavy_wolf_helm.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {},
		paintable = false,
		name = "Die Regime-Panzerung",
		localized_name = "info.tf2pm.hat.regimepanzerung",
		localized_description = "info.tf2pm.hat.regimepanzerung_desc",
		icon = "backpack/player/items/heavy/heavy_wolf_chest",
		models = {
			["basename"] = "models/player/items/heavy/heavy_wolf_chest.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_hip_pouch"},
		paintable = false,
		name = "The Little Bear",
		localized_name = "info.tf2pm.hat.littlebear",
		localized_description = "info.tf2pm.hat.littlebear_desc",
		icon = "backpack/player/items/heavy/heavy_pocket_bot",
		models = {
			["basename"] = "models/player/items/heavy/heavy_pocket_bot.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "Yeti_Head",
		localized_name = "info.tf2pm.hat.kathmanhairdo",
		localized_description = "info.tf2pm.hat.kathmanhairdo_desc",
		icon = "backpack/player/items/heavy/yeti_head",
		models = {
			["basename"] = "models/player/items/heavy/yeti_head.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"feet"},
		paintable = false,
		name = "Yeti_Legs",
		localized_name = "info.tf2pm.hat.abominablesnowpants",
		localized_description = "info.tf2pm.hat.abominablesnowpants_desc",
		icon = "backpack/player/items/heavy/yeti_legs",
		models = {
			["basename"] = "models/player/items/heavy/yeti_legs.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"arms"},
		paintable = false,
		name = "Yeti_Arms",
		localized_name = "info.tf2pm.hat.himalayanhairshirt",
		localized_description = "info.tf2pm.hat.himalayanhairshirt_desc",
		icon = "backpack/player/items/heavy/yeti_arms",
		models = {
			["basename"] = "models/player/items/heavy/yeti_arms.mdl",
		},
		bodygroup_overrides = {
			["hands"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Aztec Aggressor",
		localized_name = "info.tf2pm.hat.fall17_aztec_aggressor",
		localized_description = "info.tf2pm.hat.fall17_aztec_aggressor_desc",
		icon = "backpack/workshop/player/items/heavy/fall17_aztec_aggressor/fall17_aztec_aggressor",
		models = {
			["basename"] = "models/workshop/player/items/heavy/fall17_aztec_aggressor/fall17_aztec_aggressor.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "SandMann's Brush",
		localized_name = "info.tf2pm.hat.dec20_sandmanns_brush",
		icon = "backpack/workshop/player/items/heavy/dec20_sandmanns_brush/dec20_sandmanns_brush",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec20_sandmanns_brush/dec20_sandmanns_brush.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = true,
		name = "Siberian Tigerstripe",
		localized_name = "info.tf2pm.hat.fall17_siberian_tigerstripe",
		localized_description = "info.tf2pm.hat.fall17_siberian_tigerstripe_desc",
		icon = "backpack/workshop/player/items/heavy/fall17_siberian_tigerstripe/fall17_siberian_tigerstripe",
		models = {
			["basename"] = "models/workshop/player/items/heavy/fall17_siberian_tigerstripe/fall17_siberian_tigerstripe.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 The Big Papa",
		localized_name = "info.tf2pm.hat.dec2014_the_big_papa",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/heavy/dec2014_the_big_papa/dec2014_the_big_papa",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec2014_the_big_papa/dec2014_the_big_papa.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Minnesota Slick",
		localized_name = "info.tf2pm.hat.xms2013_heavy_slick_hair",
		icon = "backpack/workshop/player/items/heavy/xms2013_heavy_slick_hair/xms2013_heavy_slick_hair",
		models = {
			["basename"] = "models/workshop/player/items/heavy/xms2013_heavy_slick_hair/xms2013_heavy_slick_hair.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Sucker Slug",
		localized_name = "info.tf2pm.hat.invasion_sucker_slug",
		icon = "backpack/workshop/player/items/heavy/invasion_sucker_slug/invasion_sucker_slug",
		models = {
			["basename"] = "models/workshop/player/items/heavy/invasion_sucker_slug/invasion_sucker_slug.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "White Russian",
		localized_name = "info.tf2pm.hat.cc_summer2015_white_russian",
		icon = "backpack/workshop/player/items/heavy/cc_summer2015_white_russian/cc_summer2015_white_russian",
		models = {
			["basename"] = "models/workshop/player/items/heavy/cc_summer2015_white_russian/cc_summer2015_white_russian.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Tungsten Toque",
		localized_name = "info.tf2pm.hat.robo_heavy_tungsten_toque",
		localized_description = "info.tf2pm.hat.robo_heavy_tungsten_toque_desc",
		icon = "backpack/workshop/player/items/heavy/robo_heavy_tungsten_toque/robo_heavy_tungsten_toque",
		models = {
			["basename"] = "models/workshop/player/items/heavy/robo_heavy_tungsten_toque/robo_heavy_tungsten_toque.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Starboard Crusader",
		localized_name = "info.tf2pm.hat.spr18_starboard_crusader",
		icon = "backpack/workshop/player/items/heavy/spr18_starboard_crusader/spr18_starboard_crusader",
		models = {
			["basename"] = "models/workshop/player/items/heavy/spr18_starboard_crusader/spr18_starboard_crusader.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.spr18_starboard_crusader_style1",
				models = {
					["basename"] = "models/workshop/player/items/heavy/spr18_starboard_crusader/spr18_starboard_crusader.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.spr18_starboard_crusader_style2",
				models = {
					["basename"] = "models/workshop/player/items/heavy/spr18_starboard_crusader_s1/spr18_starboard_crusader_s1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"pants"},
		paintable = false,
		name = "The Mann of the House",
		localized_name = "info.tf2pm.hat.xms2013_heavy_pants",
		icon = "backpack/workshop/player/items/heavy/xms2013_heavy_pants/xms2013_heavy_pants",
		models = {
			["basename"] = "models/workshop/player/items/heavy/xms2013_heavy_pants/xms2013_heavy_pants.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Bunsen Brave",
		localized_name = "info.tf2pm.hat.robo_heavy_chief",
		localized_description = "info.tf2pm.hat.robo_heavy_chief_desc",
		icon = "backpack/workshop/player/items/heavy/robo_heavy_chief/robo_heavy_chief",
		models = {
			["basename"] = "models/workshop/player/items/heavy/robo_heavy_chief/robo_heavy_chief.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Fat Man's Field Cap",
		localized_name = "info.tf2pm.hat.fall17_fat_mans_field_cap",
		localized_description = "info.tf2pm.hat.fall17_fat_mans_field_cap_desc",
		icon = "backpack/workshop/player/items/heavy/fall17_fat_manns_field_cap/fall17_fat_manns_field_cap",
		models = {
			["basename"] = "models/workshop/player/items/heavy/fall17_fat_manns_field_cap/fall17_fat_manns_field_cap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_towel"},
		paintable = false,
		name = "Heavy Harness",
		localized_name = "info.tf2pm.hat.fall17_heavy_harness",
		localized_description = "info.tf2pm.hat.fall17_heavy_harness_desc",
		icon = "backpack/workshop/player/items/heavy/fall17_heavy_harness/fall17_heavy_harness",
		models = {
			["basename"] = "models/workshop/player/items/heavy/fall17_heavy_harness/fall17_heavy_harness.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"beard"},
		paintable = true,
		name = "El Duderino",
		localized_name = "info.tf2pm.hat.cc_summer2015_el_duderino",
		icon = "backpack/workshop/player/items/heavy/cc_summer2015_el_duderino/cc_summer2015_el_duderino",
		models = {
			["basename"] = "models/workshop/player/items/heavy/cc_summer2015_el_duderino/cc_summer2015_el_duderino.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_bullets"},
		paintable = true,
		name = "The Borscht Belt",
		localized_name = "info.tf2pm.hat.jul13_bagdolier",
		localized_description = "info.tf2pm.hat.jul13_bagdolier_desc",
		icon = "backpack/workshop/player/items/heavy/jul13_bagdolier/jul13_bagdolier",
		models = {
			["basename"] = "models/workshop/player/items/heavy/jul13_bagdolier/jul13_bagdolier.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_hip"},
		paintable = true,
		name = "Combat Slacks",
		localized_name = "info.tf2pm.hat.sbox2014_war_pants",
		icon = "backpack/workshop/player/items/heavy/sbox2014_war_pants/sbox2014_war_pants",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_war_pants/sbox2014_war_pants.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Polar Bear",
		localized_name = "info.tf2pm.hat.dec17_polar_bear",
		icon = "backpack/workshop/player/items/heavy/dec17_polar_bear/dec17_polar_bear",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec17_polar_bear/dec17_polar_bear.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Eliminators Safeguard",
		localized_name = "info.tf2pm.hat.sbox2014_war_helmet",
		icon = "backpack/workshop/player/items/heavy/sbox2014_war_helmet/sbox2014_war_helmet",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_war_helmet/sbox2014_war_helmet.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.sbox2014_war_helmet_style1",
				models = {
					["basename"] = "models/workshop/player/items/heavy/sbox2014_war_helmet/sbox2014_war_helmet.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.sbox2014_war_helmet_style2",
				models = {
					["basename"] = "models/workshop/player/items/heavy/sbox2014_war_helmet_s1/sbox2014_war_helmet_s1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = false,
		name = "Commissar's Coat",
		localized_name = "info.tf2pm.hat.cc_summer2015_commissars_coat",
		icon = "backpack/workshop/player/items/heavy/cc_summer2015_commissars_coat/cc_summer2015_commissars_coat",
		models = {
			["basename"] = "models/workshop/player/items/heavy/cc_summer2015_commissars_coat/cc_summer2015_commissars_coat.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Sammy Cap",
		localized_name = "info.tf2pm.hat.sbox2014_sammy_cap",
		icon = "backpack/workshop/player/items/heavy/sbox2014_sammy_cap/sbox2014_sammy_cap",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_sammy_cap/sbox2014_sammy_cap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"pants"},
		paintable = false,
		name = "Jungle Booty",
		localized_name = "info.tf2pm.hat.tr_jungle_booty",
		icon = "backpack/workshop_partner/player/items/all_class/tr_jungle_booty/tr_jungle_booty",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/tr_jungle_booty/tr_jungle_booty_heavy.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = false,
		name = "The Warmth Preserver",
		localized_name = "info.tf2pm.hat.sbox2014_warmth_preserver",
		icon = "backpack/workshop/player/items/heavy/sbox2014_warmth_preserver/sbox2014_warmth_preserver",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_warmth_preserver/sbox2014_warmth_preserver.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = true,
		name = "Paka Parka",
		localized_name = "info.tf2pm.hat.dec18_paka_parka",
		icon = "backpack/workshop/player/items/heavy/dec18_paka_parka/dec18_paka_parka",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec18_paka_parka/dec18_paka_parka.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"glasses"},
		paintable = true,
		name = "The War Goggles",
		localized_name = "info.tf2pm.hat.sbox2014_war_goggles",
		icon = "backpack/workshop/player/items/heavy/sbox2014_war_goggles/sbox2014_war_goggles",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_war_goggles/sbox2014_war_goggles.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Cool Capuchon",
		localized_name = "info.tf2pm.hat.dec18_cool_capuchon",
		icon = "backpack/workshop/player/items/heavy/dec18_cool_capuchon/dec18_cool_capuchon",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec18_cool_capuchon/dec18_cool_capuchon.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec18_cool_capuchon_style0",
				models = {
					["basename"] = "models/workshop/player/items/heavy/dec18_cool_capuchon/dec18_cool_capuchon.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec18_cool_capuchon_style1",
				models = {
					["basename"] = "models/workshop/player/items/heavy/dec18_cool_capuchon_style/dec18_cool_capuchon_style.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = true,
		name = "EOTL_Ursa Major",
		localized_name = "info.tf2pm.hat.eotl_ursa_major",
		icon = "backpack/workshop/player/items/heavy/ursa_major/ursa_major",
		models = {
			["basename"] = "models/workshop/player/items/heavy/ursa_major/ursa_major.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"beard"},
		paintable = true,
		name = "Wild West Whiskers",
		localized_name = "info.tf2pm.hat.dec17_wild_west_whiskers",
		icon = "backpack/workshop/player/items/heavy/dec17_wild_west_whiskers/dec17_wild_west_whiskers",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec17_wild_west_whiskers/dec17_wild_west_whiskers.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Sinner's Shade",
		localized_name = "info.tf2pm.hat.dec17_sinners_shade",
		icon = "backpack/workshop/player/items/heavy/dec17_sinners_shade/dec17_sinners_shade",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec17_sinners_shade/dec17_sinners_shade.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"feet"},
		paintable = false,
		name = "The Rat Stompers",
		localized_name = "info.tf2pm.hat.sbox2014_rat_stompers",
		icon = "backpack/workshop/player/items/heavy/sbox2014_rat_stompers/sbox2014_rat_stompers",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_rat_stompers/sbox2014_rat_stompers.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = true,
		name = "EOTL_sheavyshirt",
		localized_name = "info.tf2pm.hat.eotl_sheavyshirt",
		icon = "backpack/workshop/player/items/heavy/eotl_sheavyshirt/eotl_sheavyshirt",
		models = {
			["basename"] = "models/workshop/player/items/heavy/eotl_sheavyshirt/eotl_sheavyshirt.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Katyusha",
		localized_name = "info.tf2pm.hat.jul13_katyusha",
		localized_description = "info.tf2pm.hat.jul13_katyusha_desc",
		icon = "backpack/workshop/player/items/heavy/jul13_katyusha/jul13_katyusha",
		models = {
			["basename"] = "models/workshop/player/items/heavy/jul13_katyusha/jul13_katyusha.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"beard"},
		paintable = true,
		name = "The Unshaved Bear",
		localized_name = "info.tf2pm.hat.sept2014_unshaved_bear",
		localized_description = "info.tf2pm.hat.sept2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/heavy/sept2014_unshaved_bear/sept2014_unshaved_bear",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sept2014_unshaved_bear/sept2014_unshaved_bear.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Mo'Horn",
		localized_name = "info.tf2pm.hat.hwn2016_mo_horn",
		icon = "backpack/workshop/player/items/heavy/hwn2016_mo_horn/hwn2016_mo_horn",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hwn2016_mo_horn/hwn2016_mo_horn.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Ivan The Inedible",
		localized_name = "info.tf2pm.hat.hw2013_ivan_the_inedible",
		icon = "backpack/workshop/player/items/heavy/hw2013_ivan_the_inedible/hw2013_ivan_the_inedible",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hw2013_ivan_the_inedible/hw2013_ivan_the_inedible.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = true,
		name = "Siberian Sweater",
		localized_name = "info.tf2pm.hat.dec15_heavy_sweater",
		icon = "backpack/workshop/player/items/heavy/dec15_heavy_sweater/dec15_heavy_sweater",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec15_heavy_sweater/dec15_heavy_sweater.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "EOTL_Flat_cap",
		localized_name = "info.tf2pm.hat.eotl_flat_cap",
		icon = "backpack/workshop/player/items/heavy/eotl_flat_cap/eotl_flat_cap",
		models = {
			["basename"] = "models/workshop/player/items/heavy/eotl_flat_cap/eotl_flat_cap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "tw_heavybot_helmet",
		localized_name = "info.tf2pm.hat.tw_heavybot_helmet",
		icon = "backpack/workshop/player/items/heavy/tw_heavybot_helmet/tw_heavybot_helmet",
		models = {
			["basename"] = "models/workshop/player/items/heavy/tw_heavybot_helmet/tw_heavybot_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Tsarboosh",
		localized_name = "info.tf2pm.hat.jul13_unfamiliar_tarboosh",
		localized_description = "info.tf2pm.hat.jul13_unfamiliar_tarboosh_desc",
		icon = "backpack/workshop/player/items/heavy/jul13_unfamiliar_tarboosh/jul13_unfamiliar_tarboosh",
		models = {
			["basename"] = "models/workshop/player/items/heavy/jul13_unfamiliar_tarboosh/jul13_unfamiliar_tarboosh.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = false,
		name = "tw_heavybot_armor",
		localized_name = "info.tf2pm.hat.tw_heavybot_armor",
		icon = "backpack/workshop/player/items/heavy/tw_heavybot_armor/tw_heavybot_armor",
		models = {
			["basename"] = "models/workshop/player/items/heavy/tw_heavybot_armor/tw_heavybot_armor.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"beard"},
		paintable = true,
		name = "The Leftover Trap",
		localized_name = "info.tf2pm.hat.sbox2014_leftover_trap",
		icon = "backpack/workshop/player/items/heavy/sbox2014_leftover_trap/sbox2014_leftover_trap",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_leftover_trap/sbox2014_leftover_trap.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Convict Cap",
		localized_name = "info.tf2pm.hat.hwn2019_convict_cap",
		icon = "backpack/workshop/player/items/heavy/hwn2019_convict_cap/hwn2019_convict_cap",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hwn2019_convict_cap/hwn2019_convict_cap.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2019_convict_cap_style0",
				models = {
					["basename"] = "models/workshop/player/items/heavy/hwn2019_convict_cap/hwn2019_convict_cap.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2019_convict_cap_style1",
				models = {
					["basename"] = "models/workshop/player/items/heavy/hwn2019_convict_cap_style2/hwn2019_convict_cap_style2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Mediterranean Mercenary",
		localized_name = "info.tf2pm.hat.sum19_mediterranean_mercenary",
		icon = "backpack/workshop/player/items/heavy/sum19_mediterranean_mercenary/sum19_mediterranean_mercenary",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sum19_mediterranean_mercenary/sum19_mediterranean_mercenary.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum19_mediterranean_mercenary_style1",
				models = {
					["basename"] = "models/workshop/player/items/heavy/sum19_mediterranean_mercenary/sum19_mediterranean_mercenary.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum19_mediterranean_mercenary_style2",
				models = {
					["basename"] = "models/workshop/player/items/heavy/sum19_defiant_day/sum19_defiant_day.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"arm_tatoos"},
		paintable = false,
		name = "Soviet Strongmann",
		localized_name = "info.tf2pm.hat.hwn2019_soviet_strongmann",
		icon = "backpack/workshop/player/items/heavy/hwn2019_soviet_strongmann_style2/hwn2019_soviet_strongmann_style2",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hwn2019_soviet_strongmann_style2/hwn2019_soviet_strongmann_style2.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2019_soviet_strongmann_style1",
				models = {
					["basename"] = "models/workshop/player/items/heavy/hwn2019_soviet_strongmann_style2/hwn2019_soviet_strongmann_style2.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2019_soviet_strongmann_style2",
				models = {
					["basename"] = "models/workshop/player/items/heavy/hwn2019_soviet_strongmann_style3/hwn2019_soviet_strongmann_style3.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2019_soviet_strongmann_style0",
				models = {
					["basename"] = "models/workshop/player/items/heavy/hwn2019_soviet_strongmann/hwn2019_soviet_strongmann.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy"},
		targets = {"zombie_body"},
		paintable = false,
		name = "Zombie Heavy",
		localized_name = "info.tf2pm.hat.item_zombieheavy",
		icon = "backpack/player/items/heavy/heavy_zombie",
		models = {
			["basename"] = "models/player/items/heavy/heavy_zombie.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_towel"},
		paintable = true,
		name = "The Titanium Towel",
		localized_name = "info.tf2pm.hat.robo_heavy_boltedscraptowel",
		localized_description = "info.tf2pm.hat.robo_heavy_boltedscraptowel_desc",
		icon = "backpack/workshop/player/items/heavy/robo_heavy_boltedscraptowel/robo_heavy_boltedscraptowel",
		models = {
			["basename"] = "models/workshop/player/items/heavy/robo_heavy_boltedscraptowel/robo_heavy_boltedscraptowel.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Horned Honcho",
		localized_name = "info.tf2pm.hat.hw2013_horned_honcho",
		icon = "backpack/workshop/player/items/heavy/hw2013_horned_honcho/hw2013_horned_honcho",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hw2013_horned_honcho/hw2013_horned_honcho.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "Mannvich",
		localized_name = "info.tf2pm.hat.hwn2020_mannvich",
		icon = "backpack/workshop/player/items/heavy/hwn2020_mannvich/hwn2020_mannvich",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hwn2020_mannvich/hwn2020_mannvich.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = true,
		name = "Heavy Tourism",
		localized_name = "info.tf2pm.hat.hwn2016_heavy_tourism",
		icon = "backpack/workshop/player/items/heavy/hwn2016_heavy_tourism/hwn2016_heavy_tourism",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hwn2016_heavy_tourism/hwn2016_heavy_tourism.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Mann-O-War",
		localized_name = "info.tf2pm.hat.hwn2018_mann_o_war",
		icon = "backpack/workshop/player/items/heavy/hwn2018_mann_o_war/hwn2018_mann_o_war",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hwn2018_mann_o_war/hwn2018_mann_o_war.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = false,
		name = "The Tyurtlenek",
		localized_name = "info.tf2pm.hat.diehard_dynafil",
		localized_description = "info.tf2pm.hat.diehard_dynafil_desc",
		icon = "backpack/workshop/player/items/heavy/diehard_dynafil/diehard_dynafil",
		models = {
			["basename"] = "models/workshop/player/items/heavy/diehard_dynafil/diehard_dynafil.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"beard", "head_skin"},
		paintable = true,
		name = "The Monstrous Mandible",
		localized_name = "info.tf2pm.hat.hw2013_orcish_outburst",
		icon = "backpack/workshop/player/items/heavy/hw2013_orcish_outburst/hw2013_orcish_outburst",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hw2013_orcish_outburst/hw2013_orcish_outburst.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_hip"},
		paintable = true,
		name = "The Heavy-Weight Champ",
		localized_name = "info.tf2pm.hat.jul13_heavy_weight_belt",
		localized_description = "info.tf2pm.hat.jul13_heavy_weight_belt_desc",
		icon = "backpack/workshop/player/items/heavy/jul13_heavy_weight_belt/jul13_heavy_weight_belt",
		models = {
			["basename"] = "models/workshop/player/items/heavy/jul13_heavy_weight_belt/jul13_heavy_weight_belt.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Bullet Buzz",
		localized_name = "info.tf2pm.hat.sbox2014_heavy_buzzcut",
		icon = "backpack/workshop/player/items/heavy/sbox2014_heavy_buzzcut/sbox2014_heavy_buzzcut",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_heavy_buzzcut/sbox2014_heavy_buzzcut.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"left_shoulder"},
		paintable = false,
		name = "The Red Army Robin",
		localized_name = "info.tf2pm.hat.jul13_red_army_robin",
		localized_description = "info.tf2pm.hat.jul13_red_army_robin_desc",
		icon = "backpack/workshop/player/items/heavy/jul13_red_army_robin/jul13_red_army_robin",
		models = {
			["basename"] = "models/workshop/player/items/heavy/jul13_red_army_robin/jul13_red_army_robin.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"pants"},
		paintable = false,
		name = "Gone Commando",
		localized_name = "info.tf2pm.hat.sbox2014_heavy_camopants",
		icon = "backpack/workshop/player/items/heavy/sbox2014_heavy_camopants/sbox2014_heavy_camopants",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_heavy_camopants/sbox2014_heavy_camopants.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {},
		paintable = true,
		name = "Bone-Cut Belt",
		localized_name = "info.tf2pm.hat.sf14_halloween_bone_cut_belt",
		icon = "backpack/workshop/player/items/heavy/sf14_halloween_bone_cut_belt/sf14_halloween_bone_cut_belt",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sf14_halloween_bone_cut_belt/sf14_halloween_bone_cut_belt.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"glasses"},
		paintable = false,
		name = "The Gabe Glasses",
		localized_name = "info.tf2pm.hat.jul13_honchos_heavy_reader",
		localized_description = "info.tf2pm.hat.jul13_honchos_heavy_reader_desc",
		icon = "backpack/workshop/player/items/heavy/jul13_honchos_heavy_reader/jul13_honchos_heavy_reader",
		models = {
			["basename"] = "models/workshop/player/items/heavy/jul13_honchos_heavy_reader/jul13_honchos_heavy_reader.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Trash Man",
		localized_name = "info.tf2pm.hat.sbox2014_trash_man",
		icon = "backpack/workshop/player/items/heavy/sbox2014_trash_man/sbox2014_trash_man",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_trash_man/sbox2014_trash_man.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = true,
		name = "Kapitan's Kaftan",
		localized_name = "info.tf2pm.hat.sum19_kapitans_kaftan",
		icon = "backpack/workshop/player/items/heavy/sum19_kapitans_kaftan/sum19_kapitans_kaftan",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sum19_kapitans_kaftan/sum19_kapitans_kaftan.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = false,
		name = "Immobile Suit",
		localized_name = "info.tf2pm.hat.sf14_heavy_robo_chest",
		icon = "backpack/workshop/player/items/heavy/sf14_heavy_robo_chest/sf14_heavy_robo_chest",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sf14_heavy_robo_chest/sf14_heavy_robo_chest.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Commando Elite",
		localized_name = "info.tf2pm.hat.fall17_commando_elite",
		localized_description = "info.tf2pm.hat.fall17_commando_elite_desc",
		icon = "backpack/workshop/player/items/heavy/fall17_commando_elite/fall17_commando_elite",
		models = {
			["basename"] = "models/workshop/player/items/heavy/fall17_commando_elite/fall17_commando_elite.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = false,
		name = "The Heavy Lifter",
		localized_name = "info.tf2pm.hat.sbox2014_heavy_gunshow",
		icon = "backpack/workshop/player/items/heavy/sbox2014_heavy_gunshow/sbox2014_heavy_gunshow",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sbox2014_heavy_gunshow/sbox2014_heavy_gunshow.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"pants"},
		paintable = true,
		name = "Bear Walker",
		localized_name = "info.tf2pm.hat.dec20_bear_walker",
		icon = "backpack/workshop/player/items/heavy/dec20_bear_walker/dec20_bear_walker",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec20_bear_walker/dec20_bear_walker.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Minsk Beef",
		localized_name = "info.tf2pm.hat.sf14_halloween_minsk_beef",
		icon = "backpack/workshop/player/items/heavy/sf14_halloween_minsk_beef/sf14_halloween_minsk_beef",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sf14_halloween_minsk_beef/sf14_halloween_minsk_beef.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"beard"},
		paintable = true,
		name = "Bull Locks",
		localized_name = "info.tf2pm.hat.sf14_halloween_bull_locks",
		icon = "backpack/workshop/player/items/heavy/sf14_halloween_bull_locks/sf14_halloween_bull_locks",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sf14_halloween_bull_locks/sf14_halloween_bull_locks.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = false,
		name = "The Bolshevik Biker",
		localized_name = "info.tf2pm.hat.jul13_border_armor",
		localized_description = "info.tf2pm.hat.jul13_border_armor_desc",
		icon = "backpack/workshop/player/items/heavy/jul13_border_armor/jul13_border_armor",
		models = {
			["basename"] = "models/workshop/player/items/heavy/jul13_border_armor/jul13_border_armor.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "Warhood",
		localized_name = "info.tf2pm.hat.spr17_warhood",
		localized_description = "info.tf2pm.hat.spr17_warhood_desc",
		icon = "backpack/workshop/player/items/heavy/spr17_warhood/spr17_warhood",
		models = {
			["basename"] = "models/workshop/player/items/heavy/spr17_warhood/spr17_warhood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"face"},
		paintable = true,
		name = "Mad Mask",
		localized_name = "info.tf2pm.hat.hwn2016_mad_mask",
		icon = "backpack/workshop/player/items/heavy/hwn2016_mad_mask/hwn2016_mad_mask",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hwn2016_mad_mask/hwn2016_mad_mask.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 heavy_parka",
		localized_name = "info.tf2pm.hat.dec2014_heavy_parka",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/heavy/dec2014_heavy_parka/dec2014_heavy_parka",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec2014_heavy_parka/dec2014_heavy_parka.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"heavy_belt_back"},
		paintable = true,
		name = "The Last Bite",
		localized_name = "info.tf2pm.hat.hw2013_last_bite",
		icon = "backpack/workshop/player/items/heavy/hw2013_last_bite/hw2013_last_bite",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hw2013_last_bite/hw2013_last_bite.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "The Chicken Kiev",
		localized_name = "info.tf2pm.hat.hw2013_heavy_robin",
		icon = "backpack/workshop/player/items/heavy/hw2013_heavy_robin/hw2013_heavy_robin",
		models = {
			["basename"] = "models/workshop/player/items/heavy/hw2013_heavy_robin/hw2013_heavy_robin.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"left_shoulder"},
		paintable = false,
		name = "Momma Kiev",
		localized_name = "info.tf2pm.hat.sum20_momma_kiev",
		icon = "backpack/workshop/player/items/heavy/sum20_momma_kiev_style2/sum20_momma_kiev_style2",
		models = {
			["basename"] = "models/workshop/player/items/heavy/sum20_momma_kiev_style2/sum20_momma_kiev_style2.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum20_momma_kiev_style2",
				models = {
					["basename"] = "models/workshop/player/items/heavy/sum20_momma_kiev_style2/sum20_momma_kiev_style2.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum20_momma_kiev_style1",
				models = {
					["basename"] = "models/workshop/player/items/heavy/sum20_momma_kiev_style1/sum20_momma_kiev_style1.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = false,
		name = "Tsar Platinum",
		localized_name = "info.tf2pm.hat.spr18_tsar_platinum",
		icon = "backpack/workshop/player/items/heavy/spr18_tsar_platinum/spr18_tsar_platinum",
		models = {
			["basename"] = "models/workshop/player/items/heavy/spr18_tsar_platinum/spr18_tsar_platinum.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"beard"},
		paintable = true,
		name = "Yuri's Revenge",
		localized_name = "info.tf2pm.hat.short2014_heavy_goatee",
		icon = "backpack/workshop/player/items/heavy/short2014_heavy_goatee/short2014_heavy_goatee",
		models = {
			["basename"] = "models/workshop/player/items/heavy/short2014_heavy_goatee/short2014_heavy_goatee.mdl",
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Gridiron Guardian",
		localized_name = "info.tf2pm.hat.robo_heavy_football_helmet",
		localized_description = "info.tf2pm.hat.robo_heavy_football_helmet_desc",
		icon = "backpack/workshop/player/items/heavy/robo_heavy_football_helmet/robo_heavy_football_helmet",
		models = {
			["basename"] = "models/workshop/player/items/heavy/robo_heavy_football_helmet/robo_heavy_football_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"hat"},
		paintable = false,
		name = "The Bear Necessities",
		localized_name = "info.tf2pm.hat.jul13_bear_necessitys",
		localized_description = "info.tf2pm.hat.jul13_bear_necessitys_desc",
		icon = "backpack/workshop/player/items/heavy/jul13_bear_necessitys/jul13_bear_necessitys",
		models = {
			["basename"] = "models/workshop/player/items/heavy/jul13_bear_necessitys/jul13_bear_necessitys.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy"},
		targets = {"shirt"},
		paintable = true,
		name = "BedBug Protection",
		localized_name = "info.tf2pm.hat.dec20_bedbug_protection",
		icon = "backpack/workshop/player/items/heavy/dec20_bedbug_protection/dec20_bedbug_protection",
		models = {
			["basename"] = "models/workshop/player/items/heavy/dec20_bedbug_protection/dec20_bedbug_protection.mdl",
		},
	},

}
