
-- Auto generated at 2020-12-04 12:56:46 UTC+07:00
return {
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Trophy Belt",
		localized_name = "info.tf2pm.hat.sniper_hat_1",
		localized_description = "info.tf2pm.hat.sniper_hat_1_desc",
		icon = "backpack/player/items/sniper/tooth_hat",
		models = {
			["basename"] = "models/player/items/sniper/tooth_hat.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Professional's Panama",
		localized_name = "info.tf2pm.hat.sniper_straw_hat",
		localized_description = "info.tf2pm.hat.sniper_straw_hat_desc",
		icon = "backpack/player/items/sniper/straw_hat",
		models = {
			["basename"] = "models/player/items/sniper/straw_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_headband"},
		paintable = true,
		name = "Master's Yellow Belt",
		localized_name = "info.tf2pm.hat.sniper_jarate_headband",
		localized_description = "info.tf2pm.hat.sniper_jarate_headband_desc",
		icon = "backpack/player/items/sniper/jarate_headband",
		models = {
			["basename"] = "models/player/items/sniper/jarate_headband.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Ritzy Rick's Hair Fixative",
		localized_name = "info.tf2pm.hat.hatless_sniper",
		localized_description = "info.tf2pm.hat.hatless_sniper_desc",
		icon = "backpack/player/items/sniper/sniper_nohat",
		models = {
			["basename"] = "models/player/items/sniper/sniper_nohat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Sniper Pith Helmet",
		localized_name = "info.tf2pm.hat.sniperpithhelmet",
		localized_description = "info.tf2pm.hat.sniperpithhelmet_desc",
		icon = "backpack/player/items/sniper/pith_helmet",
		models = {
			["basename"] = "models/player/items/sniper/pith_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Sniper Fishing Hat",
		localized_name = "info.tf2pm.hat.sniperfishinghat",
		localized_description = "info.tf2pm.hat.sniperfishinghat_desc",
		icon = "backpack/player/items/sniper/sniper_fishinghat",
		models = {
			["basename"] = "models/player/items/sniper/sniper_fishinghat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Ol' Snaggletooth",
		localized_name = "info.tf2pm.hat.olsnaggletooth",
		icon = "backpack/player/items/sniper/snaggletooth",
		models = {
			["basename"] = "models/player/items/sniper/snaggletooth.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"back"},
		paintable = false,
		name = "Darwin's Danger Shield",
		localized_name = "info.tf2pm.hat.darwinsdangershield",
		icon = "backpack/workshop/player/items/sniper/croc_shield/croc_shield",
		models = {
			["basename"] = "models/workshop/player/items/sniper/croc_shield/croc_shield.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Larrikin Robin",
		localized_name = "info.tf2pm.hat.larrikinrobin",
		icon = "backpack/player/items/sniper/larrikin_robin",
		models = {
			["basename"] = "models/player/items/sniper/larrikin_robin.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Crocleather Slouch",
		localized_name = "info.tf2pm.hat.crocleather_slouch",
		localized_description = "info.tf2pm.hat.crocleather_slouch_desc",
		icon = "backpack/player/items/sniper/sniper_crocleather_slouch",
		models = {
			["basename"] = "models/player/items/sniper/sniper_crocleather_slouch.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"beard"},
		paintable = true,
		name = "Villain's Veil",
		localized_name = "info.tf2pm.hat.villainsveil",
		localized_description = "info.tf2pm.hat.villainsveil_desc",
		icon = "backpack/player/items/sniper/kerch",
		models = {
			["basename"] = "models/player/items/sniper/kerch.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.villainsveil_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.villainsveil_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.villainsveil_style2",
			},
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Desert Marauder",
		localized_name = "info.tf2pm.hat.desertmarauder",
		icon = "backpack/player/items/sniper/desert_marauder",
		models = {
			["basename"] = "models/player/items/sniper/desert_marauder.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"beard", "hat"},
		paintable = true,
		name = "The Anger",
		localized_name = "info.tf2pm.hat.brinkhood",
		icon = "backpack/workshop_partner/player/items/sniper/c_bet_brinkhood/c_bet_brinkhood",
		models = {
			["basename"] = "models/workshop_partner/player/items/sniper/c_bet_brinkhood/c_bet_brinkhood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"glasses"},
		paintable = false,
		name = "Sniper's Snipin' Glass",
		localized_name = "info.tf2pm.hat.dotasniper_hat",
		localized_description = "info.tf2pm.hat.dotagamescom2011_hat_desc",
		icon = "backpack/player/items/sniper/dotasniper_hat",
		models = {
			["basename"] = "models/player/items/sniper/dotasniper_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Holy Hunter",
		localized_name = "info.tf2pm.hat.hwn_sniperhat",
		icon = "backpack/player/items/sniper/hwn_sniper_hat",
		models = {
			["basename"] = "models/player/items/sniper/hwn_sniper_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_bullets", "necklace"},
		paintable = false,
		name = "Silver Bullets",
		localized_name = "info.tf2pm.hat.hwn_snipermisc1",
		icon = "backpack/player/items/sniper/hwn_sniper_misc1",
		models = {
			["basename"] = "models/player/items/sniper/hwn_sniper_misc1.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_legs"},
		paintable = false,
		name = "Garlic Flank Stake",
		localized_name = "info.tf2pm.hat.hwn_snipermisc2",
		icon = "backpack/player/items/sniper/hwn_sniper_misc2",
		models = {
			["basename"] = "models/player/items/sniper/hwn_sniper_misc2.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Your Worst Nightmare",
		localized_name = "info.tf2pm.hat.sniperhat1",
		localized_description = "info.tf2pm.hat.sniperhat1_desc",
		icon = "backpack/workshop/player/items/sniper/fwk_sniper_bandanahair/fwk_sniper_bandanahair",
		models = {
			["basename"] = "models/workshop/player/items/sniper/fwk_sniper_bandanahair/fwk_sniper_bandanahair.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sniperhat1_style0",
				models = {
					["basename"] = "models/workshop/player/items/sniper/fwk_sniper_bandanahair/fwk_sniper_bandanahair.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sniperhat1_style1",
				models = {
					["basename"] = "models/workshop/player/items/sniper/fwk_sniper_bandanahair_s2/fwk_sniper_bandanahair_s2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"necklace"},
		paintable = false,
		name = "The Crocodile Smile",
		localized_name = "info.tf2pm.hat.snipernecklace",
		localized_description = "info.tf2pm.hat.snipernecklace_desc",
		icon = "backpack/player/items/sniper/fwk_sniper_necklace",
		models = {
			["basename"] = "models/player/items/sniper/fwk_sniper_necklace.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Swagman's Swatter",
		localized_name = "info.tf2pm.hat.sniperhat2",
		localized_description = "info.tf2pm.hat.sniperhat2_desc",
		icon = "backpack/player/items/sniper/fwk_sniper_corkhat",
		models = {
			["basename"] = "models/player/items/sniper/fwk_sniper_corkhat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"back"},
		paintable = false,
		name = "The Cozy Camper",
		localized_name = "info.tf2pm.hat.cozycamper",
		icon = "backpack/workshop/player/items/sniper/xms_sniper_commandobackpack/xms_sniper_commandobackpack",
		models = {
			["basename"] = "models/workshop/player/items/sniper/xms_sniper_commandobackpack/xms_sniper_commandobackpack.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "The Outback Intellectual",
		localized_name = "info.tf2pm.hat.touchingstory",
		localized_description = "info.tf2pm.hat.touchingstory_desc",
		icon = "backpack/player/items/sniper/xms_sniper_sweater_vest",
		models = {
			["basename"] = "models/player/items/sniper/xms_sniper_sweater_vest.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Bushman's Boonie",
		localized_name = "info.tf2pm.hat.jag_shadow",
		localized_description = "info.tf2pm.hat.jag_shadow_desc",
		icon = "backpack/player/items/sniper/jag_shadow",
		models = {
			["basename"] = "models/player/items/sniper/jag_shadow.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Fruit Shoot",
		localized_name = "info.tf2pm.hat.sniperapplearrow",
		localized_description = "info.tf2pm.hat.sniperapplearrow_desc",
		icon = "backpack/player/items/sniper/sniper_applearrow",
		models = {
			["basename"] = "models/player/items/sniper/sniper_applearrow.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sniperapplearrow_style0",
				models = {
					["basename"] = "models/player/items/sniper/sniper_applearrow.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sniperapplearrow_style1",
				models = {
					["basename"] = "models/player/items/sniper/sniper_appleworm.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sniperapplearrow_style2",
				models = {
					["basename"] = "models/player/items/sniper/sniper_applebite.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Flamingo Kid",
		localized_name = "info.tf2pm.hat.supermnc_sniper",
		localized_description = "info.tf2pm.hat.supermnc_sniper_desc",
		icon = "backpack/player/items/sniper/smnc_sniper",
		models = {
			["basename"] = "models/player/items/sniper/smnc_sniper.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Liquidator's Lid",
		localized_name = "info.tf2pm.hat.pro_sniperhat",
		localized_description = "info.tf2pm.hat.pro_sniperhat_desc",
		icon = "backpack/player/items/sniper/pro_hat",
		models = {
			["basename"] = "models/player/items/sniper/pro_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Lone Star",
		localized_name = "info.tf2pm.hat.awes_sniper",
		localized_description = "info.tf2pm.hat.awes_sniper_desc",
		icon = "backpack/player/items/sniper/awes_hat",
		models = {
			["basename"] = "models/player/items/sniper/awes_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_pocket"},
		paintable = true,
		name = "The Koala Compact",
		localized_name = "info.tf2pm.hat.sniperpocketkoala",
		localized_description = "info.tf2pm.hat.sniperpocketkoala_desc",
		icon = "backpack/player/items/sniper/sniper_pocketkoala",
		models = {
			["basename"] = "models/player/items/sniper/sniper_pocketkoala.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.sniperpocketkoala_style0",
				models = {
					["basename"] = "models/player/items/sniper/sniper_pocketkoala.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.sniperpocketkoala_style1",
				models = {
					["basename"] = "models/player/items/sniper/sniper_pocketkoala_s2.mdl",
				}
			},
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Bolted Bushman",
		localized_name = "info.tf2pm.hat.sniper_robot_hat",
		localized_description = "info.tf2pm.hat.sniper_robot_hat_desc",
		icon = "backpack/player/items/mvm_loot/sniper/robo_sniper_hat",
		models = {
			["basename"] = "models/player/items/mvm_loot/sniper/robo_sniper_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Stovepipe Sniper Shako",
		localized_name = "info.tf2pm.hat.tw_sniper_hat",
		localized_description = "info.tf2pm.hat.tw_sniper_hat_desc",
		icon = "backpack/workshop_partner/player/items/sniper/tw_shako/tw_shako",
		models = {
			["basename"] = "models/workshop_partner/player/items/sniper/tw_shako/tw_shako.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Sir Hootsalot",
		localized_name = "info.tf2pm.hat.sirhootsalot",
		localized_description = "info.tf2pm.hat.sirhootsalot_desc",
		icon = "backpack/player/items/sniper/sniper_owl",
		models = {
			["basename"] = "models/player/items/sniper/sniper_owl.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.sirhootsalot_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.sirhootsalot_style1",
			},
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Cold Killer",
		localized_name = "info.tf2pm.hat.thecoldkiller",
		localized_description = "info.tf2pm.hat.thecoldkiller_desc",
		icon = "backpack/workshop/player/items/sniper/winter_sniper_hood/winter_sniper_hood",
		models = {
			["basename"] = "models/workshop/player/items/sniper/winter_sniper_hood/winter_sniper_hood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Sydney Straw Boat",
		localized_name = "info.tf2pm.hat.strawboat",
		localized_description = "info.tf2pm.hat.strawboat_desc",
		icon = "backpack/player/items/sniper/bio_sniper_boater",
		models = {
			["basename"] = "models/player/items/sniper/bio_sniper_boater.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"left_shoulder"},
		paintable = false,
		name = "The Steel Songbird",
		localized_name = "info.tf2pm.hat.steelsongbird",
		localized_description = "info.tf2pm.hat.steelsongbird_desc",
		icon = "backpack/player/items/sniper/bio_sniper_songbird",
		models = {
			["basename"] = "models/player/items/sniper/bio_sniper_songbird.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "MvM GateBot Light Sniper",
		icon = "backpack",
		models = {
			["basename"] = "models/bots/gameplay_cosmetic/light_sniper_on.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_sniper_on.mdl",
				}
			},
			[1] = {
				responsive_skins = {1, 1},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_sniper_off.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Smissmas Caribou",
		localized_name = "info.tf2pm.hat.winter2013_caribou",
		localized_description = "info.tf2pm.hat.winter2013_caribou_desc",
		icon = "backpack/player/items/sniper/xms_braindeer",
		models = {
			["basename"] = "models/player/items/sniper/xms_braindeer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Randolph the Blood-Nosed Caribou",
		localized_name = "info.tf2pm.hat.winter2013_randolph",
		localized_description = "info.tf2pm.hat.winter2013_randolph_desc",
		icon = "backpack/player/items/sniper/xms_braindeer_rare",
		models = {
			["basename"] = "models/player/items/sniper/xms_braindeer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_vest"},
		paintable = false,
		name = "The Criminal Cloak",
		localized_name = "info.tf2pm.hat.criminalcloak",
		icon = "backpack/workshop_partner/player/items/sniper/thief_sniper_cape/thief_sniper_cape",
		models = {
			["basename"] = "models/workshop_partner/player/items/sniper/thief_sniper_cape/thief_sniper_cape.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"beard", "hat"},
		paintable = false,
		name = "The Dread Hiding Hood",
		localized_name = "info.tf2pm.hat.dreadhidinghood",
		icon = "backpack/workshop_partner/player/items/sniper/thief_sniper_hood/thief_sniper_hood",
		models = {
			["basename"] = "models/workshop_partner/player/items/sniper/thief_sniper_hood/thief_sniper_hood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_vest"},
		paintable = true,
		name = "Down Under Duster",
		localized_name = "info.tf2pm.hat.spr17_down_under_duster",
		localized_description = "info.tf2pm.hat.spr17_down_under_duster_desc",
		icon = "backpack/workshop/player/items/sniper/spr17_down_under_duster/spr17_down_under_duster",
		models = {
			["basename"] = "models/workshop/player/items/sniper/spr17_down_under_duster/spr17_down_under_duster.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Bare Necessities",
		localized_name = "info.tf2pm.hat.sum19_bare_necessities",
		icon = "backpack/workshop/player/items/sniper/sum19_bare_necessities/sum19_bare_necessities",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sum19_bare_necessities/sum19_bare_necessities.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"feet"},
		paintable = false,
		name = "Final Frontiersman",
		localized_name = "info.tf2pm.hat.invasion_final_frontiersman",
		icon = "backpack/workshop/player/items/sniper/invasion_final_frontiersman/invasion_final_frontiersman",
		models = {
			["basename"] = "models/workshop/player/items/sniper/invasion_final_frontiersman/invasion_final_frontiersman.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_bullets"},
		paintable = true,
		name = "The Jarmaments",
		localized_name = "info.tf2pm.hat.sum20_jarmaments",
		icon = "backpack/workshop/player/items/sniper/sum20_jarmaments/sum20_jarmaments",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sum20_jarmaments/sum20_jarmaments.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_vest"},
		paintable = false,
		name = "The Scoped Spartan",
		localized_name = "info.tf2pm.hat.may16_scoped_spartan",
		icon = "backpack/workshop/player/items/sniper/headhunters_wrap/headhunters_wrap",
		models = {
			["basename"] = "models/workshop/player/items/sniper/headhunters_wrap/headhunters_wrap.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_legs"},
		paintable = false,
		name = "Mr. Mundee's Wild Ride",
		localized_name = "info.tf2pm.hat.sf14_sniper_ostrich_legs",
		icon = "backpack/workshop/player/items/sniper/sf14_sniper_ostrich_legs/sf14_sniper_ostrich_legs",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sf14_sniper_ostrich_legs/sf14_sniper_ostrich_legs.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Killing Tree",
		localized_name = "info.tf2pm.hat.dec20_killing_tree",
		icon = "backpack/workshop/player/items/sniper/dec20_killing_tree/dec20_killing_tree",
		models = {
			["basename"] = "models/workshop/player/items/sniper/dec20_killing_tree/dec20_killing_tree.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_vest"},
		paintable = true,
		name = "Down Tundra Coat",
		localized_name = "info.tf2pm.hat.dec17_down_tundra_coat",
		icon = "backpack/workshop/player/items/sniper/dec17_down_tundra_coat/dec17_down_tundra_coat",
		models = {
			["basename"] = "models/workshop/player/items/sniper/dec17_down_tundra_coat/dec17_down_tundra_coat.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"beard"},
		paintable = true,
		name = "The Five-Month Shadow",
		localized_name = "info.tf2pm.hat.xms2013_sniper_beard",
		icon = "backpack/workshop/player/items/sniper/xms2013_sniper_beard/xms2013_sniper_beard",
		models = {
			["basename"] = "models/workshop/player/items/sniper/xms2013_sniper_beard/xms2013_sniper_beard.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.xms2013_sniper_beard_style1",
				models = {
					["basename"] = "models/workshop/player/items/sniper/xms2013_sniper_beard/xms2013_sniper_beard.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.xms2013_sniper_beard_style2",
				models = {
					["basename"] = "models/workshop/player/items/sniper/xms2013_sniper_beard_s2/xms2013_sniper_beard_s2.mdl",
				}
			},
		},
	},
	{
		classes = {"sniper"},
		targets = {"feet"},
		paintable = true,
		name = "The Archers Groundings",
		localized_name = "info.tf2pm.hat.sbox2014_archers_groundings",
		icon = "backpack/workshop/player/items/sniper/sbox2014_archers_groundings/sbox2014_archers_groundings",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sbox2014_archers_groundings/sbox2014_archers_groundings.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_vest"},
		paintable = false,
		name = "The Chronomancer",
		localized_name = "info.tf2pm.hat.fall2013_kyoto_rider",
		icon = "backpack/workshop/player/items/sniper/fall2013_kyoto_rider/fall2013_kyoto_rider",
		models = {
			["basename"] = "models/workshop/player/items/sniper/fall2013_kyoto_rider/fall2013_kyoto_rider.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "The Snow Scoper",
		localized_name = "info.tf2pm.hat.xms2013_sniper_jacket",
		icon = "backpack/workshop/player/items/sniper/xms2013_sniper_jacket/xms2013_sniper_jacket",
		models = {
			["basename"] = "models/workshop/player/items/sniper/xms2013_sniper_jacket/xms2013_sniper_jacket.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"head_skin"},
		paintable = true,
		name = "Conspiratorial Cut",
		localized_name = "info.tf2pm.hat.sf14_conspiratorial_cut",
		icon = "backpack/workshop/player/items/sniper/sf14_conspiratorial_cut/sf14_conspiratorial_cut",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sf14_conspiratorial_cut/sf14_conspiratorial_cut.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "The Mislaid Sweater",
		localized_name = "info.tf2pm.hat.dec19_mislaid_sweater",
		icon = "backpack/workshop/player/items/sniper/dec19_mislaid_sweater/dec19_mislaid_sweater",
		models = {
			["basename"] = "models/workshop/player/items/sniper/dec19_mislaid_sweater/dec19_mislaid_sweater.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"zombie_body"},
		paintable = false,
		name = "Zombie Sniper",
		localized_name = "info.tf2pm.hat.item_zombiesniper",
		icon = "backpack/player/items/sniper/sniper_zombie",
		models = {
			["basename"] = "models/player/items/sniper/sniper_zombie.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "The Birdman of Australiacatraz",
		localized_name = "info.tf2pm.hat.jul13_bushmans_blazer",
		localized_description = "info.tf2pm.hat.jul13_bushmans_blazer_desc",
		icon = "backpack/workshop/player/items/sniper/jul13_bushmans_blazer/jul13_bushmans_blazer",
		models = {
			["basename"] = "models/workshop/player/items/sniper/jul13_bushmans_blazer/jul13_bushmans_blazer.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"shirt"},
		paintable = false,
		name = "The Conspicuous Camouflage",
		localized_name = "info.tf2pm.hat.fall17_conspicuous_camouflage",
		localized_description = "info.tf2pm.hat.fall17_conspicuous_camouflage_desc",
		icon = "backpack/workshop/player/items/sniper/fall17_conspicuous_camouflage_open/fall17_conspicuous_camouflage_open",
		models = {
			["basename"] = "models/workshop/player/items/sniper/fall17_conspicuous_camouflage_open/fall17_conspicuous_camouflage_open.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.fall17_conspicuous_camouflage_style0",
				models = {
					["basename"] = "models/workshop/player/items/sniper/fall17_conspicuous_camouflage_open/fall17_conspicuous_camouflage_open.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.fall17_conspicuous_camouflage_style1",
				models = {
					["basename"] = "models/workshop/player/items/sniper/fall17_conspicuous_camouflage_closed/fall17_conspicuous_camouflage_closed.mdl",
				}
			},
		},
	},
	{
		classes = {"sniper"},
		targets = {"sleeves"},
		paintable = false,
		name = "Roo Rippers",
		localized_name = "info.tf2pm.hat.sf14_roo_rippers",
		icon = "backpack/workshop/player/items/sniper/sf14_roo_rippers/sf14_roo_rippers",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sf14_roo_rippers/sf14_roo_rippers.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"soldier_legs"},
		paintable = false,
		name = "The Cammy Jammies",
		localized_name = "info.tf2pm.hat.fall17_cammy_jammies",
		localized_description = "info.tf2pm.hat.fall17_cammy_jammies_desc",
		icon = "backpack/workshop/player/items/sniper/fall17_cammy_jammies/fall17_cammy_jammies",
		models = {
			["basename"] = "models/workshop/player/items/sniper/fall17_cammy_jammies/fall17_cammy_jammies.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Marsupial Man",
		localized_name = "info.tf2pm.hat.sf14_marsupial_man",
		icon = "backpack/workshop/player/items/sniper/sf14_marsupial_man/sf14_marsupial_man",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sf14_marsupial_man/sf14_marsupial_man.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Handsome Hitman",
		localized_name = "info.tf2pm.hat.dec17_handsome_hitman",
		icon = "backpack/workshop/player/items/sniper/dec17_handsome_hitman/dec17_handsome_hitman",
		models = {
			["basename"] = "models/workshop/player/items/sniper/dec17_handsome_hitman/dec17_handsome_hitman.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sleeves"},
		paintable = true,
		name = "The Falconer",
		localized_name = "info.tf2pm.hat.jul13_falconer_punch",
		localized_description = "info.tf2pm.hat.jul13_falconer_punch_desc",
		icon = "backpack/workshop/player/items/sniper/jul13_falconer_punch/jul13_falconer_punch",
		models = {
			["basename"] = "models/workshop/player/items/sniper/jul13_falconer_punch/jul13_falconer_punch.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "The Cobber Chameleon",
		localized_name = "info.tf2pm.hat.jul13_cameleon",
		localized_description = "info.tf2pm.hat.jul13_cameleon_desc",
		icon = "backpack/workshop/player/items/sniper/jul13_cameleon/jul13_cameleon",
		models = {
			["basename"] = "models/workshop/player/items/sniper/jul13_cameleon/jul13_cameleon.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_quiver"},
		paintable = true,
		name = "Elizabeth the Third",
		localized_name = "info.tf2pm.hat.hwn2019_elizabeth_the_third",
		icon = "backpack/workshop/player/items/sniper/hwn2019_elizabeth_the_third/hwn2019_elizabeth_the_third",
		models = {
			["basename"] = "models/workshop/player/items/sniper/hwn2019_elizabeth_the_third/hwn2019_elizabeth_the_third.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"beard"},
		paintable = true,
		name = "The Most Dangerous Mane",
		localized_name = "info.tf2pm.hat.fall17_most_dangerous_mane",
		localized_description = "info.tf2pm.hat.fall17_most_dangerous_mane_desc",
		icon = "backpack/workshop/player/items/sniper/fall17_most_dangerous_mane/fall17_most_dangerous_mane",
		models = {
			["basename"] = "models/workshop/player/items/sniper/fall17_most_dangerous_mane/fall17_most_dangerous_mane.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Classy Capper",
		localized_name = "info.tf2pm.hat.fall17_classy_capper",
		localized_description = "info.tf2pm.hat.fall17_classy_capper_desc",
		icon = "backpack/workshop/player/items/sniper/fall17_hunter/fall17_hunter",
		models = {
			["basename"] = "models/workshop/player/items/sniper/fall17_hunter/fall17_hunter.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Puffy Polar Cap",
		localized_name = "info.tf2pm.hat.dec17_puffy_polar_cap",
		icon = "backpack/workshop/player/items/sniper/dec17_puffy_polar_cap/dec17_puffy_polar_cap",
		models = {
			["basename"] = "models/workshop/player/items/sniper/dec17_puffy_polar_cap/dec17_puffy_polar_cap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"shirt"},
		paintable = false,
		name = "tw_sniperbot_armor",
		localized_name = "info.tf2pm.hat.tw_sniperbot_armor",
		icon = "backpack/workshop/player/items/sniper/tw_sniperbot_armor/tw_sniperbot_armor",
		models = {
			["basename"] = "models/workshop/player/items/sniper/tw_sniperbot_armor/tw_sniperbot_armor.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_quiver"},
		paintable = true,
		name = "The Huntsman's Essentials",
		localized_name = "info.tf2pm.hat.sbox2014_sniper_quiver",
		icon = "backpack/workshop/player/items/sniper/sbox2014_sniper_quiver/sbox2014_sniper_quiver",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sbox2014_sniper_quiver/sbox2014_sniper_quiver.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_legs"},
		paintable = false,
		name = "The Triggerman's Tacticals",
		localized_name = "info.tf2pm.hat.short2014_sniper_cargo_pants",
		icon = "backpack/workshop/player/items/sniper/short2014_sniper_cargo_pants/short2014_sniper_cargo_pants",
		models = {
			["basename"] = "models/workshop/player/items/sniper/short2014_sniper_cargo_pants/short2014_sniper_cargo_pants.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Highway Star",
		localized_name = "info.tf2pm.hat.hwn2018_highway_star",
		icon = "backpack/workshop/player/items/sniper/hwn2018_highway_star/hwn2018_highway_star",
		models = {
			["basename"] = "models/workshop/player/items/sniper/hwn2018_highway_star/hwn2018_highway_star.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"beard"},
		paintable = true,
		name = "The Scoper's Smoke",
		localized_name = "info.tf2pm.hat.short2014_scopers_smoke",
		icon = "backpack/workshop/player/items/sniper/short2014_scopers_smoke/short2014_scopers_smoke",
		models = {
			["basename"] = "models/workshop/player/items/sniper/short2014_scopers_smoke/short2014_scopers_smoke.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"whole_head"},
		paintable = true,
		name = "Crocodile Mun-Dee",
		localized_name = "info.tf2pm.hat.hwn2018_crocodile_mun_dee",
		icon = "backpack/workshop/player/items/sniper/hwn2018_crocodile_mun_dee/hwn2018_crocodile_mun_dee",
		models = {
			["basename"] = "models/workshop/player/items/sniper/hwn2018_crocodile_mun_dee/hwn2018_crocodile_mun_dee.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Glow from Below",
		localized_name = "info.tf2pm.hat.hwn2020_glow_below",
		icon = "backpack/workshop/player/items/sniper/hwn2020_glow_below/hwn2020_glow_below",
		models = {
			["basename"] = "models/workshop/player/items/sniper/hwn2020_glow_below/hwn2020_glow_below.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2020_glow_below_style1",
				models = {
					["basename"] = "models/workshop/player/items/sniper/hwn2020_glow_below/hwn2020_glow_below.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2020_glow_below_style2",
				models = {
					["basename"] = "models/workshop/player/items/sniper/hwn2020_glow_below_style2/hwn2020_glow_below_style2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Archer's Sterling",
		localized_name = "info.tf2pm.hat.spr17_archers_sterling",
		localized_description = "info.tf2pm.hat.spr17_archers_sterling_desc",
		icon = "backpack/workshop/player/items/sniper/spr17_archers_sterling/spr17_archers_sterling",
		models = {
			["basename"] = "models/workshop/player/items/sniper/spr17_archers_sterling/spr17_archers_sterling.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Hallowed Headcase",
		localized_name = "info.tf2pm.hat.hw2013_the_halloween_headcase",
		icon = "backpack/workshop/player/items/sniper/hw2013_the_halloween_headcase/hw2013_the_halloween_headcase",
		models = {
			["basename"] = "models/workshop/player/items/sniper/hw2013_the_halloween_headcase/hw2013_the_halloween_headcase.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["head"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Sir Shootsalot",
		localized_name = "info.tf2pm.hat.hw2013_sir_shootsalot",
		icon = "backpack/workshop/player/items/sniper/hw2013_sir_shootsalot/hw2013_sir_shootsalot",
		models = {
			["basename"] = "models/workshop/player/items/sniper/hw2013_sir_shootsalot/hw2013_sir_shootsalot.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_vest"},
		paintable = true,
		name = "Poacher's Safari Jacket",
		localized_name = "info.tf2pm.hat.sept2014_poachers_safari_jacket",
		localized_description = "info.tf2pm.hat.sept2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/sniper/sept2014_poachers_safari_jacket/sept2014_poachers_safari_jacket",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sept2014_poachers_safari_jacket/sept2014_poachers_safari_jacket.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_legs", "arms"},
		paintable = true,
		name = "Scopers Scales",
		localized_name = "info.tf2pm.hat.hwn2018_scopers_scales",
		icon = "backpack/workshop/player/items/sniper/hwn2018_scopers_scales/hwn2018_scopers_scales",
		models = {
			["basename"] = "models/workshop/player/items/sniper/hwn2018_scopers_scales/hwn2018_scopers_scales.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Soldered Sensei",
		localized_name = "info.tf2pm.hat.robo_sniper_soldered_sensei",
		localized_description = "info.tf2pm.hat.robo_sniper_soldered_sensei_desc",
		icon = "backpack/workshop/player/items/sniper/robo_sniper_soldered_sensei/robo_sniper_soldered_sensei",
		models = {
			["basename"] = "models/workshop/player/items/sniper/robo_sniper_soldered_sensei/robo_sniper_soldered_sensei.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Letch's LED",
		localized_name = "info.tf2pm.hat.robo_sniper_liquidator",
		localized_description = "info.tf2pm.hat.robo_sniper_liquidator_desc",
		icon = "backpack/workshop/player/items/sniper/robo_sniper_liquidator/robo_sniper_liquidator",
		models = {
			["basename"] = "models/workshop/player/items/sniper/robo_sniper_liquidator/robo_sniper_liquidator.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Hawk Eyed Hunter",
		localized_name = "info.tf2pm.hat.spr17_hawk_eyed_hunter",
		localized_description = "info.tf2pm.hat.spr17_hawk_eyed_hunter_desc",
		icon = "backpack/workshop/player/items/sniper/spr17_hawk_eyed_hunter/spr17_hawk_eyed_hunter",
		models = {
			["basename"] = "models/workshop/player/items/sniper/spr17_hawk_eyed_hunter/spr17_hawk_eyed_hunter.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "Guilden Guardian",
		localized_name = "info.tf2pm.hat.spr17_guilden_guardian",
		localized_description = "info.tf2pm.hat.spr17_guilden_guardian_desc",
		icon = "backpack/workshop/player/items/sniper/spr17_guilden_guardian/spr17_guilden_guardian",
		models = {
			["basename"] = "models/workshop/player/items/sniper/spr17_guilden_guardian/spr17_guilden_guardian.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_pocket_left"},
		paintable = true,
		name = "dec2014 Wally Pocket",
		localized_name = "info.tf2pm.hat.dec2014_wally_pocket",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/sniper/dec2014_wally_pocket/dec2014_wally_pocket",
		models = {
			["basename"] = "models/workshop/player/items/sniper/dec2014_wally_pocket/dec2014_wally_pocket.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sleeves"},
		paintable = true,
		name = "Skinless Slashers",
		localized_name = "info.tf2pm.hat.sf14_skinless_slashers",
		icon = "backpack/workshop/player/items/sniper/sf14_skinless_slashers/sf14_skinless_slashers",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sf14_skinless_slashers/sf14_skinless_slashers.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = false,
		name = "tw_sniperbot_helmet",
		localized_name = "info.tf2pm.hat.tw_sniperbot_helmet",
		icon = "backpack/workshop/player/items/sniper/tw_sniperbot_helmet/tw_sniperbot_helmet",
		models = {
			["basename"] = "models/workshop/player/items/sniper/tw_sniperbot_helmet/tw_sniperbot_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_vest"},
		paintable = true,
		name = "dec2014 hunter_vest",
		localized_name = "info.tf2pm.hat.dec2014_hunter_vest",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/sniper/dec2014_hunter_vest/dec2014_hunter_vest",
		models = {
			["basename"] = "models/workshop/player/items/sniper/dec2014_hunter_vest/dec2014_hunter_vest.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 hunter_ushanka",
		localized_name = "info.tf2pm.hat.dec2014_hunter_ushanka",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/sniper/dec2014_hunter_ushanka/dec2014_hunter_ushanka",
		models = {
			["basename"] = "models/workshop/player/items/sniper/dec2014_hunter_ushanka/dec2014_hunter_ushanka.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Wet Works",
		localized_name = "info.tf2pm.hat.jul13_sniper_souwester",
		localized_description = "info.tf2pm.hat.jul13_sniper_souwester_desc",
		icon = "backpack/workshop/player/items/sniper/jul13_sniper_souwester/jul13_sniper_souwester",
		models = {
			["basename"] = "models/workshop/player/items/sniper/jul13_sniper_souwester/jul13_sniper_souwester.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "The Golden Garment",
		localized_name = "info.tf2pm.hat.xms2013_sniper_golden_garment",
		icon = "backpack/workshop/player/items/sniper/xms2013_sniper_golden_garment/xms2013_sniper_golden_garment",
		models = {
			["basename"] = "models/workshop/player/items/sniper/xms2013_sniper_golden_garment/xms2013_sniper_golden_garment.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"beard"},
		paintable = true,
		name = "dec2014 hunter_beard",
		localized_name = "info.tf2pm.hat.dec2014_hunter_beard",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/sniper/dec2014_hunter_beard/dec2014_hunter_beard",
		models = {
			["basename"] = "models/workshop/player/items/sniper/dec2014_hunter_beard/dec2014_hunter_beard.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_pocket"},
		paintable = true,
		name = "Li'l Snaggletooth",
		localized_name = "info.tf2pm.hat.fall2013_lil_snaggletooth",
		localized_description = "info.tf2pm.hat.fall2013_lil_snaggletooth_desc",
		icon = "backpack/workshop/player/items/sniper/fall2013_lil_snaggletooth/fall2013_lil_snaggletooth",
		models = {
			["basename"] = "models/workshop/player/items/sniper/fall2013_lil_snaggletooth/fall2013_lil_snaggletooth.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "The Carious Chameleon",
		localized_name = "info.tf2pm.hat.hw2013_zombie_chameleon",
		icon = "backpack/workshop/player/items/sniper/hw2013_zombie_chameleon/hw2013_zombie_chameleon",
		models = {
			["basename"] = "models/workshop/player/items/sniper/hw2013_zombie_chameleon/hw2013_zombie_chameleon.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_vest"},
		paintable = true,
		name = "Rifleman's Regalia",
		localized_name = "info.tf2pm.hat.fall17_riflemans_regalia",
		localized_description = "info.tf2pm.hat.fall17_riflemans_regalia_desc",
		icon = "backpack/workshop/player/items/sniper/fall17_riflemans_regalia/fall17_riflemans_regalia",
		models = {
			["basename"] = "models/workshop/player/items/sniper/fall17_riflemans_regalia/fall17_riflemans_regalia.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"feet"},
		paintable = false,
		name = "Kanga Kickers",
		localized_name = "info.tf2pm.hat.sf14_kanga_kickers",
		icon = "backpack/workshop/player/items/sniper/sf14_kanga_kickers/sf14_kanga_kickers",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sf14_kanga_kickers/sf14_kanga_kickers.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Brim-Full Of Bullets",
		localized_name = "info.tf2pm.hat.fall2013_brimfull_of_bullets",
		icon = "backpack/workshop/player/items/sniper/fall2013_brimfull_of_bullets/fall2013_brimfull_of_bullets",
		models = {
			["basename"] = "models/workshop/player/items/sniper/fall2013_brimfull_of_bullets/fall2013_brimfull_of_bullets.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.fall2013_brimfull_of_bullets_style0",
				models = {
					["basename"] = "models/workshop/player/items/sniper/fall2013_brimfull_of_bullets/fall2013_brimfull_of_bullets.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.fall2013_brimfull_of_bullets_style1",
				models = {
					["basename"] = "models/workshop/player/items/sniper/fall2013_brimfull_of_bullets_s2/fall2013_brimfull_of_bullets_s2.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.fall2013_brimfull_of_bullets_style2",
				models = {
					["basename"] = "models/workshop/player/items/sniper/fall2013_brimfull_of_bullets_s3/fall2013_brimfull_of_bullets_s3.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Marsupial Muzzle",
		localized_name = "info.tf2pm.hat.sf14_marsupial_muzzle",
		icon = "backpack/workshop/player/items/sniper/sf14_marsupial_muzzle/sf14_marsupial_muzzle",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sf14_marsupial_muzzle/sf14_marsupial_muzzle.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Shooter's Tin Topi",
		localized_name = "info.tf2pm.hat.robo_sniper_solar_topi",
		localized_description = "info.tf2pm.hat.robo_sniper_solar_topi_desc",
		icon = "backpack/workshop/player/items/sniper/robo_sniper_solar_topi/robo_sniper_solar_topi",
		models = {
			["basename"] = "models/workshop/player/items/sniper/robo_sniper_solar_topi/robo_sniper_solar_topi.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Missing Piece",
		localized_name = "info.tf2pm.hat.dec19_missing_piece",
		icon = "backpack/workshop/player/items/sniper/dec19_missing_piece/dec19_missing_piece",
		models = {
			["basename"] = "models/workshop/player/items/sniper/dec19_missing_piece/dec19_missing_piece.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_pocket"},
		paintable = true,
		name = "Bait and Bite",
		localized_name = "info.tf2pm.hat.fall17_bait_and_bite",
		localized_description = "info.tf2pm.hat.fall17_bait_and_bite_desc",
		icon = "backpack/workshop/player/items/sniper/fall17_bait_and_bite/fall17_bait_and_bite",
		models = {
			["basename"] = "models/workshop/player/items/sniper/fall17_bait_and_bite/fall17_bait_and_bite.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"whole_head"},
		paintable = true,
		name = "Corona Australis",
		localized_name = "info.tf2pm.hat.invasion_corona_australis",
		icon = "backpack/workshop/player/items/sniper/invasion_corona_australis/invasion_corona_australis",
		models = {
			["basename"] = "models/workshop/player/items/sniper/invasion_corona_australis/invasion_corona_australis.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_vest"},
		paintable = false,
		name = "The Extra Layer",
		localized_name = "info.tf2pm.hat.xms2013_sniper_layer_vest",
		icon = "backpack/workshop/player/items/sniper/xms2013_sniper_layer_vest/xms2013_sniper_layer_vest",
		models = {
			["basename"] = "models/workshop/player/items/sniper/xms2013_sniper_layer_vest/xms2013_sniper_layer_vest.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "Wagga Wagga Wear",
		localized_name = "info.tf2pm.hat.sum19_wagga_wagga_wear",
		icon = "backpack/workshop/player/items/sniper/sum19_wagga_wagga_wear/sum19_wagga_wagga_wear",
		models = {
			["basename"] = "models/workshop/player/items/sniper/sum19_wagga_wagga_wear/sum19_wagga_wagga_wear.mdl",
		},
	},
	{
		classes = {"sniper"},
		targets = {"sniper_vest"},
		paintable = false,
		name = "Starduster",
		localized_name = "info.tf2pm.hat.invasion_starduster",
		icon = "backpack/workshop/player/items/sniper/invasion_starduster/invasion_starduster",
		models = {
			["basename"] = "models/workshop/player/items/sniper/invasion_starduster/invasion_starduster.mdl",
		},
	},

}
