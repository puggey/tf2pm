
-- Auto generated at 2020-12-04 12:56:46 UTC+07:00
return {
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Mildly Disturbing Halloween Mask",
		localized_name = "info.tf2pm.hat.halloween_hat",
		localized_description = "info.tf2pm.hat.halloween_hat_desc",
		icon = "backpack/player/items/all_class/all_halloween",
		models = {
			["basename"] = "models/player/items/%s/%s_halloween.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Ghastly Gibus",
		localized_name = "info.tf2pm.hat.domination_hat_ghastlierest",
		localized_description = "info.tf2pm.hat.domination_hat_ghastlierest_desc",
		icon = "backpack/player/items/all_class/all_domination_b_demo",
		models = {
			["basename"] = "models/player/items/all_class/all_domination_b_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.gibus_style_ghastlier",
				models = {
					["basename"] = "models/player/items/all_class/all_domination_2009_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.gibus_style_ghastly",
				models = {
					["basename"] = "models/player/items/all_class/all_domination_%s.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.gibus_style_ghastlierest",
				models = {
					["basename"] = "models/player/items/all_class/all_domination_b_%s.mdl",
				}
			},
			[3] = {
				localized_name = "info.tf2pm.hat.gibus_style_ghostly",
				models = {
					["basename"] = "models/player/items/all_class/ghostly_gibus_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {},
		paintable = false,
		name = "Honest Halo",
		localized_name = "info.tf2pm.hat.honestyhalo",
		localized_description = "info.tf2pm.hat.honestyhalo_desc",
		icon = "backpack/player/items/all_class/all_halo",
		models = {
			["basename"] = "models/player/items/all_class/all_halo.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.honestyhalo_style0",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
			[1] = {
				localized_name = "info.tf2pm.hat.honestyhalo_style1",
			},
		},
		bodygroup_overrides = {
			["hat"] = 0,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "L4D Hat",
		localized_name = "info.tf2pm.hat.l4dhat",
		localized_description = "info.tf2pm.hat.l4dhat_desc",
		icon = "backpack/player/items/demo/demo_bill",
		models = {
			["basename"] = "models/player/items/%s/%s_bill.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Propaganda Contest First Place",
		localized_name = "info.tf2pm.hat.propagandacontest_firstplace",
		localized_description = "info.tf2pm.hat.propagandacontest_firstplace_desc",
		icon = "backpack/player/items/demo/hat_first",
		models = {
			["basename"] = "models/player/items/%s/hat_first.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Towering Pillar of Hats",
		localized_name = "info.tf2pm.hat.toweringpillar_hat",
		localized_description = "info.tf2pm.hat.toweringpillar_hat_desc",
		icon = "backpack/player/items/demo/hat_first_nr",
		models = {
			["basename"] = "models/player/items/%s/hat_first_nr.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Propaganda Contest Second Place",
		localized_name = "info.tf2pm.hat.propagandacontest_secondplace",
		localized_description = "info.tf2pm.hat.propagandacontest_secondplace_desc",
		icon = "backpack/player/items/demo/hat_second",
		models = {
			["basename"] = "models/player/items/%s/hat_second.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Noble Amassment of Hats",
		localized_name = "info.tf2pm.hat.nobleamassment_hat",
		localized_description = "info.tf2pm.hat.nobleamassment_hat_desc",
		icon = "backpack/player/items/demo/hat_second_nr",
		models = {
			["basename"] = "models/player/items/%s/hat_second_nr.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Propaganda Contest Third Place",
		localized_name = "info.tf2pm.hat.propagandacontest_thirdplace",
		localized_description = "info.tf2pm.hat.propagandacontest_thirdplace_desc",
		icon = "backpack/player/items/demo/hat_third",
		models = {
			["basename"] = "models/player/items/%s/hat_third.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Modest Pile of Hat",
		localized_name = "info.tf2pm.hat.modestpile_hat",
		localized_description = "info.tf2pm.hat.modestpile_hat_desc",
		icon = "backpack/player/items/demo/hat_third_nr",
		models = {
			["basename"] = "models/player/items/%s/hat_third_nr.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {},
		paintable = false,
		name = "OSX Item",
		localized_name = "info.tf2pm.hat.osxitem",
		localized_description = "info.tf2pm.hat.osxitem_desc",
		icon = "backpack/player/items/all_class/earbuds",
		models = {
			["basename"] = "models/player/items/%s/%s_earbuds.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {},
		paintable = true,
		name = "TTG Max Hat",
		localized_name = "info.tf2pm.hat.ttg_maxshat",
		localized_description = "info.tf2pm.hat.ttg_maxshat_desc",
		icon = "backpack/player/items/medic/medic_ttg_max",
		models = {
			["basename"] = "models/player/items/%s/%s_ttg_max.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Parasite Hat",
		localized_name = "info.tf2pm.hat.parasite_hat",
		localized_description = "info.tf2pm.hat.parasite_hat_desc",
		icon = "backpack/player/items/all_class/parasite_hat",
		models = {
			["basename"] = "models/player/items/all_class/parasite_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Dr's Dapper Topper",
		localized_name = "info.tf2pm.hat.seuss",
		icon = "backpack/workshop/player/items/all_class/dappertopper/dappertopper",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dappertopper/dappertopper_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Wiki Cap",
		localized_name = "info.tf2pm.hat.wikicap",
		localized_description = "info.tf2pm.hat.wikicap_desc",
		icon = "backpack/player/items/all_class/wikicap",
		models = {
			["basename"] = "models/player/items/all_class/wikicap_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Mann Co. Cap",
		localized_name = "info.tf2pm.hat.manncocap",
		icon = "backpack/player/items/all_class/all_manncap",
		models = {
			["basename"] = "models/player/items/%s/%s_cap.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Ellis Hat",
		localized_name = "info.tf2pm.hat.ellishat",
		icon = "backpack/player/items/all_class/all_ellis",
		models = {
			["basename"] = "models/player/items/%s/%s_ellis.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Halloween Mask - Scout",
		localized_name = "info.tf2pm.hat.halloween_mask_scout",
		icon = "backpack/player/items/all_class/halloween_bag_scout",
		models = {
			["basename"] = "models/player/items/all_class/halloween_bag_scout_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Halloween Mask - Sniper",
		localized_name = "info.tf2pm.hat.halloween_mask_sniper",
		icon = "backpack/player/items/all_class/halloween_bag_sniper",
		models = {
			["basename"] = "models/player/items/all_class/halloween_bag_sniper_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Halloween Mask - Soldier",
		localized_name = "info.tf2pm.hat.halloween_mask_soldier",
		icon = "backpack/player/items/all_class/halloween_bag_soldier",
		models = {
			["basename"] = "models/player/items/all_class/halloween_bag_soldier_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Halloween Mask - Demoman",
		localized_name = "info.tf2pm.hat.halloween_mask_demoman",
		icon = "backpack/player/items/all_class/halloween_bag_demo",
		models = {
			["basename"] = "models/player/items/all_class/halloween_bag_demo_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Halloween Mask - Medic",
		localized_name = "info.tf2pm.hat.halloween_mask_medic",
		icon = "backpack/player/items/all_class/halloween_bag_medic",
		models = {
			["basename"] = "models/player/items/all_class/halloween_bag_medic_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Halloween Mask - Heavy",
		localized_name = "info.tf2pm.hat.halloween_mask_heavy",
		icon = "backpack/player/items/all_class/halloween_bag_heavy",
		models = {
			["basename"] = "models/player/items/all_class/halloween_bag_heavy_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Halloween Mask - Spy",
		localized_name = "info.tf2pm.hat.halloween_mask_spy",
		icon = "backpack/player/items/all_class/halloween_bag_spy",
		models = {
			["basename"] = "models/player/items/all_class/halloween_bag_spy_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Halloween Mask - Engineer",
		localized_name = "info.tf2pm.hat.halloween_mask_engineer",
		icon = "backpack/player/items/all_class/halloween_bag_engineer",
		models = {
			["basename"] = "models/player/items/all_class/halloween_bag_engineer_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Halloween Mask - Pyro",
		localized_name = "info.tf2pm.hat.halloween_mask_pyro",
		icon = "backpack/player/items/all_class/halloween_bag_pyro",
		models = {
			["basename"] = "models/player/items/all_class/halloween_bag_pyro_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Halloween Mask - Saxton Hale",
		localized_name = "info.tf2pm.hat.halloween_mask_saxtonhale",
		icon = "backpack/player/items/all_class/halloween_bag_saxton",
		models = {
			["basename"] = "models/player/items/all_class/halloween_bag_saxton_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head"},
		paintable = false,
		name = "Horseless Headless Horseman's Head",
		localized_name = "info.tf2pm.hat.halloween_head",
		localized_description = "info.tf2pm.hat.halloween_head_desc",
		icon = "backpack/player/items/all_class/pumkin_hat",
		models = {
			["basename"] = "models/player/items/all_class/pumpkin_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Ghastly Gibus 2010",
		localized_name = "info.tf2pm.hat.domination_hat_ghastlier",
		localized_description = "info.tf2pm.hat.domination_hat_ghastlier_desc",
		icon = "backpack/player/items/all_class/all_domination_2009",
		models = {
			["basename"] = "models/player/items/all_class/all_domination_2009_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.gibus_style_ghastlier",
				models = {
					["basename"] = "models/player/items/all_class/all_domination_2009_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.gibus_style_ghastly",
				models = {
					["basename"] = "models/player/items/all_class/all_domination_%s.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.gibus_style_ghostly",
				models = {
					["basename"] = "models/player/items/all_class/ghostly_gibus_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Spine-Chilling Skull",
		localized_name = "info.tf2pm.hat.halloween_skullcap",
		localized_description = "info.tf2pm.hat.halloween_skullcap_desc",
		icon = "backpack/player/items/all_class/skull",
		models = {
			["basename"] = "models/player/items/all_class/skull_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Voodoo Juju",
		localized_name = "info.tf2pm.hat.halloween_voodoo",
		localized_description = "info.tf2pm.hat.halloween_voodoo_desc",
		icon = "backpack/player/items/all_class/voodoojuju_hat",
		models = {
			["basename"] = "models/player/items/all_class/voodoojuju_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Horrific Headsplitter",
		localized_name = "info.tf2pm.hat.horrific_headsplitter",
		localized_description = "info.tf2pm.hat.horrific_headsplitter_desc",
		icon = "backpack/player/items/all_class/headsplitter",
		models = {
			["basename"] = "models/player/items/all_class/headsplitter_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Camera Helm",
		localized_name = "info.tf2pm.hat.camerahelm",
		localized_description = "info.tf2pm.hat.camerahelm_desc",
		icon = "backpack/player/items/all_class/replay_hat",
		models = {
			["basename"] = "models/player/items/all_class/replay_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Treasure Hat 1",
		localized_name = "info.tf2pm.hat.treasurehat_1",
		localized_description = "info.tf2pm.hat.treasurehat_1_desc",
		icon = "backpack/player/items/all_class/treasure_hat_01",
		models = {
			["basename"] = "models/player/items/all_class/treasure_hat_01_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Treasure Hat 2",
		localized_name = "info.tf2pm.hat.treasurehat_2",
		localized_description = "info.tf2pm.hat.treasurehat_2_desc",
		icon = "backpack/player/items/all_class/treasure_hat_02",
		models = {
			["basename"] = "models/player/items/all_class/treasure_hat_02_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.treasurehat_2",
				models = {
					["basename"] = "models/player/items/all_class/treasure_hat_02_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.treasurehat_1",
				models = {
					["basename"] = "models/player/items/all_class/treasure_hat_01_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Treasure Hat 3",
		localized_name = "info.tf2pm.hat.treasurehat_3",
		localized_description = "info.tf2pm.hat.treasurehat_3_desc",
		icon = "backpack/player/items/all_class/treasure_hat_oct",
		models = {
			["basename"] = "models/player/items/%s/treasure_hat_oct.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.treasurehat_3_style",
				models = {
					["basename"] = "models/player/items/%s/treasure_hat_oct.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.treasurehat_2",
				models = {
					["basename"] = "models/player/items/all_class/treasure_hat_02_%s.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.treasurehat_1",
				models = {
					["basename"] = "models/player/items/all_class/treasure_hat_01_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "A Rather Festive Tree",
		localized_name = "info.tf2pm.hat.festivetree",
		icon = "backpack/player/items/all_class/oh_xmas_tree",
		models = {
			["basename"] = "models/player/items/all_class/oh_xmas_tree_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = false,
		name = "Friendly Item",
		localized_name = "info.tf2pm.hat.helpednewuserhat",
		localized_description = "info.tf2pm.hat.helpednewuserhat_desc",
		icon = "backpack/player/items/all_class/professor_speks",
		models = {
			["basename"] = "models/player/items/%s/professor_speks.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "MNC Hat",
		localized_name = "info.tf2pm.hat.mnc_hat",
		localized_description = "info.tf2pm.hat.mnc_hat_desc",
		icon = "backpack/player/items/all_class/mnc_hat",
		models = {
			["basename"] = "models/player/items/%s/%s_mnc.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Humanitarian's Hachimaki",
		description = "charity",
		localized_name = "info.tf2pm.hat.charityhat_a",
		localized_description = "info.tf2pm.hat.charityhat_desc",
		icon = "backpack/player/items/all_class/japan_hachimaki",
		models = {
			["basename"] = "models/player/items/%s/japan_hachimaki.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Benefactor's Kanmuri",
		description = "charity",
		localized_name = "info.tf2pm.hat.charityhat_b",
		localized_description = "info.tf2pm.hat.charityhat_desc",
		icon = "backpack/player/items/all_class/japan_hat",
		models = {
			["basename"] = "models/player/items/%s/japan_hat.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Mangnanimous Monarch",
		description = "charity",
		localized_name = "info.tf2pm.hat.charityhat_c",
		localized_description = "info.tf2pm.hat.charityhat_desc",
		icon = "backpack/player/items/all_class/japan_hat_monarch",
		models = {
			["basename"] = "models/player/items/%s/japan_hat_monarch.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Potato Hat",
		localized_name = "info.tf2pm.hat.potatohat",
		localized_description = "info.tf2pm.hat.potatohat_desc",
		icon = "backpack/player/items/all_class/hardhat",
		models = {
			["basename"] = "models/player/items/%s/hardhat.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.potatohatstyle_classified",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.potatohatstyle_logo",
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Lo-Fi Longwave",
		localized_name = "info.tf2pm.hat.towerhardhat",
		localized_description = "info.tf2pm.hat.towerhardhat_desc",
		icon = "backpack/player/items/demo/hardhat_tower",
		models = {
			["basename"] = "models/player/items/%s/hardhat_tower.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Loyalty Reward",
		localized_name = "info.tf2pm.hat.loyaltyreward",
		localized_description = "info.tf2pm.hat.loyaltyreward_desc",
		icon = "backpack/player/items/all_class/veteran_hat",
		models = {
			["basename"] = "models/player/items/%s/veteran_hat.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"face", "hat"},
		paintable = false,
		name = "Spiral Sallet",
		localized_name = "info.tf2pm.hat.spiralsallet",
		localized_description = "info.tf2pm.hat.spiralsallet_desc",
		icon = "backpack/player/items/soldier/soldier_spiral",
		models = {
			["basename"] = "models/player/items/%s/%s_spiral.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = true,
		name = "Summer Shades",
		localized_name = "info.tf2pm.hat.summer_shades",
		localized_description = "info.tf2pm.hat.summer_shades_desc",
		icon = "backpack/player/items/all_class/summer_shades",
		models = {
			["basename"] = "models/player/items/%s/summer_shades.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Summer Hat",
		localized_name = "info.tf2pm.hat.summerhat",
		localized_description = "info.tf2pm.hat.summerhat_desc",
		icon = "backpack/player/items/all_class/summer_hat_demo",
		models = {
			["basename"] = "models/player/items/%s/summer_hat_%s.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.summerhat_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.summerhat_style1",
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"lenses"},
		paintable = false,
		name = "The Deus Specs",
		localized_name = "info.tf2pm.hat.dex_glasses",
		icon = "backpack/workshop_partner/player/items/all_class/dex_glasses/dex_glasses",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/dex_glasses/dex_glasses_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Sarif Cap",
		localized_name = "info.tf2pm.hat.dex_hat",
		icon = "backpack/workshop_partner/player/items/all_class/dex_hat/dex_hat",
		models = {
			["basename"] = "models/workshop_partner/player/items/%s/dex_hat_%s/dex_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "TF Birthday Hat 2011",
		localized_name = "info.tf2pm.hat.birthday2011_hat",
		icon = "backpack/player/items/all_class/bdayhat_heavy",
		models = {
			["basename"] = "models/player/items/all_class/bdayhat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Killer Exclusive",
		localized_name = "info.tf2pm.hat.killerexclusive",
		localized_description = "info.tf2pm.hat.killerexclusive_desc",
		icon = "backpack/player/items/all_class/pcg_hat_engineer",
		models = {
			["basename"] = "models/player/items/all_class/pcg_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"necklace"},
		paintable = false,
		name = "Merc's Pride Scarf",
		localized_name = "info.tf2pm.hat.football_scarf",
		localized_description = "info.tf2pm.hat.football_scarf_desc",
		icon = "backpack/workshop_partner/player/items/demo/scarf_soccer/scarf_soccer",
		models = {
			["basename"] = "models/workshop_partner/player/items/%s/scarf_soccer/scarf_soccer.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Spine-Chilling Skull 2011",
		localized_name = "info.tf2pm.hat.halloween_skullcap2011",
		localized_description = "info.tf2pm.hat.halloween_skullcap2011_desc",
		icon = "backpack/player/items/all_class/skull_horns_b",
		models = {
			["basename"] = "models/player/items/%s/skull_horns_b.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.spinechillingskull2011_style1",
				models = {
					["basename"] = "models/player/items/%s/skull_horns_b.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.spinechillingskull2011_style2",
				models = {
					["basename"] = "models/player/items/%s/skull_horns_b2.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.spinechillingskull2011_style3",
				models = {
					["basename"] = "models/player/items/%s/skull_horns_b3.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Spine-Chilling Skull 2011 Style 1",
		localized_name = "info.tf2pm.hat.halloween_skullcap2011_style1",
		localized_description = "info.tf2pm.hat.halloween_skullcap2011_style1_desc",
		icon = "backpack/player/items/all_class/skull_horns_b",
		models = {
			["basename"] = "models/player/items/%s/skull_horns_b.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Spine-Chilling Skull 2011 Style 2",
		localized_name = "info.tf2pm.hat.halloween_skullcap2011_style2",
		localized_description = "info.tf2pm.hat.halloween_skullcap2011_style2_desc",
		icon = "backpack/player/items/all_class/skull_horns_b2",
		models = {
			["basename"] = "models/player/items/%s/skull_horns_b2.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Spine-Chilling Skull 2011 Style 3",
		localized_name = "info.tf2pm.hat.halloween_skullcap2011_style3",
		localized_description = "info.tf2pm.hat.halloween_skullcap2011_style3_desc",
		icon = "backpack/player/items/all_class/skull_horns_b3",
		models = {
			["basename"] = "models/player/items/%s/skull_horns_b3.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"whole_head", "beard"},
		paintable = false,
		name = "MONOCULUS!",
		localized_name = "info.tf2pm.hat.halloween_eyeball_boss_hat",
		localized_description = "info.tf2pm.hat.halloween_eyeball_boss_hat_desc",
		icon = "backpack/player/items/all_class/haunted_eyeball_hat",
		models = {
			["basename"] = "models/player/items/all_class/haunted_eyeball_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"face"},
		paintable = false,
		name = "Seal Mask",
		localized_name = "info.tf2pm.hat.halloween_seal_mask",
		localized_description = "info.tf2pm.hat.halloween_seal_mask_desc",
		icon = "backpack/player/items/all_class/seal_mask_demo",
		models = {
			["basename"] = "models/player/items/all_class/seal_mask_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Ghastly Gibus 2011",
		localized_name = "info.tf2pm.hat.domination_hat_ghastly",
		localized_description = "info.tf2pm.hat.domination_hat_ghastly_desc",
		icon = "backpack/player/items/all_class/all_domination",
		models = {
			["basename"] = "models/player/items/all_class/all_domination_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.gibus_style_ghastly",
				models = {
					["basename"] = "models/player/items/all_class/all_domination_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.gibus_style_ghostly",
				models = {
					["basename"] = "models/player/items/all_class/ghostly_gibus_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Manniversary Paper Hat",
		localized_name = "info.tf2pm.hat.manniversarypaperhat",
		icon = "backpack/player/items/all_class/paper_hat",
		models = {
			["basename"] = "models/player/items/%s/paper_hat.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Salty Dog",
		localized_name = "info.tf2pm.hat.soldierhat1",
		localized_description = "info.tf2pm.hat.soldierhat1_desc",
		icon = "backpack/workshop/player/items/all_class/fwk_seacaptain/fwk_seacaptain",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fwk_seacaptain/fwk_seacaptain_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.soldierhat1_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/fwk_seacaptain/fwk_seacaptain_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.soldierhat1_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/fwk_seacaptain_s2/fwk_seacaptain_s2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Hot Dogger",
		localized_name = "info.tf2pm.hat.scouthat1",
		localized_description = "info.tf2pm.hat.scouthat1_desc",
		icon = "backpack/workshop/player/items/all_class/fwk_hotdog/fwk_hotdog",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fwk_hotdog/fwk_hotdog_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"flair"},
		paintable = false,
		name = "Flair!",
		description = "can_customize_texture",
		localized_name = "info.tf2pm.hat.flairbuttons",
		localized_description = "info.tf2pm.hat.flairbuttons_desc",
		icon = "backpack/player/items/all_class/flair_buttons",
		models = {
			["basename"] = "models/player/items/all_class/flair_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Point and Shoot",
		localized_name = "info.tf2pm.hat.trnhat",
		localized_description = "info.tf2pm.hat.trnhat_desc",
		icon = "backpack/player/items/all_class/trn_wiz_hat_demo",
		models = {
			["basename"] = "models/player/items/all_class/trn_wiz_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Top Notch",
		localized_name = "info.tf2pm.hat.topnotch",
		localized_description = "info.tf2pm.hat.topnotch_desc",
		icon = "backpack/player/items/all_class/notch_head_demo",
		models = {
			["basename"] = "models/player/items/all_class/notch_head_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The B.M.O.C.",
		localized_name = "info.tf2pm.hat.winter2011_santahat",
		localized_description = "info.tf2pm.hat.winter2011_santahat_desc",
		icon = "backpack/player/items/all_class/xms_santa_hat_demo",
		models = {
			["basename"] = "models/player/items/all_class/xms_santa_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat", "necklace", "beard"},
		paintable = false,
		name = "The Holiday Headcase",
		localized_name = "info.tf2pm.hat.winter2011_gifthat",
		localized_description = "info.tf2pm.hat.winter2011_gifthat_desc",
		icon = "backpack/player/items/all_class/xms_gift_hat_demo",
		models = {
			["basename"] = "models/player/items/all_class/xms_gift_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Full Head of Steam",
		localized_name = "info.tf2pm.hat.foundryachievementhat",
		localized_description = "info.tf2pm.hat.foundryachievementhat_desc",
		icon = "backpack/player/items/all_class/xms_steamwhistle_spy",
		models = {
			["basename"] = "models/player/items/all_class/xms_steamwhistle_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Brown Bomber",
		localized_name = "info.tf2pm.hat.winter2011_winterfurcap",
		localized_description = "info.tf2pm.hat.winter2011_winterfurcap_desc",
		icon = "backpack/player/items/all_class/xms_furcap_demo",
		models = {
			["basename"] = "models/player/items/all_class/xms_furcap_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.winter2011_winterfurcap_style0",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.winter2011_winterfurcap_style1",
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Ebenezer",
		localized_name = "info.tf2pm.hat.winter2011_ebenezer",
		localized_description = "info.tf2pm.hat.winter2011_ebenezer_desc",
		icon = "backpack/player/items/all_class/xms_winter_joy_hat_demo",
		models = {
			["basename"] = "models/player/items/all_class/xms_winter_joy_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"face", "hat"},
		paintable = true,
		name = "The Warsworn Helmet",
		localized_name = "info.tf2pm.hat.warswornhelmet",
		localized_description = "info.tf2pm.hat.warswornhelmet_desc",
		icon = "backpack/player/items/all_class/all_reckoning_eagonn_spy",
		models = {
			["basename"] = "models/player/items/all_class/all_reckoning_eagonn_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Daily Duel Winner Reward Hat",
		localized_name = "info.tf2pm.hat.dailyduelwinnerrewardhat",
		localized_description = "info.tf2pm.hat.dailyduelwinnerrewardhat_desc",
		icon = "backpack/player/items/all_class/dueler_demo",
		models = {
			["basename"] = "models/player/items/all_class/dueler_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Daily Gift Giver Reward Hat",
		localized_name = "info.tf2pm.hat.dailygiftgiverrewardhat",
		localized_description = "info.tf2pm.hat.dailygiftgiverrewardhat_desc",
		icon = "backpack/player/items/all_class/generous_demo",
		models = {
			["basename"] = "models/player/items/all_class/generous_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Daily Map Stamp Reward Hat",
		localized_name = "info.tf2pm.hat.dailymapstamprewardhat",
		localized_description = "info.tf2pm.hat.dailymapstamprewardhat_desc",
		icon = "backpack/player/items/all_class/stamper_demo",
		models = {
			["basename"] = "models/player/items/all_class/stamper_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Pet Robro",
		localized_name = "info.tf2pm.hat.pet_robro",
		localized_description = "info.tf2pm.hat.pet_robro_desc",
		icon = "backpack/player/items/all_class/pet_robro",
		models = {
			["basename"] = "models/player/items/all_class/pet_robro.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Pet Balloonicorn",
		localized_name = "info.tf2pm.hat.pet_balloonicorn",
		localized_description = "info.tf2pm.hat.pet_balloonicorn_desc",
		icon = "backpack/player/items/all_class/pet_balloonicorn",
		models = {
			["basename"] = "models/player/items/all_class/pet_balloonicorn.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = false,
		name = "Autogrant Pyrovision Goggles",
		localized_name = "info.tf2pm.hat.pyrovision_goggles",
		localized_description = "info.tf2pm.hat.pyrovision_goggles_desc",
		icon = "backpack/player/items/all_class/pyrovision_goggles_heavy",
		models = {
			["heavy"] = "models/player/items/all_class/pyrovision_goggles_heavy.mdl",
			["medic"] = "models/player/items/all_class/pyrovision_goggles_medic.mdl",
			["scout"] = "models/player/items/all_class/pyrovision_goggles_scout.mdl",
			["pyro"] = "models/player/items/all_class/pyrovision_goggles_pyro.mdl",
			["demoman"] = "models/player/items/all_class/pyrovision_goggles_demo.mdl",
			["soldier"] = "models/player/items/all_class/pyrovision_goggles_soldier.mdl",
			["spy"] = "models/player/items/all_class/pyrovision_goggles_spy.mdl",
			["engineer"] = "models/player/items/all_class/pyrovision_goggles_engineer.mdl",
			["sniper"] = "models/player/items/all_class/pyrovision_goggles_sniper.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = false,
		name = "Pyrovision Goggles",
		localized_name = "info.tf2pm.hat.pyrovision_goggles",
		localized_description = "info.tf2pm.hat.pyrovision_goggles_desc",
		icon = "backpack/player/items/all_class/pyrovision_goggles_heavy",
		models = {
			["heavy"] = "models/player/items/all_class/pyrovision_goggles_heavy.mdl",
			["medic"] = "models/player/items/all_class/pyrovision_goggles_medic.mdl",
			["scout"] = "models/player/items/all_class/pyrovision_goggles_scout.mdl",
			["pyro"] = "models/player/items/all_class/pyrovision_goggles_pyro.mdl",
			["demoman"] = "models/player/items/all_class/pyrovision_goggles_demo.mdl",
			["soldier"] = "models/player/items/all_class/pyrovision_goggles_soldier.mdl",
			["spy"] = "models/player/items/all_class/pyrovision_goggles_spy.mdl",
			["engineer"] = "models/player/items/all_class/pyrovision_goggles_engineer.mdl",
			["sniper"] = "models/player/items/all_class/pyrovision_goggles_sniper.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Bolt Action Blitzer",
		localized_name = "info.tf2pm.hat.crimecrafthelmet",
		localized_description = "info.tf2pm.hat.crimecrafthelmet_desc",
		icon = "backpack/player/items/all_class/crimecraft_helmet_demo",
		models = {
			["basename"] = "models/player/items/all_class/crimecraft_helmet_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Gentle Munitionne of Leisure",
		localized_name = "info.tf2pm.hat.doomsdayachievementhat",
		localized_description = "info.tf2pm.hat.doomsdayachievementhat_desc",
		icon = "backpack/player/items/all_class/sd_rocket_spy",
		models = {
			["basename"] = "models/player/items/all_class/sd_rocket_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Robot Chicken Hat",
		localized_name = "info.tf2pm.hat.robotchickenhat",
		localized_description = "info.tf2pm.hat.robotchickenhat_desc",
		icon = "backpack/workshop_partner/player/items/all_class/as_robot_chicken/as_robot_chicken_demo",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/as_robot_chicken/as_robot_chicken_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.robotchickenhat_style0",
				models = {
					["basename"] = "models/workshop_partner/player/items/all_class/as_robot_chicken/as_robot_chicken_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.robotchickenhat_style1",
				models = {
					["basename"] = "models/workshop_partner/player/items/all_class/as_robot_chicken/as_robot_chicken_%s_b.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = true,
		name = "The Marxman",
		localized_name = "info.tf2pm.hat.sd_glasses",
		localized_description = "info.tf2pm.hat.sd_glasses_desc",
		icon = "backpack/workshop_partner/player/items/all_class/sd_glasses/sd_glasses",
		models = {
			["heavy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_heavy.mdl",
			["medic"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_medic.mdl",
			["scout"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_scout.mdl",
			["pyro"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_pyro.mdl",
			["demoman"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_demo.mdl",
			["soldier"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_soldier.mdl",
			["spy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_spy.mdl",
			["engineer"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_engineer.mdl",
			["sniper"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_sniper.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.sd_glasses_style0",
				models = {
					["heavy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_heavy.mdl",
					["medic"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_medic.mdl",
					["scout"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_scout.mdl",
					["pyro"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_pyro.mdl",
					["demoman"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_demo.mdl",
					["soldier"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_soldier.mdl",
					["spy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_spy.mdl",
					["engineer"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_engineer.mdl",
					["sniper"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_sniper.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.sd_glasses_style1",
				models = {
					["heavy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_heavy_cigar.mdl",
					["medic"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_medic_cigar.mdl",
					["scout"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_scout_cigar.mdl",
					["pyro"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_pyro_cigar.mdl",
					["demoman"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_demo_cigar.mdl",
					["soldier"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_soldier_cigar.mdl",
					["spy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_spy_cigar.mdl",
					["engineer"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_engineer_cigar.mdl",
					["sniper"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_sniper_cigar.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Human Cannonball",
		localized_name = "info.tf2pm.hat.sd_helmet",
		localized_description = "info.tf2pm.hat.sd_helmet_desc",
		icon = "backpack/workshop_partner/player/items/all_class/sd_helm/sd_helm_demo",
		models = {
			["heavy"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_heavy.mdl",
			["medic"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_medic.mdl",
			["scout"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_scout.mdl",
			["pyro"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_pyro.mdl",
			["demoman"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_demo.mdl",
			["soldier"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_soldier.mdl",
			["spy"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_spy.mdl",
			["engineer"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_engineer.mdl",
			["sniper"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_sniper.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sd_helmet_style0",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.sd_helmet_style1",
			},
			[2] = {
				responsive_skins = {4, 5},
				localized_name = "info.tf2pm.hat.sd_helmet_style2",
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = true,
		name = "Promo Marxman",
		localized_name = "info.tf2pm.hat.sd_glasses",
		localized_description = "info.tf2pm.hat.sd_glasses_desc",
		icon = "backpack/workshop_partner/player/items/all_class/sd_glasses/sd_glasses",
		models = {
			["heavy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_heavy.mdl",
			["medic"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_medic.mdl",
			["scout"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_scout.mdl",
			["pyro"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_pyro.mdl",
			["demoman"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_demo.mdl",
			["soldier"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_soldier.mdl",
			["spy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_spy.mdl",
			["engineer"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_engineer.mdl",
			["sniper"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_sniper.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.sd_glasses_style0",
				models = {
					["heavy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_heavy.mdl",
					["medic"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_medic.mdl",
					["scout"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_scout.mdl",
					["pyro"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_pyro.mdl",
					["demoman"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_demo.mdl",
					["soldier"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_soldier.mdl",
					["spy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_spy.mdl",
					["engineer"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_engineer.mdl",
					["sniper"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_sniper.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.sd_glasses_style1",
				models = {
					["heavy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_heavy_cigar.mdl",
					["medic"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_medic_cigar.mdl",
					["scout"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_scout_cigar.mdl",
					["pyro"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_pyro_cigar.mdl",
					["demoman"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_demo_cigar.mdl",
					["soldier"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_soldier_cigar.mdl",
					["spy"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_spy_cigar.mdl",
					["engineer"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_engineer_cigar.mdl",
					["sniper"] = "models/workshop_partner/player/items/all_class/sd_glasses/sd_glasses_sniper_cigar.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Promo Human Cannonball",
		localized_name = "info.tf2pm.hat.sd_helmet",
		localized_description = "info.tf2pm.hat.sd_helmet_desc",
		icon = "backpack/workshop_partner/player/items/all_class/sd_helm/sd_helm_demo",
		models = {
			["heavy"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_heavy.mdl",
			["medic"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_medic.mdl",
			["scout"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_scout.mdl",
			["pyro"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_pyro.mdl",
			["demoman"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_demo.mdl",
			["soldier"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_soldier.mdl",
			["spy"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_spy.mdl",
			["engineer"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_engineer.mdl",
			["sniper"] = "models/workshop_partner/player/items/all_class/sd_helm/sd_helm_sniper.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sd_helmet_style0",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.sd_helmet_style1",
			},
			[2] = {
				responsive_skins = {4, 5},
				localized_name = "info.tf2pm.hat.sd_helmet_style2",
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"belt_misc"},
		paintable = true,
		name = "The Rump-o'-Lantern",
		localized_name = "info.tf2pm.hat.rumpolantern",
		localized_description = "info.tf2pm.hat.rumpolantern_desc",
		icon = "backpack/player/items/all_class/pumpkin_lantern_engineer",
		models = {
			["basename"] = "models/player/items/all_class/pumpkin_lantern_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Crone's Dome",
		localized_name = "info.tf2pm.hat.cronesdome",
		localized_description = "info.tf2pm.hat.cronesdome_desc",
		icon = "backpack/workshop/player/items/all_class/witchhat/witchhat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/witchhat/witchhat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Executioner",
		localized_name = "info.tf2pm.hat.executioner",
		localized_description = "info.tf2pm.hat.executioner_desc",
		icon = "backpack/player/items/all_class/executionerhood_medic",
		models = {
			["basename"] = "models/player/items/all_class/executionerhood_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "The Boo Balloon",
		localized_name = "info.tf2pm.hat.booballoon",
		localized_description = "info.tf2pm.hat.booballoon_desc",
		icon = "backpack/player/items/all_class/hwn_pet_balloon",
		models = {
			["basename"] = "models/player/items/all_class/hwn_pet_balloon.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.booballoon_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.booballoon_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.booballoon_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.booballoon_style3",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = false,
		name = "The Unknown Monkeynaut",
		localized_name = "info.tf2pm.hat.unknownmonkeynaut",
		localized_description = "info.tf2pm.hat.unknownmonkeynaut_desc",
		icon = "backpack/player/items/all_class/hwn_ghost_pj",
		models = {
			["basename"] = "models/player/items/all_class/hwn_ghost_pj.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = false,
		name = "The Dead Little Buddy",
		localized_name = "info.tf2pm.hat.deadlittlebuddy",
		localized_description = "info.tf2pm.hat.deadlittlebuddy_desc",
		icon = "backpack/player/items/all_class/hwn_pet_ghost",
		models = {
			["heavy"] = "models/player/items/all_class/hwn_pet_ghost.mdl",
			["medic"] = "models/player/items/all_class/hwn_pet_ghost.mdl",
			["scout"] = "models/player/items/all_class/hwn_pet_ghost.mdl",
			["pyro"] = "models/player/items/all_class/hwn_pet_ghost_pyro.mdl",
			["demoman"] = "models/player/items/all_class/hwn_pet_ghost_demo.mdl",
			["soldier"] = "models/player/items/all_class/hwn_pet_ghost.mdl",
			["spy"] = "models/player/items/all_class/hwn_pet_ghost.mdl",
			["engineer"] = "models/player/items/all_class/hwn_pet_ghost.mdl",
			["sniper"] = "models/player/items/all_class/hwn_pet_ghost.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Ghostly Gibus",
		localized_name = "info.tf2pm.hat.domination_hat_ghostly",
		localized_description = "info.tf2pm.hat.domination_hat_ghostly_desc",
		icon = "backpack/player/items/all_class/ghostly_gibus_demo",
		models = {
			["basename"] = "models/player/items/all_class/ghostly_gibus_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Skull Island Topper",
		localized_name = "info.tf2pm.hat.halloween_merasmusloot_hat",
		localized_description = "info.tf2pm.hat.halloween_merasmusloot_hat_desc",
		icon = "backpack/player/items/all_class/merasmus_skull",
		models = {
			["basename"] = "models/player/items/all_class/merasmus_skull_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Cockfighter",
		localized_name = "info.tf2pm.hat.scribhat",
		localized_description = "info.tf2pm.hat.scribhat_desc",
		icon = "backpack/player/items/all_class/all_scrib_m_demo",
		models = {
			["basename"] = "models/player/items/all_class/all_scrib_m_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.scribhat_style0",
				models = {
					["basename"] = "models/player/items/all_class/all_scrib_m_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.scribhat_style1",
				models = {
					["basename"] = "models/player/items/all_class/all_scrib_l_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "That '70s Chapeau",
		localized_name = "info.tf2pm.hat.hm_disguisehat",
		localized_description = "info.tf2pm.hat.hm_disguisehat_desc",
		icon = "backpack/workshop_partner/player/items/all_class/hm_disguisehat/hm_disguisehat_demo",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/hm_disguisehat/hm_disguisehat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"shirt"},
		paintable = false,
		name = "The Tuxxy",
		localized_name = "info.tf2pm.hat.tuxxy",
		localized_description = "info.tf2pm.hat.tuxxy_desc",
		icon = "backpack/player/items/all_class/tuxxy_demo",
		models = {
			["basename"] = "models/player/items/all_class/tuxxy_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Tough Stuff Muffs",
		localized_name = "info.tf2pm.hat.toughstuffmuffs",
		localized_description = "info.tf2pm.hat.toughstuffmuffs_desc",
		icon = "backpack/workshop/player/items/all_class/all_earmuffs_style1/all_earmuffs_style1",
		models = {
			["basename"] = "models/workshop/player/items/all_class/all_earmuffs_style1/all_earmuffs_style1_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.toughstuffmuffs_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/all_earmuffs_style1/all_earmuffs_style1_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.toughstuffmuffs_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/all_earmuffs_style2/all_earmuffs_style2_%s.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.toughstuffmuffs_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/all_earmuffs_style3/all_earmuffs_style3_%s.mdl",
				}
			},
			[3] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.toughstuffmuffs_style3",
				models = {
					["basename"] = "models/workshop/player/items/all_class/all_earmuffs_style4/all_earmuffs_style4_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"necklace"},
		paintable = true,
		name = "The Merc's Muffler",
		localized_name = "info.tf2pm.hat.themercsmuffler",
		localized_description = "info.tf2pm.hat.themercsmuffler_desc",
		icon = "backpack/player/items/all_class/all_winter_scarf_engy",
		models = {
			["engineer"] = "models/player/items/all_class/all_winter_scarf_engy.mdl",
			["basename"] = "models/player/items/all_class/all_winter_scarf_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {},
		paintable = false,
		name = "Antlers",
		localized_name = "info.tf2pm.hat.antlers",
		localized_description = "info.tf2pm.hat.antlers_desc",
		icon = "backpack/player/items/all_class/xms_antlers_demo",
		models = {
			["engineer"] = "models/player/items/all_class/xms_antlers_engy.mdl",
			["basename"] = "models/player/items/all_class/xms_antlers_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.antlers_style_withhat",
			},
			[1] = {
				localized_name = "info.tf2pm.hat.antlers_style_withnohat",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Mann Co. Online Cap",
		localized_name = "info.tf2pm.hat.manncoonlinecap",
		icon = "backpack/player/items/all_class/mannco_online",
		models = {
			["basename"] = "models/player/items/%s/%s_cap_online.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = false,
		name = "Pet Reindoonicorn",
		localized_name = "info.tf2pm.hat.pet_reindoonicorn",
		localized_description = "info.tf2pm.hat.pet_reindoonicorn_desc",
		icon = "backpack/player/items/all_class/pet_reinballoonicorn",
		models = {
			["basename"] = "models/player/items/all_class/pet_reinballoonicorn.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {},
		paintable = false,
		name = "Tux",
		localized_name = "info.tf2pm.hat.linuxitem",
		localized_description = "info.tf2pm.hat.linuxitem_desc",
		icon = "backpack/player/items/all_class/all_penguin",
		models = {
			["basename"] = "models/player/items/all_class/all_penguin.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.linuxitem_style0",
				models = {
					["basename"] = "models/player/items/all_class/all_penguin.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.linuxitem_style1",
				models = {
					["basename"] = "models/player/items/all_class/all_penguin_demo.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.linuxitem_style2",
				models = {
					["basename"] = "models/player/items/all_class/all_penguin_pyro.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Brutal Bouffant",
		localized_name = "info.tf2pm.hat.brutalbouffant",
		localized_description = "info.tf2pm.hat.brutalbouffant_desc",
		icon = "backpack/workshop_partner/player/items/all_class/brutal_hair/brutal_hair_demo",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/brutal_hair/brutal_hair_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.nohat_style",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
			[1] = {
				localized_name = "info.tf2pm.hat.nohat_noheadphones_style",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"belt_misc"},
		paintable = true,
		name = "The Fortune Hunter",
		localized_name = "info.tf2pm.hat.fortunehunter",
		localized_description = "info.tf2pm.hat.fortunehunter_desc",
		icon = "backpack/player/items/all_class/tomb_pick_demo",
		models = {
			["basename"] = "models/player/items/all_class/tomb_pick_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = false,
		name = "The TF2VRH",
		localized_name = "info.tf2pm.hat.tf2vrh",
		localized_description = "info.tf2pm.hat.tf2vrh_desc",
		icon = "backpack/player/items/all_class/all_class_oculus_demo",
		models = {
			["basename"] = "models/player/items/all_class/all_class_oculus_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.tf2vrh_style0",
				models = {
					["basename"] = "models/player/items/all_class/all_class_oculus_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.tf2vrh_style1",
				models = {
					["heavy"] = "models/player/items/all_class/all_class_oculus_heavy_on.mdl",
					["medic"] = "models/player/items/all_class/all_class_oculus_medic_on.mdl",
					["scout"] = "models/player/items/all_class/all_class_oculus_scout_on.mdl",
					["pyro"] = "models/player/items/all_class/all_class_oculus_pyro_on.mdl",
					["demoman"] = "models/player/items/all_class/all_class_oculus_demo_on.mdl",
					["soldier"] = "models/player/items/all_class/all_class_oculus_soldier_on.mdl",
					["spy"] = "models/player/items/all_class/all_class_oculus_spy_on.mdl",
					["engineer"] = "models/player/items/all_class/all_class_oculus_engineer_on.mdl",
					["sniper"] = "models/player/items/all_class/all_class_oculus_sniper_on.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Conspiracy Cap",
		localized_name = "info.tf2pm.hat.conspiracycap",
		localized_description = "info.tf2pm.hat.conspiracycap_desc",
		icon = "backpack/player/items/all_class/all_class_reddit_demo",
		models = {
			["heavy"] = "models/player/items/all_class/all_class_reddit_heavy.mdl",
			["pyro"] = "models/player/items/all_class/all_class_reddit_pyro.mdl",
			["medic"] = "models/player/items/all_class/all_class_reddit_medic.mdl",
			["demoman"] = "models/player/items/all_class/all_class_reddit_demo.mdl",
			["spy"] = "models/player/items/all_class/all_class_reddit_spy.mdl",
			["basename"] = "models/player/items/all_class/all_class_reddit_%s_hat.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.conspiracycap_style0",
				models = {
					["heavy"] = "models/player/items/all_class/all_class_reddit_heavy.mdl",
					["pyro"] = "models/player/items/all_class/all_class_reddit_pyro.mdl",
					["medic"] = "models/player/items/all_class/all_class_reddit_medic.mdl",
					["demoman"] = "models/player/items/all_class/all_class_reddit_demo.mdl",
					["spy"] = "models/player/items/all_class/all_class_reddit_spy.mdl",
					["basename"] = "models/player/items/all_class/all_class_reddit_%s_hat.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.conspiracycap_style1",
				bodygroup_overrides = {
					["hat"] = 1,
				},
				models = {
					["basename"] = "models/player/items/all_class/all_class_reddit_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Public Accessor",
		localized_name = "info.tf2pm.hat.publicaccessor",
		localized_description = "info.tf2pm.hat.publicaccessor_desc",
		icon = "backpack/player/items/all_class/all_class_reddit_alt_demo",
		models = {
			["heavy"] = "models/player/items/all_class/all_class_reddit_alt_heavy.mdl",
			["pyro"] = "models/player/items/all_class/all_class_reddit_alt_pyro.mdl",
			["medic"] = "models/player/items/all_class/all_class_reddit_alt_medic.mdl",
			["demoman"] = "models/player/items/all_class/all_class_reddit_alt_demo.mdl",
			["spy"] = "models/player/items/all_class/all_class_reddit_alt_spy.mdl",
			["basename"] = "models/player/items/all_class/all_class_reddit_alt_%s_hat.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.publicaccessor_style0",
				models = {
					["heavy"] = "models/player/items/all_class/all_class_reddit_alt_heavy.mdl",
					["pyro"] = "models/player/items/all_class/all_class_reddit_alt_pyro.mdl",
					["medic"] = "models/player/items/all_class/all_class_reddit_alt_medic.mdl",
					["demoman"] = "models/player/items/all_class/all_class_reddit_alt_demo.mdl",
					["spy"] = "models/player/items/all_class/all_class_reddit_alt_spy.mdl",
					["basename"] = "models/player/items/all_class/all_class_reddit_alt_%s_hat.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.publicaccessor_style1",
				bodygroup_overrides = {
					["hat"] = 1,
				},
				models = {
					["basename"] = "models/player/items/all_class/all_class_reddit_alt_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Grandmaster",
		localized_name = "info.tf2pm.hat.grandmaster",
		localized_description = "info.tf2pm.hat.grandmaster_desc",
		icon = "backpack/player/items/all_class/chess_hat",
		models = {
			["basename"] = "models/player/items/all_class/chess_%s_red.mdl",
		},
		styles = {
			[0] = {
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Towering Pillar of Summer Shades",
		localized_name = "info.tf2pm.hat.toweringpillarofsummershades",
		icon = "backpack/player/items/all_class/summer_deal_demo",
		models = {
			["basename"] = "models/player/items/all_class/summer_deal_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Finder's Fee",
		localized_name = "info.tf2pm.hat.findersfee",
		localized_description = "info.tf2pm.hat.findersfee_desc",
		icon = "backpack/player/items/all_class/all_bug_hat_demo",
		models = {
			["basename"] = "models/player/items/all_class/all_bug_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Civilian Grade JACK Hat",
		localized_name = "info.tf2pm.hat.civiliangradejackhat",
		localized_description = "info.tf2pm.hat.civiliangradejackhat_desc",
		icon = "backpack/workshop_partner/player/items/all_class/jackhead/demo_jackhead",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/jackhead/%s_jackhead.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Military Grade JACK Hat",
		localized_name = "info.tf2pm.hat.militarygradejackhat",
		localized_description = "info.tf2pm.hat.militarygradejackhat_desc",
		icon = "backpack/workshop_partner/player/items/all_class/jackhead/demo_jackhead_digicamo",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/jackhead/%s_jackhead_digicamo.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Unusual Cap",
		localized_name = "info.tf2pm.hat.unusualcap",
		icon = "backpack/player/items/all_class/up_hat_demo",
		models = {
			["basename"] = "models/player/items/all_class/up_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Audio File",
		localized_name = "info.tf2pm.hat.audiofile",
		localized_description = "info.tf2pm.hat.audiofile_desc",
		icon = "backpack/player/items/all_class/soundtrack_hat",
		models = {
			["basename"] = "models/player/items/all_class/soundtrack_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Saxton Hat",
		localized_name = "info.tf2pm.hat.saxtonhat",
		localized_description = "info.tf2pm.hat.saxtonhat_desc",
		icon = "backpack/player/items/all_class/saxton_hat",
		models = {
			["basename"] = "models/player/items/all_class/saxton_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Yeti Park Cap",
		localized_name = "info.tf2pm.hat.monstrousmemento",
		localized_description = "info.tf2pm.hat.monstrousmemento_desc",
		icon = "backpack/player/items/all_class/yeti_park_cap",
		models = {
			["basename"] = "models/player/items/all_class/yeti_park_cap_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Mercenary Park Cap",
		localized_name = "info.tf2pm.hat.mercenarypark",
		localized_description = "info.tf2pm.hat.mercenarypark_desc",
		icon = "backpack/player/items/all_class/mercenary_park_cap",
		models = {
			["basename"] = "models/player/items/all_class/yeti_park_cap_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Mannanas Cap",
		localized_name = "info.tf2pm.hat.mannanashat",
		localized_description = "info.tf2pm.hat.mannanashat_desc",
		icon = "backpack/player/items/all_class/mannanas_cap",
		models = {
			["basename"] = "models/player/items/all_class/yeti_park_cap_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Never Forget Cap",
		localized_name = "info.tf2pm.hat.neverforgethat",
		localized_description = "info.tf2pm.hat.neverforgethat_desc",
		icon = "backpack/player/items/all_class/never_forget_cap",
		models = {
			["basename"] = "models/player/items/all_class/yeti_park_cap_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Yeti Park Hardhat",
		localized_name = "info.tf2pm.hat.yetiparkhardhat",
		localized_description = "info.tf2pm.hat.yetiparkhardhat_desc",
		icon = "backpack/player/items/demo/yeti_hardhat",
		models = {
			["basename"] = "models/player/items/%s/yeti_hardhat.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "World Traveler",
		localized_name = "info.tf2pm.hat.worldtraveler",
		localized_description = "info.tf2pm.hat.worldtraveler_desc",
		icon = "backpack/player/items/all_class/world_traveller",
		models = {
			["basename"] = "models/player/items/all_class/world_traveller_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Guano",
		localized_name = "info.tf2pm.hat.hw2013_the_fire_bat_v2",
		icon = "backpack/workshop/player/items/all_class/hw2013_the_fire_bat_v2/hw2013_the_fire_bat_v2",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_the_fire_bat_v2/hw2013_the_fire_bat_v2_%s.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"head_skin"},
		paintable = false,
		name = "Arkham Cowl",
		localized_name = "info.tf2pm.hat.bak_arkham_cowl",
		icon = "backpack/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl",
		models = {
			["heavy"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_heavy.mdl",
			["medic"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_medic.mdl",
			["scout"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_scout.mdl",
			["pyro"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_pyro.mdl",
			["soldier"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_soldier.mdl",
			["demoman"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_demo.mdl",
			["spy"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_spy.mdl",
			["engineer"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_engineer.mdl",
			["sniper"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_sniper.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Kiss King",
		localized_name = "info.tf2pm.hat.xms2013_kissking",
		icon = "backpack/workshop/player/items/all_class/xms2013_kissking/xms2013_kissking",
		models = {
			["basename"] = "models/workshop/player/items/all_class/xms2013_kissking/xms2013_kissking_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"ears"},
		paintable = false,
		name = "Festive Fascinator",
		localized_name = "info.tf2pm.hat.dec20_festive_fascinator",
		icon = "backpack/workshop/player/items/all_class/dec20_festive_fascinator/dec20_festive_fascinator",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_festive_fascinator/dec20_festive_fascinator_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.dec20_festive_fascinator_style0",
			},
			[1] = {
				localized_name = "info.tf2pm.hat.dec20_festive_fascinator_style1",
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Potassium Bonnett",
		localized_name = "info.tf2pm.hat.cc_summer2015_potassium_bonnett",
		icon = "backpack/workshop/player/items/all_class/cc_summer2015_potassium_bonnett/cc_summer2015_potassium_bonnett",
		models = {
			["basename"] = "models/workshop/player/items/all_class/cc_summer2015_potassium_bonnett/cc_summer2015_potassium_bonnett_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"feet"},
		paintable = true,
		name = "Loaf Loafers",
		localized_name = "info.tf2pm.hat.sum20_loaf_loafers",
		icon = "backpack/workshop/player/items/all_class/sum20_loaf_loafers_style1/sum20_loaf_loafers_style1",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sum20_loaf_loafers_style2/sum20_loaf_loafers_style2_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.sum20_loaf_loafers_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/sum20_loaf_loafers_style2/sum20_loaf_loafers_style2_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.sum20_loaf_loafers_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/sum20_loaf_loafers_style1/sum20_loaf_loafers_style1_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"belt_misc"},
		paintable = true,
		name = "Pocket Santa",
		localized_name = "info.tf2pm.hat.dec17_pocket_santa",
		icon = "backpack/workshop/player/items/all_class/dec17_pocket_santa/dec17_pocket_santa",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec17_pocket_santa/dec17_pocket_santa_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Professional's Pom Pom",
		localized_name = "info.tf2pm.hat.dec20_professionals_pom_pom",
		icon = "backpack/workshop/player/items/all_class/dec20_professionals_pom_pom/dec20_professionals_pom_pom",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_professionals_pom_pom/dec20_professionals_pom_pom_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Shoestring Santa",
		localized_name = "info.tf2pm.hat.dec20_shoestring_santa",
		icon = "backpack/workshop/player/items/all_class/dec20_shoestring_santa/dec20_shoestring_santa",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_shoestring_santa/dec20_shoestring_santa_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"belt_misc"},
		paintable = true,
		name = "Catastrophic Companions",
		localized_name = "info.tf2pm.hat.hwn2015_catastrophic_companions",
		icon = "backpack/workshop/player/items/all_class/hwn2015_catastrophic_companions/hwn2015_catastrophic_companions",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2015_catastrophic_companions/hwn2015_catastrophic_companions_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Hong Kong Cone",
		localized_name = "info.tf2pm.hat.fall2013_hong_kong_cone",
		icon = "backpack/workshop/player/items/all_class/fall2013_hong_kong_cone/fall2013_hong_kong_cone",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall2013_hong_kong_cone/fall2013_hong_kong_cone_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Federal Casemaker",
		localized_name = "info.tf2pm.hat.jul13_soldier_fedora",
		localized_description = "info.tf2pm.hat.jul13_soldier_fedora_desc",
		icon = "backpack/workshop/player/items/all_class/jul13_fedora/jul13_fedora",
		models = {
			["basename"] = "models/workshop/player/items/all_class/jul13_fedora/jul13_fedora_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = true,
		name = "Stapler's Specs",
		localized_name = "info.tf2pm.hat.sum19_staplers_specs",
		icon = "backpack/workshop/player/items/all_class/sum19_staplers_specs/sum19_staplers_specs",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sum19_staplers_specs/sum19_staplers_specs_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Phononaut",
		localized_name = "info.tf2pm.hat.invasion_phononaut",
		icon = "backpack/workshop/player/items/all_class/invasion_phononaut/invasion_phononaut",
		models = {
			["basename"] = "models/workshop/player/items/all_class/invasion_phononaut/invasion_phononaut_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Neckwear Headwear",
		localized_name = "info.tf2pm.hat.xms2013_winter_hat_scarf",
		icon = "backpack/workshop/player/items/all_class/xms2013_winter_hat_scarf/xms2013_winter_hat_scarf",
		models = {
			["basename"] = "models/workshop/player/items/all_class/xms2013_winter_hat_scarf/xms2013_winter_hat_scarf_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Tundra Top",
		localized_name = "info.tf2pm.hat.spr18_tundra_top",
		icon = "backpack/workshop/player/items/all_class/spr18_tundra_top/spr18_tundra_top",
		models = {
			["basename"] = "models/workshop/player/items/all_class/spr18_tundra_top/spr18_tundra_top_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {},
		paintable = false,
		name = "Brimstone",
		localized_name = "info.tf2pm.hat.brimstone_hat",
		icon = "backpack/workshop/player/items/all_class/brimstone_hat/brimstone_hat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/brimstone_hat/brimstone_hat_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.brimstone_style_withhat",
			},
			[1] = {
				localized_name = "info.tf2pm.hat.brimstone_style_withnohat",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"beard"},
		paintable = true,
		name = "The Dictator",
		localized_name = "info.tf2pm.hat.xms2013_soviet_stache",
		icon = "backpack/workshop/player/items/all_class/xms2013_soviet_stache/xms2013_soviet_stache",
		models = {
			["basename"] = "models/workshop/player/items/all_class/xms2013_soviet_stache/xms2013_soviet_stache_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"feet"},
		paintable = true,
		name = "Robin Walkers",
		localized_name = "info.tf2pm.hat.spr18_robin_walkers",
		icon = "backpack/workshop/player/items/all_class/spr18_robin_walkers/spr18_robin_walkers",
		models = {
			["basename"] = "models/workshop/player/items/all_class/spr18_robin_walkers/spr18_robin_walkers_%s.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
			["shoes"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Towering Pile Of Presents",
		localized_name = "info.tf2pm.hat.dec20_pile_of_presents",
		icon = "backpack/workshop/player/items/all_class/dec20_pile_of_presents/dec20_pile_of_presents",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_pile_of_presents/dec20_pile_of_presents_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"ears"},
		paintable = true,
		name = "Candy Cantlers",
		localized_name = "info.tf2pm.hat.dec20_candy_cantlers",
		icon = "backpack/workshop/player/items/all_class/dec20_candy_cantlers/dec20_candy_cantlers",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_candy_cantlers/dec20_candy_cantlers_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Caribou Companion",
		localized_name = "info.tf2pm.hat.dec17_caribou_companion",
		icon = "backpack/workshop/player/items/all_class/dec17_caribou_companion/dec17_caribou_companion",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec17_caribou_companion/dec17_caribou_companion_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = true,
		name = "The Tomb Readers",
		localized_name = "info.tf2pm.hat.tomb_readers",
		icon = "backpack/workshop_partner/player/items/all_class/tomb_readers/tomb_readers",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/tomb_readers/tomb_readers_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Woolen Warmer",
		localized_name = "info.tf2pm.hat.dec16_woolen_warmer",
		localized_description = "info.tf2pm.hat.dec16_woolen_warmer_desc",
		icon = "backpack/workshop/player/items/all_class/dec16_woolen_warmer/dec16_woolen_warmer",
		models = {
			["heavy"] = "models/workshop/player/items/all_class/dec16_woolen_warmer/dec16_woolen_warmer_heavy.mdl",
			["medic"] = "models/workshop/player/items/all_class/dec16_woolen_warmer/dec16_woolen_warmer_medic.mdl",
			["scout"] = "models/workshop/player/items/all_class/dec16_woolen_warmer/dec16_woolen_warmer_scout.mdl",
			["pyro"] = "models/workshop/player/items/all_class/dec16_woolen_warmer/dec16_woolen_warmer_pyro.mdl",
			["soldier"] = "models/workshop/player/items/all_class/dec16_woolen_warmer/dec16_woolen_warmer_soldier.mdl",
			["demoman"] = "models/workshop/player/items/all_class/dec16_woolen_warmer/dec16_woolen_warmer_demo.mdl",
			["spy"] = "models/workshop/player/items/all_class/dec16_woolen_warmer/dec16_woolen_warmer_spy.mdl",
			["engineer"] = "models/workshop/player/items/all_class/dec16_woolen_warmer/dec16_woolen_warmer_engineer.mdl",
			["sniper"] = "models/workshop/player/items/all_class/dec16_woolen_warmer/dec16_woolen_warmer_sniper.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Duck Billed Hatypus",
		localized_name = "info.tf2pm.hat.hwn2015_duckyhat",
		icon = "backpack/workshop/player/items/all_class/hwn2015_duckyhat/hwn2015_duckyhat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2015_duckyhat/hwn2015_duckyhat_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 CoPilot_2014",
		localized_name = "info.tf2pm.hat.dec2014_copilot_2014",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/all_class/dec2014_copilot_2014/dec2014_copilot_2014",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec2014_copilot_2014/dec2014_copilot_2014_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Captain Cardbeard Cutthroat",
		localized_name = "info.tf2pm.hat.cc_summer2015_captain_cardbeard_cutthroat",
		icon = "backpack/workshop/player/items/all_class/cc_summer2015_captain_cardbeard_cutthroat/cc_summer2015_captain_cardbeard_cutthroat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/cc_summer2015_captain_cardbeard_cutthroat/cc_summer2015_captain_cardbeard_cutthroat_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Head Prize",
		localized_name = "info.tf2pm.hat.dec16_head_prize",
		localized_description = "info.tf2pm.hat.dec16_head_prize_desc",
		icon = "backpack/workshop/player/items/all_class/dec16_head_prize/dec16_head_prize",
		models = {
			["heavy"] = "models/workshop/player/items/all_class/dec16_head_prize/dec16_head_prize_heavy.mdl",
			["medic"] = "models/workshop/player/items/all_class/dec16_head_prize/dec16_head_prize_medic.mdl",
			["scout"] = "models/workshop/player/items/all_class/dec16_head_prize/dec16_head_prize_scout.mdl",
			["pyro"] = "models/workshop/player/items/all_class/dec16_head_prize/dec16_head_prize_pyro.mdl",
			["soldier"] = "models/workshop/player/items/all_class/dec16_head_prize/dec16_head_prize_soldier.mdl",
			["demoman"] = "models/workshop/player/items/all_class/dec16_head_prize/dec16_head_prize_demo.mdl",
			["spy"] = "models/workshop/player/items/all_class/dec16_head_prize/dec16_head_prize_spy.mdl",
			["engineer"] = "models/workshop/player/items/all_class/dec16_head_prize/dec16_head_prize_engineer.mdl",
			["sniper"] = "models/workshop/player/items/all_class/dec16_head_prize/dec16_head_prize_sniper.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "Dead of Night",
		localized_name = "info.tf2pm.hat.xms2013_spy_jacket",
		icon = "backpack/workshop/player/items/all_class/xms2013_jacket/xms2013_jacket",
		models = {
			["basename"] = "models/workshop/player/items/all_class/xms2013_jacket/xms2013_jacket_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.xms2013_spy_jacket_style1",
				bodygroup_overrides = {
					["grenades"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/all_class/xms2013_jacket/xms2013_jacket_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.xms2013_spy_jacket_style2",
				bodygroup_overrides = {
					["grenades"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/all_class/xmas2013_jacket_s2/xmas2013_jacket_s2_%s.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.xms2013_spy_jacket_style3",
				models = {
					["basename"] = "models/workshop/player/items/all_class/xms2013_jacket/xms2013_jacket_%s.mdl",
				}
			},
			[3] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.xms2013_spy_jacket_style4",
				models = {
					["basename"] = "models/workshop/player/items/all_class/xmas2013_jacket_s2/xmas2013_jacket_s2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "The Sackcloth Spook",
		localized_name = "info.tf2pm.hat.hw2013_burlap_buddy",
		icon = "backpack/workshop/player/items/all_class/hw2013_burlap_buddy/hw2013_burlap_buddy",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_burlap_buddy/hw2013_burlap_buddy_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Haunted Hat",
		localized_name = "info.tf2pm.hat.hw2013_the_haunted_hat",
		icon = "backpack/workshop/player/items/all_class/hw2013_the_haunted_hat/hw2013_the_haunted_hat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_the_haunted_hat/hw2013_the_haunted_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Prehistoric Pullover",
		localized_name = "info.tf2pm.hat.hwn2015_dino_hoodie",
		icon = "backpack/workshop/player/items/all_class/hwn2015_dino_hoodie/hwn2015_dino_hoodie",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2015_dino_hoodie/hwn2015_dino_hoodie_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Cotton Head",
		localized_name = "info.tf2pm.hat.fall2013_the_cotton_head",
		icon = "backpack/workshop/player/items/all_class/fall2013_the_cotton_head/fall2013_the_cotton_head",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall2013_the_cotton_head/fall2013_the_cotton_head_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.fall2013_the_cotton_head_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/fall2013_the_cotton_head/fall2013_the_cotton_head_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.fall2013_the_cotton_head_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/fall2013_the_cotton_head_s2/fall2013_the_cotton_head_s2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Candy Crown",
		localized_name = "info.tf2pm.hat.dec19_candy_crown",
		icon = "backpack/workshop/player/items/all_class/dec19_candy_crown/dec19_candy_crown",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec19_candy_crown/dec19_candy_crown_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Ghost of Spies Checked Past",
		localized_name = "info.tf2pm.hat.sf14_ghost_of_spies_checked_past",
		icon = "backpack/workshop/player/items/all_class/sf14_ghost_of_spies_checked_past/sf14_ghost_of_spies_checked_past",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sf14_ghost_of_spies_checked_past/sf14_ghost_of_spies_checked_past_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Smissmass Sorcerer",
		localized_name = "info.tf2pm.hat.dec20_smissmas_sorcerer",
		icon = "backpack/workshop/player/items/all_class/dec20_smissmas_sorcerer/dec20_smissmas_sorcerer",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_smissmas_sorcerer/dec20_smissmas_sorcerer_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Jolly Jingler",
		localized_name = "info.tf2pm.hat.dec20_jolly_jingler",
		icon = "backpack/workshop/player/items/all_class/dec20_jolly_jingler/dec20_jolly_jingler",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_jolly_jingler/dec20_jolly_jingler_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Bedouin Bandana",
		localized_name = "info.tf2pm.hat.may16_bedouin_bandana",
		icon = "backpack/workshop/player/items/all_class/angsty_hood/angsty_hood",
		models = {
			["basename"] = "models/workshop/player/items/all_class/angsty_hood/angsty_hood_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Globetrotter",
		localized_name = "info.tf2pm.hat.dec19_globetrotter",
		icon = "backpack/workshop/player/items/all_class/dec19_globetrotter_s1/dec19_globetrotter_s1",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec19_globetrotter_s1/dec19_globetrotter_s1_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec19_globetrotter_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec19_globetrotter_s1/dec19_globetrotter_s1_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec19_globetrotter_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec19_globetrotter_s2/dec19_globetrotter_s2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = true,
		name = "Graybanns",
		localized_name = "info.tf2pm.hat.jul13_sweet_shades",
		localized_description = "info.tf2pm.hat.jul13_sweet_shades_desc",
		icon = "backpack/workshop/player/items/all_class/jul13_sweet_shades/jul13_sweet_shades",
		models = {
			["basename"] = "models/workshop/player/items/all_class/jul13_sweet_shades/jul13_sweet_shades_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.jul13_sweet_shades_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/jul13_sweet_shades/jul13_sweet_shades_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.jul13_sweet_shades_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/jul13_sweet_shades_s1/jul13_sweet_shades_s1_%s.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.jul13_sweet_shades_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/jul13_sweet_shades_s2/jul13_sweet_shades_s2_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {},
		paintable = false,
		name = "Glitched Circuit Board",
		localized_name = "info.tf2pm.hat.item_glitchedcircuitboard",
		localized_description = "info.tf2pm.hat.item_glitchedcircuitboard_desc",
		icon = "backpack/crafting/glitched_circuit_board",
		models = {
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Bread Heads",
		localized_name = "info.tf2pm.hat.dec18_bread_heads",
		icon = "backpack/workshop/player/items/all_class/dec18_bread_heads/dec18_bread_heads",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec18_bread_heads/dec18_bread_heads_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"necklace"},
		paintable = true,
		name = "Glittering Garland",
		localized_name = "info.tf2pm.hat.dec19_glittering_garland",
		icon = "backpack/workshop/player/items/all_class/dec19_glittering_garland/dec19_glittering_garland",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec19_glittering_garland/dec19_glittering_garland_%s.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"left_shoulder", "right_shoulder"},
		paintable = true,
		name = "The Polar Pal",
		localized_name = "info.tf2pm.hat.dec18_polar_pal",
		icon = "backpack/workshop/player/items/all_class/dec18_polar_pal/dec18_polar_pal",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec18_polar_pal/dec18_polar_pal_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.dec18_polar_pal_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec18_polar_pal/dec18_polar_pal_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.dec18_polar_pal_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec18_polar_pal_bamboo/dec18_polar_pal_bamboo_%s.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.dec18_polar_pal_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec18_polar_pal_liberty/dec18_polar_pal_liberty_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Grim Tweeter",
		localized_name = "info.tf2pm.hat.hwn2015_grim_tweeter",
		icon = "backpack/workshop/player/items/all_class/hwn2015_grim_tweeter/hwn2015_grim_tweeter",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2015_grim_tweeter/hwn2015_grim_tweeter_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"necklace"},
		paintable = true,
		name = "The Slithering Scarf",
		localized_name = "info.tf2pm.hat.fall17_slithering_scarf",
		localized_description = "info.tf2pm.hat.fall17_slithering_scarf_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_slithering_scarf/fall17_slithering_scarf",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_slithering_scarf/fall17_slithering_scarf_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"glasses", "hat"},
		paintable = true,
		name = "The Manneater",
		localized_name = "info.tf2pm.hat.hw2013_the_manneater",
		icon = "backpack/workshop/player/items/all_class/hw2013_the_manneater/hw2013_the_manneater",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_the_manneater/hw2013_the_manneater_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"belt_misc"},
		paintable = false,
		name = "Gingerbread Mann",
		localized_name = "info.tf2pm.hat.dec19_gingerbread_mann",
		icon = "backpack/workshop/player/items/all_class/dec19_gingerbread_mann_s1/dec19_gingerbread_mann_s1",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec19_gingerbread_mann_s1/dec19_gingerbread_mann_s1_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec19_gingerbread_mann_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec19_gingerbread_mann_s1/dec19_gingerbread_mann_s1_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec19_gingerbread_mann_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec19_gingerbread_mann_s2/dec19_gingerbread_mann_s2_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "The Croaking Hazard",
		localized_name = "info.tf2pm.hat.fall17_croaking_hazard",
		localized_description = "info.tf2pm.hat.fall17_croaking_hazard_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_croaking_hazard/fall17_croaking_hazard",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_croaking_hazard/fall17_croaking_hazard_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"belt_misc"},
		paintable = true,
		name = "Prinny Pouch",
		localized_name = "info.tf2pm.hat.prinny_pouch",
		icon = "backpack/workshop_partner/player/items/all_class/prinny_pouch/prinny_pouch",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/prinny_pouch/prinny_pouch_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Yule Hog",
		localized_name = "info.tf2pm.hat.dec19_yule_hog",
		icon = "backpack/workshop/player/items/all_class/dec19_yule_hog/dec19_yule_hog",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec19_yule_hog/dec19_yule_hog_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Chill Chullo",
		localized_name = "info.tf2pm.hat.dec15_chill_chullo",
		icon = "backpack/workshop/player/items/all_class/dec15_chill_chullo/dec15_chill_chullo",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec15_chill_chullo/dec15_chill_chullo_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Pebbles the Penguin",
		localized_name = "info.tf2pm.hat.dec19_pebbles_the_penguin",
		icon = "backpack/workshop/player/items/all_class/dec19_pebbles_the_penguin/dec19_pebbles_the_penguin",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec19_pebbles_the_penguin/dec19_pebbles_the_penguin_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "All Hallows' Hatte",
		localized_name = "info.tf2pm.hat.hwn2020_hallows_hatte",
		icon = "backpack/workshop/player/items/all_class/hwn2020_hallows_hatte/hwn2020_hallows_hatte",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_hallows_hatte/hwn2020_hallows_hatte_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Rotation Sensation",
		localized_name = "info.tf2pm.hat.cc_summer2015_the_rotation_sensation",
		icon = "backpack/workshop/player/items/all_class/cc_summer2015_the_rotation_sensation/cc_summer2015_the_rotation_sensation",
		models = {
			["basename"] = "models/workshop/player/items/all_class/cc_summer2015_the_rotation_sensation/cc_summer2015_the_rotation_sensation_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.cc_summer2015_the_rotation_sensation_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/cc_summer2015_the_rotation_sensation/cc_summer2015_the_rotation_sensation_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.cc_summer2015_the_rotation_sensation_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/cc_summer2015_the_rotation_sensation_style2/cc_summer2015_the_rotation_sensation_style2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Bumble Beenie",
		localized_name = "info.tf2pm.hat.dec19_bumble_beenie",
		icon = "backpack/workshop/player/items/all_class/dec19_bumble_beenie/dec19_bumble_beenie",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec19_bumble_beenie/dec19_bumble_beenie_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "A Well Wrapped Hat",
		localized_name = "info.tf2pm.hat.dec15_a_well_wrapped_hat",
		icon = "backpack/workshop/player/items/all_class/dec15_a_well_wrapped_hat/dec15_a_well_wrapped_hat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec15_a_well_wrapped_hat/dec15_a_well_wrapped_hat_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec15_a_well_wrapped_hat_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec15_a_well_wrapped_hat/dec15_a_well_wrapped_hat_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec15_a_well_wrapped_hat_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec15_a_well_wrapped_hat_style2/dec15_a_well_wrapped_hat_style2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Dark Falkirk Helm",
		localized_name = "info.tf2pm.hat.sbox2014_knight_helmet",
		icon = "backpack/workshop/player/items/all_class/sbox2014_knight_helmet/sbox2014_knight_helmet",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sbox2014_knight_helmet/sbox2014_knight_helmet_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sbox2014_knight_helmet_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/sbox2014_knight_helmet/sbox2014_knight_helmet_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sbox2014_knight_helmet_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/sbox2014_knight_helmet_style1/sbox2014_knight_helmet_style1_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Bot Dogger",
		localized_name = "info.tf2pm.hat.robo_scout_dogger",
		localized_description = "info.tf2pm.hat.robo_scout_dogger_desc",
		icon = "backpack/workshop/player/items/all_class/robo_dogger/robo_dogger",
		models = {
			["basename"] = "models/workshop/player/items/all_class/robo_dogger/robo_dogger_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "EOTL_FURCAP",
		localized_name = "info.tf2pm.hat.eotl_furcap",
		icon = "backpack/workshop/player/items/all_class/eotl_furcap/eotl_furcap",
		models = {
			["basename"] = "models/workshop/player/items/all_class/eotl_furcap/eotl_furcap_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Nasty Norsemann",
		localized_name = "info.tf2pm.hat.hwn2016_nasty_norsemann",
		icon = "backpack/workshop/player/items/all_class/hwn2016_nasty_norsemann/hwn2016_nasty_norsemann",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2016_nasty_norsemann/hwn2016_nasty_norsemann_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"feet"},
		paintable = true,
		name = "EOTL_hiphunter_boots",
		localized_name = "info.tf2pm.hat.eotl_hiphunter_boots",
		icon = "backpack/workshop/player/items/all_class/hiphunter_boots/hiphunter_boots",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hiphunter_boots/hiphunter_boots_%s.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
			["shoes"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {},
		paintable = true,
		name = "The Mustachioed Mann",
		localized_name = "info.tf2pm.hat.sbox2014_mustachioed_mann",
		icon = "backpack/workshop/player/items/all_class/sbox2014_mustachioed_mann/sbox2014_mustachioed_mann",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sbox2014_mustachioed_mann/sbox2014_mustachioed_mann_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.sbox2014_mustachioed_mann_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/sbox2014_mustachioed_mann/sbox2014_mustachioed_mann_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.sbox2014_mustachioed_mann_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/sbox2014_mustachioed_mann_s2/sbox2014_mustachioed_mann_s2_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"head_skin"},
		paintable = false,
		name = "Promo Arkham Cowl",
		localized_name = "info.tf2pm.hat.bak_arkham_cowl",
		icon = "backpack/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl",
		models = {
			["heavy"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_heavy.mdl",
			["medic"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_medic.mdl",
			["scout"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_scout.mdl",
			["pyro"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_pyro.mdl",
			["soldier"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_soldier.mdl",
			["demoman"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_demo.mdl",
			["spy"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_spy.mdl",
			["engineer"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_engineer.mdl",
			["sniper"] = "models/workshop/player/items/all_class/bak_arkham_cowl/bak_arkham_cowl_sniper.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Lil' Bitey",
		localized_name = "info.tf2pm.hat.hwn2016_lil_bitey",
		icon = "backpack/workshop/player/items/all_class/hwn2016_lil_bitey/hwn2016_lil_bitey",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2016_lil_bitey/hwn2016_lil_bitey_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"engineer_pocket", "sniper_pocket"},
		paintable = true,
		name = "Pocket Saxton",
		localized_name = "info.tf2pm.hat.fall17_pocket_saxton",
		localized_description = "info.tf2pm.hat.fall17_pocket_saxton_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_pocket_saxton/fall17_pocket_saxton",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_pocket_saxton/fall17_pocket_saxton_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "EOTL_hiphunter_hat",
		localized_name = "info.tf2pm.hat.eotl_hiphunter_hat",
		icon = "backpack/workshop/player/items/all_class/hiphunter_hat/hiphunter_hat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hiphunter_hat/hiphunter_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"grenades"},
		paintable = false,
		name = "Pocket Villains",
		localized_name = "info.tf2pm.hat.bak_pocket_villains",
		icon = "backpack/workshop/player/items/all_class/bak_pocket_villians/bak_pocket_villians",
		models = {
			["basename"] = "models/workshop/player/items/all_class/bak_pocket_villians/bak_pocket_villians_%s.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Dadliest Catch",
		localized_name = "info.tf2pm.hat.dadliestcatch",
		localized_description = "info.tf2pm.hat.dadliestcatch_desc",
		icon = "backpack/workshop_partner/player/items/all_class/nobody_suspects_a_thing/nobody_suspects_a_thing",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/nobody_suspects_a_thing/nobody_suspects_a_thing_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Quizzical Quetzal",
		localized_name = "info.tf2pm.hat.fall17_quizzical_quetzal",
		localized_description = "info.tf2pm.hat.fall17_quizzical_quetzal_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_quizzical_quetzal/fall17_quizzical_quetzal",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_quizzical_quetzal/fall17_quizzical_quetzal_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The MK 50",
		localized_name = "info.tf2pm.hat.mk50",
		icon = "backpack/workshop_partner/player/items/all_class/ai_spacehelmet/ai_spacehelmet",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/ai_spacehelmet/ai_spacehelmet_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Tuque or Treat",
		localized_name = "info.tf2pm.hat.hw2013_pumpkin_top",
		icon = "backpack/workshop/player/items/all_class/hw2013_pumpkin_top/hw2013_pumpkin_top",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_pumpkin_top/hw2013_pumpkin_top_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Hunter in Darkness",
		localized_name = "info.tf2pm.hat.fall17_hunter_in_darkness",
		localized_description = "info.tf2pm.hat.fall17_hunter_in_darkness_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_jungle_ops/fall17_jungle_ops",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_jungle_ops/fall17_jungle_ops_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Legendary Lid",
		localized_name = "info.tf2pm.hat.spr17_legendary_lid",
		localized_description = "info.tf2pm.hat.spr17_legendary_lid_desc",
		icon = "backpack/workshop/player/items/all_class/spr17_legendary_lid/spr17_legendary_lid",
		models = {
			["basename"] = "models/workshop/player/items/all_class/spr17_legendary_lid/spr17_legendary_lid_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"zombie_body"},
		paintable = false,
		name = "Voodoo-Cursed Soul (Armory)",
		localized_name = "info.tf2pm.hat.item_zombie_armory",
		icon = "backpack/player/items/engineer/engineer_zombie",
		models = {
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Birdie Bonnet",
		localized_name = "info.tf2pm.hat.hw2013_tricky_chicken",
		icon = "backpack/workshop/player/items/all_class/hw2013_tricky_chicken/hw2013_tricky_chicken",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_tricky_chicken/hw2013_tricky_chicken_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Wrap-A-Khamon",
		localized_name = "info.tf2pm.hat.hwn2020_wrap_a_khamon",
		icon = "backpack/workshop/player/items/all_class/hwn2020_wrap_a_khamon/hwn2020_wrap_a_khamon",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_wrap_a_khamon/hwn2020_wrap_a_khamon_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2020_wrap_a_khamon_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/hwn2020_wrap_a_khamon/hwn2020_wrap_a_khamon_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2020_wrap_a_khamon_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/hwn2020_wrap_a_khamon_style2/hwn2020_wrap_a_khamon_style2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Merc's Mohawk",
		localized_name = "info.tf2pm.hat.short2014_lil_moe",
		icon = "backpack/workshop/player/items/all_class/short2014_lil_moe/short2014_lil_moe",
		models = {
			["basename"] = "models/workshop/player/items/all_class/short2014_lil_moe/short2014_lil_moe_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {},
		paintable = false,
		name = "Something Special For Someone Special (Wearable)",
		localized_name = "info.tf2pm.hat.weddingring",
		icon = "backpack/player/items/all_class/all_class_ring",
		models = {
			["basename"] = "models/player/items/all_class/ring_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = true,
		name = "The Virtual Viewfinder",
		localized_name = "info.tf2pm.hat.jul13_se_headset",
		localized_description = "info.tf2pm.hat.jul13_se_headset_desc",
		icon = "backpack/workshop/player/items/all_class/jul13_se_headset/jul13_se_headset",
		models = {
			["basename"] = "models/workshop/player/items/all_class/jul13_se_headset/jul13_se_headset_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Patriot Peak",
		localized_name = "info.tf2pm.hat.dec15_patriot_peak",
		icon = "backpack/workshop/player/items/all_class/dec15_patriot_peak/dec15_patriot_peak",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec15_patriot_peak/dec15_patriot_peak_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec15_patriot_peak_style0",
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec15_patriot_peak_style1",
				bodygroup_overrides = {
					["headphones"] = 1,
				},
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Class Crown",
		localized_name = "info.tf2pm.hat.hwn2016_class_crown",
		icon = "backpack/workshop/player/items/all_class/hwn2016_class_crown/hwn2016_class_crown",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2016_class_crown/hwn2016_class_crown_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Breadcrab",
		localized_name = "info.tf2pm.hat.sum20_breadcrab",
		icon = "backpack/workshop/player/items/all_class/sum20_breadcrab/sum20_breadcrab",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sum20_breadcrab/sum20_breadcrab_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "A Handsome Handy Thing",
		localized_name = "info.tf2pm.hat.hwn2020_handy_thing",
		icon = "backpack/workshop/player/items/all_class/hwn2020_handy_thing/hwn2020_handy_thing",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_handy_thing/hwn2020_handy_thing_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2020_handy_thing_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/hwn2020_handy_thing/hwn2020_handy_thing_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2020_handy_thing_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/hwn2020_handy_thing_style2/hwn2020_handy_thing_style2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "EOTL_Brisk-weather Beanie",
		localized_name = "info.tf2pm.hat.eotl_briskweather_beanie",
		icon = "backpack/workshop/player/items/all_class/briskweather_beanie/briskweather_beanie",
		models = {
			["basename"] = "models/workshop/player/items/all_class/briskweather_beanie/briskweather_beanie_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Crown of the Old Kingdom",
		localized_name = "info.tf2pm.hat.tr_crown_of_the_old_kingdom",
		icon = "backpack/workshop_partner/player/items/all_class/tr_crown_of_the_old_kingdom/tr_crown_of_the_old_kingdom",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/tr_crown_of_the_old_kingdom/tr_crown_of_the_old_kingdom_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Terror-antula",
		localized_name = "info.tf2pm.hat.hwn2018_terror_antula",
		icon = "backpack/workshop/player/items/all_class/hwn2018_terror_antula/hwn2018_terror_antula",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2018_terror_antula/hwn2018_terror_antula_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Misfortune Fedora",
		localized_name = "info.tf2pm.hat.hwn2020_misfortune_fedora",
		icon = "backpack/workshop/player/items/all_class/hwn2020_misfortune_fedora/hwn2020_misfortune_fedora",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_misfortune_fedora/hwn2020_misfortune_fedora_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"grenades", "engineer_pocket", "sniper_pocket"},
		paintable = false,
		name = "The Pocket Raiders",
		localized_name = "info.tf2pm.hat.pocket_raiders",
		icon = "backpack/workshop_partner/player/items/all_class/pocket_raiders/pocket_raiders",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/pocket_raiders/pocket_raiders_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.pocket_raiders_style0",
				models = {
					["basename"] = "models/workshop_partner/player/items/all_class/pocket_raiders/pocket_raiders_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.pocket_raiders_style1",
				models = {
					["basename"] = "models/workshop_partner/player/items/all_class/pocket_raiders_carter_style/pocket_raiders_carter_style_%s.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.pocket_raiders_style2",
				models = {
					["basename"] = "models/workshop_partner/player/items/all_class/pocket_raiders_horus_style/pocket_raiders_horus_style_%s.mdl",
				}
			},
			[3] = {
				localized_name = "info.tf2pm.hat.pocket_raiders_style3",
				models = {
					["basename"] = "models/workshop_partner/player/items/all_class/pocket_raiders_isis_style/pocket_raiders_isis_style_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"engineer_pocket", "sniper_pocket"},
		paintable = true,
		name = "Pocket Yeti",
		localized_name = "info.tf2pm.hat.dec17_pocket_yeti",
		icon = "backpack/workshop/player/items/all_class/dec17_pocket_yeti/dec17_pocket_yeti",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec17_pocket_yeti/dec17_pocket_yeti_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"face"},
		paintable = true,
		name = "Handsome Devil",
		localized_name = "info.tf2pm.hat.hwn2020_handsome_devil",
		icon = "backpack/workshop/player/items/all_class/hwn2020_handsome_devil/hwn2020_handsome_devil",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_handsome_devil/hwn2020_handsome_devil_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2020_handsome_devil_style_withnohat",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2020_handsome_devil_style_withhat",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Toadstool Topper",
		localized_name = "info.tf2pm.hat.hwn2016_toadstool_topper",
		icon = "backpack/workshop/player/items/all_class/hwn2016_toadstool_topper/hwn2016_toadstool_topper",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2016_toadstool_topper/hwn2016_toadstool_topper_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Balloonihoodie",
		localized_name = "info.tf2pm.hat.dec17_balloonihoodie",
		icon = "backpack/workshop/player/items/all_class/dec17_balloonihoodie/dec17_balloonihoodie",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec17_balloonihoodie/dec17_balloonihoodie_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {},
		paintable = true,
		name = "Binoculus",
		localized_name = "info.tf2pm.hat.hwn2019_binoculus",
		icon = "backpack/workshop/player/items/all_class/hwn2019_binoculus/hwn2019_binoculus",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2019_binoculus/hwn2019_binoculus_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2019_binoculus_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/hwn2019_binoculus/hwn2019_binoculus_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2019_binoculus_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/hwn2019_binoculus_style2/hwn2019_binoculus_style2_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {},
		paintable = false,
		name = "Eye-see-you",
		localized_name = "info.tf2pm.hat.hwn2020_eye_see_you",
		icon = "backpack/workshop/player/items/all_class/hwn2020_eye_see_you/hwn2020_eye_see_you",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_eye_see_you/hwn2020_eye_see_you_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2020_eye_see_you_style_withhat",
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2020_eye_see_you_style_withnohat",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"pants"},
		paintable = true,
		name = "The Breakneck Baggies",
		localized_name = "info.tf2pm.hat.jogon",
		localized_description = "info.tf2pm.hat.jogon_desc",
		icon = "backpack/workshop/player/items/all_class/jogon/jogon",
		models = {
			["basename"] = "models/workshop/player/items/all_class/jogon/jogon_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Mister Bones",
		localized_name = "info.tf2pm.hat.hwn2019_mister_bones",
		icon = "backpack/workshop/player/items/all_class/hwn2019_mister_bones/hwn2019_mister_bones",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2019_mister_bones/hwn2019_mister_bones_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Modest Metal Pile of Scrap",
		localized_name = "info.tf2pm.hat.robo_all_modest_pile",
		localized_description = "info.tf2pm.hat.robo_all_modest_pile_desc",
		icon = "backpack/workshop/player/items/all_class/robo_all_modest_pile/robo_all_modest_pile",
		models = {
			["basename"] = "models/workshop/player/items/all_class/robo_all_modest_pile/robo_all_modest_pile_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Well-Rounded Rifleman",
		localized_name = "info.tf2pm.hat.riflemans_rallycap",
		localized_description = "info.tf2pm.hat.riflemans_rallycap_desc",
		icon = "backpack/workshop/player/items/all_class/riflemans_rallycap/riflemans_rallycap",
		models = {
			["basename"] = "models/workshop/player/items/all_class/riflemans_rallycap/riflemans_rallycap_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Bat Hat",
		localized_name = "info.tf2pm.hat.hwn2019_bat_hat",
		icon = "backpack/workshop/player/items/all_class/hwn2019_bat_hat/hwn2019_bat_hat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2019_bat_hat/hwn2019_bat_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Space Hamster Hammy",
		localized_name = "info.tf2pm.hat.invasion_space_hamster_hammy",
		icon = "backpack/workshop/player/items/pyro/invasion_space_hamster_hammy/invasion_space_hamster_hammy",
		models = {
			["basename"] = "models/workshop/player/items/pyro/invasion_space_hamster_hammy/invasion_space_hamster_hammy.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Brotherhood of Arms",
		localized_name = "info.tf2pm.hat.brotherhood_2",
		localized_description = "info.tf2pm.hat.brotherhood_2_desc",
		icon = "backpack/workshop/player/items/all_class/brotherhood_2/brotherhood_2",
		models = {
			["basename"] = "models/workshop/player/items/all_class/brotherhood_2/brotherhood_2_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Galvanized Gibus",
		localized_name = "info.tf2pm.hat.robo_all_gibus",
		localized_description = "info.tf2pm.hat.robo_all_gibus_desc",
		icon = "backpack/workshop/player/items/all_class/robo_all_gibus/robo_all_gibus",
		models = {
			["basename"] = "models/workshop/player/items/all_class/robo_all_gibus/robo_all_gibus_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Cursed Cruise",
		localized_name = "info.tf2pm.hat.sf14_cursed_cruise",
		icon = "backpack/workshop/player/items/all_class/sf14_cursed_cruise/sf14_cursed_cruise",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sf14_cursed_cruise/sf14_cursed_cruise_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Hardy Laurel",
		localized_name = "info.tf2pm.hat.tw2_roman_wreath",
		localized_description = "info.tf2pm.hat.tw2_roman_wreath_desc",
		icon = "backpack/workshop_partner/player/items/all_class/tw2_roman_wreath/tw2_roman_wreath",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/tw2_roman_wreath/tw2_roman_wreath_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Polar Pullover",
		localized_name = "info.tf2pm.hat.xms2013_polar_pullover",
		icon = "backpack/workshop/player/items/all_class/xms2013_polar_pullover/xms2013_polar_pullover",
		models = {
			["basename"] = "models/workshop/player/items/all_class/xms2013_polar_pullover/xms2013_polar_pullover_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"glasses", "hat"},
		paintable = true,
		name = "Vive La France",
		localized_name = "info.tf2pm.hat.short2014_vintage_director",
		icon = "backpack/workshop/player/items/all_class/short2014_vintage_director/short2014_vintage_director",
		models = {
			["basename"] = "models/workshop/player/items/all_class/short2014_vintage_director/short2014_vintage_director_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Lucky Cat Hat",
		localized_name = "info.tf2pm.hat.hwn2018_lucky_cat_hat",
		icon = "backpack/workshop/player/items/all_class/hwn2018_lucky_cat_hat/hwn2018_lucky_cat_hat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2018_lucky_cat_hat/hwn2018_lucky_cat_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Horace",
		localized_name = "info.tf2pm.hat.horace",
		icon = "backpack/workshop/player/items/all_class/horace/horace",
		models = {
			["basename"] = "models/workshop/player/items/all_class/horace/horace_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Jungle Wreath",
		localized_name = "info.tf2pm.hat.fall17_jungle_wreath",
		localized_description = "info.tf2pm.hat.fall17_jungle_wreath_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_jungle_wreath/fall17_jungle_wreath",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_jungle_wreath/fall17_jungle_wreath_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = false,
		name = "The Accursed Apparition",
		localized_name = "info.tf2pm.hat.hw2013_wandering_soul",
		icon = "backpack/workshop/player/items/all_class/hw2013_wandering_soul/hw2013_wandering_soul",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_wandering_soul/hw2013_wandering_soul_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Aztec Warrior",
		localized_name = "info.tf2pm.hat.fall17_aztec_warrior",
		localized_description = "info.tf2pm.hat.fall17_aztec_warrior_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_aztec_warrior/fall17_aztec_warrior",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_aztec_warrior/fall17_aztec_warrior_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Unidentified Following Object",
		localized_name = "info.tf2pm.hat.hw2013_intergalactic_intruder",
		icon = "backpack/workshop/player/items/all_class/hw2013_intergalactic_intruder/hw2013_intergalactic_intruder",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_intergalactic_intruder/hw2013_intergalactic_intruder_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Noble Nickel Amassment of Hats",
		localized_name = "info.tf2pm.hat.robo_all_noble_amassment",
		localized_description = "info.tf2pm.hat.robo_all_noble_amassment_desc",
		icon = "backpack/workshop/player/items/all_class/robo_all_noble_amassment/robo_all_noble_amassment",
		models = {
			["basename"] = "models/workshop/player/items/all_class/robo_all_noble_amassment/robo_all_noble_amassment_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"ears"},
		paintable = true,
		name = "Deadbeats",
		localized_name = "info.tf2pm.hat.hwn2018_deadbeats",
		icon = "backpack/workshop/player/items/all_class/hwn2018_deadbeats/hwn2018_deadbeats",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2018_deadbeats/hwn2018_deadbeats_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2018_deadbeats_style0",
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2018_deadbeats_style1",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {},
		paintable = true,
		name = "The Horrible Horns",
		localized_name = "info.tf2pm.hat.hwn2019_horrible_horns",
		icon = "backpack/workshop/player/items/all_class/hwn2019_horrible_horns/hwn2019_horrible_horns",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2019_horrible_horns/hwn2019_horrible_horns_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2019_horrible_horns_withnohat",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2019_horrible_horns_withhat",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Prinny Hat",
		localized_name = "info.tf2pm.hat.prinny_hat",
		icon = "backpack/workshop_partner/player/items/all_class/prinny_hat/prinny_hat",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/prinny_hat/prinny_hat_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Sir Pumpkinton",
		localized_name = "info.tf2pm.hat.hwn2020_sir_pumpkinton",
		icon = "backpack/workshop/player/items/all_class/hwn2020_sir_pumpkinton/hwn2020_sir_pumpkinton",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_sir_pumpkinton/hwn2020_sir_pumpkinton_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Reindoonibeanie",
		localized_name = "info.tf2pm.hat.dec20_reindoonibeanie",
		icon = "backpack/workshop/player/items/all_class/dec20_reindoonibeanie/dec20_reindoonibeanie",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_reindoonibeanie/dec20_reindoonibeanie_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"beard"},
		paintable = false,
		name = "Bread Biter",
		localized_name = "info.tf2pm.hat.hwn2019_bread_biter",
		icon = "backpack/workshop/player/items/all_class/hwn2019_bread_biter/hwn2019_bread_biter",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2019_bread_biter/hwn2019_bread_biter_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Hollowed Helm",
		localized_name = "info.tf2pm.hat.hwn2020_hollowed_helm",
		icon = "backpack/workshop/player/items/all_class/hwn2020_hollowed_helm/hwn2020_hollowed_helm",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_hollowed_helm/hwn2020_hollowed_helm_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Quoth",
		localized_name = "info.tf2pm.hat.hw2013_the_caws_of_death",
		icon = "backpack/workshop/player/items/all_class/hw2013_the_caws_of_death/hw2013_the_caws_of_death",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_the_caws_of_death/hw2013_the_caws_of_death_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Sledder's Sidekick",
		localized_name = "info.tf2pm.hat.dec17_sledders_sidekick",
		icon = "backpack/workshop/player/items/all_class/dec17_sledders_sidekick/dec17_sledders_sidekick",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec17_sledders_sidekick/dec17_sledders_sidekick_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"belt_misc"},
		paintable = true,
		name = "Pocket Halloween Boss",
		localized_name = "info.tf2pm.hat.hwn2019_pocket_hwn_boss",
		localized_description = "info.tf2pm.hat.hwn2019_pocket_hwn_boss_desc",
		icon = "backpack/workshop/player/items/all_class/hwn2019_pocket_merasmus/hwn2019_pocket_merasmus",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2019_pocket_merasmus/hwn2019_pocket_merasmus_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2019_pocket_hwn_boss_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/hwn2019_pocket_merasmus/hwn2019_pocket_merasmus_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2019_pocket_hwn_boss_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/hwn2019_pocket_ghost/hwn2019_pocket_ghost_%s.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.hwn2019_pocket_hwn_boss_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/hwn2019_pocket_monoculus/hwn2019_pocket_monoculus_%s.mdl",
				}
			},
			[3] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2019_pocket_hwn_boss_style3",
				models = {
					["basename"] = "models/workshop/player/items/all_class/hwn2019_pocket_skeleton_king/hwn2019_pocket_skeleton_king_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"beard"},
		paintable = true,
		name = "Gourd Grin",
		localized_name = "info.tf2pm.hat.hwn2020_gourd_grin",
		icon = "backpack/workshop/player/items/all_class/hwn2020_gourd_grin/hwn2020_gourd_grin",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_gourd_grin/hwn2020_gourd_grin_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Towering Pillar of Beanies",
		localized_name = "info.tf2pm.hat.dec20_pillar_of_beanies",
		icon = "backpack/workshop/player/items/all_class/dec20_pillar_of_beanies/dec20_pillar_of_beanies",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_pillar_of_beanies/dec20_pillar_of_beanies_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"sleeves"},
		paintable = true,
		name = "Batter's Bracers",
		localized_name = "info.tf2pm.hat.bak_batarm",
		icon = "backpack/workshop/player/items/all_class/bak_batarm/bak_batarm",
		models = {
			["basename"] = "models/workshop/player/items/all_class/bak_batarm/bak_batarm_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "Balloonicorpse",
		localized_name = "info.tf2pm.hat.hwn2020_balloonicorpse",
		icon = "backpack/workshop/player/items/all_class/hwn2020_balloonicorpse/hwn2020_balloonicorpse",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_balloonicorpse/hwn2020_balloonicorpse_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"belt_misc"},
		paintable = true,
		name = "Pocket Admin",
		localized_name = "info.tf2pm.hat.dec18_pocket_admin",
		icon = "backpack/workshop/player/items/all_class/dec18_pocket_admin/dec18_pocket_admin",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec18_pocket_admin/dec18_pocket_admin_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = false,
		name = "The Beacon from Beyond",
		localized_name = "info.tf2pm.hat.hw2013_the_enlightening_lantern",
		icon = "backpack/workshop/player/items/all_class/hw2013_the_enlightening_lantern/hw2013_the_enlightening_lantern",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_the_enlightening_lantern/hw2013_the_enlightening_lantern_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["grenades"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Law",
		localized_name = "info.tf2pm.hat.sbox2014_law",
		icon = "backpack/workshop/player/items/all_class/sbox2014_law/sbox2014_law",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sbox2014_law/sbox2014_law_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = true,
		name = "Spooktacles",
		localized_name = "info.tf2pm.hat.hwn2016_spooktacles",
		icon = "backpack/workshop/player/items/all_class/hwn2016_spooktacles/hwn2016_spooktacles",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2016_spooktacles/hwn2016_spooktacles_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Snowmann",
		localized_name = "info.tf2pm.hat.dec16_snowmann",
		localized_description = "info.tf2pm.hat.dec16_snowmann_desc",
		icon = "backpack/workshop/player/items/all_class/dec16_snowmann/dec16_snowmann",
		models = {
			["heavy"] = "models/workshop/player/items/all_class/dec16_snowmann/dec16_snowmann_heavy.mdl",
			["medic"] = "models/workshop/player/items/all_class/dec16_snowmann/dec16_snowmann_medic.mdl",
			["scout"] = "models/workshop/player/items/all_class/dec16_snowmann/dec16_snowmann_scout.mdl",
			["pyro"] = "models/workshop/player/items/all_class/dec16_snowmann/dec16_snowmann_pyro.mdl",
			["soldier"] = "models/workshop/player/items/all_class/dec16_snowmann/dec16_snowmann_soldier.mdl",
			["demoman"] = "models/workshop/player/items/all_class/dec16_snowmann/dec16_snowmann_demo.mdl",
			["spy"] = "models/workshop/player/items/all_class/dec16_snowmann/dec16_snowmann_spy.mdl",
			["engineer"] = "models/workshop/player/items/all_class/dec16_snowmann/dec16_snowmann_engineer.mdl",
			["sniper"] = "models/workshop/player/items/all_class/dec16_snowmann/dec16_snowmann_sniper.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Tipped Lid",
		localized_name = "info.tf2pm.hat.short2014_tip_of_the_hats",
		localized_description = "info.tf2pm.hat.short2014_tip_of_the_hats_desc",
		icon = "backpack/workshop/player/items/all_class/short2014_tip_of_the_hats/short2014_tip_of_the_hats",
		models = {
			["basename"] = "models/workshop/player/items/all_class/short2014_tip_of_the_hats/short2014_tip_of_the_hats_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"ears"},
		paintable = true,
		name = "Impish Ears",
		localized_name = "info.tf2pm.hat.hwn2020_impish_ears",
		icon = "backpack/workshop/player/items/all_class/hwn2020_impish_ears/hwn2020_impish_ears",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_impish_ears/hwn2020_impish_ears_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2020_impish_ears_withhat",
				bodygroup_overrides = {
					["headphones"] = 1,
				},
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2020_impish_ears_withnohat",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"necklace"},
		paintable = true,
		name = "The Cryptic Keepsake",
		localized_name = "info.tf2pm.hat.hw2013_all_skull_necklace",
		icon = "backpack/workshop/player/items/all_class/hw2013_all_skull_necklace/hw2013_all_skull_necklace",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_all_skull_necklace/hw2013_all_skull_necklace_%s.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Universal Translator",
		localized_name = "info.tf2pm.hat.invasion_universal_translator",
		icon = "backpack/workshop/player/items/all_class/invasion_universal_translator/invasion_universal_translator",
		models = {
			["basename"] = "models/workshop/player/items/all_class/invasion_universal_translator/invasion_universal_translator_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Smissmas Saxton",
		localized_name = "info.tf2pm.hat.dec19_smissmas_saxton",
		icon = "backpack/workshop/player/items/all_class/dec19_smissmas_saxton/dec19_smissmas_saxton",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec19_smissmas_saxton/dec19_smissmas_saxton_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"beard"},
		paintable = true,
		name = "The Bruiser's Bandanna",
		localized_name = "info.tf2pm.hat.short2014_all_mercs_mask",
		icon = "backpack/workshop/player/items/all_class/short2014_all_mercs_mask/short2014_all_mercs_mask",
		models = {
			["basename"] = "models/workshop/player/items/all_class/short2014_all_mercs_mask/short2014_all_mercs_mask_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.short2014_all_mercs_mask_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/short2014_all_mercs_mask/short2014_all_mercs_mask_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.short2014_all_mercs_mask_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/short2014_all_mercs_mask_s1/short2014_all_mercs_mask_s1_%s.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.short2014_all_mercs_mask_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/short2014_all_mercs_mask_s2/short2014_all_mercs_mask_s2_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Magical Mercenary",
		localized_name = "info.tf2pm.hat.hw2013_the_magical_mercenary",
		icon = "backpack/workshop/player/items/all_class/hw2013_the_magical_mercenary/hw2013_the_magical_mercenary",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_the_magical_mercenary/hw2013_the_magical_mercenary_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Towering Titanium Pillar of Hats",
		localized_name = "info.tf2pm.hat.robo_all_towering_pillar",
		localized_description = "info.tf2pm.hat.robo_all_towering_pillar_desc",
		icon = "backpack/workshop/player/items/all_class/robo_all_towering_pillar/robo_all_towering_pillar",
		models = {
			["basename"] = "models/workshop/player/items/all_class/robo_all_towering_pillar/robo_all_towering_pillar_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Crosslinker's Coil",
		localized_name = "info.tf2pm.hat.gunpointcoilhat",
		localized_description = "info.tf2pm.hat.gunpointcoilhat_desc",
		icon = "backpack/workshop/player/items/all_class/gunpointcoilhat/gunpointcoilhat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/gunpointcoilhat/gunpointcoilhat_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.nohat_style",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.nohat_noheadphones_style",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Citizen Cane",
		localized_name = "info.tf2pm.hat.dec19_citizen_cane",
		icon = "backpack/workshop/player/items/all_class/dec19_citizen_cane_s1/dec19_citizen_cane_s1",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec19_citizen_cane_s1/dec19_citizen_cane_s1_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec19_citizen_cane_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec19_citizen_cane_s1/dec19_citizen_cane_s1_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec19_citizen_cane_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/dec19_citizen_cane_s2/dec19_citizen_cane_s2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"belt_misc"},
		paintable = true,
		name = "The Pocket Horsemann",
		localized_name = "info.tf2pm.hat.hw2013_allclass_horseman",
		icon = "backpack/workshop/player/items/all_class/hw2013_allclass_horseman/hw2013_allclass_horseman",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_allclass_horseman/hw2013_allclass_horseman_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"feet"},
		paintable = true,
		name = "Festive Flip-thwomps",
		localized_name = "info.tf2pm.hat.dec20_flip_thwomps",
		icon = "backpack/workshop/player/items/all_class/dec20_flip_thwomps/dec20_flip_thwomps",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_flip_thwomps/dec20_flip_thwomps_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"disconnected_floating_item"},
		paintable = true,
		name = "The Hooded Haunter",
		localized_name = "info.tf2pm.hat.sf14_hooded_haunter_classes",
		icon = "backpack/workshop/player/items/all_class/sf14_hooded_haunter_classes/sf14_hooded_haunter_classes",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sf14_hooded_haunter_classes/sf14_hooded_haunter_classes_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {},
		paintable = true,
		name = "Hypno-eyes",
		localized_name = "info.tf2pm.hat.sum20_spectre_cles",
		icon = "backpack/workshop/player/items/all_class/sum20_spectre_cles_style2/sum20_spectre_cles_style2",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sum20_spectre_cles_style2/sum20_spectre_cles_style2_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				localized_name = "info.tf2pm.hat.sum20_spectre_cles_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/sum20_spectre_cles_style2/sum20_spectre_cles_style2_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum20_spectre_cles_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/sum20_spectre_cles_style1/sum20_spectre_cles_style1_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Dark Helm",
		localized_name = "info.tf2pm.hat.hw2013_the_dark_helm",
		icon = "backpack/workshop/player/items/all_class/hw2013_the_dark_helm/hw2013_the_dark_helm",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_the_dark_helm/hw2013_the_dark_helm_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "King Cardbeard",
		localized_name = "info.tf2pm.hat.hwn2020_king_cardbeard",
		icon = "backpack/workshop/player/items/all_class/hwn2020_king_cardbeard/hwn2020_king_cardbeard",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_king_cardbeard/hwn2020_king_cardbeard_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The One-Way Ticket",
		localized_name = "info.tf2pm.hat.hw2013_stiff_buddy",
		icon = "backpack/workshop/player/items/all_class/hw2013_stiff_buddy/hw2013_stiff_buddy",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_stiff_buddy/hw2013_stiff_buddy_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"lenses"},
		paintable = true,
		name = "The Eye-Catcher",
		localized_name = "info.tf2pm.hat.short2014_all_eyepatch",
		icon = "backpack/workshop/player/items/all_class/short2014_all_eyepatch/short2014_all_eyepatch",
		models = {
			["basename"] = "models/workshop/player/items/all_class/short2014_all_eyepatch/short2014_all_eyepatch_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"face"},
		paintable = false,
		name = "Captain Space Mann",
		localized_name = "info.tf2pm.hat.invasion_captain_space_mann",
		icon = "backpack/workshop/player/items/all_class/invasion_captain_space_mann/invasion_captain_space_mann",
		models = {
			["basename"] = "models/workshop/player/items/all_class/invasion_captain_space_mann/invasion_captain_space_mann_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["backpack"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"belt_misc"},
		paintable = true,
		name = "Batbelt",
		localized_name = "info.tf2pm.hat.bak_batbelt",
		icon = "backpack/workshop/player/items/all_class/bak_batbelt/bak_batbelt",
		models = {
			["basename"] = "models/workshop/player/items/all_class/bak_batbelt/bak_batbelt_%s.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "soldier", "demo", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Pithy Professional",
		localized_name = "info.tf2pm.hat.fall17_pithy_pro",
		localized_description = "info.tf2pm.hat.fall17_pithy_pro_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_scholar/fall17_scholar",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_scholar/fall17_scholar_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},

}
